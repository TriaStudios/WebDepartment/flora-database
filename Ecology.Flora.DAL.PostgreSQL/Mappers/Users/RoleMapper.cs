﻿using AutoMapper;
using Ecology.Flora.DAL.Logic.ViewModels.Users;
using Ecology.Flora.DAL.PostgreSQL.Models.Users;

namespace Ecology.Flora.DAL.PostgreSQL.Mappers.Users;

public class RoleMapper : Profile
{
    public RoleMapper()
    {
        CreateMap<Role, ViewModelBase>()
            .IncludeBase<EntityBase, ViewModelBase>();
        CreateMap<Role, RoleViewModel>()
            .IncludeBase<EntityBase, ViewModelBase>();
    }
}