﻿using AutoMapper;
using Ecology.Flora.DAL.Logic.ViewModels.Users;
using Ecology.Flora.DAL.PostgreSQL.Models.Users;

namespace Ecology.Flora.DAL.PostgreSQL.Mappers.Users;

public class UserMapper : Profile
{
    public UserMapper()
    {
        CreateMap<User, ViewModelBase>()
            .IncludeBase<EntityBase, ViewModelBase>();
        CreateMap<User, UserViewModel>()
            .IncludeBase<EntityBase, ViewModelBase>();
    }
}