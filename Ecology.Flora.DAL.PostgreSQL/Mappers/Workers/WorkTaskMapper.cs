﻿using AutoMapper;
using Ecology.Flora.DAL.Logic.ViewModels.Workers;
using Ecology.Flora.DAL.PostgreSQL.Models.Workers;

namespace Ecology.Flora.DAL.PostgreSQL.Mappers.Workers;

public class WorkTaskMapper : Profile
{
    public WorkTaskMapper()
    {
        CreateMap<WorkTask, TaskViewModel>()
            .IncludeBase<EntityBase, ViewModelBase>();
    }
}