﻿using AutoMapper;
using Ecology.Flora.DAL.Logic.ViewModels.Metrics;
using Ecology.Flora.DAL.PostgreSQL.Models.Metrics;

namespace Ecology.Flora.DAL.PostgreSQL.Mappers.Metrics;

public class MetricRaspredMapper : Profile
{
    public MetricRaspredMapper()
    {
        CreateMap<MetricRaspred, ViewModelBase>()
            .IncludeBase<EntityBase, ViewModelBase>();
        CreateMap<MetricRaspred, MetricRaspredViewModel>()
            .IncludeBase<EntityBase, ViewModelBase>();
    }
}