﻿using AutoMapper;
using Ecology.Flora.DAL.Logic.ViewModels.Metrics;
using Ecology.Flora.DAL.PostgreSQL.Models.Metrics;

namespace Ecology.Flora.DAL.PostgreSQL.Mappers.Metrics;

public class MetricMapper : Profile
{
    public MetricMapper()
    {
        CreateMap<Metric, ViewModelBase>()
            .IncludeBase<EntityBase, ViewModelBase>();
        CreateMap<Metric, MetricViewModel>()
            .IncludeBase<EntityBase, ViewModelBase>()
            .ForMember(x => x.MetricRaspreds,
                expression => expression.MapFrom(x => x.MetricRaspreds.Select(y => y.Id)));
    }
}
