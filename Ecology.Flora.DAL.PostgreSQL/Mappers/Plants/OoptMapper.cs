﻿using AutoMapper;
using Ecology.Flora.DAL.Logic.ViewModels.Plants;
using Ecology.Flora.DAL.PostgreSQL.Models.Plants;

namespace Ecology.Flora.DAL.PostgreSQL.Mappers.Plants;

public class OoptMapper : Profile
{
    public OoptMapper()
    {
        CreateMap<Oopt, ViewModelBase>()
            .IncludeBase<EntityBase, ViewModelBase>();
        CreateMap<Oopt, OoptViewModel>()
            .IncludeBase<EntityBase, ViewModelBase>();
    }
}
