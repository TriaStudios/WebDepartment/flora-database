﻿using AutoMapper;
using Ecology.Flora.DAL.Logic.ViewModels.Plants;
using Ecology.Flora.DAL.PostgreSQL.Models.Plants;

namespace Ecology.Flora.DAL.PostgreSQL.Mappers.Plants;

public class PlantClassMapper : Profile
{
    public PlantClassMapper()
    {
        CreateMap<PlantClass, ViewModelBase>()
            .IncludeBase<EntityBase, ViewModelBase>();
        CreateMap<PlantClass, PlantClassViewModel>()
            .IncludeBase<EntityBase, ViewModelBase>();
    }
}