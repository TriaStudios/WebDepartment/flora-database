﻿using AutoMapper;
using Ecology.Flora.DAL.Logic.ViewModels.Plants;
using Ecology.Flora.DAL.PostgreSQL.Models.Plants;

namespace Ecology.Flora.DAL.PostgreSQL.Mappers.Plants;

public class RedBookPlantMapper : Profile
{
    public RedBookPlantMapper()
    {
        CreateMap<RedBookPlant, ViewModelBase>()
            .IncludeBase<EntityBase, ViewModelBase>();
        CreateMap<RedBookPlant, RedBookPlantViewModel>()
            .IncludeBase<EntityBase, ViewModelBase>();
    }
}