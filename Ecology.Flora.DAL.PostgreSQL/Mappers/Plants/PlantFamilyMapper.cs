﻿using AutoMapper;
using Ecology.Flora.DAL.Logic.ViewModels.Plants;
using Ecology.Flora.DAL.PostgreSQL.Models.Plants;

namespace Ecology.Flora.DAL.PostgreSQL.Mappers.Plants;

public class PlantFamilyMapper : Profile
{
    public PlantFamilyMapper()
    {
        CreateMap<PlantFamily, ViewModelBase>()
            .IncludeBase<EntityBase, ViewModelBase>();
        CreateMap<PlantFamily, PlantFamilyViewModel>()
            .IncludeBase<EntityBase, ViewModelBase>();
    }
}