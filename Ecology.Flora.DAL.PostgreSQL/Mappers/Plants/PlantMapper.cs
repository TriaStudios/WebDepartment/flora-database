﻿using System.Collections.Generic;
using AutoMapper;
using Ecology.Flora.DAL.Logic.ViewModels.Plants;
using Ecology.Flora.DAL.PostgreSQL.Models.Plants;

namespace Ecology.Flora.DAL.PostgreSQL.Mappers.Plants;

public class PlantMapper : Profile
{
    public PlantMapper()
    {
        CreateMap<RedBookPlant, RedBookRarityCategory>();
        CreateMap<Plant, ViewModelBase>()
            .IncludeBase<EntityBase, ViewModelBase>();
        CreateMap<Plant, PlantViewModel>()
            .IncludeBase<EntityBase, ViewModelBase>()
            .ForMember(x => x.PlantCharacteristicValues, expression => expression.MapFrom(x => x.PlantCharacteristicValues
                .GroupBy(y => new KeyValuePair<Guid, LocalizeProperty>(y.PlantCharacteristic.Id, y.PlantCharacteristic.Name))
                .Select(y => new PlantCharacteristicValuesViewModel
                {
                    Characteristic = new ViewModelBase
                    {
                        Id = y.Key.Key,
                        Name = y.Key.Value,
                    },
                    Values = y.Select(z => new ViewModelBase
                    {
                        Id = z.Id,
                        Name = z.Name
                    }).ToList()
                })));
    }
}
