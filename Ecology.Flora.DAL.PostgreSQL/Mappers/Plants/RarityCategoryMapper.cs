﻿using AutoMapper;
using Ecology.Flora.DAL.Logic.ViewModels.Plants;
using Ecology.Flora.DAL.PostgreSQL.Models.Plants;

namespace Ecology.Flora.DAL.PostgreSQL.Mappers.Plants;

public class RarityCategoryMapper : Profile
{
    public RarityCategoryMapper()
    {
        CreateMap<RarityCategory, ViewModelBase>()
            .IncludeBase<EntityBase, ViewModelBase>();
        CreateMap<RarityCategory, RarityCategoryViewModel>()
            .IncludeBase<EntityBase, ViewModelBase>();
    }
}