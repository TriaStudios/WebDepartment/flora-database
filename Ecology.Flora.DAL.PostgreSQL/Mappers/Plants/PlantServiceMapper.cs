﻿using System.Collections.Generic;
using AutoMapper;
using Ecology.Flora.DAL.Logic.ViewModels.Plants;
using Ecology.Flora.DAL.PostgreSQL.Models.Plants;

namespace Ecology.Flora.DAL.PostgreSQL.Mappers.Plants;

public class PlantServiceMapper : Profile
{
    public PlantServiceMapper()
    {
        CreateMap<Plant, SerivcePlantViewModel>()
            .ForMember(x => x.PlantCharacteristicValues, expression => expression.MapFrom(x => x.PlantCharacteristicValues
                .GroupBy(y => new KeyValuePair<Guid, LocalizeProperty>(y.PlantCharacteristic.Id, y.PlantCharacteristic.Name))
                .Select(y => new PlantCharacteristicValuesViewModel
                {
                    Characteristic = new ViewModelBase
                    {
                        Id = y.Key.Key,
                        Name = y.Key.Value,
                    },
                    Values = y.Select(z => new ViewModelBase
                    {
                        Id = z.Id,
                        Name = z.Name
                    }).ToList()
                })));
    }
}
