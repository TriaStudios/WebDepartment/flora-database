﻿using AutoMapper;
using Ecology.Flora.DAL.Logic.ViewModels.Plants;
using Ecology.Flora.DAL.PostgreSQL.Models.Plants;

namespace Ecology.Flora.DAL.PostgreSQL.Mappers.Plants;

public class RedBookMapper : Profile
{
    public RedBookMapper()
    {
        CreateMap<RedBook, ViewModelBase>()
            .IncludeBase<EntityBase, ViewModelBase>();
        CreateMap<RedBook, RedBookViewModel>()
            .IncludeBase<EntityBase, ViewModelBase>();
        CreateMap<RedBookPlant, RedBookRarityCategory>();
    }
}