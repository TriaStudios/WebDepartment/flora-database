﻿using AutoMapper;

namespace Ecology.Flora.DAL.PostgreSQL.Mappers;

public class BaseMapper : Profile
{
    public BaseMapper()
    {
        CreateMap<EntityBase, ViewModelBase>();
    }
}