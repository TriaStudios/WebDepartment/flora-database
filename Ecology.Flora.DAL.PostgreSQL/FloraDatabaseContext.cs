using Ecology.DAL.Common;
using Ecology.Flora.DAL.Logic.Enums;
using Ecology.Flora.DAL.PostgreSQL.Models.Metrics;
using Ecology.Flora.DAL.PostgreSQL.Models.Plants;
using Ecology.Flora.DAL.PostgreSQL.Models.Storage;
using Ecology.Flora.DAL.PostgreSQL.Models.Users;
using Ecology.Flora.DAL.PostgreSQL.Models.Workers;
using Npgsql;

namespace Ecology.Flora.DAL.PostgreSQL;

public sealed class FloraDatabaseContext : DbContext
{
    private static bool _migrated;

    public FloraDatabaseContext(DbContextOptions<FloraDatabaseContext> options)
        : base(options)
    {
        NpgsqlConnection.GlobalTypeMapper.UseJsonNet(
            new[] {typeof(LocalizeProperty), typeof(PositionProperty)},
            new[] {typeof(LocalizeProperty), typeof(PositionProperty)});

        if (!Database.IsRelational())
        {
            Database.EnsureCreated();
            return;
        }
        if (_migrated) return;

        Database.Migrate();
        _migrated = true;
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.LogTo(Console.WriteLine, LogLevel.Warning);
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Oopt>().Property(x => x.Name).HasJsonConversion();
        modelBuilder.Entity<PlantClade>().Property(x => x.Name).HasJsonConversion();
        modelBuilder.Entity<PlantClass>().Property(x => x.Name).HasJsonConversion();
        modelBuilder.Entity<Plant>(builder =>
        {
            builder.Property(x => x.Name).HasJsonConversion();
            builder.Property(x => x.Position).HasJsonConversion();
            builder.Property(x => x.RedBookComment).HasJsonConversion();
            builder.Property(x => x.EcologicalPhytocenoticGroupComment).HasJsonConversion();
        });
        modelBuilder.Entity<PlantFamily>().Property(x => x.Name).HasJsonConversion();
        modelBuilder.Entity<RedBook>().Property(x => x.Name).HasJsonConversion();
        modelBuilder.Entity<RedBookPlant>().Property(x => x.Name).HasJsonConversion();
        modelBuilder.Entity<PlantCharacteristicValue>().Property(x => x.Name).HasJsonConversion();
        modelBuilder.Entity<WorkTask>().Property(x => x.Name).HasJsonConversion();
        modelBuilder.Entity<Image>().Property(x => x.Name).HasJsonConversion();
        
        modelBuilder.Entity<PlantCharacteristic>(builder =>
        {
            builder.Property(x => x.Name).HasJsonConversion();
            builder.HasData(new PlantCharacteristic
            {
                Id = new Guid("B2DA098F-31BA-4A5F-B78F-6BD589E24198"),
                Name = new LocalizeProperty {Ru = "Тип ареала", La = PlantCharacteristicNames.ArealType}
            }, new PlantCharacteristic
            {
                Id = new Guid("F29B07A4-A955-4E21-9D7F-B2AB572CC402"),
                Name = new LocalizeProperty {Ru = "Эколого-фитоценотическая группа", La = PlantCharacteristicNames.EcologicalPhytocenoticGroup}
            }, new PlantCharacteristic
            {
                Id = new Guid("1903B354-EB38-43FD-B5E9-C2F1B299C1EE"),
                Name = new LocalizeProperty {Ru = "Хозяйственное значение вида", La = PlantCharacteristicNames.EconomicValue}
            }, new PlantCharacteristic
            {
                Id = new Guid("064616BA-8DDA-4CA4-B314-C46D78DBE0AA"),
                Name = new LocalizeProperty {Ru = "Гидротипическая группа", La = PlantCharacteristicNames.HydrotypeGroup}
            }, new PlantCharacteristic
            {
                Id = new Guid("F2ADA7D8-7CCC-45EF-B935-97C2ED475F5D"),
                Name = new LocalizeProperty {Ru = "Группа по времени заноса", La = PlantCharacteristicNames.IntroductionTimeGroup}
            }, new PlantCharacteristic
            {
                Id = new Guid("C92897CB-6FA0-4D11-8E58-F10A0C2DDFCE"),
                Name = new LocalizeProperty {Ru = "Способ заноса", La = PlantCharacteristicNames.IntroductionWay}
            }, new PlantCharacteristic
            {
                Id = new Guid("8A9D7052-8F1F-4C3F-8808-87EFA4ED8C9B"),
                Name = new LocalizeProperty {Ru = "Степень натурализации", La = PlantCharacteristicNames.NaturalizationDegree}
            }, new PlantCharacteristic
            {
                Id = new Guid("3607F450-8B2D-42E7-97C7-FF697E0ED19C"),
                Name = new LocalizeProperty {Ru = "Жизненная форма по К. Раункиеру (Raunkiaer, 1934)", La = PlantCharacteristicNames.LifeFormRaunkiaer}
            }, new PlantCharacteristic
            {
                Id = new Guid("75A35A00-F0DB-4C3F-B901-4064F7ACD909"),
                Name = new LocalizeProperty {Ru = "Жизненная форма по И.Г. Серебрякову (1962) и В.Н. Голубеву (1972)", La = PlantCharacteristicNames.LifeFormSerebryakovGolubev}
            }, new PlantCharacteristic
            {
                Id = new Guid("925B0E4C-3B34-4EFB-AA67-8CDB9FA55DD9"),
                Name = new LocalizeProperty {Ru = "Встречаемость вида на изученной территории", La = PlantCharacteristicNames.OccurrenceSpecies}
            },  new PlantCharacteristic
            {
                Id = new Guid("1941177F-805A-4971-BDF6-16413A9809E0"),
                Name = new LocalizeProperty {Ru = "Происхождение", La = PlantCharacteristicNames.OriginSpecies}
            }, new PlantCharacteristic
            {
                Id = new Guid("69E1C360-675D-4AD1-88DC-58E5E92A80CB"),
                Name = new LocalizeProperty {Ru = "Река", La = PlantCharacteristicNames.River}
            }, new PlantCharacteristic
            {
                Id = new Guid("CC6691F8-A7CA-4009-89A9-2AEE45DCEC1B"),
                Name = new LocalizeProperty {Ru = "ООПТ", La = PlantCharacteristicNames.Oopt}
            });
        });

        var adminRole = new Role
            {Id = Guid.Parse("13F58593-D0D5-43D0-A2E6-404AF9C49AF1"), Name = new LocalizeProperty {Ru = Roles.Admin}};
        var userRole = new Role
            {Id = Guid.Parse("F6C16D1D-98B0-430D-A4BC-B31B37DF5AC1"), Name = new LocalizeProperty {Ru = Roles.User}};
        User adminUser = new()
        {
            Id = Guid.Parse("AEE1CD0D-7109-475B-913C-84574D7B1CA7"),
            Email = "admin@admin.ru",
            Name = new LocalizeProperty {Ru = "Adminov Admin Adminovich"},
            Username = "Admin",
            Password = "51dc47aa1066e41966a8f11573b4de7840b1f849c3062315beca024ceceeca00" //"triastudios"
        };

        modelBuilder.Entity<Role>(builder =>
        {
            builder.Property(x => x.Name).HasJsonConversion();
            builder.HasData(adminRole, userRole);
        });
        modelBuilder.Entity<User>(builder =>
        {
            builder.Property(x => x.Name).HasJsonConversion();
            builder.HasData(adminUser);
        });
        modelBuilder.Entity("RoleUser", builder =>
        {
            builder.HasData(new
            {
                RolesId = Guid.Parse("13f58593-d0d5-43d0-a2e6-404af9c49af1"),
                UsersId = Guid.Parse("aee1cd0d-7109-475b-913c-84574d7b1ca7")
            }, new
            {
                RolesId = Guid.Parse("f6c16d1d-98b0-430d-a4bc-b31b37df5ac1"),
                UsersId = Guid.Parse("aee1cd0d-7109-475b-913c-84574d7b1ca7")
            });
        });

        modelBuilder.Entity<Metric>(builder =>
        {
            builder.Property(x => x.Name).HasJsonConversion();
            builder.HasData(new Metric
            {
                Id = Guid.Parse("ACED897E-1284-444A-A618-86D8F2B1B838"),
                Name = new LocalizeProperty {Ru = "Cтепень изученности растительного покрова"}
            }, new Metric
            {
                Id = Guid.Parse("F4A86D36-D4A5-4EEC-B6E5-22C8CDA0FE4A"),
                Name = new LocalizeProperty {Ru = "Демонстрационное (эталонное) значение"}
            }, new Metric
            {
                Id = Guid.Parse("0BC1A1AF-CE4C-4403-B4AC-13E0984275C1"),
                Name = new LocalizeProperty {Ru = "Площадь памятника природы"}
            }, new Metric
            {
                Id = Guid.Parse("A516C974-6828-4F64-A90A-F3F912C8B683"),
                Name = new LocalizeProperty {Ru = "Антропотолерантность растительного покрова"}
            }, new Metric
            {
                Id = Guid.Parse("E67C62DF-FE6F-420A-92A4-CBE1BBD08AFA"),
                Name = new LocalizeProperty {Ru = "Ценотнческое разнообрази"}
            }, new Metric
            {
                Id = Guid.Parse("3EC5BE41-785F-41F1-8DDA-A66214D2D999"),
                Name = new LocalizeProperty {Ru = "Общая численность видового разнообразия"}
            }, new Metric
            {
                Id = Guid.Parse("7665A6D9-319A-4D29-8F93-BB9ADED0993E"),
                Name = new LocalizeProperty
                    {Ru = "Число видов, занесенных в Красную книгу Российской Федерации и Самарской области"}
            }, new Metric
            {
                Id = Guid.Parse("F4075D07-9BAA-4C06-A475-E518B850B4F2"),
                Name = new LocalizeProperty {Ru = "Степень трансформпрованности"}
            }, new Metric
            {
                Id = Guid.Parse("81445E34-2819-4664-9FC7-3DDE860A1FEB"),
                Name = new LocalizeProperty {Ru = "Восстановительный потенциал"}
            });
        });

        modelBuilder.Entity<MetricRaspred>(builder =>
        {
            builder.Property(x => x.Name).HasJsonConversion();
            builder.HasData(
                // Guid("ACED897E-1284-444A-A618-86D8F2B1B838")
                // Name = "Cтепень изученности растительного покрова"
                new MetricRaspred
                {
                    Id = Guid.Parse("BCC16160-9931-486C-BAAF-2A8E93AED5EA"),
                    Bal = 0,
                    Name = new LocalizeProperty {Ru = "не изучен"},
                    Value = -1,
                    MetricId = Guid.Parse("ACED897E-1284-444A-A618-86D8F2B1B838")
                },
                new MetricRaspred
                {
                    Id = Guid.Parse("65CF14CF-70BA-4E89-912C-57A6722291F8"),
                    Bal = 1,
                    Name = new LocalizeProperty {Ru = "очень слабо изучен"},
                    Value = 0,
                    MetricId = Guid.Parse("ACED897E-1284-444A-A618-86D8F2B1B838")
                },
                new MetricRaspred
                {
                    Id = Guid.Parse("782184AB-93CB-4FE7-BE6F-CF40F88EF128"),
                    Bal = 2,
                    Name = new LocalizeProperty {Ru = "слабо изучен"},
                    Value = 2,
                    MetricId = Guid.Parse("ACED897E-1284-444A-A618-86D8F2B1B838")
                },
                new MetricRaspred
                {
                    Id = Guid.Parse("22286C99-68CB-4FBE-92A4-F682FB64987F"),
                    Bal = 3,
                    Name = new LocalizeProperty {Ru = "средне изучен"},
                    Value = 4,
                    MetricId = Guid.Parse("ACED897E-1284-444A-A618-86D8F2B1B838")
                },
                new MetricRaspred
                {
                    Id = Guid.Parse("41D32B10-F9AF-4D49-BEE5-284108B6E3B7"),
                    Bal = 4,
                    Name = new LocalizeProperty {Ru = "хорошо изучен"},
                    Value = 10,
                    MetricId = Guid.Parse("ACED897E-1284-444A-A618-86D8F2B1B838")
                },
                // Guid("F4A86D36-D4A5-4EEC-B6E5-22C8CDA0FE4A")
                // Name = "Демонстрационное (эталонное) значение".ToLocalizeProperty()
                new MetricRaspred
                {
                    Id = Guid.Parse("37A5B524-A401-4CC5-9E5B-1D1BD973E5AE"),
                    Bal = 0,
                    Name = new LocalizeProperty {Ru = "не имеет"},
                    Value = -1,
                    MetricId = Guid.Parse("F4A86D36-D4A5-4EEC-B6E5-22C8CDA0FE4A")
                },
                new MetricRaspred
                {
                    Id = Guid.Parse("BA0B4792-6406-4384-8CC2-B175D68D537A"),
                    Bal = 2,
                    Name = new LocalizeProperty {Ru = "незначительное"},
                    Value = 20,
                    MetricId = Guid.Parse("F4A86D36-D4A5-4EEC-B6E5-22C8CDA0FE4A")
                },
                new MetricRaspred
                {
                    Id = Guid.Parse("BB475D8C-B4FA-4E68-BF31-B822815DF72C"),
                    Bal = 4,
                    Name = new LocalizeProperty {Ru = "среднее"},
                    Value = 20,
                    MetricId = Guid.Parse("F4A86D36-D4A5-4EEC-B6E5-22C8CDA0FE4A")
                },
                new MetricRaspred
                {
                    Id = Guid.Parse("A5C58AB6-E56B-4F39-BD9D-9EDD899527FF"),
                    Bal = 6,
                    Name = new LocalizeProperty {Ru = "большое"},
                    Value = 50,
                    MetricId = Guid.Parse("F4A86D36-D4A5-4EEC-B6E5-22C8CDA0FE4A")
                },
                new MetricRaspred
                {
                    Id = Guid.Parse("F2595E32-666F-493D-BDFC-D7254BC1471D"),
                    Bal = 8,
                    Name = new LocalizeProperty {Ru = "очень большое"},
                    Value = 90,
                    MetricId = Guid.Parse("F4A86D36-D4A5-4EEC-B6E5-22C8CDA0FE4A")
                },
                // Guid("0BC1A1AF-CE4C-4403-B4AC-13E0984275C1")
                // Name = "Площадь памятника природы".ToLocalizeProperty()
                new MetricRaspred
                {
                    Id = Guid.Parse("A84C2FE6-DEE5-4DC3-9423-E3221F19401A"),
                    Bal = 0,
                    Name = new LocalizeProperty {Ru = "больше 1 га"},
                    Value = -1,
                    MetricId = Guid.Parse("0BC1A1AF-CE4C-4403-B4AC-13E0984275C1")
                },
                new MetricRaspred
                {
                    Id = Guid.Parse("EED727BA-F364-4934-B2BC-DABFCD72D5FA"),
                    Bal = 3,
                    Name = new LocalizeProperty {Ru = "до 10 га"},
                    Value = 1,
                    MetricId = Guid.Parse("0BC1A1AF-CE4C-4403-B4AC-13E0984275C1")
                },
                new MetricRaspred
                {
                    Id = Guid.Parse("1A9FF714-37AE-424A-9A74-327BA8AB706E"),
                    Bal = 6,
                    Name = new LocalizeProperty {Ru = "до 100 га"},
                    Value = 10,
                    MetricId = Guid.Parse("0BC1A1AF-CE4C-4403-B4AC-13E0984275C1")
                },
                new MetricRaspred
                {
                    Id = Guid.Parse("BCEDCBE1-DFE3-42BC-8143-15FEDE359AE0"),
                    Bal = 9,
                    Name = new LocalizeProperty {Ru = "до 300 га"},
                    Value = 100,
                    MetricId = Guid.Parse("0BC1A1AF-CE4C-4403-B4AC-13E0984275C1")
                },
                new MetricRaspred
                {
                    Id = Guid.Parse("14B5A520-082A-429B-A617-6F40D58CDFD4"),
                    Bal = 12,
                    Name = new LocalizeProperty {Ru = "больше 300 га"},
                    Value = 300,
                    MetricId = Guid.Parse("0BC1A1AF-CE4C-4403-B4AC-13E0984275C1")
                },
                // Guid("A516C974-6828-4F64-A90A-F3F912C8B683")
                // Name = "Антропотолерантность растительного покрова".ToLocalizeProperty()
                new MetricRaspred
                {
                    Id = Guid.Parse("6BAB36CD-3F9B-476B-85A9-BB6F6375C036"),
                    Bal = 0,
                    Name = new LocalizeProperty {Ru = "очень высокая"},
                    Value = 0,
                    MetricId = Guid.Parse("A516C974-6828-4F64-A90A-F3F912C8B683")
                },
                new MetricRaspred
                {
                    Id = Guid.Parse("DE10375F-B293-4493-A6D1-04528009CA98"),
                    Bal = 4,
                    Name = new LocalizeProperty {Ru = "высокая"},
                    Value = 0,
                    MetricId = Guid.Parse("A516C974-6828-4F64-A90A-F3F912C8B683")
                },
                new MetricRaspred
                {
                    Id = Guid.Parse("CCEC6D5D-67AD-4325-A494-F079FE00FCF1"),
                    Bal = 8,
                    Name = new LocalizeProperty {Ru = "средняя"},
                    Value = 0,
                    MetricId = Guid.Parse("A516C974-6828-4F64-A90A-F3F912C8B683")
                },
                new MetricRaspred
                {
                    Id = Guid.Parse("0BC7FD4D-D965-4572-A6D1-6CEA1AA3AA23"),
                    Bal = 12,
                    Name = new LocalizeProperty {Ru = "слабая"},
                    Value = 0,
                    MetricId = Guid.Parse("A516C974-6828-4F64-A90A-F3F912C8B683")
                },
                new MetricRaspred
                {
                    Id = Guid.Parse("77127F02-9072-4A8D-8E9A-3028D2C26F16"),
                    Bal = 16,
                    Name = new LocalizeProperty {Ru = "очень слабая"},
                    Value = 0,
                    MetricId = Guid.Parse("A516C974-6828-4F64-A90A-F3F912C8B683")
                },
                // Guid("E67C62DF-FE6F-420A-92A4-CBE1BBD08AFA")
                // Name = "Ценотнческое разнообрази".ToLocalizeProperty()
                new MetricRaspred
                {
                    Id = Guid.Parse("25F789C0-3971-4BE3-B0F1-4E0742F415C5"),
                    Bal = 0,
                    Name = new LocalizeProperty {Ru = "один тип растительности"},
                    Value = -1,
                    MetricId = Guid.Parse("E67C62DF-FE6F-420A-92A4-CBE1BBD08AFA")
                },
                new MetricRaspred
                {
                    Id = Guid.Parse("288BF0F4-A085-48DC-9350-BAFE6F81F610"),
                    Bal = 5,
                    Name = new LocalizeProperty {Ru = "два типа растительности"},
                    Value = 1,
                    MetricId = Guid.Parse("E67C62DF-FE6F-420A-92A4-CBE1BBD08AFA")
                },
                new MetricRaspred
                {
                    Id = Guid.Parse("C443ABFE-DF93-4E9B-BB95-B31BC33A8EFA"),
                    Bal = 10,
                    Name = new LocalizeProperty {Ru = "три типа растительности"},
                    Value = 2,
                    MetricId = Guid.Parse("E67C62DF-FE6F-420A-92A4-CBE1BBD08AFA")
                },
                new MetricRaspred
                {
                    Id = Guid.Parse("7D47DEEF-8497-4A6B-BDD1-8D695593FCD1"),
                    Bal = 15,
                    Name = new LocalizeProperty {Ru = "четыре типа растительности"},
                    Value = 3,
                    MetricId = Guid.Parse("E67C62DF-FE6F-420A-92A4-CBE1BBD08AFA")
                },
                new MetricRaspred
                {
                    Id = Guid.Parse("97C9693C-A91E-4D20-A896-1670C2D69F7F"),
                    Bal = 20,
                    Name = new LocalizeProperty {Ru = "более четырех типов растительности"},
                    Value = 4,
                    MetricId = Guid.Parse("E67C62DF-FE6F-420A-92A4-CBE1BBD08AFA")
                },
                // Guid("3EC5BE41-785F-41F1-8DDA-A66214D2D999")
                // Name = "Общая численность видового разнообразия".ToLocalizeProperty()
                new MetricRaspred
                {
                    Id = Guid.Parse("0E3CB4EE-1AD8-4AE6-B86B-4AE65C413147"),
                    Bal = 0,
                    Name = new LocalizeProperty {Ru = "до 50 видов"},
                    Value = -1,
                    MetricId = Guid.Parse("3EC5BE41-785F-41F1-8DDA-A66214D2D999")
                },
                new MetricRaspred
                {
                    Id = Guid.Parse("1C582251-C662-4115-A900-EA444D25D6AC"),
                    Bal = 6,
                    Name = new LocalizeProperty {Ru = "до 100 видов"},
                    Value = 50,
                    MetricId = Guid.Parse("3EC5BE41-785F-41F1-8DDA-A66214D2D999")
                },
                new MetricRaspred
                {
                    Id = Guid.Parse("801C4000-81F7-4C81-8A54-1FFAFEBB050E"),
                    Bal = 12,
                    Name = new LocalizeProperty {Ru = "до 150 видов"},
                    Value = 100,
                    MetricId = Guid.Parse("3EC5BE41-785F-41F1-8DDA-A66214D2D999")
                },
                new MetricRaspred
                {
                    Id = Guid.Parse("C386E693-DE2A-4401-8175-08F216794FA8"),
                    Bal = 18,
                    Name = new LocalizeProperty {Ru = "до 300 видов"},
                    Value = 150,
                    MetricId = Guid.Parse("3EC5BE41-785F-41F1-8DDA-A66214D2D999")
                },
                new MetricRaspred
                {
                    Id = Guid.Parse("33D71236-BB48-4E1A-B93B-721BBA344152"),
                    Bal = 24,
                    Name = new LocalizeProperty {Ru = "более 300 видов"},
                    Value = 300,
                    MetricId = Guid.Parse("3EC5BE41-785F-41F1-8DDA-A66214D2D999")
                },
                // Guid("7665A6D9-319A-4D29-8F93-BB9ADED0993E"),
                // Name = "Число видов, занесенных в Красную книгу Российской Федерации и Самарской области".ToLocalizeProperty()
                new MetricRaspred
                {
                    Id = Guid.Parse("855FE008-C723-4A76-84F5-F017E4B04626"),
                    Bal = 0,
                    Name = new LocalizeProperty {Ru = "не занесено"},
                    Value = -1,
                    MetricId = Guid.Parse("7665A6D9-319A-4D29-8F93-BB9ADED0993E")
                },
                new MetricRaspred
                {
                    Id = Guid.Parse("2FF80421-E8B2-4CB0-A595-92A0A994FE82"),
                    Bal = 7,
                    Name = new LocalizeProperty {Ru = "от 1 до 5 видов"},
                    Value = 0,
                    MetricId = Guid.Parse("7665A6D9-319A-4D29-8F93-BB9ADED0993E")
                },
                new MetricRaspred
                {
                    Id = Guid.Parse("2CEA744F-A7A8-4B4C-B47E-2463B60CBB0F"),
                    Bal = 14,
                    Name = new LocalizeProperty {Ru = "6-10 видов"},
                    Value = 5,
                    MetricId = Guid.Parse("7665A6D9-319A-4D29-8F93-BB9ADED0993E")
                },
                new MetricRaspred
                {
                    Id = Guid.Parse("F9184881-CC8C-434F-B40F-426F2ABC4182"),
                    Bal = 21,
                    Name = new LocalizeProperty {Ru = "11-20 видов"},
                    Value = 10,
                    MetricId = Guid.Parse("7665A6D9-319A-4D29-8F93-BB9ADED0993E")
                },
                new MetricRaspred
                {
                    Id = Guid.Parse("E9302E68-B55B-41E4-A812-A8B1575C1869"),
                    Bal = 28,
                    Name = new LocalizeProperty {Ru = "более 21 вида"},
                    Value = 20,
                    MetricId = Guid.Parse("7665A6D9-319A-4D29-8F93-BB9ADED0993E")
                },
                // Guid("F4075D07-9BAA-4C06-A475-E518B850B4F2"),
                // Name = new LocalizeProperty{Ru = "Степень трансформпрованности"},
                new MetricRaspred
                {
                    Id = Guid.Parse("F7263813-D795-4ED0-A4C0-295F35D8F934"),
                    Bal = 0,
                    Name = new LocalizeProperty {Ru = "полностью трансформирован"},
                    Value = -1,
                    MetricId = Guid.Parse("F4075D07-9BAA-4C06-A475-E518B850B4F2")
                },
                new MetricRaspred
                {
                    Id = Guid.Parse("ECE4881E-D0F7-4D05-ADF5-614E9ACCF30B"),
                    Bal = 8,
                    Name = new LocalizeProperty {Ru = "сильно трансформирован"},
                    Value = 20,
                    MetricId = Guid.Parse("F4075D07-9BAA-4C06-A475-E518B850B4F2")
                },
                new MetricRaspred
                {
                    Id = Guid.Parse("A52D6397-3E1B-4C35-8F12-78C10408A9DB"),
                    Bal = 16,
                    Name = new LocalizeProperty {Ru = "слабо трансформирован"},
                    Value = 40,
                    MetricId = Guid.Parse("F4075D07-9BAA-4C06-A475-E518B850B4F2")
                },
                new MetricRaspred
                {
                    Id = Guid.Parse("31D36BF2-19FA-46CF-A01D-2B9762732BD8"),
                    Bal = 24,
                    Name = new LocalizeProperty {Ru = "условно коренной"},
                    Value = 60,
                    MetricId = Guid.Parse("F4075D07-9BAA-4C06-A475-E518B850B4F2")
                },
                new MetricRaspred
                {
                    Id = Guid.Parse("9961A32A-97EC-4D96-8623-CB666DFFDE96"),
                    Bal = 32,
                    Name = new LocalizeProperty {Ru = "коренной"},
                    Value = 80,
                    MetricId = Guid.Parse("F4075D07-9BAA-4C06-A475-E518B850B4F2")
                },
                // Guid("81445E34-2819-4664-9FC7-3DDE860A1FEB"),
                // Name = new LocalizeProperty{Ru = "Восстановительный потенциал"},
                new MetricRaspred
                {
                    Id = Guid.Parse("BAF4167A-9F6D-49E4-AA96-B7808484BA6F"),
                    Bal = 0,
                    Name = new LocalizeProperty {Ru = "очень хороший"},
                    Value = -1,
                    MetricId = Guid.Parse("81445E34-2819-4664-9FC7-3DDE860A1FEB")
                },
                new MetricRaspred
                {
                    Id = Guid.Parse("FA8A9D98-51E7-4C8C-B53D-1344995FA282"),
                    Bal = 9,
                    Name = new LocalizeProperty {Ru = "хороший"},
                    Value = 50,
                    MetricId = Guid.Parse("81445E34-2819-4664-9FC7-3DDE860A1FEB")
                },
                new MetricRaspred
                {
                    Id = Guid.Parse("FC4BDC25-6953-4B9A-98F0-A896CC10E48F"),
                    Bal = 18,
                    Name = new LocalizeProperty {Ru = "удовлетворительный"},
                    Value = 100,
                    MetricId = Guid.Parse("81445E34-2819-4664-9FC7-3DDE860A1FEB")
                },
                new MetricRaspred
                {
                    Id = Guid.Parse("74CF4C2F-7885-4486-A0F4-56019AD88B47"),
                    Bal = 27,
                    Name = new LocalizeProperty {Ru = "слабый"},
                    Value = 150,
                    MetricId = Guid.Parse("81445E34-2819-4664-9FC7-3DDE860A1FEB")
                },
                new MetricRaspred
                {
                    Id = Guid.Parse("FD072C8D-9C48-4FBC-B73B-8BD503F3D09C"),
                    Bal = 36,
                    Name = new LocalizeProperty {Ru = "очень слабый"},
                    Value = 200,
                    MetricId = Guid.Parse("81445E34-2819-4664-9FC7-3DDE860A1FEB")
                });
        });

        modelBuilder.Entity<RarityCategory>(builder =>
        {
            builder.Property(x => x.Name).HasJsonConversion();
            builder.HasData(new RarityCategory
            {
                Id = Guid.Parse("CEFDBF7A-2AE5-4166-BEC4-CE65D742283B"),
                Code = "Ex",
                Name = new LocalizeProperty {Ru = "По-видимому, исчезнувшие на территории бассейна реки Свияги"}
            }, new RarityCategory
            {
                Id = Guid.Parse("41C67E4A-8A65-42EF-9C6C-3EC197B6A3E2"),
                Code = "E",
                Name = new LocalizeProperty {Ru = "Виды, находящиеся под угрозой исчезновения"}
            }, new RarityCategory
            {
                Id = Guid.Parse("6C9A8D99-3460-4FDF-800C-CC2EA147C9CC"),
                Code = "V",
                Name = new LocalizeProperty {Ru = "Уязвимые виды, сокращающиеся в численности"}
            }, new RarityCategory
            {
                Id = Guid.Parse("EBEDC54F-5392-4A0E-BA40-5E7511D323E8"),
                Code = "R",
                Name = new LocalizeProperty {Ru = "Редкие виды"}
            }, new RarityCategory
            {
                Id = Guid.Parse("8A111549-6E7B-46CF-97F1-63189880DB30"),
                Code = "I",
                Name = new LocalizeProperty {Ru = "Виды с неопределенным статусом"}
            });
        });
    }
}
