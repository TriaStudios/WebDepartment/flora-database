using Ecology.Flora.DAL.Logic.Crud.Metrics;
using Ecology.Flora.DAL.Logic.Services.Storage;
using Ecology.Flora.DAL.PostgreSQL.Implements.Metrics;
using Ecology.Flora.DAL.PostgreSQL.Implements.Storage;
using Ecology.Flora.DAL.PostgreSQL.Mappers.Users;

namespace Ecology.Flora.DAL.PostgreSQL;

public static class FloraDalExtensions
{
    public static void AddFloraDb(this IServiceCollection services, string connectionString)
    {
        services.AddDbContextFactory<FloraDatabaseContext>(x => 
            x.UseNpgsql(
                connectionString, 
                o => o.UseQuerySplittingBehavior(QuerySplittingBehavior.SplitQuery)));

        services.AddAutoMapper(typeof(UserMapper));

        services.AddSingleton<IImageService, ImageService>();

        services.AddSingleton<IPlantCladeCrudAsync, PlantCladeCrudAsync>();
        services.AddSingleton<IPlantClassCrudAsync, PlantClassCrudAsync>();
        services.AddSingleton<IPlantCrudAsync, PlantCrudAsync>();
        services.AddSingleton<IPlantFamilyCrudAsync, PlantFamilyCrudAsync>();
        services.AddSingleton<IRarityCategoryCrudAsync, RarityCategoryCrudAsync>();
        services.AddSingleton<IRedBookCrudAsync, RedBookCrudAsync>();
        services.AddSingleton<IRedBookPlantCrudAsync, RedBookPlantCrudAsync>();
        services.AddSingleton<IOoptCrudAsync, OoptCrudAsync>();
        services.AddSingleton<IPlantCharacteristicCrudAsync, PlantCharacteristicCrudAsync>();
        services.AddSingleton<IPlantCharacteristicValueCrudAsync, PlantCharacteristicValueCrudAsync>();
        services.AddSingleton<IMetricCrudAsync, MetricCrudAsync>();
        services.AddSingleton<IMetricRaspredCrudAsync, MetricRaspredCrudAsync>();

        services.AddSingleton<IRoleCrudAsync, RoleCrudAsync>();
        services.AddSingleton<IUserCrudAsync, UserCrudAsync>();

        services.AddSingleton<ITaskCrudAsync, TaskCrudAsync>();
    }
}
