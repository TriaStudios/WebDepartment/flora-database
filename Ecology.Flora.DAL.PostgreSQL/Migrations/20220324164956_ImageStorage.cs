﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Ecology.Flora.DAL.PostgreSQL.Migrations
{
    public partial class ImageStorage : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AvatarData",
                table: "Plant");

            migrationBuilder.AddColumn<Guid>(
                name: "AvatarId",
                table: "Plant",
                type: "uuid",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Image",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "jsonb", nullable: true),
                    Created = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Updated = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Deleted = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Image", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Plant_AvatarId",
                table: "Plant",
                column: "AvatarId");

            migrationBuilder.AddForeignKey(
                name: "FK_Plant_Image_AvatarId",
                table: "Plant",
                column: "AvatarId",
                principalTable: "Image",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Plant_Image_AvatarId",
                table: "Plant");

            migrationBuilder.DropTable(
                name: "Image");

            migrationBuilder.DropIndex(
                name: "IX_Plant_AvatarId",
                table: "Plant");

            migrationBuilder.DropColumn(
                name: "AvatarId",
                table: "Plant");

            migrationBuilder.AddColumn<string>(
                name: "AvatarData",
                table: "Plant",
                type: "text",
                nullable: true);
        }
    }
}
