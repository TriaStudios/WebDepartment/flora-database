﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Ecology.Flora.DAL.PostgreSQL.Migrations
{
    public partial class MultiplyCharacters : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Plant_ArealType_ArealTypeId",
                table: "Plant");

            migrationBuilder.DropForeignKey(
                name: "FK_Plant_HydrotypeGroup_HydrotypeGroupId",
                table: "Plant");

            migrationBuilder.DropForeignKey(
                name: "FK_Plant_IntroductionTimeGroup_IntroductionTimeGroupId",
                table: "Plant");

            migrationBuilder.DropForeignKey(
                name: "FK_Plant_IntroductionWay_IntroductionWayId",
                table: "Plant");

            migrationBuilder.DropForeignKey(
                name: "FK_Plant_LifeFormSerebryakovGolubev_LifeFormSerebryakovGolubev~",
                table: "Plant");

            migrationBuilder.DropForeignKey(
                name: "FK_Plant_NaturalizationDegree_NaturalizationDegreeId",
                table: "Plant");

            migrationBuilder.DropForeignKey(
                name: "FK_Plant_OccurrenceSpecies_OccurrenceSpeciesId",
                table: "Plant");

            migrationBuilder.DropForeignKey(
                name: "FK_Plant_Oopt_OoptId",
                table: "Plant");

            migrationBuilder.DropForeignKey(
                name: "FK_Plant_OriginSpecies_OriginSpeciesId",
                table: "Plant");

            migrationBuilder.DropForeignKey(
                name: "FK_Plant_PlantFamily_PlantFamilyId",
                table: "Plant");

            migrationBuilder.DropForeignKey(
                name: "FK_Plant_River_RiverId",
                table: "Plant");

            migrationBuilder.DropTable(
                name: "ArealType");

            migrationBuilder.DropTable(
                name: "EcologicalPhytocenoticGroupPlant");

            migrationBuilder.DropTable(
                name: "EconomicValuePlant");

            migrationBuilder.DropTable(
                name: "HydrotypeGroup");

            migrationBuilder.DropTable(
                name: "IntroductionTimeGroup");

            migrationBuilder.DropTable(
                name: "IntroductionWay");

            migrationBuilder.DropTable(
                name: "LifeFormRaunkiaerPlant");

            migrationBuilder.DropTable(
                name: "LifeFormSerebryakovGolubev");

            migrationBuilder.DropTable(
                name: "NaturalizationDegree");

            migrationBuilder.DropTable(
                name: "OccurrenceSpecies");

            migrationBuilder.DropTable(
                name: "OriginSpecies");

            migrationBuilder.DropTable(
                name: "River");

            migrationBuilder.DropTable(
                name: "EcologicalPhytocenoticGroup");

            migrationBuilder.DropTable(
                name: "EconomicValue");

            migrationBuilder.DropTable(
                name: "LifeFormRaunkiaer");

            migrationBuilder.DropIndex(
                name: "IX_Plant_ArealTypeId",
                table: "Plant");

            migrationBuilder.DropIndex(
                name: "IX_Plant_HydrotypeGroupId",
                table: "Plant");

            migrationBuilder.DropIndex(
                name: "IX_Plant_IntroductionTimeGroupId",
                table: "Plant");

            migrationBuilder.DropIndex(
                name: "IX_Plant_IntroductionWayId",
                table: "Plant");

            migrationBuilder.DropIndex(
                name: "IX_Plant_LifeFormSerebryakovGolubevId",
                table: "Plant");

            migrationBuilder.DropIndex(
                name: "IX_Plant_NaturalizationDegreeId",
                table: "Plant");

            migrationBuilder.DropIndex(
                name: "IX_Plant_OccurrenceSpeciesId",
                table: "Plant");

            migrationBuilder.DropIndex(
                name: "IX_Plant_OoptId",
                table: "Plant");

            migrationBuilder.DropIndex(
                name: "IX_Plant_OriginSpeciesId",
                table: "Plant");

            migrationBuilder.DropIndex(
                name: "IX_Plant_RiverId",
                table: "Plant");

            migrationBuilder.DropColumn(
                name: "ArealTypeId",
                table: "Plant");

            migrationBuilder.DropColumn(
                name: "HydrotypeGroupId",
                table: "Plant");

            migrationBuilder.DropColumn(
                name: "IntroductionTimeGroupId",
                table: "Plant");

            migrationBuilder.DropColumn(
                name: "IntroductionWayId",
                table: "Plant");

            migrationBuilder.DropColumn(
                name: "LifeFormSerebryakovGolubevId",
                table: "Plant");

            migrationBuilder.DropColumn(
                name: "NaturalizationDegreeId",
                table: "Plant");

            migrationBuilder.DropColumn(
                name: "OccurrenceSpeciesId",
                table: "Plant");

            migrationBuilder.DropColumn(
                name: "OoptId",
                table: "Plant");

            migrationBuilder.DropColumn(
                name: "OriginSpeciesId",
                table: "Plant");

            migrationBuilder.DropColumn(
                name: "RiverId",
                table: "Plant");

            migrationBuilder.AlterColumn<Guid>(
                name: "PlantFamilyId",
                table: "Plant",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"),
                oldClrType: typeof(Guid),
                oldType: "uuid",
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "OoptPlant",
                columns: table => new
                {
                    OoptsId = table.Column<Guid>(type: "uuid", nullable: false),
                    PlantsId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OoptPlant", x => new { x.OoptsId, x.PlantsId });
                    table.ForeignKey(
                        name: "FK_OoptPlant_Oopt_OoptsId",
                        column: x => x.OoptsId,
                        principalTable: "Oopt",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OoptPlant_Plant_PlantsId",
                        column: x => x.PlantsId,
                        principalTable: "Plant",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PlantCharacteristic",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "jsonb", nullable: true),
                    Created = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Updated = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Deleted = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlantCharacteristic", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PlantCharacteristicValue",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    PlantCharacteristicId = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "jsonb", nullable: true),
                    Created = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Updated = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Deleted = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlantCharacteristicValue", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PlantCharacteristicValue_PlantCharacteristic_PlantCharacter~",
                        column: x => x.PlantCharacteristicId,
                        principalTable: "PlantCharacteristic",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PlantPlantCharacteristicValue",
                columns: table => new
                {
                    PlantCharacteristicValuesId = table.Column<Guid>(type: "uuid", nullable: false),
                    PlantsId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlantPlantCharacteristicValue", x => new { x.PlantCharacteristicValuesId, x.PlantsId });
                    table.ForeignKey(
                        name: "FK_PlantPlantCharacteristicValue_Plant_PlantsId",
                        column: x => x.PlantsId,
                        principalTable: "Plant",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PlantPlantCharacteristicValue_PlantCharacteristicValue_Plan~",
                        column: x => x.PlantCharacteristicValuesId,
                        principalTable: "PlantCharacteristicValue",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "PlantCharacteristic",
                columns: new[] { "Id", "Created", "Deleted", "Name", "Updated" },
                values: new object[,]
                {
                    { new Guid("064616ba-8dda-4ca4-b314-c46d78dbe0aa"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, "{\"Ru\":\"Гидротипическая группа\",\"La\":\"HydrotypeGroup\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { new Guid("1903b354-eb38-43fd-b5e9-c2f1b299c1ee"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, "{\"Ru\":\"Хозяйственное значение вида\",\"La\":\"EconomicValue\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { new Guid("1941177f-805a-4971-bdf6-16413a9809e0"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, "{\"Ru\":\"Происхождение\",\"La\":\"OriginSpecies\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { new Guid("3607f450-8b2d-42e7-97c7-ff697e0ed19c"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, "{\"Ru\":\"Жизненная форма по К. Раункиеру (Raunkiaer, 1934)\",\"La\":\"LifeFormRaunkiaer\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { new Guid("69e1c360-675d-4ad1-88dc-58e5e92a80cb"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, "{\"Ru\":\"Река\",\"La\":\"River\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { new Guid("75a35a00-f0db-4c3f-b901-4064f7acd909"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, "{\"Ru\":\"Жизненная форма по И.Г. Серебрякову (1962) и В.Н. Голубеву (1972)\",\"La\":\"LifeFormSerebryakovGolubev\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { new Guid("8a9d7052-8f1f-4c3f-8808-87efa4ed8c9b"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, "{\"Ru\":\"Степень натурализации\",\"La\":\"NaturalizationDegree\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { new Guid("925b0e4c-3b34-4efb-aa67-8cdb9fa55dd9"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, "{\"Ru\":\"Встречаемость вида на изученной территории\",\"La\":\"OccurrenceSpecies\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { new Guid("b2da098f-31ba-4a5f-b78f-6bd589e24198"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, "{\"Ru\":\"Тип ареала\",\"La\":\"ArealType\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { new Guid("c92897cb-6fa0-4d11-8e58-f10a0c2ddfce"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, "{\"Ru\":\"Способ заноса\",\"La\":\"IntroductionWay\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { new Guid("cc6691f8-a7ca-4009-89a9-2aee45dcec1b"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, "{\"Ru\":\"ООПТ\",\"La\":\"Oopt\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { new Guid("f29b07a4-a955-4e21-9d7f-b2ab572cc402"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, "{\"Ru\":\"Эколого-фитоценотическая группа\",\"La\":\"EcologicalPhytocenoticGroup\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { new Guid("f2ada7d8-7ccc-45ef-b935-97c2ed475f5d"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, "{\"Ru\":\"Группа по времени заноса\",\"La\":\"IntroductionTimeGroup\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) }
                });

            migrationBuilder.CreateIndex(
                name: "IX_OoptPlant_PlantsId",
                table: "OoptPlant",
                column: "PlantsId");

            migrationBuilder.CreateIndex(
                name: "IX_PlantCharacteristicValue_PlantCharacteristicId",
                table: "PlantCharacteristicValue",
                column: "PlantCharacteristicId");

            migrationBuilder.CreateIndex(
                name: "IX_PlantPlantCharacteristicValue_PlantsId",
                table: "PlantPlantCharacteristicValue",
                column: "PlantsId");

            migrationBuilder.AddForeignKey(
                name: "FK_Plant_PlantFamily_PlantFamilyId",
                table: "Plant",
                column: "PlantFamilyId",
                principalTable: "PlantFamily",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Plant_PlantFamily_PlantFamilyId",
                table: "Plant");

            migrationBuilder.DropTable(
                name: "OoptPlant");

            migrationBuilder.DropTable(
                name: "PlantPlantCharacteristicValue");

            migrationBuilder.DropTable(
                name: "PlantCharacteristicValue");

            migrationBuilder.DropTable(
                name: "PlantCharacteristic");

            migrationBuilder.AlterColumn<Guid>(
                name: "PlantFamilyId",
                table: "Plant",
                type: "uuid",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uuid");

            migrationBuilder.AddColumn<Guid>(
                name: "ArealTypeId",
                table: "Plant",
                type: "uuid",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "HydrotypeGroupId",
                table: "Plant",
                type: "uuid",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "IntroductionTimeGroupId",
                table: "Plant",
                type: "uuid",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "IntroductionWayId",
                table: "Plant",
                type: "uuid",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "LifeFormSerebryakovGolubevId",
                table: "Plant",
                type: "uuid",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "NaturalizationDegreeId",
                table: "Plant",
                type: "uuid",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "OccurrenceSpeciesId",
                table: "Plant",
                type: "uuid",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "OoptId",
                table: "Plant",
                type: "uuid",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "OriginSpeciesId",
                table: "Plant",
                type: "uuid",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "RiverId",
                table: "Plant",
                type: "uuid",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "ArealType",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Created = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Deleted = table.Column<bool>(type: "boolean", nullable: false),
                    Name = table.Column<string>(type: "jsonb", nullable: true),
                    Updated = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ArealType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "EcologicalPhytocenoticGroup",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Created = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Deleted = table.Column<bool>(type: "boolean", nullable: false),
                    Name = table.Column<string>(type: "jsonb", nullable: true),
                    Updated = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EcologicalPhytocenoticGroup", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "EconomicValue",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Created = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Deleted = table.Column<bool>(type: "boolean", nullable: false),
                    Name = table.Column<string>(type: "jsonb", nullable: true),
                    Updated = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EconomicValue", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "HydrotypeGroup",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Created = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Deleted = table.Column<bool>(type: "boolean", nullable: false),
                    Name = table.Column<string>(type: "jsonb", nullable: true),
                    Updated = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HydrotypeGroup", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "IntroductionTimeGroup",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Created = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Deleted = table.Column<bool>(type: "boolean", nullable: false),
                    Name = table.Column<string>(type: "jsonb", nullable: true),
                    Updated = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IntroductionTimeGroup", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "IntroductionWay",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Created = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Deleted = table.Column<bool>(type: "boolean", nullable: false),
                    Name = table.Column<string>(type: "jsonb", nullable: true),
                    Updated = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IntroductionWay", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "LifeFormRaunkiaer",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Created = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Deleted = table.Column<bool>(type: "boolean", nullable: false),
                    Name = table.Column<string>(type: "jsonb", nullable: true),
                    Updated = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LifeFormRaunkiaer", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "LifeFormSerebryakovGolubev",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Created = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Deleted = table.Column<bool>(type: "boolean", nullable: false),
                    Name = table.Column<string>(type: "jsonb", nullable: true),
                    Updated = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LifeFormSerebryakovGolubev", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "NaturalizationDegree",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Created = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Deleted = table.Column<bool>(type: "boolean", nullable: false),
                    Name = table.Column<string>(type: "jsonb", nullable: true),
                    Updated = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NaturalizationDegree", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "OccurrenceSpecies",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Created = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Deleted = table.Column<bool>(type: "boolean", nullable: false),
                    Name = table.Column<string>(type: "jsonb", nullable: true),
                    Updated = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OccurrenceSpecies", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "OriginSpecies",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Created = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Deleted = table.Column<bool>(type: "boolean", nullable: false),
                    Name = table.Column<string>(type: "jsonb", nullable: true),
                    Updated = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OriginSpecies", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "River",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Created = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Deleted = table.Column<bool>(type: "boolean", nullable: false),
                    Name = table.Column<string>(type: "jsonb", nullable: true),
                    Updated = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_River", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "EcologicalPhytocenoticGroupPlant",
                columns: table => new
                {
                    EcologicalPhytocenoticGroupsId = table.Column<Guid>(type: "uuid", nullable: false),
                    PlantsId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EcologicalPhytocenoticGroupPlant", x => new { x.EcologicalPhytocenoticGroupsId, x.PlantsId });
                    table.ForeignKey(
                        name: "FK_EcologicalPhytocenoticGroupPlant_EcologicalPhytocenoticGrou~",
                        column: x => x.EcologicalPhytocenoticGroupsId,
                        principalTable: "EcologicalPhytocenoticGroup",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EcologicalPhytocenoticGroupPlant_Plant_PlantsId",
                        column: x => x.PlantsId,
                        principalTable: "Plant",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "EconomicValuePlant",
                columns: table => new
                {
                    EconomicValuesId = table.Column<Guid>(type: "uuid", nullable: false),
                    PlantsId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EconomicValuePlant", x => new { x.EconomicValuesId, x.PlantsId });
                    table.ForeignKey(
                        name: "FK_EconomicValuePlant_EconomicValue_EconomicValuesId",
                        column: x => x.EconomicValuesId,
                        principalTable: "EconomicValue",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EconomicValuePlant_Plant_PlantsId",
                        column: x => x.PlantsId,
                        principalTable: "Plant",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "LifeFormRaunkiaerPlant",
                columns: table => new
                {
                    LifeFormRaunkiaersId = table.Column<Guid>(type: "uuid", nullable: false),
                    PlantsId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LifeFormRaunkiaerPlant", x => new { x.LifeFormRaunkiaersId, x.PlantsId });
                    table.ForeignKey(
                        name: "FK_LifeFormRaunkiaerPlant_LifeFormRaunkiaer_LifeFormRaunkiaers~",
                        column: x => x.LifeFormRaunkiaersId,
                        principalTable: "LifeFormRaunkiaer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_LifeFormRaunkiaerPlant_Plant_PlantsId",
                        column: x => x.PlantsId,
                        principalTable: "Plant",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Plant_ArealTypeId",
                table: "Plant",
                column: "ArealTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Plant_HydrotypeGroupId",
                table: "Plant",
                column: "HydrotypeGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_Plant_IntroductionTimeGroupId",
                table: "Plant",
                column: "IntroductionTimeGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_Plant_IntroductionWayId",
                table: "Plant",
                column: "IntroductionWayId");

            migrationBuilder.CreateIndex(
                name: "IX_Plant_LifeFormSerebryakovGolubevId",
                table: "Plant",
                column: "LifeFormSerebryakovGolubevId");

            migrationBuilder.CreateIndex(
                name: "IX_Plant_NaturalizationDegreeId",
                table: "Plant",
                column: "NaturalizationDegreeId");

            migrationBuilder.CreateIndex(
                name: "IX_Plant_OccurrenceSpeciesId",
                table: "Plant",
                column: "OccurrenceSpeciesId");

            migrationBuilder.CreateIndex(
                name: "IX_Plant_OoptId",
                table: "Plant",
                column: "OoptId");

            migrationBuilder.CreateIndex(
                name: "IX_Plant_OriginSpeciesId",
                table: "Plant",
                column: "OriginSpeciesId");

            migrationBuilder.CreateIndex(
                name: "IX_Plant_RiverId",
                table: "Plant",
                column: "RiverId");

            migrationBuilder.CreateIndex(
                name: "IX_EcologicalPhytocenoticGroupPlant_PlantsId",
                table: "EcologicalPhytocenoticGroupPlant",
                column: "PlantsId");

            migrationBuilder.CreateIndex(
                name: "IX_EconomicValuePlant_PlantsId",
                table: "EconomicValuePlant",
                column: "PlantsId");

            migrationBuilder.CreateIndex(
                name: "IX_LifeFormRaunkiaerPlant_PlantsId",
                table: "LifeFormRaunkiaerPlant",
                column: "PlantsId");

            migrationBuilder.AddForeignKey(
                name: "FK_Plant_ArealType_ArealTypeId",
                table: "Plant",
                column: "ArealTypeId",
                principalTable: "ArealType",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Plant_HydrotypeGroup_HydrotypeGroupId",
                table: "Plant",
                column: "HydrotypeGroupId",
                principalTable: "HydrotypeGroup",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Plant_IntroductionTimeGroup_IntroductionTimeGroupId",
                table: "Plant",
                column: "IntroductionTimeGroupId",
                principalTable: "IntroductionTimeGroup",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Plant_IntroductionWay_IntroductionWayId",
                table: "Plant",
                column: "IntroductionWayId",
                principalTable: "IntroductionWay",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Plant_LifeFormSerebryakovGolubev_LifeFormSerebryakovGolubev~",
                table: "Plant",
                column: "LifeFormSerebryakovGolubevId",
                principalTable: "LifeFormSerebryakovGolubev",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Plant_NaturalizationDegree_NaturalizationDegreeId",
                table: "Plant",
                column: "NaturalizationDegreeId",
                principalTable: "NaturalizationDegree",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Plant_OccurrenceSpecies_OccurrenceSpeciesId",
                table: "Plant",
                column: "OccurrenceSpeciesId",
                principalTable: "OccurrenceSpecies",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Plant_Oopt_OoptId",
                table: "Plant",
                column: "OoptId",
                principalTable: "Oopt",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Plant_OriginSpecies_OriginSpeciesId",
                table: "Plant",
                column: "OriginSpeciesId",
                principalTable: "OriginSpecies",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Plant_PlantFamily_PlantFamilyId",
                table: "Plant",
                column: "PlantFamilyId",
                principalTable: "PlantFamily",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Plant_River_RiverId",
                table: "Plant",
                column: "RiverId",
                principalTable: "River",
                principalColumn: "Id");
        }
    }
}
