﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Ecology.Flora.DAL.PostgreSQL.Migrations
{
    public partial class Init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ArealType",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "jsonb", nullable: true),
                    Created = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Updated = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Deleted = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ArealType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "EcologicalPhytocenoticGroup",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "jsonb", nullable: true),
                    Created = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Updated = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Deleted = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EcologicalPhytocenoticGroup", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "EconomicValue",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "jsonb", nullable: true),
                    Created = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Updated = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Deleted = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EconomicValue", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "HydrotypeGroup",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "jsonb", nullable: true),
                    Created = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Updated = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Deleted = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HydrotypeGroup", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "IntroductionTimeGroup",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "jsonb", nullable: true),
                    Created = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Updated = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Deleted = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IntroductionTimeGroup", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "IntroductionWay",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "jsonb", nullable: true),
                    Created = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Updated = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Deleted = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IntroductionWay", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "LifeFormRaunkiaer",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "jsonb", nullable: true),
                    Created = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Updated = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Deleted = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LifeFormRaunkiaer", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "LifeFormSerebryakovGolubev",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "jsonb", nullable: true),
                    Created = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Updated = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Deleted = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LifeFormSerebryakovGolubev", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Metric",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "jsonb", nullable: true),
                    Created = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Updated = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Deleted = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Metric", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "NaturalizationDegree",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "jsonb", nullable: true),
                    Created = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Updated = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Deleted = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NaturalizationDegree", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "OccurrenceSpecies",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "jsonb", nullable: true),
                    Created = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Updated = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Deleted = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OccurrenceSpecies", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Oopt",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Square = table.Column<int>(type: "integer", nullable: false),
                    Metric8 = table.Column<int>(type: "integer", nullable: false),
                    Name = table.Column<string>(type: "jsonb", nullable: true),
                    Created = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Updated = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Deleted = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Oopt", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "OriginSpecies",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "jsonb", nullable: true),
                    Created = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Updated = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Deleted = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OriginSpecies", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PlantClade",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "jsonb", nullable: true),
                    Created = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Updated = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Deleted = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlantClade", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "RarityCategory",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Code = table.Column<string>(type: "text", nullable: true),
                    Name = table.Column<string>(type: "jsonb", nullable: true),
                    Created = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Updated = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Deleted = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RarityCategory", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "RedBook",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "jsonb", nullable: true),
                    Created = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Updated = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Deleted = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RedBook", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "River",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "jsonb", nullable: true),
                    Created = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Updated = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Deleted = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_River", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Role",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "jsonb", nullable: true),
                    Created = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Updated = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Deleted = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Role", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "User",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Email = table.Column<string>(type: "text", nullable: true),
                    Password = table.Column<string>(type: "text", nullable: true),
                    Username = table.Column<string>(type: "text", nullable: true),
                    RefreshToken = table.Column<string>(type: "text", nullable: true),
                    Name = table.Column<string>(type: "jsonb", nullable: true),
                    Created = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Updated = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Deleted = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "WorkTask",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Namespace = table.Column<string>(type: "text", nullable: true),
                    Status = table.Column<int>(type: "integer", nullable: false),
                    Message = table.Column<string>(type: "text", nullable: true),
                    Name = table.Column<string>(type: "jsonb", nullable: true),
                    Created = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Updated = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Deleted = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkTask", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MetricRaspred",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Bal = table.Column<int>(type: "integer", nullable: false),
                    Value = table.Column<int>(type: "integer", nullable: false),
                    MetricId = table.Column<Guid>(type: "uuid", nullable: true),
                    Name = table.Column<string>(type: "jsonb", nullable: true),
                    Created = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Updated = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Deleted = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MetricRaspred", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MetricRaspred_Metric_MetricId",
                        column: x => x.MetricId,
                        principalTable: "Metric",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "PlantClass",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    PlantCladeId = table.Column<Guid>(type: "uuid", nullable: true),
                    Name = table.Column<string>(type: "jsonb", nullable: true),
                    Created = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Updated = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Deleted = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlantClass", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PlantClass_PlantClade_PlantCladeId",
                        column: x => x.PlantCladeId,
                        principalTable: "PlantClade",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "RoleUser",
                columns: table => new
                {
                    RolesId = table.Column<Guid>(type: "uuid", nullable: false),
                    UsersId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RoleUser", x => new { x.RolesId, x.UsersId });
                    table.ForeignKey(
                        name: "FK_RoleUser_Role_RolesId",
                        column: x => x.RolesId,
                        principalTable: "Role",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_RoleUser_User_UsersId",
                        column: x => x.UsersId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PlantFamily",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    PlantClassId = table.Column<Guid>(type: "uuid", nullable: true),
                    Name = table.Column<string>(type: "jsonb", nullable: true),
                    Created = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Updated = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Deleted = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlantFamily", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PlantFamily_PlantClass_PlantClassId",
                        column: x => x.PlantClassId,
                        principalTable: "PlantClass",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Plant",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    EcologicalPhytocenoticGroupComment = table.Column<string>(type: "jsonb", nullable: true),
                    RedBookComment = table.Column<string>(type: "jsonb", nullable: true),
                    AvatarData = table.Column<string>(type: "text", nullable: true),
                    Position = table.Column<string>(type: "jsonb", nullable: true),
                    HydrotypeGroupId = table.Column<Guid>(type: "uuid", nullable: true),
                    LifeFormSerebryakovGolubevId = table.Column<Guid>(type: "uuid", nullable: true),
                    NaturalizationDegreeId = table.Column<Guid>(type: "uuid", nullable: true),
                    IntroductionTimeGroupId = table.Column<Guid>(type: "uuid", nullable: true),
                    IntroductionWayId = table.Column<Guid>(type: "uuid", nullable: true),
                    OccurrenceSpeciesId = table.Column<Guid>(type: "uuid", nullable: true),
                    RiverId = table.Column<Guid>(type: "uuid", nullable: true),
                    PlantFamilyId = table.Column<Guid>(type: "uuid", nullable: true),
                    ArealTypeId = table.Column<Guid>(type: "uuid", nullable: true),
                    OriginSpeciesId = table.Column<Guid>(type: "uuid", nullable: true),
                    OoptId = table.Column<Guid>(type: "uuid", nullable: true),
                    Name = table.Column<string>(type: "jsonb", nullable: true),
                    Created = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Updated = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Deleted = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Plant", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Plant_ArealType_ArealTypeId",
                        column: x => x.ArealTypeId,
                        principalTable: "ArealType",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Plant_HydrotypeGroup_HydrotypeGroupId",
                        column: x => x.HydrotypeGroupId,
                        principalTable: "HydrotypeGroup",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Plant_IntroductionTimeGroup_IntroductionTimeGroupId",
                        column: x => x.IntroductionTimeGroupId,
                        principalTable: "IntroductionTimeGroup",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Plant_IntroductionWay_IntroductionWayId",
                        column: x => x.IntroductionWayId,
                        principalTable: "IntroductionWay",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Plant_LifeFormSerebryakovGolubev_LifeFormSerebryakovGolubev~",
                        column: x => x.LifeFormSerebryakovGolubevId,
                        principalTable: "LifeFormSerebryakovGolubev",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Plant_NaturalizationDegree_NaturalizationDegreeId",
                        column: x => x.NaturalizationDegreeId,
                        principalTable: "NaturalizationDegree",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Plant_OccurrenceSpecies_OccurrenceSpeciesId",
                        column: x => x.OccurrenceSpeciesId,
                        principalTable: "OccurrenceSpecies",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Plant_Oopt_OoptId",
                        column: x => x.OoptId,
                        principalTable: "Oopt",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Plant_OriginSpecies_OriginSpeciesId",
                        column: x => x.OriginSpeciesId,
                        principalTable: "OriginSpecies",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Plant_PlantFamily_PlantFamilyId",
                        column: x => x.PlantFamilyId,
                        principalTable: "PlantFamily",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Plant_River_RiverId",
                        column: x => x.RiverId,
                        principalTable: "River",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "EcologicalPhytocenoticGroupPlant",
                columns: table => new
                {
                    EcologicalPhytocenoticGroupsId = table.Column<Guid>(type: "uuid", nullable: false),
                    PlantsId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EcologicalPhytocenoticGroupPlant", x => new { x.EcologicalPhytocenoticGroupsId, x.PlantsId });
                    table.ForeignKey(
                        name: "FK_EcologicalPhytocenoticGroupPlant_EcologicalPhytocenoticGrou~",
                        column: x => x.EcologicalPhytocenoticGroupsId,
                        principalTable: "EcologicalPhytocenoticGroup",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EcologicalPhytocenoticGroupPlant_Plant_PlantsId",
                        column: x => x.PlantsId,
                        principalTable: "Plant",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "EconomicValuePlant",
                columns: table => new
                {
                    EconomicValuesId = table.Column<Guid>(type: "uuid", nullable: false),
                    PlantsId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EconomicValuePlant", x => new { x.EconomicValuesId, x.PlantsId });
                    table.ForeignKey(
                        name: "FK_EconomicValuePlant_EconomicValue_EconomicValuesId",
                        column: x => x.EconomicValuesId,
                        principalTable: "EconomicValue",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EconomicValuePlant_Plant_PlantsId",
                        column: x => x.PlantsId,
                        principalTable: "Plant",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "LifeFormRaunkiaerPlant",
                columns: table => new
                {
                    LifeFormRaunkiaersId = table.Column<Guid>(type: "uuid", nullable: false),
                    PlantsId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LifeFormRaunkiaerPlant", x => new { x.LifeFormRaunkiaersId, x.PlantsId });
                    table.ForeignKey(
                        name: "FK_LifeFormRaunkiaerPlant_LifeFormRaunkiaer_LifeFormRaunkiaers~",
                        column: x => x.LifeFormRaunkiaersId,
                        principalTable: "LifeFormRaunkiaer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_LifeFormRaunkiaerPlant_Plant_PlantsId",
                        column: x => x.PlantsId,
                        principalTable: "Plant",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PlantUser",
                columns: table => new
                {
                    AuthorsId = table.Column<Guid>(type: "uuid", nullable: false),
                    PlantsId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlantUser", x => new { x.AuthorsId, x.PlantsId });
                    table.ForeignKey(
                        name: "FK_PlantUser_Plant_PlantsId",
                        column: x => x.PlantsId,
                        principalTable: "Plant",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PlantUser_User_AuthorsId",
                        column: x => x.AuthorsId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "RedBookPlant",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    RarityCategoryId = table.Column<Guid>(type: "uuid", nullable: false),
                    RedBookId = table.Column<Guid>(type: "uuid", nullable: false),
                    PlantId = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "jsonb", nullable: true),
                    Created = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Updated = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Deleted = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RedBookPlant", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RedBookPlant_Plant_PlantId",
                        column: x => x.PlantId,
                        principalTable: "Plant",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_RedBookPlant_RarityCategory_RarityCategoryId",
                        column: x => x.RarityCategoryId,
                        principalTable: "RarityCategory",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_RedBookPlant_RedBook_RedBookId",
                        column: x => x.RedBookId,
                        principalTable: "RedBook",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Metric",
                columns: new[] { "Id", "Created", "Deleted", "Name", "Updated" },
                values: new object[,]
                {
                    { new Guid("0bc1a1af-ce4c-4403-b4ac-13e0984275c1"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, "{\"Ru\":\"Площадь памятника природы\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { new Guid("3ec5be41-785f-41f1-8dda-a66214d2d999"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, "{\"Ru\":\"Общая численность видового разнообразия\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { new Guid("7665a6d9-319a-4d29-8f93-bb9aded0993e"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, "{\"Ru\":\"Число видов, занесенных в Красную книгу Российской Федерации и Самарской области\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { new Guid("81445e34-2819-4664-9fc7-3dde860a1feb"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, "{\"Ru\":\"Восстановительный потенциал\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { new Guid("a516c974-6828-4f64-a90a-f3f912c8b683"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, "{\"Ru\":\"Антропотолерантность растительного покрова\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { new Guid("aced897e-1284-444a-a618-86d8f2b1b838"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, "{\"Ru\":\"Cтепень изученности растительного покрова\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { new Guid("e67c62df-fe6f-420a-92a4-cbe1bbd08afa"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, "{\"Ru\":\"Ценотнческое разнообрази\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { new Guid("f4075d07-9baa-4c06-a475-e518b850b4f2"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, "{\"Ru\":\"Степень трансформпрованности\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { new Guid("f4a86d36-d4a5-4eec-b6e5-22c8cda0fe4a"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, "{\"Ru\":\"Демонстрационное (эталонное) значение\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) }
                });

            migrationBuilder.InsertData(
                table: "RarityCategory",
                columns: new[] { "Id", "Code", "Created", "Deleted", "Name", "Updated" },
                values: new object[,]
                {
                    { new Guid("41c67e4a-8a65-42ef-9c6c-3ec197b6a3e2"), "E", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, "{\"Ru\":\"Виды, находящиеся под угрозой исчезновения\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { new Guid("6c9a8d99-3460-4fdf-800c-cc2ea147c9cc"), "V", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, "{\"Ru\":\"Уязвимые виды, сокращающиеся в численности\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { new Guid("8a111549-6e7b-46cf-97f1-63189880db30"), "I", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, "{\"Ru\":\"Виды с неопределенным статусом\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { new Guid("cefdbf7a-2ae5-4166-bec4-ce65d742283b"), "Ex", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, "{\"Ru\":\"По-видимому, исчезнувшие на территории бассейна реки Свияги\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { new Guid("ebedc54f-5392-4a0e-ba40-5e7511d323e8"), "R", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, "{\"Ru\":\"Редкие виды\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) }
                });

            migrationBuilder.InsertData(
                table: "Role",
                columns: new[] { "Id", "Created", "Deleted", "Name", "Updated" },
                values: new object[,]
                {
                    { new Guid("13f58593-d0d5-43d0-a2e6-404af9c49af1"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, "{\"Ru\":\"ADMIN\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { new Guid("f6c16d1d-98b0-430d-a4bc-b31b37df5ac1"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, "{\"Ru\":\"USER\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) }
                });

            migrationBuilder.InsertData(
                table: "User",
                columns: new[] { "Id", "Created", "Deleted", "Email", "Name", "Password", "RefreshToken", "Updated", "Username" },
                values: new object[] { new Guid("aee1cd0d-7109-475b-913c-84574d7b1ca7"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, "admin@admin.ru", "{\"Ru\":\"Adminov Admin Adminovich\"}", "51dc47aa1066e41966a8f11573b4de7840b1f849c3062315beca024ceceeca00", null, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Admin" });

            migrationBuilder.InsertData(
                table: "MetricRaspred",
                columns: new[] { "Id", "Bal", "Created", "Deleted", "MetricId", "Name", "Updated", "Value" },
                values: new object[,]
                {
                    { new Guid("0bc7fd4d-d965-4572-a6d1-6cea1aa3aa23"), 12, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, new Guid("a516c974-6828-4f64-a90a-f3f912c8b683"), "{\"Ru\":\"слабая\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 0 },
                    { new Guid("0e3cb4ee-1ad8-4ae6-b86b-4ae65c413147"), 0, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, new Guid("3ec5be41-785f-41f1-8dda-a66214d2d999"), "{\"Ru\":\"до 50 видов\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), -1 },
                    { new Guid("14b5a520-082a-429b-a617-6f40d58cdfd4"), 12, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, new Guid("0bc1a1af-ce4c-4403-b4ac-13e0984275c1"), "{\"Ru\":\"больше 300 га\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 300 },
                    { new Guid("1a9ff714-37ae-424a-9a74-327ba8ab706e"), 6, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, new Guid("0bc1a1af-ce4c-4403-b4ac-13e0984275c1"), "{\"Ru\":\"до 100 га\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 10 },
                    { new Guid("1c582251-c662-4115-a900-ea444d25d6ac"), 6, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, new Guid("3ec5be41-785f-41f1-8dda-a66214d2d999"), "{\"Ru\":\"до 100 видов\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 50 },
                    { new Guid("22286c99-68cb-4fbe-92a4-f682fb64987f"), 3, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, new Guid("aced897e-1284-444a-a618-86d8f2b1b838"), "{\"Ru\":\"средне изучен\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 4 },
                    { new Guid("25f789c0-3971-4be3-b0f1-4e0742f415c5"), 0, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, new Guid("e67c62df-fe6f-420a-92a4-cbe1bbd08afa"), "{\"Ru\":\"один тип растительности\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), -1 },
                    { new Guid("288bf0f4-a085-48dc-9350-bafe6f81f610"), 5, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, new Guid("e67c62df-fe6f-420a-92a4-cbe1bbd08afa"), "{\"Ru\":\"два типа растительности\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1 },
                    { new Guid("2cea744f-a7a8-4b4c-b47e-2463b60cbb0f"), 14, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, new Guid("7665a6d9-319a-4d29-8f93-bb9aded0993e"), "{\"Ru\":\"6-10 видов\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 5 },
                    { new Guid("2ff80421-e8b2-4cb0-a595-92a0a994fe82"), 7, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, new Guid("7665a6d9-319a-4d29-8f93-bb9aded0993e"), "{\"Ru\":\"от 1 до 5 видов\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 0 },
                    { new Guid("31d36bf2-19fa-46cf-a01d-2b9762732bd8"), 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, new Guid("f4075d07-9baa-4c06-a475-e518b850b4f2"), "{\"Ru\":\"условно коренной\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 60 },
                    { new Guid("33d71236-bb48-4e1a-b93b-721bba344152"), 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, new Guid("3ec5be41-785f-41f1-8dda-a66214d2d999"), "{\"Ru\":\"более 300 видов\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 300 },
                    { new Guid("37a5b524-a401-4cc5-9e5b-1d1bd973e5ae"), 0, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, new Guid("f4a86d36-d4a5-4eec-b6e5-22c8cda0fe4a"), "{\"Ru\":\"не имеет\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), -1 },
                    { new Guid("41d32b10-f9af-4d49-bee5-284108b6e3b7"), 4, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, new Guid("aced897e-1284-444a-a618-86d8f2b1b838"), "{\"Ru\":\"хорошо изучен\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 10 },
                    { new Guid("65cf14cf-70ba-4e89-912c-57a6722291f8"), 1, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, new Guid("aced897e-1284-444a-a618-86d8f2b1b838"), "{\"Ru\":\"очень слабо изучен\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 0 },
                    { new Guid("6bab36cd-3f9b-476b-85a9-bb6f6375c036"), 0, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, new Guid("a516c974-6828-4f64-a90a-f3f912c8b683"), "{\"Ru\":\"очень высокая\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 0 },
                    { new Guid("74cf4c2f-7885-4486-a0f4-56019ad88b47"), 27, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, new Guid("81445e34-2819-4664-9fc7-3dde860a1feb"), "{\"Ru\":\"слабый\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 150 },
                    { new Guid("77127f02-9072-4a8d-8e9a-3028d2c26f16"), 16, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, new Guid("a516c974-6828-4f64-a90a-f3f912c8b683"), "{\"Ru\":\"очень слабая\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 0 },
                    { new Guid("782184ab-93cb-4fe7-be6f-cf40f88ef128"), 2, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, new Guid("aced897e-1284-444a-a618-86d8f2b1b838"), "{\"Ru\":\"слабо изучен\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 2 },
                    { new Guid("7d47deef-8497-4a6b-bdd1-8d695593fcd1"), 15, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, new Guid("e67c62df-fe6f-420a-92a4-cbe1bbd08afa"), "{\"Ru\":\"четыре типа растительности\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 3 },
                    { new Guid("801c4000-81f7-4c81-8a54-1ffafebb050e"), 12, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, new Guid("3ec5be41-785f-41f1-8dda-a66214d2d999"), "{\"Ru\":\"до 150 видов\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 100 },
                    { new Guid("855fe008-c723-4a76-84f5-f017e4b04626"), 0, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, new Guid("7665a6d9-319a-4d29-8f93-bb9aded0993e"), "{\"Ru\":\"не занесено\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), -1 },
                    { new Guid("97c9693c-a91e-4d20-a896-1670c2d69f7f"), 20, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, new Guid("e67c62df-fe6f-420a-92a4-cbe1bbd08afa"), "{\"Ru\":\"более четырех типов растительности\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 4 },
                    { new Guid("9961a32a-97ec-4d96-8623-cb666dffde96"), 32, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, new Guid("f4075d07-9baa-4c06-a475-e518b850b4f2"), "{\"Ru\":\"коренной\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 80 },
                    { new Guid("a52d6397-3e1b-4c35-8f12-78c10408a9db"), 16, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, new Guid("f4075d07-9baa-4c06-a475-e518b850b4f2"), "{\"Ru\":\"слабо трансформирован\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 40 },
                    { new Guid("a5c58ab6-e56b-4f39-bd9d-9edd899527ff"), 6, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, new Guid("f4a86d36-d4a5-4eec-b6e5-22c8cda0fe4a"), "{\"Ru\":\"большое\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 50 },
                    { new Guid("a84c2fe6-dee5-4dc3-9423-e3221f19401a"), 0, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, new Guid("0bc1a1af-ce4c-4403-b4ac-13e0984275c1"), "{\"Ru\":\"больше 1 га\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), -1 },
                    { new Guid("ba0b4792-6406-4384-8cc2-b175d68d537a"), 2, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, new Guid("f4a86d36-d4a5-4eec-b6e5-22c8cda0fe4a"), "{\"Ru\":\"незначительное\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 20 },
                    { new Guid("baf4167a-9f6d-49e4-aa96-b7808484ba6f"), 0, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, new Guid("81445e34-2819-4664-9fc7-3dde860a1feb"), "{\"Ru\":\"очень хороший\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), -1 },
                    { new Guid("bb475d8c-b4fa-4e68-bf31-b822815df72c"), 4, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, new Guid("f4a86d36-d4a5-4eec-b6e5-22c8cda0fe4a"), "{\"Ru\":\"среднее\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 20 },
                    { new Guid("bcc16160-9931-486c-baaf-2a8e93aed5ea"), 0, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, new Guid("aced897e-1284-444a-a618-86d8f2b1b838"), "{\"Ru\":\"не изучен\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), -1 },
                    { new Guid("bcedcbe1-dfe3-42bc-8143-15fede359ae0"), 9, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, new Guid("0bc1a1af-ce4c-4403-b4ac-13e0984275c1"), "{\"Ru\":\"до 300 га\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 100 },
                    { new Guid("c386e693-de2a-4401-8175-08f216794fa8"), 18, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, new Guid("3ec5be41-785f-41f1-8dda-a66214d2d999"), "{\"Ru\":\"до 300 видов\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 150 },
                    { new Guid("c443abfe-df93-4e9b-bb95-b31bc33a8efa"), 10, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, new Guid("e67c62df-fe6f-420a-92a4-cbe1bbd08afa"), "{\"Ru\":\"три типа растительности\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 2 },
                    { new Guid("ccec6d5d-67ad-4325-a494-f079fe00fcf1"), 8, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, new Guid("a516c974-6828-4f64-a90a-f3f912c8b683"), "{\"Ru\":\"средняя\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 0 },
                    { new Guid("de10375f-b293-4493-a6d1-04528009ca98"), 4, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, new Guid("a516c974-6828-4f64-a90a-f3f912c8b683"), "{\"Ru\":\"высокая\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 0 },
                    { new Guid("e9302e68-b55b-41e4-a812-a8b1575c1869"), 28, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, new Guid("7665a6d9-319a-4d29-8f93-bb9aded0993e"), "{\"Ru\":\"более 21 вида\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 20 },
                    { new Guid("ece4881e-d0f7-4d05-adf5-614e9accf30b"), 8, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, new Guid("f4075d07-9baa-4c06-a475-e518b850b4f2"), "{\"Ru\":\"сильно трансформирован\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 20 },
                    { new Guid("eed727ba-f364-4934-b2bc-dabfcd72d5fa"), 3, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, new Guid("0bc1a1af-ce4c-4403-b4ac-13e0984275c1"), "{\"Ru\":\"до 10 га\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1 },
                    { new Guid("f2595e32-666f-493d-bdfc-d7254bc1471d"), 8, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, new Guid("f4a86d36-d4a5-4eec-b6e5-22c8cda0fe4a"), "{\"Ru\":\"очень большое\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 90 },
                    { new Guid("f7263813-d795-4ed0-a4c0-295f35d8f934"), 0, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, new Guid("f4075d07-9baa-4c06-a475-e518b850b4f2"), "{\"Ru\":\"полностью трансформирован\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), -1 },
                    { new Guid("f9184881-cc8c-434f-b40f-426f2abc4182"), 21, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, new Guid("7665a6d9-319a-4d29-8f93-bb9aded0993e"), "{\"Ru\":\"11-20 видов\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 10 },
                    { new Guid("fa8a9d98-51e7-4c8c-b53d-1344995fa282"), 9, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, new Guid("81445e34-2819-4664-9fc7-3dde860a1feb"), "{\"Ru\":\"хороший\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 50 },
                    { new Guid("fc4bdc25-6953-4b9a-98f0-a896cc10e48f"), 18, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, new Guid("81445e34-2819-4664-9fc7-3dde860a1feb"), "{\"Ru\":\"удовлетворительный\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 100 },
                    { new Guid("fd072c8d-9c48-4fbc-b73b-8bd503f3d09c"), 36, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, new Guid("81445e34-2819-4664-9fc7-3dde860a1feb"), "{\"Ru\":\"очень слабый\"}", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 200 }
                });

            migrationBuilder.InsertData(
                table: "RoleUser",
                columns: new[] { "RolesId", "UsersId" },
                values: new object[,]
                {
                    { new Guid("13f58593-d0d5-43d0-a2e6-404af9c49af1"), new Guid("aee1cd0d-7109-475b-913c-84574d7b1ca7") },
                    { new Guid("f6c16d1d-98b0-430d-a4bc-b31b37df5ac1"), new Guid("aee1cd0d-7109-475b-913c-84574d7b1ca7") }
                });

            migrationBuilder.CreateIndex(
                name: "IX_EcologicalPhytocenoticGroupPlant_PlantsId",
                table: "EcologicalPhytocenoticGroupPlant",
                column: "PlantsId");

            migrationBuilder.CreateIndex(
                name: "IX_EconomicValuePlant_PlantsId",
                table: "EconomicValuePlant",
                column: "PlantsId");

            migrationBuilder.CreateIndex(
                name: "IX_LifeFormRaunkiaerPlant_PlantsId",
                table: "LifeFormRaunkiaerPlant",
                column: "PlantsId");

            migrationBuilder.CreateIndex(
                name: "IX_MetricRaspred_MetricId",
                table: "MetricRaspred",
                column: "MetricId");

            migrationBuilder.CreateIndex(
                name: "IX_Plant_ArealTypeId",
                table: "Plant",
                column: "ArealTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Plant_HydrotypeGroupId",
                table: "Plant",
                column: "HydrotypeGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_Plant_IntroductionTimeGroupId",
                table: "Plant",
                column: "IntroductionTimeGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_Plant_IntroductionWayId",
                table: "Plant",
                column: "IntroductionWayId");

            migrationBuilder.CreateIndex(
                name: "IX_Plant_LifeFormSerebryakovGolubevId",
                table: "Plant",
                column: "LifeFormSerebryakovGolubevId");

            migrationBuilder.CreateIndex(
                name: "IX_Plant_NaturalizationDegreeId",
                table: "Plant",
                column: "NaturalizationDegreeId");

            migrationBuilder.CreateIndex(
                name: "IX_Plant_OccurrenceSpeciesId",
                table: "Plant",
                column: "OccurrenceSpeciesId");

            migrationBuilder.CreateIndex(
                name: "IX_Plant_OoptId",
                table: "Plant",
                column: "OoptId");

            migrationBuilder.CreateIndex(
                name: "IX_Plant_OriginSpeciesId",
                table: "Plant",
                column: "OriginSpeciesId");

            migrationBuilder.CreateIndex(
                name: "IX_Plant_PlantFamilyId",
                table: "Plant",
                column: "PlantFamilyId");

            migrationBuilder.CreateIndex(
                name: "IX_Plant_RiverId",
                table: "Plant",
                column: "RiverId");

            migrationBuilder.CreateIndex(
                name: "IX_PlantClass_PlantCladeId",
                table: "PlantClass",
                column: "PlantCladeId");

            migrationBuilder.CreateIndex(
                name: "IX_PlantFamily_PlantClassId",
                table: "PlantFamily",
                column: "PlantClassId");

            migrationBuilder.CreateIndex(
                name: "IX_PlantUser_PlantsId",
                table: "PlantUser",
                column: "PlantsId");

            migrationBuilder.CreateIndex(
                name: "IX_RedBookPlant_PlantId",
                table: "RedBookPlant",
                column: "PlantId");

            migrationBuilder.CreateIndex(
                name: "IX_RedBookPlant_RarityCategoryId",
                table: "RedBookPlant",
                column: "RarityCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_RedBookPlant_RedBookId",
                table: "RedBookPlant",
                column: "RedBookId");

            migrationBuilder.CreateIndex(
                name: "IX_RoleUser_UsersId",
                table: "RoleUser",
                column: "UsersId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EcologicalPhytocenoticGroupPlant");

            migrationBuilder.DropTable(
                name: "EconomicValuePlant");

            migrationBuilder.DropTable(
                name: "LifeFormRaunkiaerPlant");

            migrationBuilder.DropTable(
                name: "MetricRaspred");

            migrationBuilder.DropTable(
                name: "PlantUser");

            migrationBuilder.DropTable(
                name: "RedBookPlant");

            migrationBuilder.DropTable(
                name: "RoleUser");

            migrationBuilder.DropTable(
                name: "WorkTask");

            migrationBuilder.DropTable(
                name: "EcologicalPhytocenoticGroup");

            migrationBuilder.DropTable(
                name: "EconomicValue");

            migrationBuilder.DropTable(
                name: "LifeFormRaunkiaer");

            migrationBuilder.DropTable(
                name: "Metric");

            migrationBuilder.DropTable(
                name: "Plant");

            migrationBuilder.DropTable(
                name: "RarityCategory");

            migrationBuilder.DropTable(
                name: "RedBook");

            migrationBuilder.DropTable(
                name: "Role");

            migrationBuilder.DropTable(
                name: "User");

            migrationBuilder.DropTable(
                name: "ArealType");

            migrationBuilder.DropTable(
                name: "HydrotypeGroup");

            migrationBuilder.DropTable(
                name: "IntroductionTimeGroup");

            migrationBuilder.DropTable(
                name: "IntroductionWay");

            migrationBuilder.DropTable(
                name: "LifeFormSerebryakovGolubev");

            migrationBuilder.DropTable(
                name: "NaturalizationDegree");

            migrationBuilder.DropTable(
                name: "OccurrenceSpecies");

            migrationBuilder.DropTable(
                name: "Oopt");

            migrationBuilder.DropTable(
                name: "OriginSpecies");

            migrationBuilder.DropTable(
                name: "PlantFamily");

            migrationBuilder.DropTable(
                name: "River");

            migrationBuilder.DropTable(
                name: "PlantClass");

            migrationBuilder.DropTable(
                name: "PlantClade");
        }
    }
}
