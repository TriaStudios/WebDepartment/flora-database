﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Ecology.Flora.DAL.PostgreSQL.Migrations
{
    public partial class ImageStorageFix : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Plant_Image_AvatarId",
                table: "Plant");

            migrationBuilder.DropIndex(
                name: "IX_Plant_AvatarId",
                table: "Plant");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Plant_AvatarId",
                table: "Plant",
                column: "AvatarId");

            migrationBuilder.AddForeignKey(
                name: "FK_Plant_Image_AvatarId",
                table: "Plant",
                column: "AvatarId",
                principalTable: "Image",
                principalColumn: "Id");
        }
    }
}
