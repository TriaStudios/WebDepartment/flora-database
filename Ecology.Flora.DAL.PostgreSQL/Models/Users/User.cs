using System.Collections.Generic;
using Ecology.Flora.DAL.PostgreSQL.Models.Plants;

namespace Ecology.Flora.DAL.PostgreSQL.Models.Users;

public sealed class User : EntityBase
{
    public User()
    {
        Roles = new HashSet<Role>();
        Plants = new HashSet<Plant>();
    }

    public string Email { get; set; }
    public string Password { get; set; }
    public string Username { get; set; }
    public string RefreshToken { get; set; }
    public ICollection<Role> Roles { get; set; }
    public ICollection<Plant> Plants { get; set; }
}
