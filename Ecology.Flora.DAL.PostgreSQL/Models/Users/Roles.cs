﻿namespace Ecology.Flora.DAL.PostgreSQL.Models.Users;

public static class Roles
{
    public const string Admin = "ADMIN";
    public const string User = "USER";
}
