using System.Collections.Generic;

namespace Ecology.Flora.DAL.PostgreSQL.Models.Users;

public sealed class Role : EntityBase
{
    public Role()
    {
        Users = new HashSet<User>();
    }

    public ICollection<User> Users { get; set; }
}