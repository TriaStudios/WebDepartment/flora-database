﻿using System.Collections.Generic;

namespace Ecology.Flora.DAL.PostgreSQL.Models.Metrics;

public class Metric : EntityBase
{
    public virtual ICollection<MetricRaspred> MetricRaspreds { get; set; }
}
