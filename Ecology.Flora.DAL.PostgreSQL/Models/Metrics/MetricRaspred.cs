﻿namespace Ecology.Flora.DAL.PostgreSQL.Models.Metrics;

public sealed class MetricRaspred : EntityBase
{
    public int Bal { get; set; }
    public int Value { get; set; }
    public Guid? MetricId { get; set; }
    public Metric Metric { get; set; }
}