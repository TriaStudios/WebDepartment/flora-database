using Ecology.Flora.DAL.Logic.Enums;

namespace Ecology.Flora.DAL.PostgreSQL.Models.Workers;

public sealed class WorkTask : EntityBase
{
    public string Namespace { get; set; }
    public StatusWorker Status { get; set; }
    public string Message { get; set; }
}