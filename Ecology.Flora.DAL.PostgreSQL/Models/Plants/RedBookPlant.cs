﻿namespace Ecology.Flora.DAL.PostgreSQL.Models.Plants;

public class RedBookPlant : EntityBase
{
    public Guid RarityCategoryId { get; set; }
    public Guid RedBookId { get; set; }
    public Guid PlantId { get; set; }

    public virtual RarityCategory RarityCategory { get; set; }
    public virtual RedBook RedBook { get; set; }
    public virtual Plant Plant { get; set; }
}