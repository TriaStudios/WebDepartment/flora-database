﻿using System.Collections.Generic;

namespace Ecology.Flora.DAL.PostgreSQL.Models.Plants;

public sealed class PlantClade : EntityBase
{
    public PlantClade()
    {
        PlantClasses = new HashSet<PlantClass>();
    }

    public ICollection<PlantClass> PlantClasses { get; set; }
}