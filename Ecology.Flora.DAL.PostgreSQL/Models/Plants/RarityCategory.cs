namespace Ecology.Flora.DAL.PostgreSQL.Models.Plants;

public class RarityCategory : EntityBase
{
    public string Code { get; set; }
}