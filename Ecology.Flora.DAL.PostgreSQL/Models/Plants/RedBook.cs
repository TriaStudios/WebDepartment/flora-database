using System.Collections.Generic;

namespace Ecology.Flora.DAL.PostgreSQL.Models.Plants;

public sealed class RedBook : EntityBase
{
    public RedBook()
    {
        RedBookPlants = new HashSet<RedBookPlant>();
    }

    public ICollection<RedBookPlant> RedBookPlants { get; set; }
}