﻿using System.Collections.Generic;

namespace Ecology.Flora.DAL.PostgreSQL.Models.Plants;

public sealed class PlantClass : EntityBase
{
    public PlantClass()
    {
        PlantFamilies = new HashSet<PlantFamily>();
    }

    public Guid? PlantCladeId { get; set; }

    public PlantClade PlantClade { get; set; }
    public ICollection<PlantFamily> PlantFamilies { get; set; }
}