﻿using System.Collections.Generic;

namespace Ecology.Flora.DAL.PostgreSQL.Models.Plants;

public class Oopt : EntityBase
{
    public int Square { get; set; }
    public int Metric8 { get; set; }

    public virtual ICollection<Plant> Plants { get; set; }
}