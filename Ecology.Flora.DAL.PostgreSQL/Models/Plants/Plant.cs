using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Ecology.Flora.DAL.PostgreSQL.Models.Users;

namespace Ecology.Flora.DAL.PostgreSQL.Models.Plants;

public sealed class Plant : EntityBase
{
    public Plant()
    {
        Oopts = new HashSet<Oopt>();
        PlantCharacteristicValues = new HashSet<PlantCharacteristicValue>();
        RedBookPlants = new HashSet<RedBookPlant>();
        Authors = new HashSet<User>();
    }

    public LocalizeProperty EcologicalPhytocenoticGroupComment { get; set; } = new();
    public LocalizeProperty RedBookComment { get; set; } = new();
    public PositionProperty Position { get; set; } = new();
    public Guid? AvatarId { get; set; }
    public Guid PlantFamilyId { get; set; }
    
    public PlantFamily PlantFamily { get; set; }
    [NotNull] public ICollection<Oopt> Oopts { get; set; }
    [NotNull] public ICollection<PlantCharacteristicValue> PlantCharacteristicValues { get; set; }
    [NotNull] public ICollection<RedBookPlant> RedBookPlants { get; set; }
    [NotNull] public ICollection<User> Authors { get; set; }
}
