﻿using System.Collections.Generic;

namespace Ecology.Flora.DAL.PostgreSQL.Models.Plants;

public sealed class PlantFamily : EntityBase
{
    public PlantFamily()
    {
        Plants = new HashSet<Plant>();
    }

    public Guid? PlantClassId { get; set; }
    public PlantClass PlantClass { get; set; }
    public ICollection<Plant> Plants { get; set; }
}