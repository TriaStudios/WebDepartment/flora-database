﻿using System.Collections.Generic;

namespace Ecology.Flora.DAL.PostgreSQL.Models.Plants;

public sealed class PlantCharacteristic : EntityBase
{
    public PlantCharacteristic()
    {
        PlantCharacteristicValues = new HashSet<PlantCharacteristicValue>();
    }

    public ICollection<PlantCharacteristicValue> PlantCharacteristicValues { get; set; }
}
