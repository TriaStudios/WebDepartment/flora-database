﻿using System.Collections.Generic;

namespace Ecology.Flora.DAL.PostgreSQL.Models.Plants;

public class PlantCharacteristicValue : EntityBase
{
    public Guid PlantCharacteristicId { get; set; }
    
    public virtual PlantCharacteristic PlantCharacteristic { get; set; }
    public virtual ICollection<Plant> Plants { get; set; }
}
