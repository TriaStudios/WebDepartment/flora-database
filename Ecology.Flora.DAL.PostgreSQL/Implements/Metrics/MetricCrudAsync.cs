﻿using AutoMapper;
using Ecology.Flora.DAL.Logic.BindingModels.Metrics;
using Ecology.Flora.DAL.Logic.Crud.Metrics;
using Ecology.Flora.DAL.Logic.ViewModels.Metrics;
using Ecology.Flora.DAL.PostgreSQL.Models.Metrics;

namespace Ecology.Flora.DAL.PostgreSQL.Implements.Metrics;

public class MetricCrudAsync :
    CrudAsyncBase<MetricViewModel, MetricBindingModel, Metric, FilterBase, FloraDatabaseContext>, IMetricCrudAsync
{
    public MetricCrudAsync(IDbContextFactory<FloraDatabaseContext> dbContextFactory, IMapper mapper) : base(
        dbContextFactory, mapper)
    {
    }

    protected override IQueryable<Metric> Includes(IQueryable<Metric> queryable)
    {
        return queryable.Include(x => x.MetricRaspreds);
    }
}
