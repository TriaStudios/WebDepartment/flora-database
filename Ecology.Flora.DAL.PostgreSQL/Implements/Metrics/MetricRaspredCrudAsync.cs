﻿using AutoMapper;
using Ecology.Flora.DAL.Logic.BindingModels.Metrics;
using Ecology.Flora.DAL.Logic.Crud.Metrics;
using Ecology.Flora.DAL.Logic.ViewModels.Metrics;
using Ecology.Flora.DAL.PostgreSQL.Models.Metrics;

namespace Ecology.Flora.DAL.PostgreSQL.Implements.Metrics;

public class MetricRaspredCrudAsync :
    CrudAsyncBase<MetricRaspredViewModel, MetricRaspredBindingModel, MetricRaspred, FloraDatabaseContext>,
    IMetricRaspredCrudAsync
{
    public MetricRaspredCrudAsync(IDbContextFactory<FloraDatabaseContext> context, IMapper mapper) : base(context,
        mapper)
    {
    }

    protected override async Task BindingToDbModel(MetricRaspredBindingModel model, MetricRaspred dbModel,
        DbContext context)
    {
        dbModel.Bal = model.Bal;
        dbModel.Name = model.Name;
        dbModel.Value = model.Value;
        dbModel.MetricId = model.MetricId;
        await Task.CompletedTask;
    }
}
