using AutoMapper;
using Ecology.Flora.DAL.Logic.BindingModels.Plants;
using Ecology.Flora.DAL.Logic.ViewModels.Plants;
using Ecology.Flora.DAL.PostgreSQL.Models.Plants;

namespace Ecology.Flora.DAL.PostgreSQL.Implements.Plants;

public class PlantCladeCrudAsync :
    CrudAsyncBase<PlantCladeViewModel, PlantCladeBindingModel, PlantClade, FloraDatabaseContext>, IPlantCladeCrudAsync
{
    public PlantCladeCrudAsync(IDbContextFactory<FloraDatabaseContext> context, IMapper mapper) : base(context, mapper)
    {
    }
}