using AutoMapper;
using Ecology.Flora.DAL.Logic.BindingModels.Plants;
using Ecology.Flora.DAL.Logic.Filters.Plants;
using Ecology.Flora.DAL.Logic.ViewModels.Plants;
using Ecology.Flora.DAL.PostgreSQL.Models.Plants;

namespace Ecology.Flora.DAL.PostgreSQL.Implements.Plants;

public class PlantCharacteristicValueCrudAsync 
    : CrudAsyncBase<PlantCharacteristicValueViewModel, PlantCharacteristicValueBindingModel,
    PlantCharacteristicValue, PlantCharacteristicValueFilter, FloraDatabaseContext>, 
        IPlantCharacteristicValueCrudAsync
{
    public PlantCharacteristicValueCrudAsync(IDbContextFactory<FloraDatabaseContext> context, IMapper mapper)
        : base(context, mapper)
    {
    }

    protected override Task BindingToDbModel(PlantCharacteristicValueBindingModel model, PlantCharacteristicValue dbModel, DbContext context)
    {
        dbModel.Name = model.Name;
        dbModel.PlantCharacteristicId = model.PlantCharacteristicId;
        return Task.CompletedTask;
    }

    protected override Expression<Func<PlantCharacteristicValue, bool>> ReadPredicate(
        PlantCharacteristicValueFilter filter) =>
        model => filter.PlantCharacteristicIds == null || filter.PlantCharacteristicIds.Count == 0 ||
                 filter.PlantCharacteristicIds.Contains(model.PlantCharacteristicId);
}
