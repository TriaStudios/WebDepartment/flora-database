﻿using System.Collections.Generic;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Ecology.Flora.DAL.Logic.Services.Plants;
using Ecology.Flora.DAL.Logic.ViewModels.Plants;
using Ecology.Flora.DAL.PostgreSQL.Models.Plants;

namespace Ecology.Flora.DAL.PostgreSQL.Implements.Plants;

public class PlantService : IPlantService
{
    private readonly IDbContextFactory<FloraDatabaseContext> _contextFactory;
    private readonly IMapper _mapper;
    
    public PlantService(IDbContextFactory<FloraDatabaseContext> contextFactory, IMapper mapper)
    {
        _contextFactory = contextFactory;
        _mapper = mapper;
    }
    
    public async Task<List<SerivcePlantViewModel>> GetServicePlantByOopt(Guid ooptId)
    {
        await using var context = await _contextFactory.CreateDbContextAsync();

        return await context.Set<Plant>()
            .Include(x => x.PlantFamily)
            .ThenInclude(x => x.PlantClass)
            .Include(x => x.PlantCharacteristicValues)
            .ThenInclude(x => x.PlantCharacteristic)
            .Include(x => x.RedBookPlants)
            .ThenInclude(x => x.Plant)
            .Include(x => x.RedBookPlants)
            .ThenInclude(x => x.RedBook)
            .Include(x => x.RedBookPlants)
            .ThenInclude(x => x.RarityCategory)
            .AsNoTracking()
            .Where(x => x.Oopts.Select(x => x.Id).Contains(ooptId))
            .ProjectTo<SerivcePlantViewModel>(_mapper.ConfigurationProvider)
            .ToListAsync();
    }
}
