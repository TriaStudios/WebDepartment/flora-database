using AutoMapper;
using Ecology.Flora.DAL.Logic.BindingModels.Plants;
using Ecology.Flora.DAL.Logic.Filters.Plants;
using Ecology.Flora.DAL.Logic.ViewModels.Plants;
using Ecology.Flora.DAL.PostgreSQL.Models.Plants;

namespace Ecology.Flora.DAL.PostgreSQL.Implements.Plants;

public class PlantFamilyCrudAsync :
    CrudAsyncBase<PlantFamilyViewModel, PlantFamilyBindingModel, PlantFamily, PlantFamilyFilter, FloraDatabaseContext>,
    IPlantFamilyCrudAsync
{
    public PlantFamilyCrudAsync(IDbContextFactory<FloraDatabaseContext> context, IMapper mapper) : base(context, mapper)
    {
    }

    protected override async Task BindingToDbModel(PlantFamilyBindingModel model, PlantFamily dbModel,
        DbContext context)
    {
        dbModel.PlantClassId = model.PlantClassId;
        dbModel.Name = model.Name;
        await Task.CompletedTask;
    }

    protected override Expression<Func<PlantFamily, bool>> ReadPredicate(PlantFamilyFilter filter)
    {
        return model => filter.PlantClassIds == null || !model.PlantClassId.HasValue ||
                        filter.PlantClassIds.Contains(model.PlantClassId.Value);
    }

    protected override IQueryable<PlantFamily> Includes(IQueryable<PlantFamily> queryable)
    {
        return queryable.Include(x => x.PlantClass);
    }
}
