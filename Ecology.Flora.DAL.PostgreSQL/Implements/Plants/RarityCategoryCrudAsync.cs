using AutoMapper;
using Ecology.Flora.DAL.Logic.BindingModels.Plants;
using Ecology.Flora.DAL.Logic.Filters.Plants;
using Ecology.Flora.DAL.Logic.ViewModels.Plants;
using Ecology.Flora.DAL.PostgreSQL.Models.Plants;

namespace Ecology.Flora.DAL.PostgreSQL.Implements.Plants;

public class RarityCategoryCrudAsync :
    CrudAsyncBase<RarityCategoryViewModel, RarityCategoryBindingModel, RarityCategory, RarityCategoryFilter,
        FloraDatabaseContext>, IRarityCategoryCrudAsync
{
    public RarityCategoryCrudAsync(IDbContextFactory<FloraDatabaseContext> context, IMapper mapper) : base(context,
        mapper)
    {
    }

    protected override async Task BindingToDbModel(RarityCategoryBindingModel model, RarityCategory dbModel,
        DbContext context)
    {
        dbModel.Code = model.Code;
        dbModel.Name = model.Name;
        await Task.CompletedTask;
    }

    protected override Expression<Func<RarityCategory, bool>> ReadPredicate(RarityCategoryFilter filter)
    {
        return model => filter.Codes.Count == 0 || filter.Codes.Contains(model.Code);
    }
}
