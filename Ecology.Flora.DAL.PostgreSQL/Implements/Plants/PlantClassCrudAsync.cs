using AutoMapper;
using Ecology.Flora.DAL.Logic.BindingModels.Plants;
using Ecology.Flora.DAL.Logic.Filters.Plants;
using Ecology.Flora.DAL.Logic.ViewModels.Plants;
using Ecology.Flora.DAL.PostgreSQL.Models.Plants;

namespace Ecology.Flora.DAL.PostgreSQL.Implements.Plants;

public class PlantClassCrudAsync :
    CrudAsyncBase<PlantClassViewModel, PlantClassBindingModel, PlantClass, PlantClassFilter, FloraDatabaseContext>,
    IPlantClassCrudAsync
{
    public PlantClassCrudAsync(IDbContextFactory<FloraDatabaseContext> context, IMapper mapper) : base(context, mapper)
    {
    }

    protected override async Task BindingToDbModel(PlantClassBindingModel model, PlantClass dbModel, DbContext context)
    {
        dbModel.PlantCladeId = model.PlantCladeId;
        dbModel.Name = model.Name;
        await Task.CompletedTask;
    }

    protected override Expression<Func<PlantClass, bool>> ReadPredicate(PlantClassFilter filter)
    {
        return model => filter.PlantCladeIds == null || !model.PlantCladeId.HasValue ||
                        filter.PlantCladeIds.Contains(model.PlantCladeId.Value);
    }

    protected override IQueryable<PlantClass> Includes(IQueryable<PlantClass> queryable)
    {
        return queryable.Include(x => x.PlantClade);
    }
}
