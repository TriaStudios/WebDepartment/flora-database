using System.Collections.Generic;
using AutoMapper;
using Ecology.Flora.DAL.Logic.BindingModels.Plants;
using Ecology.Flora.DAL.Logic.Filters.Plants;
using Ecology.Flora.DAL.Logic.Services.Storage;
using Ecology.Flora.DAL.Logic.ViewModels.Plants;
using Ecology.Flora.DAL.PostgreSQL.Models.Plants;
using Ecology.Flora.DAL.PostgreSQL.Models.Users;

namespace Ecology.Flora.DAL.PostgreSQL.Implements.Plants;

public class PlantCrudAsync :
    CrudAsyncBase<PlantViewModel, PlantBindingModel, Plant, PlantFilter, FloraDatabaseContext>, IPlantCrudAsync
{
    private readonly IImageService _imageService;
    private readonly IPlantCharacteristicValueCrudAsync _plantCharacteristicValueCrud;

    public PlantCrudAsync(IDbContextFactory<FloraDatabaseContext> context, IMapper mapper, IImageService imageService,
        IPlantCharacteristicValueCrudAsync plantCharacteristicValueCrud) 
        : base(context, mapper)
    {
        _imageService = imageService;
        _plantCharacteristicValueCrud = plantCharacteristicValueCrud;
    }

    public async Task<IEnumerable<PointMap>> GetPointsByBounds(BoundsModel model)
    {
        await using var context = await ContextFactory.CreateDbContextAsync();
        var queryable = context.Set<Plant>()
            .AsNoTracking()
            .Where(x =>
                (
                    model.RightUp.Latitude < model.LeftDown.Latitude &&
                    (x.Position.Latitude > model.LeftDown.Latitude ||
                     x.Position.Latitude < model.RightUp.Latitude) ||
                    model.RightUp.Latitude > model.LeftDown.Latitude &&
                    x.Position.Latitude > model.LeftDown.Latitude &&
                    x.Position.Latitude < model.RightUp.Latitude
                ) &&
                (
                    model.RightUp.Longitude < model.LeftDown.Longitude &&
                    (x.Position.Longitude > model.LeftDown.Longitude ||
                     x.Position.Longitude < model.RightUp.Longitude) ||
                    model.RightUp.Longitude > model.LeftDown.Longitude &&
                    x.Position.Longitude > model.LeftDown.Longitude &&
                    x.Position.Longitude < model.RightUp.Longitude
                ));

        var positionModels = queryable.Select(x => new PointMap
        {
            Latitude = x.Position.Latitude,
            Longitude = x.Position.Longitude,
            Name = x.Name,
            Id = x.Id
        });

        return await positionModels.ToArrayAsync();
    }

    protected override async Task BindingToDbModel(PlantBindingModel model, Plant dbModel, DbContext context)
    {
        dbModel.RedBookComment = model.RedBookComment;
        dbModel.EcologicalPhytocenoticGroupComment = model.EcologicalPhytocenoticGroupComment;
        dbModel.Name = model.Name;
        dbModel.Position = model.Position;
        dbModel.PlantFamilyId = model.PlantFamilyId;
        if (model.Avatar != null)
        {
            if (dbModel.AvatarId.HasValue) _imageService.Remove(dbModel.AvatarId.Value);
            dbModel.AvatarId = _imageService.Save(model.Avatar);
        }
        dbModel.Oopts = context.Set<Oopt>()
            .Where(x => model.OoptIds.Contains(x.Id))
            .ToHashSet();

        var names = new HashSet<string>(model.PlantCharacteristicValues.Select(x => x.Name.Ru));
        var plantCharacteristicValueIds = context.Set<PlantCharacteristicValue>()
            .AsSingleQuery()
            .AsNoTracking()
            .Where(x => names.Contains(x.Name.Ru))
            .Select(x => new ViewModelBase {Id = x.Id, Name = x.Name})
            .ToList();
        var dbNames = plantCharacteristicValueIds.Select(x => x.Name.Ru).ToHashSet();
        var charValueIds = new HashSet<Guid>(plantCharacteristicValueIds.Select(x => x.Id));
        foreach (var plantCharacteristicValueBindingModel in model.PlantCharacteristicValues
                     .Where(x => !dbNames.Contains(x.Name.Ru)))
        {
            charValueIds.Add(
                await _plantCharacteristicValueCrud.CreateOrUpdateAsync(plantCharacteristicValueBindingModel,
                    context: context));
        }

        dbModel.PlantCharacteristicValues = context.Set<PlantCharacteristicValue>()
            .Where(x => charValueIds.Contains(x.Id))
            .ToHashSet();
        dbModel.RedBookPlants = model.RedBooksPlantIds
            .Select(guidLink => context.Set<RedBookPlant>().FirstOrDefault(x => x.Id == guidLink))
            .Where(firstOrDefault => firstOrDefault != null).ToHashSet();
        dbModel.Authors = model.AuthorIds
            .Select(guidLink => context.Set<User>().FirstOrDefault(x => x.Id == guidLink))
            .Where(firstOrDefault => firstOrDefault != null).ToHashSet();

        await context.SaveChangesAsync();
        
        var removedValues = context.Set<PlantCharacteristicValue>()
            .AsSingleQuery()
            .Include(x => x.Plants)
            .Where(x => x.Plants.Count == 0);
        
        context.RemoveRange(removedValues);
    }

    protected override Expression<Func<Plant, bool>> ReadPredicate(PlantFilter filter)
    {
        return model =>
            (filter.PlantFamilyIds == null || filter.PlantFamilyIds.Count == 0 || filter.PlantFamilyIds.Contains(model.PlantFamilyId)) &&
            (filter.PlantCharacteristicValueIds == null || filter.PlantCharacteristicValueIds.Count == 0 || model.PlantCharacteristicValues.Select(x => x.Id).Any(x => filter.PlantCharacteristicValueIds.Contains(x))) &&
            (filter.RarityCategoryIds == null || filter.RarityCategoryIds.Count == 0 || model.RedBookPlants.Select(x => x.RarityCategoryId).Any(x => filter.RarityCategoryIds.Contains(x))) &&
            (filter.RedBookIds == null || filter.RedBookIds.Count == 0 || model.RedBookPlants.Select(x => x.RedBookId).Any(x => filter.RedBookIds.Contains(x))) &&
            (filter.AuthorIds == null || filter.AuthorIds.Count == 0 || model.Authors.Select(x => x.Id).Any(x => filter.AuthorIds.Contains(x))) &&
            (filter.OoptIds == null || filter.OoptIds.Count == 0 || model.Oopts.Select(x => x.Id).Any(x => filter.OoptIds.Contains(x))) &&
            (string.IsNullOrEmpty(filter.RedBookCommentQuery) || EF.Functions.ILike(
                string.Concat(model.RedBookComment.Ru, model.RedBookComment.La, model.RedBookComment.En),
                $"%{filter.RedBookCommentQuery}%")) &&
            (string.IsNullOrEmpty(filter.EcologicalPhytocenoticGroupCommentQuery) || EF.Functions.ILike(
                string.Concat(model.EcologicalPhytocenoticGroupComment.Ru, model.EcologicalPhytocenoticGroupComment.La,
                    model.EcologicalPhytocenoticGroupComment.En),
                $"%{filter.EcologicalPhytocenoticGroupCommentQuery}%"));
    }

    protected override IQueryable<Plant> Includes(IQueryable<Plant> queryable)
    {
        return queryable.Include(x => x.PlantCharacteristicValues)
            .ThenInclude(x => x.PlantCharacteristic)
            .Include(x => x.PlantFamily)
            .Include(x => x.RedBookPlants)
            .ThenInclude(x => x.RarityCategory)
            .Include(x => x.RedBookPlants)
            .ThenInclude(x => x.RedBook)
            .Include(x => x.Oopts)
            .Include(x => x.Authors);
    }
}
