﻿using AutoMapper;
using Ecology.Flora.DAL.Logic.BindingModels.Plants;
using Ecology.Flora.DAL.Logic.ViewModels.Plants;
using Ecology.Flora.DAL.PostgreSQL.Models.Plants;

namespace Ecology.Flora.DAL.PostgreSQL.Implements.Plants;

public class OoptCrudAsync : CrudAsyncBase<OoptViewModel, OoptBindingModel, Oopt, FilterBase, FloraDatabaseContext>,
    IOoptCrudAsync
{
    public OoptCrudAsync(IDbContextFactory<FloraDatabaseContext> dbContextFactory, IMapper mapper) : base(
        dbContextFactory, mapper)
    {
    }

    protected override Task BindingToDbModel(OoptBindingModel model, Oopt dbModel, DbContext context)
    {
        dbModel.Metric8 = model.Metric8;
        dbModel.Square = model.Square;
        dbModel.Name = model.Name;
        dbModel.Plants = context.Set<Plant>().Where(x => model.PlantIds.Contains(x.Id)).ToHashSet();
        return Task.CompletedTask;
    }
}
