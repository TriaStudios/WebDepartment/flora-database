using AutoMapper;
using Ecology.Flora.DAL.Logic.BindingModels.Plants;
using Ecology.Flora.DAL.Logic.ViewModels.Plants;
using Ecology.Flora.DAL.PostgreSQL.Models.Plants;

namespace Ecology.Flora.DAL.PostgreSQL.Implements.Plants;

public class RedBookCrudAsync : CrudAsyncBase<RedBookViewModel, RedBookBindingModel, RedBook, FloraDatabaseContext>,
    IRedBookCrudAsync
{
    public RedBookCrudAsync(IDbContextFactory<FloraDatabaseContext> context, IMapper mapper) : base(context, mapper)
    {
    }
}