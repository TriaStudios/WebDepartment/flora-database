﻿using AutoMapper;
using Ecology.Flora.DAL.Logic.BindingModels.Plants;
using Ecology.Flora.DAL.Logic.ViewModels.Plants;
using Ecology.Flora.DAL.PostgreSQL.Models.Plants;

namespace Ecology.Flora.DAL.PostgreSQL.Implements.Plants;

public class RedBookPlantCrudAsync :
    CrudAsyncBase<RedBookPlantViewModel, RedBookPlantBindingModel, RedBookPlant, FloraDatabaseContext>,
    IRedBookPlantCrudAsync
{
    public RedBookPlantCrudAsync(IDbContextFactory<FloraDatabaseContext> context, IMapper mapper) : base(context,
        mapper)
    {
    }

    protected override async Task BindingToDbModel(RedBookPlantBindingModel model, RedBookPlant dbModel,
        DbContext context)
    {
        dbModel.PlantId = model.PlantId;
        dbModel.RarityCategoryId = model.RarityCategoryId;
        dbModel.RedBookId = model.RedBookId;
        await Task.CompletedTask;
    }

    protected override IQueryable<RedBookPlant> Includes(IQueryable<RedBookPlant> queryable)
    {
        return queryable
            .Include(x => x.Plant)
            .Include(x => x.RarityCategory)
            .Include(x => x.RedBook);
    }
}
