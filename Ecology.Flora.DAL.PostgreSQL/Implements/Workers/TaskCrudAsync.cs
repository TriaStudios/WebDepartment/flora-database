using AutoMapper;
using Ecology.Flora.DAL.Logic.BindingModels.Workers;
using Ecology.Flora.DAL.Logic.Filters.Workers;
using Ecology.Flora.DAL.Logic.ViewModels.Workers;
using Ecology.Flora.DAL.PostgreSQL.Models.Workers;

namespace Ecology.Flora.DAL.PostgreSQL.Implements.Workers;

public class TaskCrudAsync : CrudAsyncBase<TaskViewModel, TaskBindingModel, WorkTask, TaskFilter, FloraDatabaseContext>,
    ITaskCrudAsync
{
    public TaskCrudAsync(IDbContextFactory<FloraDatabaseContext> context, IMapper mapper) : base(context, mapper)
    {
    }

    protected override async Task BindingToDbModel(TaskBindingModel model, WorkTask dbModel, DbContext context)
    {
        dbModel.Status = model.Status;
        dbModel.Namespace = model.Namespace;
        dbModel.Message = model.Message;
        dbModel.Name = model.Name;
        await Task.CompletedTask;
    }

    protected override Expression<Func<WorkTask, bool>> ReadPredicate(TaskFilter filter)
    {
        return model => filter.Statuses == null || filter.Statuses.Contains(model.Status);
    }
}
