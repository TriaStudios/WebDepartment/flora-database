using AutoMapper;
using Ecology.Flora.DAL.Logic.ViewModels.Users;
using Ecology.Flora.DAL.PostgreSQL.Models.Users;

namespace Ecology.Flora.DAL.PostgreSQL.Implements.Users;

public class RoleCrudAsync : CrudAsyncBase<RoleViewModel, RoleBindingModel, Role, FloraDatabaseContext>, IRoleCrudAsync
{
    public RoleCrudAsync(IDbContextFactory<FloraDatabaseContext> context, IMapper mapper) : base(context, mapper)
    {
    }
}