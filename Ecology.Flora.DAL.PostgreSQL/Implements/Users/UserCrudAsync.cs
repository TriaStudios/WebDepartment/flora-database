using AutoMapper;
using Ecology.Flora.DAL.Logic.ViewModels.Users;
using Ecology.Flora.DAL.PostgreSQL.Models.Users;

namespace Ecology.Flora.DAL.PostgreSQL.Implements.Users;

public class UserCrudAsync : CrudAsyncBase<UserViewModel, UserBindingModel, User, UserFilter, FloraDatabaseContext>,
    IUserCrudAsync
{
    public UserCrudAsync(IDbContextFactory<FloraDatabaseContext> context, IMapper mapper) : base(context, mapper)
    {
    }

    public async Task UpdateRefreshToken(UserRefreshTokenBindingModel userRefreshTokenBindingModel,
        CancellationToken cancellationToken = default)
    {
        await using var context = await ContextFactory.CreateDbContextAsync(cancellationToken);
        var user = await context.Set<User>()
            .FirstOrDefaultAsync(x => x.Id == userRefreshTokenBindingModel.Id && !x.Deleted, cancellationToken);
        if (user == null) throw new Exception("Пользователь не найден");

        user.RefreshToken = userRefreshTokenBindingModel.RefreshToken;
        await context.SaveChangesAsync(cancellationToken);
    }

    public async Task<Guid> CreateUser(UserBindingModel userBindingModel, CancellationToken cancellationToken = default)
    {
        await using var context = await ContextFactory.CreateDbContextAsync(cancellationToken);
        var user = await context.Set<User>()
            .FirstOrDefaultAsync(x => x.Email == userBindingModel.Email && !x.Deleted, cancellationToken);
        if (user != null) throw new Exception("Пользователь с такой почтой уже существует");

        user = await context.Set<User>().FirstOrDefaultAsync(x => x.Username == userBindingModel.Username && !x.Deleted,
            cancellationToken);
        if (user != null) throw new Exception("Пользователь с таким ником уже существует");

        if (string.IsNullOrEmpty(userBindingModel.PasswordHash)) throw new Exception("Пароль не задан");

        user = new User {Created = DateTime.UtcNow};
        context.Add(user);
        await BindingToDbModel(userBindingModel, user, context);
        user.Password = userBindingModel.PasswordHash;
        await context.SaveChangesAsync(cancellationToken);

        return user.Id;
    }

    public async Task<Guid> UpdateUser(UserBindingModel userBindingModel, CancellationToken cancellationToken = default)
    {
        await using var context = await ContextFactory.CreateDbContextAsync(cancellationToken);
        var user = await context.Set<User>().Include(x => x.Roles)
            .FirstOrDefaultAsync(x => x.Id == userBindingModel.Id && !x.Deleted, cancellationToken);
        if (user == null) throw new Exception("Пользователь не найден");

        if (string.IsNullOrEmpty(userBindingModel.PasswordHash)) throw new Exception("Пароль не задан");

        await BindingToDbModel(userBindingModel, user, context);
        if (!string.IsNullOrEmpty(userBindingModel.PasswordHash))
            user.Password = userBindingModel.PasswordHash;

        user.Updated = DateTime.UtcNow;

        await context.SaveChangesAsync(cancellationToken);

        return user.Id;
    }

    protected override async Task BindingToDbModel(UserBindingModel model, User dbModel, DbContext context)
    {
        dbModel.Email = model.Email;
        dbModel.Username = model.Username;
        dbModel.Password = model.PasswordHash;
        dbModel.Name = model.Name;
        if (model.RoleIds != null)
            dbModel.Roles = context.Set<Role>().Where(x => model.RoleIds.Contains(x.Id)).ToHashSet();
        await Task.CompletedTask;
    }

    protected override Expression<Func<User, bool>> ReadPredicate(UserFilter filter)
    {
        return model => (string.IsNullOrEmpty(filter.Username) || model.Username == filter.Username) &&
                        (string.IsNullOrEmpty(filter.PasswordHash) || model.Password == filter.PasswordHash) &&
                        (string.IsNullOrEmpty(filter.RefreshToken) || model.RefreshToken == filter.RefreshToken);
    }

    protected override IQueryable<User> Includes(IQueryable<User> queryable)
    {
        return queryable.Include(x => x.Roles);
    }
}
