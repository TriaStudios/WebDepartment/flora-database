﻿using System.IO;
using Ecology.Flora.DAL.Logic.BindingModels.Storage;
using Ecology.Flora.DAL.Logic.Services.Storage;
using Ecology.Flora.DAL.PostgreSQL.Models.Storage;
using ImageMagick;

namespace Ecology.Flora.DAL.PostgreSQL.Implements.Storage;

public class ImageService : IImageService
{
    private readonly IDbContextFactory<FloraDatabaseContext> _contextFactory;
    private readonly string _storagePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "storage");

    public ImageService(IDbContextFactory<FloraDatabaseContext> contextFactory)
    {
        _contextFactory = contextFactory;

        if (!Directory.Exists(_storagePath)) Directory.CreateDirectory(_storagePath);
    }

    public Guid Save(ImageBinding imageBinding)
    {
        Guid id;
        using (var context = _contextFactory.CreateDbContext())
        {
            id = context.Set<Image>().Add(new Image
            {
                Created = DateTime.UtcNow,
                Updated = DateTime.UtcNow,
                Name = imageBinding.Description
            }).Entity.Id;
            context.SaveChanges();
        }

        var fromBase64String = Convert.FromBase64String(imageBinding.FileBase64);
        using var image = new MagickImage(fromBase64String);
        foreach (var imageSize in Enum.GetValues<ImageSize>())
        {
            var path = Path.Combine(_storagePath, $"{id}_{imageSize}.jpg");

            using var changed = new MagickImage(image);
            if (imageSize != ImageSize.Original)
            {
                var ratioX = (double) imageSize / image.Width;
                var ratioY = (double) imageSize / image.Height;
                var ratio = Math.Max(ratioX, ratioY);
                var sizeX = (int) Math.Round(image.Width * ratio);
                var sizeY = (int) Math.Round(image.Height * ratio);
                changed.InterpolativeResize(sizeX, sizeY, PixelInterpolateMethod.Bilinear);
                changed.Crop(new MagickGeometry((changed.Width - (int) imageSize) / 2,
                    (changed.Height - (int) imageSize) / 2, (int) imageSize, (int) imageSize));
            }

            changed.Write(path);
        }

        return id;
    }

    public byte[] Load(Guid id, ImageSize imageSize)
    {
        using var context = _contextFactory.CreateDbContext();

        var image = context.Set<Image>().FirstOrDefault(x => x.Id == id);
        if (image == null) throw new FileNotFoundException("Файл с таким ID не существует");
        var path = Path.Combine(_storagePath, $"{image.Id}_{imageSize}.jpg");
        if (File.Exists(path)) return File.ReadAllBytes(path);

        context.Set<Image>().Remove(image);
        context.SaveChanges();
        throw new FileNotFoundException("Файл с таким ID не существует");
    }

    public void Remove(Guid id)
    {
        foreach (var imageSize in Enum.GetValues<ImageSize>())
        {
            var path = Path.Combine(_storagePath, $"{id}_{imageSize}.jpg");

            if (File.Exists(path)) File.Delete(path);
        }
    }
}
