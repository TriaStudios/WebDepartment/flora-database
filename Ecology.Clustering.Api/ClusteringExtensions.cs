﻿using System;
using Microsoft.Extensions.DependencyInjection;

namespace Ecology.Clustering.Api;

public static class ClusteringExtensions
{
    public static void AddClusteringGrpcClient(this IServiceCollection services, string uri)
    {
        services.AddGrpcClient<ClusteringGrpc.ClusteringGrpcClient>(options => options.Address = new Uri(uri));
    }
}