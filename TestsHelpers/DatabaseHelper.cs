﻿using Ecology.Flora.DAL.PostgreSQL;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace TestsHelpers;

public static class DatabaseHelper
{
    public static IServiceCollection UseInMemoryDatabase(this IServiceCollection services)
    {
        var dbContextDescriptor = services.SingleOrDefault(x => x.ServiceType == typeof(DbContextOptions<FloraDatabaseContext>));
        if (dbContextDescriptor != null) services.Remove(dbContextDescriptor);

        return services.AddDbContextFactory<FloraDatabaseContext>(optionsBuilder => 
            optionsBuilder.UseInMemoryDatabase("flora-database"));
    }
}
