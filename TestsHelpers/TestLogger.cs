﻿using NUnit.Framework;
using NUnit.Framework.Interfaces;

namespace TestsHelpers;

public class TestBase
{
    [TearDown]
    public void TearDown()
    {
        TestContext.Out.WriteLine($"{TestContext.CurrentContext.Result.Outcome}: {TestContext.CurrentContext.Test.MethodName}");
        if (TestContext.CurrentContext.Result.Outcome != ResultState.Failure) return;
        
        TestContext.Out.WriteLine($"{TestContext.CurrentContext.Result.Message}");
        TestContext.Out.WriteLine($"{TestContext.CurrentContext.Result.StackTrace}");
    }
}
