using System;
using Ecology.Clustering.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Serilog;

var builder = WebApplication.CreateBuilder(args);

builder.Host.UseSerilog((context, services, configuration) => configuration
    .ReadFrom.Configuration(context.Configuration)
    .ReadFrom.Services(services));

builder.Services.AddGrpc();

var app = builder.Build();

app.MapGrpcService<ClusteringService>();
app.MapGet("/",
    () =>
        "Communication with gRPC endpoints must be made through a gRPC client. To learn how to create a client, visit: https://go.microsoft.com/fwlink/?linkid=2086909");

var logger = app.Services.GetService<ILogger>();
logger.Information("NET_ENVIRONMENT = {Kek}", Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT"));
logger.Information(
    "\n ▄████████  ▄█       ███    █▄     ▄████████     ███        ▄████████    ▄████████  ▄█  ███▄▄▄▄      ▄██████▄  \n" +
    "███    ███ ███       ███    ███   ███    ███ ▀█████████▄   ███    ███   ███    ███ ███  ███▀▀▀██▄   ███    ███ \n" +
    "███    █▀  ███       ███    ███   ███    █▀     ▀███▀▀██   ███    █▀    ███    ███ ███▌ ███   ███   ███    █▀  \n" +
    "███        ███       ███    ███   ███            ███   ▀  ▄███▄▄▄      ▄███▄▄▄▄██▀ ███▌ ███   ███  ▄███        \n" +
    "███        ███       ███    ███ ▀███████████     ███     ▀▀███▀▀▀     ▀▀███▀▀▀▀▀   ███▌ ███   ███ ▀▀███ ████▄  \n" +
    "███    █▄  ███       ███    ███          ███     ███       ███    █▄  ▀███████████ ███  ███   ███   ███    ███ \n" +
    "███    ███ ███▌    ▄ ███    ███    ▄█    ███     ███       ███    ███   ███    ███ ███  ███   ███   ███    ███ \n" +
    "████████▀  █████▄▄██ ████████▀   ▄████████▀     ▄████▀     ██████████   ███    ███ █▀    ▀█   █▀    ████████▀  \n" +
    "           ▀                                                            ███    ███                             \n"
);

app.Run();