﻿using System;
using System.Numerics;
using Ecology.Clustering.Api;
using Ecology.Clustering.Models;
using Index = Ecology.Clustering.Api.Index;

namespace Ecology.Clustering.Utils;

public static class PointUtils
{
    private static readonly float Sqrt3 = MathF.Sqrt(3);

    public static CartesianPoint ToKavrayskiyViiProjection(this GeographicPoint point)
    {
        var pointLatitude = point.Latitude / 180;
        return new CartesianPoint(
            3 / 2.0f * point.Longitude * MathF.Sqrt(1 / 3.0f - pointLatitude * pointLatitude),
            point.Latitude
        );
    }

    public static GeographicPoint FromKavrayskiyViiProjection(this CartesianPoint point)
    {
        var (d, y) = point;
        return new GeographicPoint
        {
            Longitude = 2 / 3.0f * d / MathF.Sqrt(1 / 3.0f - y / MathF.PI * (y / MathF.PI)),
            Latitude = y
        };
    }

    public static Index FromPositionToHexagonIndex(this CartesianPoint point, float hexagonSize)
    {
        var as3 = hexagonSize * Sqrt3;
        var as3d2 = as3 / 2.0f;
        var a32 = as3 * 3.0f / 2.0f;
        var xModA32 = point.X % a32;
        var x = (int) MathF.Floor(point.X / a32);
        var x2 = x % 2;
        var ind = new Index
        {
            Q = x,
            R = (int) MathF.Floor((point.Y + x2 * as3d2) / as3)
        };
        if (!(xModA32 > hexagonSize)) return ind;
        var ydAs3d2 = (int) MathF.Floor(point.Y / as3d2);
        var side = new[]
        {
            GetSide(
                point,
                new CartesianPoint(a32 * ind.Q + hexagonSize, as3d2 * ydAs3d2),
                new CartesianPoint(a32 * (ind.Q + 1), as3d2 * (ydAs3d2 + 1))
            ),
            GetSide(
                point,
                new CartesianPoint(a32 * (ind.Q + 1), as3d2 * ydAs3d2),
                new CartesianPoint(a32 * ind.Q + hexagonSize, as3d2 * (ydAs3d2 + 1))
            )
        };

        switch (ind.Q % 2)
        {
            case 0 when ydAs3d2 % 2 == 0:
                ind.Q += side[0];
                break;
            case 0:
                ind.Q += side[1];
                ind.R += side[1];
                break;
            default:
            {
                switch (ydAs3d2 % 2)
                {
                    case 0:
                        ind.Q += side[1];
                        break;
                    default:
                        ind.Q += side[0];
                        ind.R -= side[0];
                        break;
                }

                break;
            }
        }

        return ind;
    }

    private static int GetSide(CartesianPoint a, CartesianPoint b, CartesianPoint c)
    {
        return GetTriangleSquare(a, b, c) <= 0 ? 1 : 0;
    }

    private static double GetTriangleSquare(CartesianPoint a, CartesianPoint b, CartesianPoint c)
    {
        return (a.X - c.X) * (b.Y - c.Y) - (a.Y - c.Y) * (b.X - c.X);
    }
}
