﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Ecology.Clustering.Api;
using Ecology.Clustering.Utils;
using Grpc.Core;
using Serilog;

namespace Ecology.Clustering.Services;

public class ClusteringService : ClusteringGrpc.ClusteringGrpcBase
{
    private readonly ILogger _logger;

    public ClusteringService(ILogger logger)
    {
        _logger = logger;
    }

    public override Task<ClusterPointsResponse> ClusterPoints(ClusterPointsRequest request, ServerCallContext context)
    {
        var hexSize = request.HexagonSize;
        _logger.Information("Start clustering with resolution = {Size}", hexSize);
        var clusters = request.Points
            .Select(geoObject => new
            {
                geoObject,
                cartPoint = geoObject.Point.ToKavrayskiyViiProjection()
            })
            .Select(t => new Cluster
            {
                MinBound = t.geoObject.Point.Clone(),
                MaxBound = t.geoObject.Point.Clone(),
                Index = t.cartPoint.FromPositionToHexagonIndex(hexSize),
                Center = t.geoObject.Point,
                CountPoints = 1,
                UniqueIndex = t.geoObject.UniqueIndex
            }).ToList();
        var clusterizePointsResponse = new ClusterPointsResponse
        {
            Clusters =
            {
                clusters.GroupBy(x => x.Index)
                    .Select(x =>
                    {
                        var aggregate = x.Aggregate((a, b) =>
                        {
                            a.Center.Latitude = (a.Center.Latitude * a.CountPoints + b.Center.Latitude) /
                                                (a.CountPoints + 1);
                            a.Center.Longitude = (a.Center.Longitude * a.CountPoints + b.Center.Longitude) /
                                                 (a.CountPoints + 1);
                            a.CountPoints++;
                            a.ClearUniqueIndex();
                            a.MinBound.Latitude = Math.Min(a.MinBound.Latitude, b.MinBound.Latitude);
                            a.MinBound.Longitude = Math.Min(a.MinBound.Longitude, b.MinBound.Longitude);
                            a.MaxBound.Longitude = Math.Max(a.MaxBound.Longitude, b.MaxBound.Longitude);
                            a.MaxBound.Latitude = Math.Max(a.MaxBound.Latitude, b.MaxBound.Latitude);
                            return a;
                        });
                        _logger.Debug(
                            "Found cluster with {Count} points and center on {{ {Lat}, {Lon} }}",
                            aggregate.CountPoints,
                            aggregate.Center.Latitude,
                            aggregate.Center.Longitude);
                        return aggregate;
                    })
                    .ToArray()
            }
        };
        _logger.Information("Clustering is over");
        return Task.FromResult(clusterizePointsResponse);
    }
}
