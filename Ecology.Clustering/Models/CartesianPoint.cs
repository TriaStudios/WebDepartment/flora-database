﻿namespace Ecology.Clustering.Models;

public record CartesianPoint(float X, float Y);
