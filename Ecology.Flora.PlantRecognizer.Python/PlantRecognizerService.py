import json
import os
import random

import numpy as np

import ImageLoader
import NeuralNetwork
import consts
from ecology.flora.plantRecognizer import plantRecognizer_pb2_grpc
from ecology.flora.plantRecognizer.plantRecognizer_pb2 import TrainNetResponse, UpdateDataSetResponse, \
    PredictNetResponse
from loggers import logging
from tempfile import TemporaryFile

class PlantRecognizerService(plantRecognizer_pb2_grpc.PlantRecognizerGrpcServicer):
    def __init__(self, neural_network: NeuralNetwork.NeuralNetwork):
        self.neural_network = neural_network
        super().__init__()

    def TrainNet(self, request, context):
        if not os.path.exists(consts.DATASET_NUMPY_PATH):
            raise Exception("You have to call UpdateDataSet to train neural network!")
        logging.info("Start train net. Preparing train data...")
        numpy_zips = sorted([os.path.join(consts.DATASET_NUMPY_PATH, i) for i in os.listdir(consts.DATASET_NUMPY_PATH)])
        names = json.load(open("output-dataset.json", "r"))
        for i in range(request.epochs):
            logging.info("Prepare data for epoch {}/{}".format(i + 1, request.epochs))
            x_data = []
            y_data = []
            for j in names:
                nzips = [a for a in numpy_zips if j["Name"] in a]
                rnd = random.randint(0, len(nzips) // 2 - 1)
                xx = np.load(nzips[rnd * 2 + 0])["data"]
                yy = np.load(nzips[rnd * 2 + 1])["data"]
                x_data.append(xx)
                y_data.append(yy)
            x = np.asarray(x_data)
            y = np.asarray(y_data)
            self.neural_network.train_by_batch(x, y, 5)
        return TrainNetResponse()

    def UpdateDataSet(self, request, context):
        logging.info("Start update data set...")
        dirs = os.listdir(consts.PATH_TO_DATASET_DIRECTORY)
        classifiers_fin = open(consts.CLASSIFIERS_PATH, "r")
        classifiers = json.load(classifiers_fin)
        classifiers_fin.close()
        y_example = [0] * len(classifiers)
        y_datas = {
            i["Name"]: np.array([*y_example[:i["Row"]], 1, *y_example[i["Row"] + 1:]], dtype=float)
            for i in classifiers
        }
        for directory in dirs:
            path = os.path.join(consts.PATH_TO_DATASET_DIRECTORY, directory)
            if directory in y_datas:
                images = os.listdir(path)
                counter = 0
                for image in images:
                    sub_path = os.path.join(path, image)
                    load_image: np.ndarray = ImageLoader.import_image(sub_path)
                    np.savez_compressed(
                        os.path.join(consts.DATASET_NUMPY_PATH, "{0}.{1}.X".format(directory, counter)),
                        data=load_image
                    )
                    np.savez_compressed(
                        os.path.join(consts.DATASET_NUMPY_PATH, "{0}.{1}.Y".format(directory, counter)),
                        data=y_datas[directory]
                    )
                    counter += 1
        logging.info("Data set is successfully changed...")
        return UpdateDataSetResponse()

    def PredictNet(self, request, context):
        kek = TemporaryFile()
        kek.write(request.image_data)
        kek.seek(0)
        img = ImageLoader.import_image(kek)
        kek.close()
        x = np.asarray([img])
        y = self.neural_network.predict(x)[0]
        json_fin = open("output-dataset.json", encoding='utf-8', mode="r")
        outputs = {i["Row"]: i["Name"] for i in json.load(json_fin)}
        response = PredictNetResponse()
        response.plant_family_name = outputs[np.argmax(y, axis=0)]
        return response
