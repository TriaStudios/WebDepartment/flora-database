from concurrent import futures

import NeuralNetwork
from PlantRecognizerService import PlantRecognizerService
from ecology.flora.plantRecognizer import plantRecognizer_pb2_grpc
import grpc


def serve():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    plantRecognizer_pb2_grpc.add_PlantRecognizerGrpcServicer_to_server(
        PlantRecognizerService(NeuralNetwork.NeuralNetwork()), server)
    server.add_insecure_port('[::]:5012')
    server.start()
    server.wait_for_termination()


if __name__ == '__main__':
    serve()
