import logging
from logging.handlers import RotatingFileHandler
from os import mkdir, listdir


if "Logs" not in listdir("."):
    mkdir("Logs")
logging.basicConfig(format='%(asctime)s :: %(levelname)s :: %(funcName)s :: %(lineno)d :: %(message)s',
                    level=logging.INFO,
                    handlers=[
                        RotatingFileHandler('./Logs/log.log', maxBytes=100000, backupCount=10),
                        logging.StreamHandler()
                    ])
