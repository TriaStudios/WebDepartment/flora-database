import numpy as np
import tensorflow.keras as keras

import ImageLoader
import json

model: keras.Sequential = keras.models.load_model("model-data")
image = ImageLoader.import_image("907265_71269.jpg")
X = np.asarray([image])
Y = model.predict(X)
json_fin = open("output-dataset.json", "r")
outputs = {i["Row"]: i["Name"] for i in json.load(json_fin)}
print(*[outputs[i] for i in np.argmax(Y, axis=1)])
