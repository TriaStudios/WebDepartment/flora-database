from loggers import logging

import numpy as np
from PIL import Image


def import_image(path):
    img: Image.Image = Image.open(path)
    img = img.resize((256, 256))
    return np.asarray(img) / 255


if __name__ == '__main__':
    for i in range(100):
        logging.info(import_image("datasets/Лилейные/lilium.jpg"))
