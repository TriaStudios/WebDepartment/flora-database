import os

import numpy as np

np.random.seed(1)
DATASET_NUMPY_PATH = "train-data"
PATH_TO_DATASET_DIRECTORY = "datasets"
CLASSIFIERS_PATH = "output-dataset.json"
MODEL_DATA_PATH = "model-data"

if not os.path.exists(PATH_TO_DATASET_DIRECTORY):
    os.mkdir(PATH_TO_DATASET_DIRECTORY)
if not os.path.exists(DATASET_NUMPY_PATH):
    os.mkdir(DATASET_NUMPY_PATH)
if not os.path.exists(MODEL_DATA_PATH):
    os.mkdir(MODEL_DATA_PATH)
