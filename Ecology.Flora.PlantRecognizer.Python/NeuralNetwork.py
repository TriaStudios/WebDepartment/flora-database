import os

from keras import Sequential
from keras.constraints import maxnorm
from keras.layers import Conv2D, Activation, Dropout, BatchNormalization, MaxPooling2D, Flatten, Dense

import consts
from loggers import logging
import tensorflow.keras as keras


class NeuralNetwork:
    def __init__(self):
        logging.info("Initialize neural network")
        self.model = None
        try:
            logging.info("Load saved neural network from folder model-data")
            model = keras.models.load_model(consts.MODEL_DATA_PATH)
            logging.info("Neural network was successfully loaded!")

            self.model = model
            return
        except:
            logging.warning("Folder 'model-data' is not empty but also not match to neural network structure, "
                            "loading is stopped")

        logging.info("Create new neural network")
        model = Sequential()

        model.add(Conv2D(5, (3, 3), input_shape=(256, 256, 3), padding='same'))
        model.add(Activation('relu'))
        model.add(Dropout(0.2))
        model.add(BatchNormalization())

        model.add(Conv2D(10, (3, 3), padding='same'))
        model.add(Activation('relu'))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Dropout(0.2))
        model.add(BatchNormalization())

        model.add(Flatten())
        model.add(Dropout(0.2))

        model.add(Dense(10, kernel_constraint=maxnorm(3)))
        model.add(Activation('relu'))
        model.add(Dropout(0.2))
        model.add(BatchNormalization())
        model.add(Dense(len(os.listdir("datasets"))))
        model.add(Activation('softmax'))

        logging.info("Compile model...")
        model.compile(optimizer="sgd", loss="categorical_crossentropy", metrics=["categorical_accuracy"])
        logging.info("Model was compiled!")

        self.model = model
        logging.info("Save neural network to file")
        self.save_model()
        logging.info("Neural network was successfully created!")

    def fit(self, x, y, batch_size, epochs):
        logging.info("Start fit model with BatchSize = {} by {} epochs...".format(batch_size, epochs))
        self.model.fit(x, y, batch_size, epochs)
        logging.info("Model was fitted!")
        self.save_model()

    def train_by_batch(self, x, y, epochs):
        logging.info("Start fit model with one batch by {} epochs...".format(epochs))
        for i in range(epochs):
            history = self.model.train_on_batch(x, y)
            logging.info("{}/{} :: Error={} :: categorical_accuracy={}".format(i + 1, epochs, *history))
        logging.info("Model was fitted!")
        self.save_model()

    def predict(self, x):
        logging.info("Predict model")
        return self.model.predict(x)

    def save_model(self):
        self.model.save(consts.MODEL_DATA_PATH)
