namespace Ecology.Flora.BFF;

public static class Consts
{
    public const string BaseRoute = "api/v1";
    public const string RouteWithControllerName = BaseRoute + "/[controller]";
}