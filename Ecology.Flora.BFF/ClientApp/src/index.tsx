import 'index.scss';
import '@vkontakte/vkui/dist/unstable.css';

import { AdaptivityProvider, ConfigProvider, WebviewType } from '@vkontakte/vkui';
import { App } from 'App';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter, HashRouter } from 'react-router-dom';
import { store } from 'store';

const BaseRouter = (props: { children: React.ReactElement<any, any> }) =>
    !(window as any).cordova ? (
        <BrowserRouter>{props.children}</BrowserRouter>
    ) : (
        <HashRouter>{props.children}</HashRouter>
    );

const startApp = () => {
    ReactDOM.render(
        <ConfigProvider webviewType={WebviewType.INTERNAL}>
            <AdaptivityProvider>
                <BaseRouter>
                    <Provider store={store}>
                        <App />
                    </Provider>
                </BaseRouter>
            </AdaptivityProvider>
        </ConfigProvider>,
        document.getElementById('root'),
    );
};

if (!(window as any).cordova) {
    startApp();
} else {
    document.addEventListener('deviceready', startApp, false);
}
