import { Div, Group, Title, useAdaptivity, ViewWidth } from '@vkontakte/vkui';
import { ClusterizationApi, PlantAPI } from 'api';
import { ClusterView, PlantView, PointView } from 'api/view';
import { MapPlantDescription } from 'component/MapPlantDescription';
import { Bounds, Map, Marker, Overlay, Point, ZoomControl } from 'pigeon-maps';
import { osm } from 'pigeon-maps/providers';
import React, { useEffect, useState } from 'react';

export const MapPage = () => {
    const [clusters, setClusters] = useState<Array<ClusterView>>([]);
    const [overlayContent, setOverlayContent] = useState<ClusterView>();
    const [overlayAnchor, setOverlayAnchor] = useState<Point>();
    const [hover, setHover] = useState<ClusterView>();
    const [mapBounds, setMapBounds] = useState<Bounds>({
        sw: [0, 0],
        ne: [0, 0],
    });
    const [overlayLoading, setOverlayLoading] = useState<PlantView>();
    const { viewWidth } = useAdaptivity();
    const isDesktop = (viewWidth ?? ViewWidth.DESKTOP) >= ViewWidth.TABLET;
    const refMap = React.createRef<Map>();

    const boundsChange = (props: { center: [number, number]; zoom: number; bounds: Bounds; initial: boolean }) => {
        setMapBounds(props.bounds);
        ClusterizationApi.clusterize({
            points: [
                { latitude: props.bounds.sw[0], longitude: props.bounds.sw[1] },
                { latitude: props.bounds.ne[0], longitude: props.bounds.ne[1] },
            ],
        }).then(setClusters);
    };

    const onClickMarker = (p: { event: React.MouseEvent; anchor: Point; payload: ClusterView }) => {
        if (p.payload.countPoints === 1) {
            refMap.current?.setCenterZoom([p.payload.center.latitude, p.payload.center.longitude], 14);
            return;
        }

        const getPoint = (pv: PointView) => [pv.latitude, pv.longitude] as [number, number];

        const center: [number, number] = getPoint(p.payload.center);
        const minMapLength = Math.min(
            Math.abs(mapBounds.sw[0] - mapBounds.ne[0]) * 2,
            Math.abs(mapBounds.sw[1] - mapBounds.ne[1]),
        );
        const maxLength = Math.max(
            Math.abs(p.payload.maxBound.latitude - p.payload.minBound.latitude) * 2,
            Math.abs(p.payload.maxBound.longitude - p.payload.minBound.longitude),
        );
        let angle = 360;
        while (angle > minMapLength) {
            angle /= 2;
        }
        angle *= 2;
        const koef = minMapLength / angle;

        let zoom = 0;
        angle = 360;
        while (angle * koef > maxLength) {
            zoom++;
            angle /= 2;
        }
        refMap.current?.setCenterZoomTarget(center, zoom, undefined, undefined, 1000);
    };

    useEffect(() => {
        if (overlayContent) {
            if (overlayContent.id) {
                PlantAPI.get(overlayContent.id).then(setOverlayLoading);
            }
        } else {
            setOverlayLoading(undefined);
        }
    }, [overlayContent]);

    useEffect(() => {
        if (
            hover?.center.latitude !== overlayContent?.center.latitude ||
            hover?.center.longitude !== overlayContent?.center.longitude
        ) {
            setOverlayContent(hover);
            setOverlayAnchor(hover ? [hover.center.latitude, hover.center.longitude] : undefined);
        }
    }, [hover]);

    return (
        <Group>
            <div style={{ width: '100%', height: isDesktop ? '70vh' : 'calc(100vh - 61px - 48px)' }}>
                <Map
                    defaultCenter={[54.314192, 48.403132]}
                    defaultZoom={9}
                    provider={osm}
                    onBoundsChanged={boundsChange}
                    animate
                    ref={refMap}>
                    <ZoomControl />
                    {clusters.map((cluster) => (
                        <Marker
                            key={cluster.index.r * 10000000 + cluster.index.q}
                            anchor={[cluster.center.latitude, cluster.center.longitude]}
                            onClick={onClickMarker}
                            payload={cluster}
                            color={cluster.color}
                            hover={hover === cluster}
                            width={30}
                            onMouseOver={() => {
                                setHover(cluster);
                            }}
                            onMouseOut={() => {
                                setHover(undefined);
                            }}
                        />
                    ))}
                    {overlayAnchor && overlayContent && (
                        <Overlay anchor={overlayAnchor}>
                            <div style={{ position: 'relative', width: '100vw' }}>
                                <Div
                                    style={{
                                        position: 'absolute',
                                        transform: 'translate(-50%, 0)',
                                        backgroundColor: 'white',
                                        borderRadius: '4px',
                                        textAlign: 'center',
                                    }}>
                                    <Title level="3" weight="bold">
                                        {overlayContent.title}
                                    </Title>
                                    {overlayContent.id && <MapPlantDescription plant={overlayLoading} />}
                                </Div>
                            </div>
                        </Overlay>
                    )}
                </Map>
            </div>
        </Group>
    );
};
