import { Icon24Filter, Icon24Search } from '@vkontakte/icons';
import {
    CellButton,
    HorizontalScroll,
    Search,
    SimpleCell,
    Spinner,
    Tabs,
    TabsItem,
    useAdaptivity,
    ViewWidth,
} from '@vkontakte/vkui';
import { useQuery } from 'hook/useQuery';
import React, { ChangeEvent, ReactNode, useCallback, useEffect, useLayoutEffect, useRef, useState } from 'react';
import { connect, ConnectedProps } from 'react-redux';
import { useHistory, useLocation } from 'react-router-dom';
import { RootState } from 'store';
import { MenuItem } from 'store/models';
import { openModalFilter } from 'store/slice/modalSlice';
import { buttonsAdmin, buttonsCatalog } from 'utils/menuButtons';

const connector = connect((state: RootState) => ({}), {
    openModalFilter,
});

interface Props extends ConnectedProps<typeof connector> {
    onFetch: (ended: () => void, fetched: () => void, query: URLSearchParams) => void;
    onReset: () => void;
    isReload: boolean;
    children: ReactNode;
}

export const ListPageBase = connector((props: Props) => {
    const loader = useRef<any>(null);
    const [isFetching, setIsFetching] = useState<boolean>(false);
    const [isEnd, setIsEnd] = useState<boolean>(false);
    const { query, setQuery } = useQuery();
    const [search, setSearch] = useState('');
    const [isReset, setIsReset] = useState(false);
    const { viewWidth } = useAdaptivity();
    const isDesktop = (viewWidth ?? ViewWidth.DESKTOP) >= ViewWidth.TABLET;
    const location = useLocation();
    const history = useHistory();
    const horizontalScrollRef = useRef<HTMLDivElement>(null);
    const mainPath = location.pathname.split('/')[1];
    const isAdmin = mainPath === 'admin';

    const scrollHorizontal = (e: any) => {
        if (!e) return;

        horizontalScrollRef.current?.scrollTo({
            left: e.offsetLeft + e.offsetWidth / 2 - e.offsetParent.offsetParent.offsetWidth / 2,
            behavior: 'smooth',
        });
    };

    const onClickTab = (x: MenuItem) => (e: any) => {
        scrollHorizontal(e.target.offsetParent);
        history.push(`/${mainPath}/${x.id}`);
    };

    useLayoutEffect(() => {
        const strings = location.pathname.split('/');
        const elementId = `TAB_MENU_${strings[strings.length - 1]}`;
        scrollHorizontal(window.document.getElementById(elementId));
    }, []);

    useEffect(() => {
        setIsEnd(true);
        setIsFetching(true);
        setIsReset(true);
        setSearch(query.get('query') ?? '');
    }, [query]);

    useEffect(() => {
        setIsEnd(true);
        setIsFetching(true);
        setIsReset(true);
    }, [props.isReload, location.pathname]);

    useEffect(() => {
        if (isReset) {
            props.onReset();
            setIsEnd(false);
            setIsFetching(false);
            setIsReset(false);

            window.scrollTo({
                top: 0,
                behavior: 'smooth',
            });
        }
    }, [isReset]);

    const loadMoreCallback = useCallback(
        (entries) => {
            const target = entries[0];
            if (target.isIntersecting && !isFetching && !isEnd) {
                setIsFetching(true);
                props.onFetch(
                    () => setIsEnd(true),
                    () => setIsFetching(false),
                    query,
                );
            }
        },
        [isFetching, isEnd, query, isReset],
    );

    useEffect(() => {
        const options = {
            root: null,
            rootMargin: '0px',
            threshold: 0.25,
        };

        const observer = new IntersectionObserver(loadMoreCallback, options);

        const { current } = loader;
        if (loader && current) {
            observer.observe(current);
        }

        return () => observer.unobserve(current);
    }, [loader, loadMoreCallback]);

    return (
        <div style={{ height: '100vh !important' }}>
            {isDesktop && (
                <SimpleCell
                    style={{ padding: 0, paddingRight: '1rem' }}
                    disabled
                    before={
                        <Search
                            placeholder="Поиск"
                            value={search}
                            onChange={(e: ChangeEvent<HTMLInputElement>) =>
                                setSearch((e.currentTarget ?? e.target).value)
                            }
                            onKeyDown={(e: { key: string }) =>
                                e.key === 'Enter' ? setQuery({ name: 'query', values: search }) : undefined
                            }
                            onIconClick={() => props.openModalFilter()}
                        />
                    }
                    after={
                        <CellButton
                            onClick={() => setQuery({ name: 'query', values: search })}
                            after={<Icon24Search />}>
                            Поиск
                        </CellButton>
                    }
                />
            )}
            {!isDesktop && (
                <div>
                    <Search
                        placeholder="Поиск"
                        value={search}
                        onChange={(e: { currentTarget: any; target: any }) =>
                            setSearch((e.currentTarget ?? e.target).value)
                        }
                        icon={<Icon24Filter />}
                        onIconClick={() => props.openModalFilter()}
                    />
                    <Tabs>
                        <HorizontalScroll
                            getRef={horizontalScrollRef}
                            getScrollToLeft={(i: number) => i - 120}
                            getScrollToRight={(i: number) => i + 120}>
                            {Object.values(isAdmin ? buttonsAdmin : buttonsCatalog)
                                .filter((x) => x.id)
                                .map((x) => (
                                    <TabsItem
                                        key={x.id}
                                        id={`TAB_MENU_${x.id}`}
                                        selected={location.pathname === `/${mainPath}/${x.id}`}
                                        onClick={onClickTab(x)}>
                                        {x.title}
                                    </TabsItem>
                                ))}
                        </HorizontalScroll>
                    </Tabs>
                </div>
            )}
            {props.children}
            <div style={{ position: 'relative' }}>
                <div
                    ref={loader}
                    style={{
                        position: 'absolute',
                        top: '-10rem',
                        left: '0',
                        width: '100%',
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'center',
                    }}>
                    {isFetching && !isEnd && <Spinner size="medium" className="LOADER" style={{ margin: '11rem 0' }} />}
                </div>
            </div>
        </div>
    );
});
