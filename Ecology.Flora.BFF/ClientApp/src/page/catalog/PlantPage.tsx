import { Icon20ArrowUturnLeftOutline, Icon24Home } from '@vkontakte/icons';
import {
    Avatar,
    Button,
    FormItem,
    Group,
    Header,
    InfoRow,
    RichCell,
    SimpleCell,
    Spinner,
    useAdaptivity,
    ViewWidth,
} from '@vkontakte/vkui';
import { GeocoderAPI, PlantAPI } from 'api';
import { ImageSize } from 'api/enums';
import { GeocodeInfoView, PlantView, RedBookPlantView } from 'api/view';
import { SadHamburger } from 'component/SadHamburger/SadHamburger';
import { useQuery } from 'hook/useQuery';
import { Map, Marker, ZoomControl } from 'pigeon-maps';
import { osm } from 'pigeon-maps/providers';
import * as React from 'react';
import { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { fileDataToUrl } from 'utils/utils';

interface Props {}

export const PlantPage = (props: Props) => {
    const history = useHistory();
    const { query } = useQuery();
    const [plant, setPlant] = useState<PlantView | null>(null);
    const [notFound, setNotFound] = useState<boolean>(false);
    const [plantInfo, setPlantInfo] = useState<GeocodeInfoView>();

    const { viewWidth } = useAdaptivity();
    const isDesktop = (viewWidth ?? ViewWidth.DESKTOP) >= ViewWidth.TABLET;

    useEffect(() => {
        PlantAPI.get(query.get('plantId')!)
            .then((x) => {
                setPlant(x);
            })
            .catch(() => setNotFound(true));
    }, []);

    useEffect(() => {
        if (plant) {
            GeocoderAPI.info(plant.position).then((res) => {
                setPlantInfo(res);
            });
        }
    }, [plant]);

    const toKK = (kk: RedBookPlantView) => `${kk.rarityCategory.name.en} - ${kk.rarityCategory.name.ru}`;
    const upFirst = (data?: string) => (data?.charAt(0).toUpperCase() ?? '') + data?.slice(1);

    return plant == null ? (
        <div>
            {!notFound ? (
                <Spinner size="large" style={{ margin: '20px 0' }} />
            ) : (
                <SadHamburger outerBackgroundColor="white" />
            )}
        </div>
    ) : (
        <Group
            mode="plain"
            header={
                <Button
                    size="l"
                    mode="tertiary"
                    before={<Icon20ArrowUturnLeftOutline />}
                    onClick={() =>
                        history.push({
                            pathname: '/catalog/plants',
                        })
                    }>
                    Назад
                </Button>
            }>
            <RichCell
                disabled
                multiline
                before={
                    <Avatar
                        size={250}
                        mode="app"
                        src={plant.avatarId ? fileDataToUrl(plant.avatarId, ImageSize.Medium) : '/notAvatar.jpg'}
                    />
                }
                text={plant.name.ru}
                caption={
                    plant.redBookPlants.length < 1 &&
                    plant.redBookPlants.map((x: RedBookPlantView) => `${x.redBook.name.ru}: ${toKK(x)} `)
                }>
                <SimpleCell
                    style={{ padding: '0' }}
                    disabled
                    before={plant.name.la}
                    after={
                        <SimpleCell
                            disabled
                            before={
                                plant.plantCharacteristicValues.find(
                                    (x) => x.characteristic.name.la === 'OriginSpecies',
                                )?.values[0].name.ru
                            }
                            after={
                                <Icon24Home
                                    style={{
                                        color:
                                            plant.plantCharacteristicValues.find(
                                                (x) => x.characteristic.name.la === 'OriginSpecies',
                                            )?.values[0].name.ru === 'аборигенное'
                                                ? 'var(--dynamic_green)'
                                                : 'var(--dynamic_red)',
                                    }}
                                />
                            }
                        />
                    }
                />
            </RichCell>
            <Group mode="plain">
                <Header mode="secondary">Информация о растении!</Header>
                {plantInfo && (
                    <SimpleCell multiline disabled>
                        <InfoRow header="Местонахождение">
                            {plantInfo.path}
                            <br />
                            {plantInfo.description.charAt(0).toUpperCase()}
                            {plantInfo.description.slice(1)}
                        </InfoRow>
                    </SimpleCell>
                )}
                {plant.plantCharacteristicValues.map((x) => (
                    <SimpleCell key={x.characteristic.id} multiline disabled>
                        <InfoRow header={x.characteristic.name.ru}>
                            {x.values.map(
                                (y, index: number) =>
                                    `${upFirst(y.name.ru)}${index === x.values.length - 1 ? '' : ', '}`,
                            )}
                        </InfoRow>
                    </SimpleCell>
                ))}
                {plant.position.longitude !== 0 && plant.position.latitude !== 0 && (
                    <FormItem top="Позиция на карте">
                        <div style={{ width: '100%', height: isDesktop ? '70vh' : 'calc(100vh - 61px - 48px)' }}>
                            <Map
                                defaultCenter={[plant.position.latitude, plant.position.longitude]}
                                defaultZoom={9}
                                provider={osm}>
                                <ZoomControl />
                                <Marker
                                    anchor={[plant.position.latitude, plant.position.longitude]}
                                    color="rgb(0, 255, 0)"
                                    width={30}
                                />
                            </Map>
                        </div>
                    </FormItem>
                )}
            </Group>
        </Group>
    );
};
