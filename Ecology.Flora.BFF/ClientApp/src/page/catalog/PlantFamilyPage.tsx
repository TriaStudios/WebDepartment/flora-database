import { PlantFamilyAPI } from 'api';
import { PlantFamilyFilter } from 'api/binding';
import { PlantFamilyView } from 'api/view';
import { PlantFamilyCell } from 'component/catalog/plantFamily/PlantFamilyCell';
import { ListPage } from 'page/catalog/ListPage';
import React, { useState } from 'react';

interface Props {
    isReload: boolean;
}

export const PlantFamilyPage = (props: Props) => {
    const [subcategories, setSubcategories] = useState<PlantFamilyView[]>([]);
    const [page, setPage] = useState<number>(0);
    const countOnPage = 10;

    const onReset = () => {
        setSubcategories([]);
        setPage(0);
    };

    const onFetch = (ended: Function, fetched: Function, query: URLSearchParams) => {
        PlantFamilyAPI.search({
            filter: {
                plantClassIds: query.getAll('plantClassIds'),
                query: query.get('query') ?? '',
            } as PlantFamilyFilter,
            pagination: { page, count: countOnPage },
        }).then((res) => {
            if (res.elements.length === 0) {
                ended();
                fetched();
                return;
            }

            setSubcategories((x) => [...x, ...res.elements]);
            setPage((x) => x + 1);
            fetched();
        });
    };

    return (
        <ListPage onFetch={onFetch} onReset={onReset} title="Подклассы" isReload={props.isReload}>
            {subcategories.map((x) => (
                <PlantFamilyCell key={x.id} plantFamily={x} />
            ))}
            {subcategories.length === 0 ? <p style={{ textAlign: 'center', color: 'black' }}>Ничего нет</p> : undefined}
        </ListPage>
    );
};
