import { PlantAPI, PlantCharacteristicValueAPI } from 'api';
import { PlantFilter } from 'api/binding';
import { PlantCharacteristicValuesView, PlantView } from 'api/view';
import { PlantCell } from 'component/catalog/plant/PlantCell';
import { ListPage } from 'page/catalog/ListPage';
import React, { useEffect, useState } from 'react';

interface Props {
    isReload: boolean;
}

export const PlantsPage = (props: Props) => {
    const [plants, setPlants] = useState<PlantView[]>([]);
    const [page, setPage] = useState<number>(0);
    const [characteristics, setCharacteristics] = useState<PlantCharacteristicValuesView[]>([]);
    const countOnPage = 10;

    useEffect(() => {
        if (!characteristics) {
            PlantCharacteristicValueAPI.getAll().then(setCharacteristics);
        }
    }, [characteristics]);

    const onReset = () => {
        setPlants([]);
        setPage(0);
    };

    const onFetch = (ended: () => void, fetched: () => void, query: URLSearchParams) => {
        const filter: PlantFilter = {
            plantCharacteristicValueIds: characteristics.flatMap((x) => query.getAll(x.characteristic.name.la!)),
            plantFamilyIds: query.getAll('plantFamilyIds'),
            rarityCategoryIds: query.getAll('rarityCategoryIds'),
            redBookIds: query.getAll('redBookIds'),
            authorIds: query.getAll('authorIds'),
            ooptIds: query.getAll('ooptIds'),
            query: query.get('query') ?? undefined,
        };
        PlantAPI.search({
            filter,
            pagination: { page, count: countOnPage },
        }).then((res) => {
            if (res.elements.length === 0) {
                ended();
                fetched();
                return;
            }

            setPlants((x) => [...x, ...res.elements]);
            setPage((x) => x + 1);
            fetched();
        });
    };

    return characteristics ? (
        <ListPage onReset={onReset} onFetch={onFetch} title="Растения" isReload={props.isReload}>
            {plants.map((x) => (
                <PlantCell key={x.id} plant={x} />
            ))}
            {plants.length === 0 ? <p style={{ textAlign: 'center', color: 'black' }}>Ничего нет</p> : undefined}
        </ListPage>
    ) : (
        <></>
    );
};
