import { PlantClassAPI } from 'api';
import { PlantClassView } from 'api/view';
import { PlantClassCell } from 'component/catalog/plantClass/PlantClassCell';
import { ListPage } from 'page/catalog/ListPage';
import React, { useState } from 'react';

interface Props {
    isReload: boolean;
}

export const PlantClassesPage = (props: Props) => {
    const [categories, setCategories] = useState<PlantClassView[]>([]);
    const [page, setPage] = useState<number>(0);
    const countOnPage = 10;

    const onReset = () => {
        setCategories([]);
        setPage(0);
    };

    const onFetch = (ended: () => void, fetched: () => void, query: URLSearchParams) => {
        PlantClassAPI.search({
            filter: {
                plantCladeIds: query.getAll('plantCladeIds'),
                query: query.get('query') ?? undefined,
            },
            pagination: { page, count: countOnPage },
        }).then((res) => {
            if (res.elements.length === 0) {
                ended();
                fetched();
                return;
            }

            setCategories((x) => [...x, ...res.elements]);
            setPage((x) => x + 1);
            fetched();
        });
    };

    return (
        <ListPage onReset={onReset} onFetch={onFetch} title="Классы" isReload={props.isReload}>
            {categories.map((x) => (
                <PlantClassCell key={x.id} category={x} />
            ))}
            {categories.length === 0 ? <p style={{ textAlign: 'center', color: 'black' }}>Ничего нет</p> : undefined}
        </ListPage>
    );
};
