import { Header } from '@vkontakte/vkui';
import { ListPageBase } from 'page/ListPageBase';
import React, { ReactNode } from 'react';

interface Props {
    onFetch: (ended: () => void, fetched: () => void, query: URLSearchParams) => void;
    onReset: () => void;
    isReload: boolean;
    children: ReactNode;
    title: string;
}

export const ListPage = (props: Props) => (
    <ListPageBase onFetch={props.onFetch} onReset={props.onReset} isReload={props.isReload}>
        <Header mode="secondary">{props.title}</Header>
        {props.children}
    </ListPageBase>
);
