import { Group } from '@vkontakte/vkui';
import { SadHamburger } from 'component/SadHamburger/SadHamburger';
import { PlantCladePage } from 'page/catalog/PlantCladePage';
import { PlantClassesPage } from 'page/catalog/PlantClassesPage';
import { PlantFamilyPage } from 'page/catalog/PlantFamilyPage';
import { PlantPage } from 'page/catalog/PlantPage';
import { PlantsPage } from 'page/catalog/PlantsPage';
import React from 'react';
import { Redirect, Route, Switch, useRouteMatch } from 'react-router-dom';

interface Props {
    isReload: boolean;
}

export const CatalogPage = (props: Props) => {
    let { path } = useRouteMatch();
    if (path === '/') path = '';

    return (
        <>
            <Switch>
                <Route exact path={path} render={() => <Redirect to={`${path}/plant-clade`} />} />
                <Route exact path={`${path}/plant-clade`}>
                    <Group>
                        <PlantCladePage isReload={props.isReload} />
                    </Group>
                </Route>
                <Route exact path={`${path}/plant-classes`}>
                    <Group>
                        <PlantClassesPage isReload={props.isReload} />
                    </Group>
                </Route>
                <Route exact path={`${path}/plant-families`}>
                    <Group>
                        <PlantFamilyPage isReload={props.isReload} />
                    </Group>
                </Route>
                <Route exact path={`${path}/plants`}>
                    <Group>
                        <PlantsPage isReload={props.isReload} />
                    </Group>
                </Route>
                <Route exact path={`${path}/plant`}>
                    <Group>
                        <PlantPage />
                    </Group>
                </Route>
                <Route component={() => <SadHamburger outerBackgroundColor="white" />} />
            </Switch>
        </>
    );
};
