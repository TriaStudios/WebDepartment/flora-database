import { PlantCladeAPI } from 'api';
import { PlantCladeView } from 'api/view';
import { PlantCladeCell } from 'component/catalog/plantClade/PlantCladeCell';
import { ListPage } from 'page/catalog/ListPage';
import React, { useState } from 'react';

interface Props {
    isReload: boolean;
}

export const PlantCladePage = (props: Props) => {
    const [plantClade, setPlantClade] = useState<PlantCladeView[]>([]);
    const [page, setPage] = useState<number>(0);
    const countOnPage = 10;

    const onReset = () => {
        setPlantClade([]);
        setPage(0);
    };

    const onFetch = (ended: () => void, fetched: () => void, query: URLSearchParams) => {
        PlantCladeAPI.search({
            filter: {
                query: query.get('query') ?? '',
            },
            pagination: { page, count: countOnPage },
        }).then((res) => {
            if (res.elements.length === 0) {
                ended();
                fetched();
                return;
            }

            setPlantClade((x) => [...x, ...res.elements]);
            setPage((x) => x + 1);
            fetched();
        });
    };

    return (
        <ListPage onReset={onReset} onFetch={onFetch} title="Отделы" isReload={props.isReload}>
            {plantClade.map((x) => (
                <PlantCladeCell key={x.id} department={x} />
            ))}
            {plantClade.length === 0 ? <p style={{ textAlign: 'center', color: 'black' }}>Ничего нет</p> : undefined}
        </ListPage>
    );
};
