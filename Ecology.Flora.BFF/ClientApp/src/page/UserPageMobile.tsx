import { CellButton, Group, Panel, Title } from '@vkontakte/vkui';
import { AuthAPI } from 'api';
import { LoginForm } from 'component/form/LoginForm';
import React from 'react';
import { connect, ConnectedProps } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { RootState } from 'store';

const connector = connect(
    (state: RootState) => ({
        user: state.user.user,
        isLogin: state.user.loaded,
    }),
    {},
);

interface Props extends ConnectedProps<typeof connector> {}

export const UserPageMobile = connector((props: Props) => {
    const history = useHistory();

    const logout = () => {
        AuthAPI.logout();
        history.go(0);
    };

    return (
        <Group>
            {props.isLogin ? (
                <Panel style={{ textAlign: 'center', padding: '1rem 0' }}>
                    <Title weight="heavy" level="1">
                        {props.user?.name?.ru}
                    </Title>
                    <CellButton centered onClick={logout}>
                        Выйти
                    </CellButton>
                </Panel>
            ) : (
                <LoginForm />
            )}
        </Group>
    );
});
