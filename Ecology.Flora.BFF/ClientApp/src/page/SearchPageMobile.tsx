import { Icon24ChevronRight } from '@vkontakte/icons';
import { CellButton, Group, Input, SimpleCell, SizeType } from '@vkontakte/vkui';
import { useTotalSearch } from 'hook/useTotalSearch';
import React, { ChangeEvent, useState } from 'react';
import { connect, ConnectedProps } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { RootState } from 'store';
import { buttonsCatalog } from 'utils/menuButtons';

const connector = connect((state: RootState) => ({}), {});

interface Props extends ConnectedProps<typeof connector> {}

export const SearchPageMobile = connector((props: Props) => {
    const [search, setSearch] = useState('');
    const history = useHistory();
    const searchData = useTotalSearch(search);

    return (
        <Group>
            <Input
                sizeY={SizeType.COMPACT}
                sizeX={SizeType.COMPACT}
                placeholder="Поиск"
                value={search}
                onChange={(e: ChangeEvent<HTMLInputElement>) => setSearch((e.currentTarget ?? e.target).value)}
                style={{
                    margin: '0 1rem',
                }}
            />
            {Object.entries(buttonsCatalog)
                .filter((x) => x[1].id)
                .map(([name, y]) => (
                    <div key={y.id}>
                        <SimpleCell
                            sizeY={SizeType.COMPACT}
                            sizeX={SizeType.COMPACT}
                            disabled
                            before={y.Icon}
                            after={
                                <CellButton
                                    sizeY={SizeType.COMPACT}
                                    sizeX={SizeType.COMPACT}
                                    style={{ borderRadius: '0 0 10px 10px' }}
                                    after={<Icon24ChevronRight />}
                                    onClick={() => {
                                        history.push({
                                            pathname: `/catalog/${y.id}`,
                                            search: `?query=${search}`,
                                        });
                                        setSearch('');
                                    }}>
                                    Развернуть
                                </CellButton>
                            }
                            style={{ margin: '0' }}>
                            {y.title}
                        </SimpleCell>
                        {searchData[name]?.map((x) => (
                            <CellButton
                                key={x.id}
                                sizeY={SizeType.COMPACT}
                                sizeX={SizeType.COMPACT}
                                style={{
                                    borderRadius: '0',
                                    padding: '0 2rem',
                                    margin: '0',
                                    color: 'black',
                                }}
                                onClick={() => {
                                    history.push({
                                        pathname: `/catalog/${y.child}`,
                                        search: `?${y.fieldFilter}=${x.id}`,
                                    });
                                    setSearch('');
                                }}>
                                {x.name.ru} ({x.name.la})
                            </CellButton>
                        ))}
                        {searchData[name]?.length === 0 ? (
                            <p style={{ textAlign: 'center', color: 'black' }}>Ничего нет</p>
                        ) : undefined}
                    </div>
                ))}
        </Group>
    );
});
