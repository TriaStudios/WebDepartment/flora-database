import { Icon28AddOutline } from '@vkontakte/icons';
import { CellButton, Header, SimpleCell } from '@vkontakte/vkui';
import { ListPageBase } from 'page/ListPageBase';
import React, { ReactNode } from 'react';
import { useHistory } from 'react-router-dom';

interface Props {
    onFetch: (ended: () => void, fetched: () => void, query: URLSearchParams) => void;
    onReset: () => void;
    isReload: boolean;
    children: ReactNode;
    title: string;
    path: string;
}

export const ListPage = (props: Props) => {
    const history = useHistory();

    return (
        <ListPageBase onFetch={props.onFetch} onReset={props.onReset} isReload={props.isReload}>
            <SimpleCell
                disabled
                before={<Header mode="secondary">{props.title}</Header>}
                after={
                    <CellButton
                        after={<Icon28AddOutline />}
                        centered
                        onClick={() => {
                            history.push({
                                pathname: `${props.path}/add`,
                            });
                        }}
                        style={{ maxWidth: '10rem', marginRight: 'auto', marginLeft: 'auto' }}>
                        Добавить
                    </CellButton>
                }
            />
            {props.children}
        </ListPageBase>
    );
};
