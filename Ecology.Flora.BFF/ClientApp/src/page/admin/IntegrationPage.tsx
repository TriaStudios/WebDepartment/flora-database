import { CellButton, File, Header, SimpleCell, Title } from '@vkontakte/vkui';
import { IntegrationApi } from 'api';
import React, { ChangeEvent, useState } from 'react';
import { connect, ConnectedProps } from 'react-redux';
import { RootState } from 'store';

const connector = connect((state: RootState) => ({}), {});

interface Props extends ConnectedProps<typeof connector> {}

export const IntegrationPage = connector((props: Props) => {
    const [error, setError] = useState('');
    const [file, setFile] = useState<File>();

    const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
        if (e.currentTarget.files && e.currentTarget.files.length > 0) {
            setFile(e.currentTarget.files[0]);
        }
    };

    const handleIntegrate = () => {
        if (file) {
            IntegrationApi.integrate(file).then((r) => setError(r));
        }
    };

    return (
        <div>
            <Header mode="secondary">Интеграция</Header>
            <SimpleCell
                disabled
                after={
                    <Title weight="semibold" level="3">
                        {file?.name}
                    </Title>
                }>
                <File mode="outline" onChange={handleChange} />
            </SimpleCell>
            <CellButton centered onClick={handleIntegrate}>
                Запустить интеграцию
            </CellButton>
            <Title weight="heavy" level="1">
                {error}
            </Title>
        </div>
    );
});
