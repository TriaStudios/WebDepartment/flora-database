import { Icon28MentionOutline } from '@vkontakte/icons';
import { Group } from '@vkontakte/vkui';
import {
    MetricAPI,
    OoptAPI,
    PlantAPI,
    PlantCladeAPI,
    PlantClassAPI,
    PlantFamilyAPI,
    RarityCategoryAPI,
    RedBookAPI,
    UserAPI,
} from 'api';
import { SadHamburger } from 'component/SadHamburger/SadHamburger';
import { PlantAddPage } from 'page/admin/add-pages/PlantAddPage';
import { PlantFamilyAddPage } from 'page/admin/add-pages/PlantFamilyAddPage';
import { DefaultPage } from 'page/admin/DefaultPage';
import { MetricEditPage } from 'page/admin/edit-pages/MetricEditPage';
import { NameEditPage } from 'page/admin/edit-pages/NameEditPage';
import { OoptEditPage } from 'page/admin/edit-pages/OoptEditPage';
import { PlantClassEditPage } from 'page/admin/edit-pages/PlantClassEditPage';
import { PlantFamilyEditPage } from 'page/admin/edit-pages/PlantFamilyEditPage';
import { UserEditPage } from 'page/admin/edit-pages/UserEditPage';
import { IntegrationPage } from 'page/admin/IntegrationPage';
import * as React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

interface Props {
    isReload: boolean;
}

export const AdminPage = (p: Props) => (
    <>
        <Switch>
            <Route exact path="/admin" render={() => <Redirect to="/admin/users" />} />
            <Route exact path="/admin/integration">
                <Group>
                    <IntegrationPage />
                </Group>
            </Route>
            <Route path="/admin/oopts">
                <Group>
                    <DefaultPage
                        isReload={p.isReload}
                        onSearch={OoptAPI.searchLinks}
                        cellIcon={Icon28MentionOutline}
                        getCellTitle={(item) => item.name.ru}
                        title="ООПТ"
                        path="/admin/oopts"
                        editPage={(props) => <OoptEditPage id={props.id} />}
                        addPage={() => <OoptEditPage />}
                    />
                </Group>
            </Route>
            <Route path="/admin/metrics">
                <Group>
                    <DefaultPage
                        isReload={p.isReload}
                        onSearch={MetricAPI.searchLinks}
                        cellIcon={Icon28MentionOutline}
                        getCellTitle={(item) => item.name.ru}
                        title="Метрики"
                        path="/admin/metrics"
                        editPage={(props) => <MetricEditPage id={props.id} />}
                        addPage={() => <MetricEditPage />}
                    />
                </Group>
            </Route>
            <Route path="/admin/plant-clades">
                <Group>
                    <DefaultPage
                        isReload={p.isReload}
                        onSearch={PlantCladeAPI.searchLinks}
                        cellIcon={Icon28MentionOutline}
                        getCellTitle={(item) => `${item.name.ru} (${item.name.la})`}
                        title="Отделы"
                        path="/admin/plant-clades"
                        editPage={(props) => <NameEditPage Searcher={PlantCladeAPI} id={props.id} />}
                        addPage={() => <NameEditPage Searcher={PlantCladeAPI} />}
                    />
                </Group>
            </Route>
            <Route path="/admin/plant-classes">
                <Group>
                    <DefaultPage
                        isReload={p.isReload}
                        onSearch={PlantClassAPI.searchLinks}
                        cellIcon={Icon28MentionOutline}
                        getCellTitle={(item) => `${item.name.ru} (${item.name.la})`}
                        title="Классы"
                        path="/admin/plant-classes"
                        editPage={PlantClassEditPage}
                        addPage={() => <PlantClassEditPage />}
                    />
                </Group>
            </Route>
            <Route path="/admin/plant-families">
                <Group>
                    <DefaultPage
                        isReload={p.isReload}
                        onSearch={PlantFamilyAPI.searchLinks}
                        cellIcon={Icon28MentionOutline}
                        getCellTitle={(item) => `${item.name.ru} (${item.name.la})`}
                        title="Семейства растений"
                        path="/admin/plant-families"
                        editPage={PlantFamilyEditPage}
                        addPage={() => <PlantFamilyAddPage />}
                    />
                </Group>
            </Route>
            <Route path="/admin/plants">
                <Group>
                    <DefaultPage
                        isReload={p.isReload}
                        onSearch={PlantAPI.search}
                        getCellTitle={(item) => `${item.name.ru} (${item.name.la})`}
                        getImage={(item) => item.avatarId}
                        title="Растения"
                        path="/admin/plants"
                        editPage={PlantAddPage}
                        addPage={() => <PlantAddPage />}
                    />
                </Group>
            </Route>
            <Route path="/admin/users">
                <Group>
                    <DefaultPage
                        isReload={p.isReload}
                        onSearch={UserAPI.searchLinks}
                        cellIcon={Icon28MentionOutline}
                        getCellTitle={(item: any) => item.name.ru}
                        title="Пользователи"
                        path="/admin/users"
                        editPage={UserEditPage}
                        addPage={() => <UserEditPage />}
                    />
                </Group>
            </Route>
            <Route path="/admin/red-books">
                <Group>
                    <DefaultPage
                        isReload={p.isReload}
                        onSearch={RedBookAPI.searchLinks}
                        cellIcon={Icon28MentionOutline}
                        getCellTitle={(item) => item.name.ru}
                        title="Красная книга"
                        path="/admin/red-books"
                        editPage={(props) => <NameEditPage Searcher={RedBookAPI} id={props.id} />}
                        addPage={() => <NameEditPage Searcher={RedBookAPI} />}
                    />
                </Group>
            </Route>
            <Route path="/admin/rarity-categories">
                <Group>
                    <DefaultPage
                        isReload={p.isReload}
                        onSearch={RarityCategoryAPI.searchLinks}
                        cellIcon={Icon28MentionOutline}
                        getCellTitle={(item) => item.name.ru}
                        title="Категория редкости"
                        path="/admin/rarity-categories"
                        editPage={(props) => <NameEditPage Searcher={RarityCategoryAPI} id={props.id} />}
                        addPage={() => <NameEditPage Searcher={RarityCategoryAPI} />}
                    />
                </Group>
            </Route>
            <Route component={() => <SadHamburger outerBackgroundColor="white" />} />
        </Switch>
    </>
);
