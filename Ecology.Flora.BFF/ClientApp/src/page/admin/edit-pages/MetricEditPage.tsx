import { Icon28ArrowLeftOutline } from '@vkontakte/icons';
import { CellButton, FormItem, FormLayout, FormLayoutGroup, Group, Header, IconButton } from '@vkontakte/vkui';
import { MetricAPI } from 'api';
import { MetricBinding } from 'api/binding';
import { LocalizeProperty } from 'api/models';
import { LocalizableNameFormItem } from 'component/form/LocalizableNameFormItem';
import { TableMetricRaspredsFormItem } from 'component/form/TableMetricRaspredsFormItem';
import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';

interface Props {
    id?: string;
}

export function MetricEditPage(props: Props) {
    const history = useHistory();
    const [id, setId] = useState<string>();
    const [name, setName] = useState<LocalizeProperty>();
    const [metricRaspreds, setMetricRaspreds] = useState<string[]>([]);

    useEffect(() => {
        if (!props.id) {
            MetricAPI.put({
                name: { ru: 'Metric' },
            }).then((res) => {
                history.push({
                    pathname: `/admin/metrics/edit/${res}`,
                });
            });
        } else {
            MetricAPI.get(props.id).then((res) => {
                setId(res.id);
                setName(res.name);
                setMetricRaspreds(res.metricRaspreds);
            });
        }
    }, [props.id]);

    const onChange = () => {
        MetricAPI.patch(createModel()).then(() => {
            history.goBack();
        });
    };

    const createModel = () =>
        ({
            id,
            name,
        } as MetricBinding);

    return id ? (
        <Group
            mode="plain"
            header={
                <>
                    <IconButton onClick={() => history.goBack()}>
                        <Icon28ArrowLeftOutline />
                    </IconButton>
                    <Header>Метрика</Header>
                </>
            }>
            <FormLayout>
                <LocalizableNameFormItem name={name} setName={setName} title="Имя" mode="horizontal" />
                <TableMetricRaspredsFormItem value={metricRaspreds} onChange={setMetricRaspreds} metricId={id} />
                <FormLayoutGroup mode="horizontal">
                    <FormItem>
                        <CellButton onClick={onChange} centered>
                            Изменить
                        </CellButton>
                    </FormItem>
                    <FormItem>
                        <CellButton
                            style={{ color: 'var(--dynamic_red)' }}
                            onClick={() =>
                                MetricAPI.delete(id).then(() => {
                                    history.goBack();
                                })
                            }
                            centered>
                            Удалить
                        </CellButton>
                    </FormItem>
                </FormLayoutGroup>
            </FormLayout>
        </Group>
    ) : (
        <></>
    );
}
