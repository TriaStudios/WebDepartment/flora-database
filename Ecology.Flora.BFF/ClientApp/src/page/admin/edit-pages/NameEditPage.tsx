import { Icon28ArrowLeftOutline } from '@vkontakte/icons';
import { CellButton, FormItem, FormLayout, FormLayoutGroup, Group, Header, IconButton } from '@vkontakte/vkui';
import { BaseBinding } from 'api/binding';
import { CRUD } from 'api/interfaces';
import { LocalizeProperty } from 'api/models';
import { BaseView } from 'api/view';
import { LocalizableNameFormItem } from 'component/form/LocalizableNameFormItem';
import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';

interface Props {
    id?: string;
    Searcher?: CRUD<BaseView, BaseBinding>;
}

export function NameEditPage(props: Props) {
    const history = useHistory();
    const [id, setId] = useState<string>();
    const [name, setName] = useState<LocalizeProperty>();

    useEffect(() => {
        if (!props.id) return;

        props.Searcher?.get(props.id).then((res) => {
            setId(res.id);
            setName(res.name);
        });
    }, [props.id]);

    const createModel = () => ({
        id,
        name,
    });

    return (
        <Group
            mode="plain"
            header={
                <>
                    <IconButton onClick={() => history.goBack()}>
                        <Icon28ArrowLeftOutline />
                    </IconButton>
                    <Header>Характеристика</Header>
                </>
            }>
            <FormLayout>
                <LocalizableNameFormItem name={name} setName={setName} title="Имя" mode="horizontal" />
                <FormLayoutGroup mode="horizontal">
                    <FormItem>
                        <CellButton
                            onClick={() => {
                                if (props.id) {
                                    props.Searcher?.patch(createModel()).then(() => {
                                        history.goBack();
                                    });
                                } else {
                                    props.Searcher?.put(createModel()).then(() => {
                                        history.goBack();
                                    });
                                }
                            }}
                            centered>
                            {props.id ? 'Изменить' : 'Добавить'}
                        </CellButton>
                    </FormItem>
                    {props.id && (
                        <FormItem>
                            <CellButton
                                style={{ color: 'var(--dynamic_red)' }}
                                onClick={() =>
                                    id &&
                                    props.Searcher?.delete(id).then(() => {
                                        history.goBack();
                                    })
                                }
                                centered>
                                Удалить
                            </CellButton>
                        </FormItem>
                    )}
                </FormLayoutGroup>
            </FormLayout>
        </Group>
    );
}
