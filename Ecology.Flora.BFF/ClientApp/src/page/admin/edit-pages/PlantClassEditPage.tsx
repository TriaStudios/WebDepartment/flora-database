import { Icon28ArrowLeftOutline } from '@vkontakte/icons';
import { CellButton, FormItem, FormLayout, FormLayoutGroup, Group, Header, IconButton, Select } from '@vkontakte/vkui';
import { PlantCladeAPI, PlantClassAPI } from 'api';
import { LocalizeProperty } from 'api/models';
import { LocalizableNameFormItem } from 'component/form/LocalizableNameFormItem';
import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';

interface Props {
    id?: string;
}

export function PlantClassEditPage(props: Props) {
    const history = useHistory();
    const [name, setName] = useState<LocalizeProperty>();
    const [selectedDepartment, setSelectedDepartment] = useState('0');

    const [plantClade, setPlantClade] = useState<Array<{ label: string; value: string }>>([]);

    useEffect(() => {
        PlantCladeAPI.search().then((res) => {
            setPlantClade(
                res.elements.map(
                    (x) => ({ label: x.name.ru, value: x.id.toString() } as { label: string; value: string }),
                ),
            );
        });
        if (!props.id) return;

        PlantClassAPI.get(props.id).then((res) => {
            setName(res.name);
            setSelectedDepartment(res.plantClade.id.toString());
        });
    }, [props.id]);

    return (
        <Group
            mode="plain"
            header={
                <>
                    <IconButton onClick={() => history.goBack()}>
                        <Icon28ArrowLeftOutline />
                    </IconButton>
                    <Header>Класс растения</Header>
                </>
            }>
            <FormLayout>
                <LocalizableNameFormItem name={name} setName={setName} title="Имя" mode="horizontal" />
                <FormItem top="Отдел">
                    <Select
                        value={selectedDepartment}
                        options={plantClade}
                        onChange={(event: { currentTarget: any; target: any }) => {
                            setSelectedDepartment((event.currentTarget ?? event.target).value);
                        }}
                    />
                </FormItem>
                <FormLayoutGroup mode="horizontal">
                    <FormItem>
                        <CellButton
                            onClick={() => {
                                if (props.id) {
                                    PlantClassAPI.patch({
                                        plantCladeId: selectedDepartment,
                                        id: props.id,
                                        name,
                                    }).then(() => {
                                        history.goBack();
                                    });
                                } else {
                                    PlantClassAPI.put({
                                        plantCladeId: selectedDepartment,
                                        name,
                                    }).then(() => {
                                        history.goBack();
                                    });
                                }
                            }}
                            centered>
                            {props.id ? 'Изменить' : 'Добавить'}
                        </CellButton>
                    </FormItem>
                    {props.id && (
                        <FormItem>
                            <CellButton
                                style={{ color: 'var(--dynamic_red)' }}
                                onClick={() => {
                                    if (props.id) {
                                        PlantClassAPI.delete(props.id).then(() => {
                                            history.goBack();
                                        });
                                    }
                                }}
                                centered>
                                Удалить
                            </CellButton>
                        </FormItem>
                    )}
                </FormLayoutGroup>
            </FormLayout>
        </Group>
    );
}
