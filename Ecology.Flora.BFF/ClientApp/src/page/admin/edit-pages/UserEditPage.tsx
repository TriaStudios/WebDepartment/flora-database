import { Icon28ArrowLeftOutline } from '@vkontakte/icons';
import { CellButton, FormItem, FormLayout, Group, Header, IconButton, Input } from '@vkontakte/vkui';
import { ChipsInputOption } from '@vkontakte/vkui/dist/components/ChipsInput/ChipsInput';
import { ChipsSelect } from '@vkontakte/vkui/dist/unstable';
import { UserAPI } from 'api';
import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';

interface Props {
    id?: string;
}

export const UserEditPage = (props: Props) => {
    const history = useHistory();
    const [username, setUsername] = useState('');
    const [passwordHash, setPasswordHash] = useState('');
    const [lastName, setLastName] = useState('');
    const [firstName, setFirstName] = useState('');
    const [middleName, setMiddleName] = useState('');
    const [email, setEmail] = useState('');
    const [selectedRoles, setSelectedRoles] = useState<ChipsInputOption[]>([]);

    const [roles, setRoles] = useState<ChipsInputOption[]>([]);
    const [userId, setUserId] = useState<string>();

    useEffect(() => {
        if (userId !== props.id) {
            setUserId(props.id);
        }
    }, [props.id]);

    useEffect(() => {
        UserAPI.roles().then((res) => {
            setRoles(res.map((x) => ({ label: x.name.ru ?? '', value: x.id })));
        });
        if (!userId) return;

        UserAPI.get(userId).then((res) => {
            setUsername(res.username);
            setLastName(res.name.ru?.split(' ')[0] ?? '');
            setFirstName(res.name.ru?.split(' ')[1] ?? '');
            setMiddleName(res.name.ru?.split(' ')[2] ?? '');
            setEmail(res.email);
            setSelectedRoles(res.roles.map((x) => ({ label: x.name.ru ?? '', value: x.id })));
        });
    }, [userId]);

    return (
        <Group
            mode="plain"
            header={
                <>
                    <IconButton onClick={() => history.goBack()}>
                        <Icon28ArrowLeftOutline />
                    </IconButton>
                    <Header>Пользователь</Header>
                </>
            }>
            <FormLayout>
                <FormItem top="Имя пользователя">
                    <Input
                        type="text"
                        value={username}
                        onChange={(e: { currentTarget: any; target: any }) =>
                            setUsername((e.currentTarget ?? e.target).value)
                        }
                    />
                </FormItem>
                <FormItem top="Пароль">
                    <Input
                        type="text"
                        value={passwordHash}
                        onChange={(e: { currentTarget: any; target: any }) =>
                            setPasswordHash((e.currentTarget ?? e.target).value)
                        }
                    />
                </FormItem>
                <FormItem top="Фамилия">
                    <Input
                        type="text"
                        value={lastName}
                        onChange={(e: { currentTarget: any; target: any }) =>
                            setLastName((e.currentTarget ?? e.target).value)
                        }
                    />
                </FormItem>
                <FormItem top="Имя">
                    <Input
                        type="text"
                        value={firstName}
                        onChange={(e: { currentTarget: any; target: any }) =>
                            setFirstName((e.currentTarget ?? e.target).value)
                        }
                    />
                </FormItem>
                <FormItem top="Отчество">
                    <Input
                        type="text"
                        value={middleName}
                        onChange={(e: { currentTarget: any; target: any }) =>
                            setMiddleName((e.currentTarget ?? e.target).value)
                        }
                    />
                </FormItem>
                <FormItem top="E-mail">
                    <Input
                        type="email"
                        value={email}
                        onChange={(e: { currentTarget: any; target: any }) =>
                            setEmail((e.currentTarget ?? e.target).value)
                        }
                    />
                </FormItem>
                <FormItem top="Роль">
                    <ChipsSelect value={selectedRoles} onChange={setSelectedRoles} options={roles} />
                </FormItem>
                <FormItem>
                    <CellButton
                        onClick={() => {
                            if (userId) {
                                UserAPI.patch({
                                    email,
                                    passwordHash,
                                    name: { ru: `${lastName} ${firstName} ${middleName}` },
                                    id: userId,
                                    username,
                                    roleIds: selectedRoles.filter((x) => x.value).map((x) => x.value as string),
                                }).then(() => {
                                    history.goBack();
                                });
                            } else {
                                UserAPI.put({
                                    email,
                                    passwordHash,
                                    name: { ru: `${lastName} ${firstName} ${middleName}` },
                                    username,
                                    roleIds: selectedRoles.filter((x) => x.value).map((x) => x.value as string),
                                }).then(() => {
                                    history.goBack();
                                });
                            }
                        }}
                        centered>
                        {userId ? 'Изменить' : 'Добавить'}
                    </CellButton>
                    {userId && (
                        <CellButton
                            style={{ color: 'var(--dynamic_red)' }}
                            onClick={() => {
                                if (userId) {
                                    UserAPI.delete(userId).then(() => {
                                        history.goBack();
                                    });
                                }
                            }}
                            centered>
                            Удалить
                        </CellButton>
                    )}
                </FormItem>
            </FormLayout>
        </Group>
    );
};
