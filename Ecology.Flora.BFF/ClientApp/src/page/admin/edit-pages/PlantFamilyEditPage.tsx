import { Icon28ArrowLeftOutline } from '@vkontakte/icons';
import {
    CellButton,
    CustomSelectOption,
    CustomSelectOptionProps,
    FormItem,
    FormLayout,
    FormLayoutGroup,
    Group,
    Header,
    IconButton,
    Select,
} from '@vkontakte/vkui';
import { PlantClassAPI, PlantFamilyAPI } from 'api';
import { PlantFamilyBinding } from 'api/binding';
import { LocalizeProperty } from 'api/models';
import { PlantClassView } from 'api/view';
import { LocalizableNameFormItem } from 'component/form/LocalizableNameFormItem';
import * as React from 'react';
import { ReactNode, useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';

type Props = { id: string };
export const PlantFamilyEditPage = (props: Props) => {
    const [name, setName] = useState<LocalizeProperty>();
    const [plantClassId, setplantClassId] = useState<string>();
    const [categories, setCategories] = useState<PlantClassView[]>([]);
    const history = useHistory();

    const edit = () => {
        if (!plantClassId) return;
        PlantFamilyAPI.patch({
            plantClassId,
            name,
            id: props.id,
        } as PlantFamilyBinding).then(() => {
            history.goBack();
        });
    };

    const remove = () => {
        PlantFamilyAPI.delete(props.id).then(() => {
            history.goBack();
        });
    };

    useEffect(() => {
        PlantClassAPI.search().then((res) => {
            setCategories(res.elements);
        });
        PlantFamilyAPI.get(props.id!).then((x) => {
            setplantClassId(x.plantClass.id);
            setName(x.name);
        });
    }, []);

    return (
        <Group
            mode="plain"
            header={
                <>
                    <IconButton onClick={() => history.goBack()}>
                        <Icon28ArrowLeftOutline />
                    </IconButton>
                    <Header>Семейство растения</Header>
                </>
            }>
            <FormLayout>
                <LocalizableNameFormItem name={name} setName={setName} title="Имя" mode="horizontal" />
                <FormItem
                    top="Класс"
                    bottom={plantClassId ? '' : 'Пожалуйста, укажите класс'}
                    status={plantClassId ? 'valid' : 'error'}>
                    <Select
                        onChange={(e: { currentTarget: any; target: any }) => {
                            setplantClassId((e.currentTarget ?? e.target).value);
                        }}
                        value={plantClassId}
                        options={[{ label: 'Не выбран', value: '' }].concat(
                            categories.map((x) => ({
                                label: `${x.name.ru}(${x.name.la})`,
                                value: x.id,
                            })),
                        )}
                        renderOption={({ option, ...restProps }: any) => <CustomSelectOption {...restProps} />}
                    />
                </FormItem>
                <FormLayoutGroup mode="horizontal">
                    <FormItem>
                        <CellButton centered onClick={() => edit()}>
                            Изменить
                        </CellButton>
                    </FormItem>
                    <FormItem>
                        <CellButton centered onClick={() => remove()}>
                            Удалить
                        </CellButton>
                    </FormItem>
                </FormLayoutGroup>
            </FormLayout>
        </Group>
    );
};
