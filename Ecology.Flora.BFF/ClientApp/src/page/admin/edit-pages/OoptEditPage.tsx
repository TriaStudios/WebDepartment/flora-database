import { Icon28ArrowLeftOutline } from '@vkontakte/icons';
import { CellButton, FormItem, FormLayout, FormLayoutGroup, Group, Header, IconButton, Input } from '@vkontakte/vkui';
import { OoptAPI } from 'api';
import { OoptBinding } from 'api/binding';
import { LocalizeProperty } from 'api/models';
import { LocalizableNameFormItem } from 'component/form/LocalizableNameFormItem';
import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';

interface Props {
    id?: string;
}

export function OoptEditPage(props: Props) {
    const history = useHistory();
    const [id, setId] = useState<string>();
    const [name, setName] = useState<LocalizeProperty>();
    const [metric8, setMetric8] = useState<number>();
    const [square, setSquare] = useState<number>();

    useEffect(() => {
        if (!props.id) return;

        OoptAPI.get(props.id).then((res) => {
            setId(res.id);
            setName(res.name);
            setMetric8(res.metric8);
            setSquare(res.square);
        });
    }, [props.id]);

    const createModel = () =>
        ({
            id,
            name,
            metric8,
            square,
        } as OoptBinding);

    return (
        <Group
            mode="plain"
            header={
                <>
                    <IconButton onClick={() => history.goBack()}>
                        <Icon28ArrowLeftOutline />
                    </IconButton>
                    <Header>ООПТ</Header>
                </>
            }>
            <FormLayout>
                <LocalizableNameFormItem name={name} setName={setName} title="Имя" mode="horizontal" />
                <FormItem top="Степень трансформпрованности (количество тропинок, мусорок и тд)">
                    <Input
                        value={metric8}
                        onChange={(x: { currentTarget: any; target: any }) =>
                            setMetric8(+(x.currentTarget ?? x.target).value)
                        }
                    />
                </FormItem>
                <FormItem top="Площадь (га)">
                    <Input
                        value={square}
                        onChange={(x: { currentTarget: any; target: any }) =>
                            setSquare(+(x.currentTarget ?? x.target).value)
                        }
                    />
                </FormItem>
                <FormLayoutGroup mode="horizontal">
                    <FormItem>
                        <CellButton
                            onClick={() => {
                                if (props.id) {
                                    OoptAPI.patch(createModel()).then(() => {
                                        history.goBack();
                                    });
                                } else {
                                    OoptAPI.put(createModel()).then(() => {
                                        history.goBack();
                                    });
                                }
                            }}
                            centered>
                            {props.id ? 'Изменить' : 'Добавить'}
                        </CellButton>
                    </FormItem>
                    {props.id && (
                        <FormItem>
                            <CellButton
                                style={{ color: 'var(--dynamic_red)' }}
                                onClick={() =>
                                    id &&
                                    OoptAPI.delete(id).then(() => {
                                        history.goBack();
                                    })
                                }
                                centered>
                                Удалить
                            </CellButton>
                        </FormItem>
                    )}
                </FormLayoutGroup>
            </FormLayout>
        </Group>
    );
}
