import { Icon24Camera, Icon28ArrowLeftOutline } from '@vkontakte/icons';
import {
    Avatar,
    CellButton,
    CustomSelectOption,
    File,
    FormItem,
    FormLayoutGroup,
    Group,
    Header,
    IconButton,
    Select,
    SimpleCell,
    Spinner,
    useAdaptivity,
    ViewWidth,
} from '@vkontakte/vkui';
import { ChipsInputOption } from '@vkontakte/vkui/dist/components/ChipsInput/ChipsInput';
import { OoptAPI, PlantAPI, PlantCharacteristicValueAPI, PlantFamilyAPI, UserAPI } from 'api';
import { FilterBase, ImageBinding, PlantBinding, PlantCharacteristicValueBinding } from 'api/binding';
import { ImageSize } from 'api/enums';
import { ISearch } from 'api/interfaces';
import { LocalizeProperty } from 'api/models';
import { BaseView, PlantCharacteristicValuesView } from 'api/view';
import { Chipset } from 'component/form/Chipset';
import { LocalizableNameFormItem } from 'component/form/LocalizableNameFormItem';
import { SadHamburger } from 'component/SadHamburger/SadHamburger';
import { Map, Marker, ZoomControl } from 'pigeon-maps';
import { osm } from 'pigeon-maps/providers';
import * as React from 'react';
import { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { encode } from 'typescript-base64-arraybuffer';
import { fileDataToUrl } from 'utils/utils';

type Props = { id?: string };
export const PlantAddPage = (props: Props) => {
    const [plantFamilies, setPlantFamilies] = useState<BaseView[]>([]);
    const [authors, setAuthors] = useState<BaseView[]>([]);
    const [oopts, setOopts] = useState<BaseView[]>([]);
    const [characteristics, setCharacteristics] = useState<PlantCharacteristicValuesView[]>([]);

    const [name, setName] = useState<LocalizeProperty>();
    const [EPGroupComment, setEPGroupComment] = useState<LocalizeProperty>();
    const [IUCHComment, setIUCHComment] = useState<LocalizeProperty>();
    const [pointPlant, setPointPlant] = useState<[number, number]>([54.314192, 48.403132]);
    const [avatar, setAvatar] = useState<string>();
    const [avatarForSave, setAvatarForSave] = useState<[string, string]>();
    const [plantFamilyId, setPlantFamilyId] = useState<string>('');
    const [selectedOopts, setSelectedOopts] = useState<ChipsInputOption[]>([]);
    const [selectedRedBookPlants, setSelectedRedBookPlants] = useState<string[]>([]);
    const [selectedAuthors, setSelectedAuthors] = useState<ChipsInputOption[]>([]);
    const [selectedCharacteristics, setSelectedCharacteristics] = useState<{ [key: string]: ChipsInputOption[] }>({});

    const history = useHistory();

    const [isLoad, setIsLoad] = useState<boolean>(false);
    const [notFound, setNotFound] = useState<boolean>(false);
    const [imageLoadError, setImageLoadError] = useState<string>();
    const { viewWidth } = useAdaptivity();
    const isDesktop = (viewWidth ?? ViewWidth.DESKTOP) >= ViewWidth.TABLET;

    const clickOnMap = (p: { event: MouseEvent; latLng: [number, number]; pixel: [number, number] }) => {
        setPointPlant(p.latLng);
    };

    const imageToUrl = (bytes: string, type: string) => `data:${type};base64, ${bytes}`;

    useEffect(() => {
        const load = <TFilter extends FilterBase, TDto extends BaseView>(
            search: ISearch<TFilter, TDto>,
            save: (m: Array<BaseView>) => void,
        ) => {
            search.searchLinks<BaseView>().then((res) => {
                save(res.elements);
            });
        };
        load(PlantFamilyAPI, setPlantFamilies);
        load(UserAPI, setAuthors);
        load(OoptAPI, setOopts);
        PlantCharacteristicValueAPI.getAll().then(setCharacteristics);
        if (!props.id) {
            setIsLoad(true);
            return;
        }
        PlantAPI.get(props.id)
            .then((x) => {
                setName(x.name);
                setPlantFamilyId(x.plantFamily.id);
                const assign = x.plantCharacteristicValues
                    .map((y) => ({
                        [y.characteristic.id]: y.values.map(
                            (z) => ({ value: z.id, label: z.name.ru } as ChipsInputOption),
                        ),
                    }))
                    .reduce((previousValue, currentValue) => Object.assign(previousValue, currentValue));
                setSelectedCharacteristics(assign);
                setAvatarForSave(undefined);
                setAvatar(x.avatarId ? fileDataToUrl(x.avatarId, ImageSize.Medium) : '/notAvatar.jpg');
                setSelectedRedBookPlants(x.redBookPlants.map((z) => z.id));
                setSelectedOopts(x.oopts.map((z) => ({ value: z.id, label: z.name.ru } as never)));
                setSelectedAuthors(x.authors.map((z) => ({ value: z.id, label: z.name.ru } as never)));
                if (Math.abs(x.position.longitude) + Math.abs(x.position.latitude) > 0) {
                    setPointPlant([x.position.latitude, x.position.longitude]);
                }
                setIsLoad(true);
            })
            .catch((x) => {
                setNotFound(true);
            });
    }, []);

    const createModel = () =>
        ({
            name,
            ecologicalPhytocenoticGroupComment: EPGroupComment,
            redBookComment: IUCHComment,
            plantFamilyId,
            avatar: avatarForSave
                ? ({
                      fileBase64: avatarForSave[1],
                      description: { ru: 'Аватар растения' },
                  } as ImageBinding)
                : undefined,
            ooptIds: selectedOopts.map((x) => x.value),
            position: { latitude: pointPlant[0], longitude: pointPlant[1] },
            redBooksPlantIds: selectedRedBookPlants,
            authorIds: selectedAuthors.map((x) => x.value),
            plantCharacteristicValues: Object.entries(selectedCharacteristics).flatMap((x) =>
                x[1].map(
                    (y) =>
                        ({
                            plantCharacteristicId: x[0],
                            name: { ru: y.label },
                        } as PlantCharacteristicValueBinding),
                ),
            ),
        } as PlantBinding);

    const add = () => {
        PlantAPI.put(createModel()).then((r) => {
            history.goBack();
        });
    };

    const edit = () => {
        PlantAPI.patch({
            id: props.id,
            ...createModel(),
        }).then((r) => {
            history.goBack();
        });
    };

    const remove = () => {
        PlantAPI.delete(props.id!).then((x) => {
            history.goBack();
        });
    };

    const onLoadFile = (x: { target: { files: any } }) => {
        const reader = new FileReader();
        const { files } = x.target;
        reader.onload = (e) => {
            if (!files || files.length === 0) {
                setImageLoadError('Загрузите картинку');
                return;
            }

            const res = e.target?.result;
            if (!(res instanceof ArrayBuffer)) {
                setImageLoadError('Загрузите картинку');
                return;
            }

            const { type } = files[0];
            if (!type.startsWith('image')) {
                setImageLoadError('Загрузите картинку');
                return;
            }

            const resStr = encode(res);
            const resStr2 = imageToUrl(resStr, type);
            setAvatar(resStr2);
            setAvatarForSave([type, resStr]);
            setImageLoadError(undefined);
        };
        if (x.target.files) {
            reader.readAsArrayBuffer(x.target.files[0]);
        }
    };

    return isLoad ? (
        <Group
            mode="plain"
            header={
                <>
                    <IconButton onClick={() => history.goBack()}>
                        <Icon28ArrowLeftOutline />
                    </IconButton>
                    <Header>Растение</Header>
                </>
            }>
            <SimpleCell
                disabled
                before={
                    <Avatar
                        className="AvatarPlantEdit"
                        mode="app"
                        src={avatarForSave ? imageToUrl(avatarForSave[1], avatarForSave[0]) : avatar}
                        size={250}
                    />
                }>
                <FormItem
                    top="Загрузите ваше фото"
                    status={imageLoadError ? 'error' : 'default'}
                    bottom={imageLoadError ?? ''}>
                    <File before={<Icon24Camera />} controlSize="m" onChange={onLoadFile}>
                        Открыть галерею
                    </File>
                </FormItem>
            </SimpleCell>
            <LocalizableNameFormItem name={name} setName={setName} title="Имя" mode="horizontal" />
            <LocalizableNameFormItem
                name={EPGroupComment}
                setName={setEPGroupComment}
                title="Комментарий к экологической фитоценотической группе"
                mode="horizontal"
            />
            <LocalizableNameFormItem
                name={IUCHComment}
                setName={setIUCHComment}
                title="Комментарий к красной книге"
                mode="horizontal"
            />
            <FormItem
                top="Подкласс"
                bottom={plantFamilyId ? '' : 'Пожалуйста, укажите подкласс'}
                status={plantFamilyId ? 'valid' : 'error'}>
                <Select
                    onChange={(e: { currentTarget: any; target: any }) => {
                        setPlantFamilyId((e.currentTarget ?? e.target).value);
                    }}
                    placeholder="Не выбран"
                    value={plantFamilyId}
                    options={[{ label: 'Не выбран', value: '' }].concat(
                        plantFamilies.map((x) => ({
                            label: `${x.name.ru}(${x.name.la})`,
                            value: x.id,
                        })),
                    )}
                    renderOption={({ ...restProps }) => <CustomSelectOption {...restProps} />}
                />
            </FormItem>
            <Chipset all={authors} value={selectedAuthors} onChange={setSelectedAuthors} title="Выберите авторов" />
            <Chipset all={oopts} value={selectedOopts} onChange={setSelectedOopts} title="ООПТ" />
            <Header>Напишите, чтобы создать новую характеристику</Header>
            {characteristics.map((x) => (
                <Chipset
                    key={x.characteristic.id}
                    characteristic={x}
                    value={selectedCharacteristics[x.characteristic.id]}
                    onChange={(y) => {
                        setSelectedCharacteristics((prev) => ({
                            ...prev,
                            [x.characteristic.id]: y,
                        }));
                    }}
                    creatable
                />
            ))}
            <FormItem top="Позиция на карте">
                <div style={{ width: '100%', height: isDesktop ? '70vh' : 'calc(100vh - 61px - 48px)' }}>
                    <Map defaultCenter={pointPlant} defaultZoom={9} provider={osm} onClick={clickOnMap}>
                        <ZoomControl />
                        <Marker anchor={pointPlant} color="rgb(0, 255, 0)" width={30} />
                    </Map>
                </div>
            </FormItem>
            {!props.id ? (
                <FormItem>
                    <CellButton centered onClick={() => add()}>
                        Добавить
                    </CellButton>
                </FormItem>
            ) : (
                <FormLayoutGroup mode="horizontal">
                    <FormItem>
                        <CellButton centered onClick={() => edit()}>
                            Изменить
                        </CellButton>
                    </FormItem>
                    <FormItem>
                        <CellButton centered onClick={() => remove()}>
                            Удалить
                        </CellButton>
                    </FormItem>
                </FormLayoutGroup>
            )}
        </Group>
    ) : (
        <div>
            {!notFound ? (
                <Spinner size="large" style={{ margin: '20px 0' }} />
            ) : (
                <SadHamburger outerBackgroundColor="white" />
            )}
        </div>
    );
};
