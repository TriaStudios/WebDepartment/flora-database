import { BaseBinding } from 'api/binding';
import { BaseView } from 'api/view';
import { SadHamburger } from 'component/SadHamburger/SadHamburger';
import * as React from 'react';
import { Route, Switch } from 'react-router-dom';

export interface Props<T extends BaseView, V extends BaseBinding> {
    listPage: React.ReactElement<any, any>;
    editPage: React.FC<{ id: string }>;
    addPage: React.FC;
    path: string;
}

export const AdminElementSwitch = <T extends BaseView, V extends BaseBinding>(props: Props<T, V>) => {
    const EditPage = props.editPage;
    return (
        <Switch>
            <Route exact path={`${props.path}`}>
                {props.listPage}
            </Route>
            <Route exact path={`${props.path}/edit/:id`} component={(e: any) => <EditPage id={e.match.params.id} />} />
            <Route exact path={`${props.path}/add`} component={props.addPage} />
            <Route component={() => <SadHamburger outerBackgroundColor="white" />} />
        </Switch>
    );
};
