import { PlantCharacteristicValueAPI } from 'api';
import { FilterBase, SearchBinding } from 'api/binding';
import { BaseView, PageView } from 'api/view';
import { DefaultCell } from 'component/admin/DefaultCell';
import { AdminElementSwitch } from 'page/admin/AdminElementSwitch';
import { ListPage } from 'page/admin/ListPage';
import React, { useEffect, useState } from 'react';

interface Props<T extends FilterBase, U extends BaseView> extends InnerProps<T, U> {
    editPage: React.FC<{ id: string }>;
    addPage: React.FC;
}

interface InnerProps<T extends FilterBase, U extends BaseView> {
    onSearch: (model?: SearchBinding<T> | undefined) => Promise<PageView<U>>;
    cellIcon?: React.FC;
    getCellTitle: (item: U) => React.ReactNode;
    title: string;
    path: string;
    isReload: boolean;
    regExp?: RegExp;
    getImage?: (item: U) => string | undefined;
}

export const Lol = <T extends FilterBase, U extends BaseView>(innerProps: InnerProps<T, U>) => {
    const [list, setList] = useState<U[]>([]);
    const [page, setPage] = useState<number>(0);
    const countOnPage = 30;

    const onReset = () => {
        setList([]);
        setPage(0);
    };

    const onFetch = (ended: () => void, fetched: () => void, query: URLSearchParams) => {
        let s = decodeURI(query.toString());
        if (innerProps.regExp) s = s.replaceAll(innerProps.regExp, 'plantCharacteristicValueIds');
        innerProps
            .onSearch({
                filter: s,
                pagination: {
                    count: countOnPage,
                    page,
                },
            })
            .then((res) => {
                if (res.elements.length === 0) {
                    ended();
                    fetched();
                    return;
                }

                setList((x) => [...x, ...res.elements]);
                setPage((x) => x + 1);
                fetched();
            });
    };

    return (
        <ListPage
            onReset={onReset}
            onFetch={onFetch}
            title={innerProps.title}
            path={innerProps.path}
            isReload={innerProps.isReload}>
            {list.map((x, index) => (
                <DefaultCell
                    key={`${x.id}_${index}`}
                    item={x}
                    icon={innerProps.cellIcon}
                    path={innerProps.path}
                    getTitle={innerProps.getCellTitle}
                    image={innerProps.getImage && innerProps.getImage(x)}
                />
            ))}
            {list.length === 0 ? <p style={{ textAlign: 'center', color: 'black' }}>Ничего нет</p> : undefined}
        </ListPage>
    );
};

export function DefaultPage<T extends FilterBase, U extends BaseView>(props: Props<T, U>) {
    const [regExp, setRegExp] = useState<RegExp>();

    useEffect(() => {
        if (!regExp) {
            PlantCharacteristicValueAPI.getAll().then((res) => {
                setRegExp(new RegExp(res.map((x) => x.characteristic.name.la).join('|'), 'g'));
            });
        }
    }, [regExp]);

    return regExp ? (
        <AdminElementSwitch
            path={props.path}
            listPage={<Lol regExp={regExp} {...props} />}
            editPage={props.editPage}
            addPage={props.addPage}
        />
    ) : (
        <></>
    );
}
