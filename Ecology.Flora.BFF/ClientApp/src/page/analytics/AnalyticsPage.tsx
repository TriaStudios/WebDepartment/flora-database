import { FormItem, Group, Header, Pagination, Search, Select } from '@vkontakte/vkui';
import { ReportAPI } from 'api';
import { OoptMetricsResponse } from 'api/view';
import React, { useCallback, useEffect, useState } from 'react';
import DataTable, { TableColumn } from 'react-data-table-component';

export const AnalyticsPage = () => {
    const [query, setQuery] = useState('');
    const [pageOffset, setPageOffset] = useState(0);
    const [countOnPage, setCountOnPage] = useState(10);
    const [countPages, setCountPages] = useState(0);
    const [columns, setColumns] = useState<TableColumn<OoptMetricsResponse>[]>([]);
    const [data, setData] = useState<OoptMetricsResponse[]>([]);
    const [pending, setPending] = useState(false);

    const subHeaderComponentMemo = React.useMemo(
        () => (
            <Search
                onChange={(e: { target: { value: React.SetStateAction<string> } }) => setQuery(e.target.value)}
                value={query}
            />
        ),
        [query],
    );

    useEffect(() => {
        setPending(true);
        ReportAPI.ooptMetrics({
            pagination: {
                page: Math.round(pageOffset / countOnPage),
                count: countOnPage,
            },
            query,
        }).then((res) => {
            setPending(false);
            setData(res.elements);
            setColumns([
                {
                    name: 'ООПТ',
                    selector: (y) => y.oopt.name.ru,
                    allowOverflow: true,
                } as TableColumn<OoptMetricsResponse>,
                ...res.elements[0].metrics.map((x) => {
                    const buf = x.metric.id;
                    return {
                        name: x.metric.name.ru,
                        selector: (y) => y.metrics.find((z) => z.metric.id === buf)?.value,
                        allowOverflow: true,
                    } as TableColumn<OoptMetricsResponse>;
                }),
                {
                    name: 'Итог',
                    selector: (y) => y.total,
                    allowOverflow: true,
                } as TableColumn<OoptMetricsResponse>,
            ]);
            const cPages = Math.round(res.count / countOnPage);
            setCountPages(cPages);
        });
    }, [pageOffset, countOnPage, query]);

    useEffect(() => {
        setPageOffset((prev) => Math.floor(prev / countOnPage) * countOnPage);
    }, [countOnPage]);

    const Paga = useCallback(
        () => (
            <Pagination
                style={{
                    display: 'inline-flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                }}
                currentPage={Math.round(pageOffset / countOnPage) + 1}
                siblingCount={1}
                boundaryCount={1}
                totalPages={countPages}
                onChange={(p: number) => {
                    window.scrollTo({
                        top: 0,
                        behavior: 'smooth',
                    });
                    setPageOffset((p - 1) * countOnPage);
                }}
            />
        ),
        [pageOffset, countOnPage, countPages],
    );

    return (
        <>
            <Header>Аналитика</Header>
            <Paga />
            <br />
            <Group>
                <div data-testid="data-table">
                    <DataTable
                        subHeader
                        subHeaderComponent={subHeaderComponentMemo}
                        progressPending={pending}
                        highlightOnHover
                        columns={columns}
                        data={data}
                    />
                </div>
            </Group>
            <Paga />
            <FormItem top="Кол-во на странице">
                <Select
                    style={{
                        width: '6rem',
                    }}
                    value={countOnPage}
                    onChange={(e: { currentTarget: { value: string | number }; target: { value: string | number } }) =>
                        setCountOnPage(+e.currentTarget.value ?? +e.target.value)
                    }
                    placeholder="Не выбран"
                    options={[10, 25, 50, 100].map((n) => ({
                        label: n.toString(),
                        value: n,
                    }))}
                />
            </FormItem>
        </>
    );
};
