import { render, screen, waitFor } from '@testing-library/react';
import { AnalyticsPage } from 'page/analytics/AnalyticsPage';
import React from 'react';
import userEvent from '@testing-library/user-event';
import MockAdapter from 'axios-mock-adapter';
import { instance } from 'api/axios';
import { OoptMetricsResponse, PageView } from 'api/view';

describe('AnalyticsPage', () => {
    it('test', async () => {
        const mock = new MockAdapter(instance);
        const data: PageView<OoptMetricsResponse> = {
            count: 105,
            elements: [
                {
                    metrics: [],
                    oopt: {
                        id: 'string',
                        name: { ru: 'string' },
                    },
                    total: '105',
                },
            ],
        };
        const matcher = /\/report\/oopt-metrics\??([\w]+=[\w\d]+)*/i;
        mock.onGet(matcher).reply(200, data);
        jest.spyOn(window, 'scrollTo').mockImplementation((args) => {});
        render(<AnalyticsPage />);
        const all = await screen.findAllByTestId('data-table');
        expect(mock.history.get.length).toBe(1);
        const buttons = screen.getAllByRole('button').slice(0, 2);
        mock.resetHistory();
        userEvent.click(buttons[1]);
        const neall = await screen.findAllByTestId('data-table');
        expect(mock.history.get.length).toBe(1);
        expect(mock.history.get).toBe(1);
        mock.restore();
        jest.restoreAllMocks();
    });
});
