import { Icon24Camera } from '@vkontakte/icons';
import { File, FormItem, Group, Header, InfoRow, SimpleCell, Spinner } from '@vkontakte/vkui';
import { RecognizeApi } from 'api';
import { PlantFamilyView } from 'api/view';
import React, { useState } from 'react';

export const SearchFamilyByPhoto = () => {
    const [photo, setPhoto] = useState<string>('/defaultImagePlant.png');
    const [loaderEnabled, setLoaderEnabled] = useState(false);
    const [familyRes, setFamilyRes] = useState<PlantFamilyView | null>(null);

    const uploadFile = (event: any) => {
        const file = event.target.files[0];

        if (file) {
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = (e) => {
                setPhoto(reader.result as string);
            };
            setLoaderEnabled(true);
            RecognizeApi.predictFamily(file).then((res) => {
                setLoaderEnabled(false);
                setFamilyRes(res);
            });
        }
    };

    return (
        <Group>
            <FormItem top="Загрузите фото расстения">
                <File before={<Icon24Camera />} controlSize="m" onChange={uploadFile}>
                    Открыть галерею
                </File>
            </FormItem>
            <img src={photo} style={{ display: 'block' }} alt="" width="auto" height="400" />
            {loaderEnabled && <Spinner size="large" style={{ margin: '20px 0' }} />}
            {familyRes != null && (
                <Group mode="plain">
                    <Header mode="secondary">Информация о расстении</Header>
                    <SimpleCell multiline>
                        <InfoRow header="Класс растения">{familyRes?.plantClass.name.ru}</InfoRow>
                    </SimpleCell>
                    <SimpleCell>
                        <InfoRow header="Подкласс растения">{familyRes?.name.ru}</InfoRow>
                    </SimpleCell>
                </Group>
            )}
        </Group>
    );
};
