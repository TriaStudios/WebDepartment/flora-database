import '@vkontakte/vkui/dist/vkui.css';

import {
    AppRoot,
    Epic,
    PanelHeader,
    SplitCol,
    SplitLayout,
    Tabbar,
    TabbarItem,
    useAdaptivity,
    usePlatform,
    ViewWidth,
    VKCOM,
} from '@vkontakte/vkui';
import { Roles } from 'api/enums';
import { AppSwitch } from 'component/AppSwitch';
import { GlobalView } from 'component/GlobalView';
import { LeftMenu } from 'component/LeftMenu';
import { Modal } from 'component/modal/Modal';
import { useVisibilityPage } from 'hook/useVisibilityPage';
import React, { useState } from 'react';
import { connect, ConnectedProps } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { RootState } from 'store';
import { loginAsync, meAsync } from 'store/slice/userSlice';
import { buttonsMain } from 'utils/menuButtons';

const connector = connect(
    (state: RootState) => ({
        user: state.user.user,
        loaded: state.user.loaded,
        requested: state.user.requested,
    }),
    { meAsync, loginAsync },
);

interface Props extends ConnectedProps<typeof connector> {}

export const App = connector((props: Props): JSX.Element => {
    const { viewWidth } = useAdaptivity();
    const isDesktop = (viewWidth ?? ViewWidth.DESKTOP) >= ViewWidth.TABLET;
    const platform = usePlatform();
    const hasHeader = platform !== VKCOM;
    const [isReload, setIsReload] = useState<boolean>(false);

    useVisibilityPage(() => {
        props.meAsync();
    });

    return (
        <AppRoot>
            <SplitLayout header={hasHeader && <PanelHeader separator={false} />} style={{ justifyContent: 'center' }}>
                {isDesktop && <LeftMenu onReload={() => setIsReload((prev) => !prev)} />}
                <SplitCol
                    animate={!isDesktop}
                    spaced={isDesktop}
                    width={isDesktop ? '560px' : '100%'}
                    maxWidth={isDesktop ? '80rem' : '100%'}>
                    <Epic
                        activeStory="main"
                        tabbar={
                            !isDesktop && (
                                <Tabbar>
                                    {Object.values(buttonsMain)
                                        .filter(
                                            (x) =>
                                                !x.admin ||
                                                (props.user?.roles.filter((y) => y.name.ru === Roles.ADMIN).length ??
                                                    0) > 0,
                                        )
                                        .map((x) => (
                                            <NavLink
                                                key={x.id}
                                                to={`/${x.id}`}
                                                component={(kek) => (
                                                    <TabbarItem
                                                        style={{ userSelect: 'none' }}
                                                        key={x.id}
                                                        selected={kek.className === 'active'}
                                                        onClick={kek.navigate}
                                                        text={x.title}>
                                                        {x.Icon}
                                                    </TabbarItem>
                                                )}
                                            />
                                        ))}
                                </Tabbar>
                            )
                        }>
                        {props.requested && (
                            <GlobalView id="main" isDesktop={isDesktop} modal={<Modal />}>
                                <AppSwitch isReload={isReload} />
                            </GlobalView>
                        )}
                    </Epic>
                </SplitCol>
            </SplitLayout>
        </AppRoot>
    );
});
