export interface MenuItem {
    id?: string;
    child?: string;
    fieldFilter?: string;
    title: string;
    Icon: JSX.Element;
    mobile?: boolean;
    separatorBefore?: boolean;
    separatorAfter?: boolean;
    admin?: boolean;
    inactive?: boolean;
}
