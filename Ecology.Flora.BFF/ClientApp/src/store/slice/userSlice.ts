import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { AuthAPI } from 'api';
import { AuthenticationRequest, UserView } from 'api/view';

interface InitialState {
    user?: UserView;
    loaded: boolean;
    error?: string;
    pending: boolean;
    requested: boolean;
}

const initialState: InitialState = {
    user: undefined,
    loaded: false,
    error: undefined,
    pending: false,
    requested: false,
};

export const loginAsync = createAsyncThunk(
    'user/login',
    async (data: AuthenticationRequest) => await AuthAPI.login(data.username, data.password),
);

export const meAsync = createAsyncThunk('user/me', async (data) => await AuthAPI.me());

const slice = createSlice({
    name: 'user',
    initialState,
    reducers: {
        logout: (state) => {
            AuthAPI.logout();
            state.loaded = false;
            state.user = undefined;
        },
    },
    extraReducers: (builder) => {
        builder.addCase(loginAsync.pending, (state) => {
            state.pending = true;
            state.loaded = false;
            state.error = undefined;
            state.requested = false;
        });
        builder.addCase(loginAsync.fulfilled, (state, action) => {
            state.user = action.payload;
            state.loaded = true;
            state.pending = false;
            state.requested = true;
        });
        builder.addCase(loginAsync.rejected, (state) => {
            state.error = 'Неправильный логин или пароль';
            state.pending = false;
            state.requested = true;
        });
        builder.addCase(meAsync.fulfilled, (state, action) => {
            state.loaded = true;
            state.pending = false;
            state.requested = true;
        });
        builder.addCase(meAsync.rejected, (state) => {
            AuthAPI.logout();
            state.loaded = false;
            state.pending = false;
            state.requested = true;
            state.user = undefined;
        });
    },
});

const { actions, reducer } = slice;

export const { logout } = actions;
export const UserReducer = reducer;
