import { createSlice } from '@reduxjs/toolkit';

interface InitialState {
    modalId: string | null;
}

const initialState: InitialState = {
    modalId: null,
};

const slice = createSlice({
    name: 'modal',
    initialState,
    reducers: {
        openModalFilter: (state) => {
            state.modalId = 'filters';
        },
        closeModal: (state) => {
            state.modalId = null;
        },
    },
});

const { actions, reducer } = slice;

export const { openModalFilter, closeModal } = actions;
export const ModalReducer = reducer;
