import { closeModal, openModalFilter } from 'store/slice/modalSlice';
import React from 'react';
import { store } from 'store/index';
import { loginAsync, logout, meAsync } from 'store/slice/userSlice';
import { instance } from 'api/axios';
import { LocalizeProperty } from 'api/models';
import { parsePair } from 'utils/utils';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';

jest.setTimeout(100000);

describe('store', () => {
    it('openThenCloseModalFilter', () => {
        const startState = store.getState();
        const startStateWithoutModalFilter = Object.fromEntries(
            Object.entries(startState).filter((x) => x[0] !== 'modal'),
        );
        store.dispatch(openModalFilter());
        let endState = store.getState();
        let endStateWithoutModalFilter = Object.fromEntries(Object.entries(endState).filter((x) => x[0] !== 'modal'));
        expect(startStateWithoutModalFilter).toEqual(endStateWithoutModalFilter);
        expect(store.getState().modal.modalId).toBe('filters');
        store.dispatch(closeModal());
        endState = store.getState();
        endStateWithoutModalFilter = Object.fromEntries(Object.entries(endState).filter((x) => x[0] !== 'modal'));
        expect(startStateWithoutModalFilter).toEqual(endStateWithoutModalFilter);
        expect(store.getState().modal.modalId).toBe(null);
    });

    it('check auth methods', async () => {
        const data = {
            token: 'token',
            refreshToken: 'refresh_token',
            user: {
                id: 'string',
                name: {
                    ru: 'string',
                } as LocalizeProperty,
            },
        };
        const mock = new MockAdapter(instance);
        mock.onPost('/auth/login').reply(404, 'Invalid username or password');

        let startState = store.getState();
        let startStateOthers = Object.fromEntries(Object.entries(startState).filter((x) => x[0] !== 'user'));
        await store.dispatch(
            loginAsync({
                username: 'string',
                password: 'string',
            }),
        );
        let endState = store.getState();
        let endStateOthers = Object.fromEntries(Object.entries(endState).filter((x) => x[0] !== 'user'));

        expect(startStateOthers).toEqual(endStateOthers);
        expect(endState.user.user).not.toBeTruthy();
        expect(endState.user.loaded).toBe(false);
        expect(endState.user.requested).toBe(true);
        expect(endState.user.pending).toBe(false);
        expect(endState.user.error).toBe('Неправильный логин или пароль');
        let token = sessionStorage.getItem('token');
        expect(token).not.toBeTruthy();

        mock.reset();
        mock.onPost('/auth/login').reply(200, data);
        startStateOthers = endStateOthers;
        await store.dispatch(
            loginAsync({
                username: 'string',
                password: 'string2',
            }),
        );
        endState = store.getState();
        endStateOthers = Object.fromEntries(Object.entries(endState).filter((x) => x[0] !== 'user'));

        expect(startStateOthers).toEqual(endStateOthers);
        expect(endState.user.user).toBeTruthy();
        expect(endState.user.loaded).toBe(true);
        expect(endState.user.requested).toBe(true);
        expect(endState.user.pending).toBe(false);
        expect(endState.user.error).not.toBeTruthy();
        token = sessionStorage.getItem('token');
        expect(token).toBeTruthy();
        expect(parsePair(token!)).toEqual([data.token, data.refreshToken]);

        mock.reset();
        mock.onGet('/user/me').reply(400, 'BadRequest');
        startStateOthers = endStateOthers;
        await store.dispatch(meAsync());
        endState = store.getState();
        endStateOthers = Object.fromEntries(Object.entries(endState).filter((x) => x[0] !== 'user'));

        expect(startStateOthers).toEqual(endStateOthers);
        expect(endState.user.user).not.toBeTruthy();
        expect(endState.user.loaded).toBe(false);
        expect(endState.user.requested).toBe(true);
        expect(endState.user.pending).toBe(false);
        expect(endState.user.error).not.toBeTruthy();
        token = sessionStorage.getItem('token');
        expect(token).not.toBeTruthy();

        mock.reset();
        mock.onGet('/user/me').reply(200, data.user);
        mock.onPost('/auth/login').reply(200, data);
        startStateOthers = endStateOthers;
        await store.dispatch(
            loginAsync({
                username: 'string',
                password: 'string2',
            }),
        );
        await store.dispatch(meAsync());
        endState = store.getState();
        endStateOthers = Object.fromEntries(Object.entries(endState).filter((x) => x[0] !== 'user'));

        expect(startStateOthers).toEqual(endStateOthers);
        expect(endState.user.user).toBeTruthy();
        expect(endState.user.loaded).toBe(true);
        expect(endState.user.requested).toBe(true);
        expect(endState.user.pending).toBe(false);
        expect(endState.user.error).not.toBeTruthy();
        token = sessionStorage.getItem('token');
        expect(token).toBeTruthy();
        expect(parsePair(token!)).toEqual([data.token, data.refreshToken]);

        mock.reset();
        mock.onGet('/test').replyOnce(401, 'Unauthorized').onGet('/test').reply(200);
        data.token = 'updated_token';
        mock.onPost('/auth/refresh-token').reply(200, { token: data.token });
        startStateOthers = endStateOthers;
        await instance.get('/test');
        endState = store.getState();
        endStateOthers = Object.fromEntries(Object.entries(endState).filter((x) => x[0] !== 'user'));

        expect(startStateOthers).toEqual(endStateOthers);
        expect(endState.user.user).toBeTruthy();
        expect(endState.user.loaded).toBe(true);
        expect(endState.user.requested).toBe(true);
        expect(endState.user.pending).toBe(false);
        expect(endState.user.error).not.toBeTruthy();
        token = sessionStorage.getItem('token');
        expect(token).toBeTruthy();
        expect(parsePair(token!)).toEqual([data.token, data.refreshToken]);

        mock.reset();
        startStateOthers = endStateOthers;
        store.dispatch(logout());
        endState = store.getState();
        endStateOthers = Object.fromEntries(Object.entries(endState).filter((x) => x[0] !== 'user'));

        expect(startStateOthers).toEqual(endStateOthers);
        expect(endState.user.user).not.toBeTruthy();
        expect(endState.user.loaded).toBe(false);
        expect(endState.user.requested).toBe(true);
        expect(endState.user.pending).toBe(false);
        expect(endState.user.error).not.toBeTruthy();
        token = sessionStorage.getItem('token');
        expect(token).not.toBeTruthy();

        mock.restore();
    });
});
