import { combineReducers, configureStore } from '@reduxjs/toolkit';
import { reducer as formReducer } from 'redux-form';
import { ModalReducer } from 'store/slice/modalSlice';
import { UserReducer } from 'store/slice/userSlice';

const reducer = combineReducers({
    form: formReducer,
    user: UserReducer,
    modal: ModalReducer,
});

export const store = configureStore({
    reducer,
});

export type RootState = ReturnType<typeof reducer>;
