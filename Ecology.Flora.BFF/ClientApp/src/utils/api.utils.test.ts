import { getApi, getCRUD, serialize } from 'utils/api.utils';
import { BaseView, PageView } from 'api/view';
import { BaseBinding, FilterBase, SearchBinding } from 'api/binding';
import { instance } from 'api/axios';

jest.setTimeout(100000);

describe('api.utils', () => {
    describe('serialize', () => {
        it('#1', () => {
            expect(serialize(null)).toBe(null);
        });
        it('#2', () => {
            expect(serialize({})).toBe('');
        });
        it('#3', () => {
            expect(serialize({ a: 1 })).toBe('a=1');
        });
        it('#4', () => {
            expect(serialize({ a: 1, b: undefined })).toBe('a=1');
        });
        it('#5', () => {
            expect(serialize({ a: 1, b: [] })).toBe('a=1');
        });
        it('#6', () => {
            expect(serialize({ a: 1, b: [2] })).toBe('a=1&b=2');
        });
        it('#7', () => {
            expect(serialize({ a: 1, b: [2, 3] })).toBe('a=1&b=2&b=3');
        });
        it('#8', () => {
            expect(serialize({ a: 1, b: [2, 3], c: { d: 4 } })).toBe('a=1&b=2&b=3&d=4');
        });
    });

    describe('getApi', () => {
        it('get', async () => {
            const mockInstanceGet = jest
                .spyOn(instance, 'get')
                .mockReturnValue(new Promise((resolve) => resolve({ data: { a: 1 } })));
            const crud = getApi<FilterBase, BaseView, BaseBinding>('test');
            expect(crud).toHaveProperty('get');
            expect(await crud.get('test')).toEqual({ a: 1 });
            expect(mockInstanceGet).toBeCalledTimes(1);
            expect(mockInstanceGet.mock.calls[0]).toEqual(['test/test']);
        });

        it('delete', async () => {
            const mockInstanceDelete = jest
                .spyOn(instance, 'delete')
                .mockReturnValue(new Promise((resolve) => resolve({})));
            const crud = getApi<FilterBase, BaseView, BaseBinding>('test');
            expect(crud).toHaveProperty('delete');
            expect(await crud.delete('test')).toEqual({});
            expect(mockInstanceDelete).toBeCalledTimes(1);
            expect(mockInstanceDelete.mock.calls[0]).toEqual(['test/test']);
        });

        it('put', async () => {
            const obj = { name: { ru: 'string' } };
            const rndId = 'random_id';
            const mockInstancePut = jest
                .spyOn(instance, 'put')
                .mockReturnValue(new Promise((resolve) => resolve({ data: rndId })));
            const crud = getApi<FilterBase, BaseView, BaseBinding>('test');
            expect(crud).toHaveProperty('put');
            expect(await crud.put(obj)).toEqual(rndId);
            expect(mockInstancePut).toBeCalledTimes(1);
            expect(mockInstancePut.mock.calls[0]).toEqual(['test', obj]);
        });

        it('patch', async () => {
            const obj = { id: 'string', name: { ru: 'string' } };
            const mockInstancePatch = jest
                .spyOn(instance, 'patch')
                .mockReturnValue(new Promise((resolve) => resolve({ data: obj })));
            const crud = getApi<FilterBase, BaseView, BaseBinding>('test');
            expect(crud).toHaveProperty('patch');
            expect(await crud.patch(obj)).toEqual(obj);
            expect(mockInstancePatch).toBeCalledTimes(1);
            expect(mockInstancePatch.mock.calls[0]).toEqual(['test', obj]);
        });

        describe('searches', () => {
            const obj = {
                filter: {
                    query: 'string',
                    objectIds: ['1', '2'],
                },
                pagination: {
                    page: 1,
                    count: 10,
                },
            } as SearchBinding<FilterBase>;
            const result = {
                count: 10,
                elements: [
                    {
                        id: 'string',
                        name: { ru: 'string' },
                    },
                ],
            } as PageView<BaseView>;

            it('search', async () => {
                const mockInstanceSearch = jest
                    .spyOn(instance, 'get')
                    .mockReturnValue(new Promise((resolve) => resolve({ data: result })));
                const crud = getApi<FilterBase, BaseView, BaseBinding>('test');
                expect(crud).toHaveProperty('search');
                expect(await crud.search(obj)).toEqual(result);
                expect(mockInstanceSearch).toBeCalledTimes(1);
                expect(mockInstanceSearch.mock.calls[0]).toEqual([
                    'test/search?query=string&objectIds=1&objectIds=2&page=1&count=10',
                ]);
            });

            it('search-links', async () => {
                const mockInstanceSearchLinks = jest
                    .spyOn(instance, 'get')
                    .mockReturnValue(new Promise((resolve) => resolve({ data: result })));
                const crud = getApi<FilterBase, BaseView, BaseBinding>('test');
                expect(crud).toHaveProperty('search');
                expect(await crud.searchLinks(obj)).toEqual(result);
                expect(mockInstanceSearchLinks).toBeCalledTimes(1);
                expect(mockInstanceSearchLinks.mock.calls[0]).toEqual([
                    'test/search-links?query=string&objectIds=1&objectIds=2&page=1&count=10',
                ]);
            });
        });
    });
});
