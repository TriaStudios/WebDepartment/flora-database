import { fileDataToUrl, isArray, isObject, isPrimitive } from 'utils/utils';
import { ImageSize } from 'api/enums';

describe('utils', () => {
    describe('isArray', () => {
        it('simple array', () => {
            expect(isArray([1, 2])).toBe(true);
        });
        it('empty array', () => {
            expect(isArray([])).toBe(true);
        });
        it('null', () => {
            expect(isArray(null)).toBe(false);
        });
        it('number', () => {
            expect(isArray(1)).toBe(false);
        });
    });

    describe('isObject', () => {
        it('simple object', () => {
            expect(isObject({ a: 1 })).toBe(true);
        });
        it('empty object', () => {
            expect(isObject({})).toBe(true);
        });
        it('null', () => {
            expect(isObject(null)).toBe(true);
        });
        it('number', () => {
            expect(isObject(1)).toBe(false);
        });
    });

    describe('isPrimitive', () => {
        it('number', () => {
            expect(isPrimitive(1)).toBe(true);
        });
        it('string', () => {
            expect(isPrimitive('abc')).toBe(true);
        });
        it('object', () => {
            expect(isPrimitive({ a: 1 })).toBe(false);
        });
        it('null', () => {
            expect(isPrimitive(null)).toBe(true);
        });
    });

    describe('fileDataToUrl', () => {
        it("fileId='' fileSize=Original", () => {
            expect(fileDataToUrl('', ImageSize.Original)).toBe(
                'http://ecology.flora.ru:5000/api/v1/image/?imageSize=-1',
            );
        });
        it("fileId='abc' fileSize=Large", () => {
            expect(fileDataToUrl('abc', ImageSize.Large)).toBe(
                'http://ecology.flora.ru:5000/api/v1/image/abc?imageSize=500',
            );
        });
    });
});
