import { ImageSize } from 'api/enums';
import { baseURL } from 'config';
import CryptoJS from 'crypto-js';
import md5 from 'md5';

export const genPair = (accessToken?: string, refreshToken?: string) => {
    const md = md5(Math.random().toString(2));
    return `${md}|${CryptoJS.AES.encrypt(`${accessToken}${md}${refreshToken}`, md)}`;
};
export const parsePair = (sha: string) => {
    const md = sha.split('|')[0];
    const msg = sha.split('|')[1];
    return CryptoJS.AES.decrypt(msg, md).toString(CryptoJS.enc.Utf8).split(md);
};

export const fileDataToUrl = (fileId: string, fileSize: ImageSize) => `${baseURL}image/${fileId}?imageSize=${fileSize}`;

export const isPrimitive = (val: any) => val !== Object(val);

export const isObject = (obj: any) => !obj || (typeof obj === 'object' && !Array.isArray(obj));
export const isArray = (obj: any) => Array.isArray(obj);
