import {
    Icon28BankOutline,
    Icon28BillheadOutline,
    Icon28DiamondOutline,
    Icon28GridLayoutOutline,
    Icon28GridSquareOutline,
    Icon28ListCheckOutline,
    Icon28LocationMapOutline,
    Icon28LotusOutline,
    Icon28SearchOutline,
    Icon28SettingsOutline,
    Icon28ShareExternal,
    Icon28StoryOutline,
    Icon28UserCircleOutline,
    Icon28UserOutline,
} from '@vkontakte/icons';
import React from 'react';
import { MenuItem } from 'store/models';

export const buttonsMain: { [key: string]: MenuItem } = {
    main: {
        id: 'main',
        title: 'Главная',
        Icon: <Icon28BankOutline style={{ paddingTop: 0, paddingBottom: 0 }} />,
    },
    catalog: {
        id: 'catalog',
        title: 'Каталог',
        Icon: <Icon28BankOutline style={{ paddingTop: 0, paddingBottom: 0 }} />,
    },
    searchFamilyByPhoto: {
        id: 'search-family-by-photo',
        title: 'Определить семейство по фото',
        Icon: <Icon28StoryOutline style={{ paddingTop: 0, paddingBottom: 0 }} />,
    },
    analytics: {
        id: 'analytics',
        title: 'Аналитика',
        Icon: <Icon28ListCheckOutline style={{ paddingTop: 0, paddingBottom: 0 }} />,
        inactive: false,
    },
    search: {
        id: 'search',
        title: 'Поиск',
        Icon: <Icon28SearchOutline style={{ paddingTop: 0, paddingBottom: 0 }} />,
        mobile: true,
    },
    map: {
        id: 'map',
        title: 'Карта',
        Icon: <Icon28LocationMapOutline style={{ paddingTop: 0, paddingBottom: 0 }} />,
    },
    user: {
        id: 'user',
        title: 'Пользователь',
        Icon: <Icon28UserCircleOutline style={{ paddingTop: 0, paddingBottom: 0 }} />,
        mobile: true,
    },
    admin: {
        id: 'admin',
        title: 'Администрирование',
        Icon: <Icon28SettingsOutline style={{ paddingTop: 0, paddingBottom: 0 }} />,
        admin: true,
        separatorBefore: true,
    },
};

export const buttonsCatalog: { [key: string]: MenuItem } = {
    plantClade: {
        id: 'plant-clade',
        child: 'plant-classes',
        fieldFilter: 'plantCladeIds',
        title: 'Отделы',
        Icon: <Icon28BillheadOutline style={{ paddingTop: 0, paddingBottom: 0 }} />,
    },
    plantClasses: {
        id: 'plant-classes',
        child: 'plant-families',
        fieldFilter: 'plantClassIds',
        title: 'Классы',
        Icon: <Icon28GridSquareOutline style={{ paddingTop: 0, paddingBottom: 0 }} />,
    },
    plantFamilies: {
        id: 'plant-families',
        child: 'plants',
        fieldFilter: 'plantFamilyIds',
        title: 'Подклассы',
        Icon: <Icon28GridLayoutOutline style={{ paddingTop: 0, paddingBottom: 0 }} />,
    },
    plants: {
        id: 'plants',
        child: 'plant',
        fieldFilter: 'plantId',
        title: 'Растения',
        Icon: <Icon28LotusOutline style={{ paddingTop: 0, paddingBottom: 0 }} />,
    },
};

export const buttonsAdmin: { [key: string]: MenuItem } = {
    integration: {
        id: 'integration',
        title: 'Интеграция',
        Icon: <Icon28ShareExternal style={{ paddingTop: 0, paddingBottom: 0 }} />,
    },
    users: {
        id: 'users',
        title: 'Пользователи',
        Icon: <Icon28UserOutline style={{ paddingTop: 0, paddingBottom: 0 }} />,
        separatorAfter: true,
    },
    oopt: {
        id: 'oopts',
        title: 'ООПТ',
        Icon: <Icon28DiamondOutline style={{ paddingTop: 0, paddingBottom: 0 }} />,
    },
    metric: {
        id: 'metrics',
        title: 'Метрики',
        Icon: <Icon28BillheadOutline style={{ paddingTop: 0, paddingBottom: 0 }} />,
    },
    plantClade: {
        id: 'plant-clades',
        title: 'Отделы',
        Icon: <Icon28BillheadOutline style={{ paddingTop: 0, paddingBottom: 0 }} />,
    },
    plantClasses: {
        id: 'plant-classes',
        title: 'Классы',
        Icon: <Icon28GridSquareOutline style={{ paddingTop: 0, paddingBottom: 0 }} />,
    },
    plantFamilies: {
        id: 'plant-families',
        title: 'Подклассы',
        Icon: <Icon28GridLayoutOutline style={{ paddingTop: 0, paddingBottom: 0 }} />,
    },
    plants: {
        id: 'plants',
        title: 'Растения',
        Icon: <Icon28LotusOutline style={{ paddingTop: 0, paddingBottom: 0 }} />,
    },
    rarityCategories: {
        id: 'rarity-categories',
        title: 'Категория редкости',
        Icon: <Icon28BillheadOutline style={{ paddingTop: 0, paddingBottom: 0 }} />,
    },
    redBooks: {
        id: 'red-books',
        title: 'Красная книга',
        Icon: <Icon28BillheadOutline style={{ paddingTop: 0, paddingBottom: 0 }} />,
    },
};
