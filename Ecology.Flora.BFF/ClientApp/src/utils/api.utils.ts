import { instance } from 'api/axios';
import * as Binding from 'api/binding';
import { CRUD, ISearch } from 'api/interfaces';
import * as View from 'api/view';
import { isArray, isObject, isPrimitive } from 'utils/utils';

export const getCRUD = <T extends View.BaseView, V extends Binding.BaseBinding>(url: string): CRUD<T, V> => ({
    get(id: string): Promise<T> {
        return instance.get<T>(`${url}/${id}`).then((res) => res.data);
    },

    put(model: V): Promise<string> {
        return instance.put<string>(`${url}`, model).then((res) => res.data);
    },

    delete(id: string): Promise<never> {
        return instance.delete(`${url}/${id}`);
    },

    patch(model: V): Promise<V> {
        return instance.patch<V>(`${url}`, model).then((res) => res.data);
    },
});

export const getSearch = <TFilter extends Binding.FilterBase, TDto extends View.BaseView>(
    url: string,
): ISearch<TFilter, TDto> => ({
    search(model?: Binding.SearchBinding<TFilter>): Promise<View.PageView<TDto>> {
        return instance
            .get<View.PageView<TDto>>(`${url}/search?${serialize(model?.filter)}&${serialize(model?.pagination)}`)
            .then((res) => res.data);
    },
    searchLinks<TLink extends View.BaseView>(model?: Binding.SearchBinding<TFilter>): Promise<View.PageView<TLink>> {
        return instance
            .get<View.PageView<TLink>>(
                `${url}/search-links?${serialize(model?.filter)}&${serialize(model?.pagination)}`,
            )
            .then((res) => res.data);
    },
});

export const getApi = <
    TFilter extends Binding.FilterBase,
    TDto extends View.BaseView,
    TBinding extends Binding.BaseBinding,
>(
    url: string,
): ISearch<TFilter, TDto> & CRUD<TDto, TBinding> => ({
    ...getCRUD<TDto, TBinding>(url),
    ...getSearch<TFilter, TDto>(url),
});

export const serialize = (obj: any): string => {
    if (isPrimitive(obj)) {
        return obj;
    }
    return Object.entries<any>(obj)
        .map((x) => {
            if (isObject(x[1])) return serialize(x[1]);
            if (isArray(x[1])) {
                return x[1].map((y: any) => `${encodeURIComponent(x[0])}=${encodeURIComponent(y)}`).join('&');
            }
            return `${encodeURIComponent(x[0])}=${encodeURIComponent(x[1])}`;
        })
        .filter((x) => x && x !== '')
        .join('&');
};
