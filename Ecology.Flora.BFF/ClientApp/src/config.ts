export const production = process.env.NODE_ENV === 'production' || process.env.REACT_APP_ENV === 'production';
export const baseURL = production ? 'http://flora.athene.tech/api/v1/' : 'http://ecology.flora.ru:5000/api/v1/';
