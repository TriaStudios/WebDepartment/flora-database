export interface LocalizeProperty {
    ru?: string;
    la?: string;
    en?: string;
}

export interface PositionModel {
    latitude: number;
    longitude: number;
}
