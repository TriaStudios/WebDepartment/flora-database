export enum Roles {
    ADMIN = 'ADMIN',
    USER = 'USER',
}

export enum ImageSize {
    Original = -1,
    Small = 100,
    Medium = 250,
    Large = 500,
}
