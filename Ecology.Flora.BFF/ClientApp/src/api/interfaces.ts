import * as Binding from 'api/binding';
import { PositionModel } from 'api/models';
import * as View from 'api/view';
import { BaseView, PlantCharacteristicValuesView } from 'api/view';

export interface CRUD<T extends View.BaseView, V extends Binding.BaseBinding> {
    get(id: string): Promise<T>;
    put(model: V): Promise<string>;
    delete(id: string): Promise<never>;
    patch(model: V): Promise<V>;
}

export interface ISearch<TFilter extends Binding.FilterBase, TDto extends View.BaseView> {
    search(model?: Binding.SearchBinding<TFilter>): Promise<View.PageView<TDto>>;
    searchLinks<TLink extends View.BaseView>(model?: Binding.SearchBinding<TFilter>): Promise<View.PageView<TLink>>;
}

export interface IUserAPI extends CRUD<View.UserView, Binding.UserBinding>, ISearch<Binding.UserFilter, View.UserView> {
    roles(): Promise<Array<View.RoleView>>;
}

export interface IAuthAPI {
    login(username: string, password: string): Promise<View.UserView>;

    logout(): void;

    me(): Promise<View.UserView>;
}

export interface IRecognizeAPI {
    predictFamily(file: File): Promise<View.PlantFamilyView>;
}

export interface IPlantCharacteristicAPI
    extends CRUD<View.PlantCharacteristicView, Binding.PlantCharacteristicBinding>,
        ISearch<Binding.PlantCharacteristicFilter, View.PlantCharacteristicView> {}

export interface IPlantCladeAPI
    extends CRUD<View.PlantCladeView, Binding.PlantCladeBinding>,
        ISearch<Binding.PlantCladeFilter, View.PlantCladeView> {}

export interface IPlantClassAPI
    extends CRUD<View.PlantClassView, Binding.PlantClassBinding>,
        ISearch<Binding.PlantClassFilter, View.PlantClassView> {}

export interface IPlantFamilyAPI
    extends CRUD<View.PlantFamilyView, Binding.PlantFamilyBinding>,
        ISearch<Binding.PlantFamilyFilter, View.PlantFamilyView> {}

export interface IPlantAPI
    extends CRUD<View.PlantView, Binding.PlantBinding>,
        ISearch<Binding.PlantFilter, View.PlantView> {}

export interface IRedBookAPI
    extends CRUD<View.RedBookView, Binding.RedBookBinding>,
        ISearch<Binding.RedBookFilter, View.RedBookView> {}
export interface IRedBookPlantAPI
    extends CRUD<View.RedBookPlantView, Binding.RedBookPlantBinding>,
        ISearch<Binding.RedBookPlantFilter, View.RedBookPlantView> {}

export interface IRarityCategoryAPI
    extends CRUD<View.RarityCategoryView, Binding.RarityCategoryBinding>,
        ISearch<Binding.RarityCategoryFilter, View.RarityCategoryView> {}

export interface IOoptAPI
    extends CRUD<View.OoptView, Binding.OoptBinding>,
        ISearch<Binding.OoptFilter, View.OoptView> {}

export interface IMetricAPI
    extends CRUD<View.MetricView, Binding.MetricBinding>,
        ISearch<Binding.MetricFilter, View.MetricView> {}

export interface IMetricRaspredAPI
    extends CRUD<View.MetricRaspredView, Binding.MetricRaspredBinding>,
        ISearch<Binding.MetricRaspredFilter, View.MetricRaspredView> {}

export interface ITotalSearchAPI {
    search(query: string): Promise<Array<Array<View.BaseView>>>;
}

export interface IGeocoderApi {
    info(position: PositionModel): Promise<View.GeocodeInfoView | undefined>;
}

export interface IClusterizationApi {
    clusterize(mapPositionRequestModel: Binding.MapPositionRequestModel): Promise<Array<View.ClusterView>>;
}

export interface IIntegrationApi {
    integrate(file: File): Promise<string>;
}

export interface IReportAPI {
    ooptMetrics(model: Binding.OoptMetricsReportRequest): Promise<View.PageView<View.OoptMetricsResponse>>;
}

export interface IPlantCharacteristicValueAPI {
    get(plantCharacteristicId: string): Promise<BaseView[]>;
    getAll(): Promise<PlantCharacteristicValuesView[]>;
}
