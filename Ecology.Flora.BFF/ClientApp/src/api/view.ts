import { LocalizeProperty, PositionModel } from 'api/models';

export interface BaseView {
    id: string;
    name: LocalizeProperty;
}

export interface AuthenticationRequest {
    username: string;
    password: string;
}

export interface RedBookView extends BaseView {}
export interface RedBookPlantView extends BaseView {
    redBook: BaseView;
    rarityCategory: BaseView;
    plant: BaseView;
}

export interface RarityCategoryView extends BaseView {
    code: string;
    description: LocalizeProperty;
}

export interface PlantCladeView extends BaseView {}

export interface PlantClassView extends BaseView {
    plantClade: BaseView;
}

export interface PlantFamilyView extends BaseView {
    plantClass: BaseView;
}

export interface PlantCharacteristicValuesView {
    characteristic: BaseView;
    values: BaseView[];
}

export interface PlantView extends BaseView {
    ecologicalPhytocenoticGroupComment: LocalizeProperty;
    redBookComment: LocalizeProperty;
    position: PositionModel;
    avatarId?: string;
    plantFamily: BaseView;
    plantCharacteristicValues: PlantCharacteristicValuesView[];
    oopts: BaseView[];
    redBookPlants: RedBookPlantView[];
    authors: BaseView[];
}

export interface OoptView extends BaseView {
    square: number;
    metric8: number;
    plants: BaseView[];
}
export interface MetricRaspredView extends BaseView {
    bal: number;
    value: number;
    metricId: string;
}
export interface MetricView extends BaseView {
    metricRaspreds: string[];
}

export interface PageView<T> {
    elements: Array<T>;
    count: number;
}

export interface RoleView extends BaseView {}
export interface UserView extends BaseView {
    username: string;
    email: string;
    roles: Array<RoleView>;
}

export interface LoginResponse {
    token: string;
    refreshToken: string;
    user: UserView;
}

export interface RefreshTokenResponse {
    token: string;
}

export interface PointView {
    latitude: number;
    longitude: number;
}

export interface IndexView {
    q: number;
    r: number;
}

export interface ClusterView {
    index: IndexView;
    center: PointView;
    minBound: PointView;
    maxBound: PointView;
    countPoints: number;
    id: string | null;
    title: string | null;
    color: string;

    hover: boolean;
}

export interface GeocodeInfoView {
    path: string;
    description: string;
}

export interface GeocodeAllView {
    latitude: number;
    longitude: number;
    lookupSource: string;
    plusCode: string;
    localityLanguageRequested: string;
    continent: string;
    continentCode: string;
    countryName: string;
    countryCode: string;
    principalSubdivision: string;
    principalSubdivisionCode: string;
    city: string;
    locality: string;
    postcode: string;
    localityInfo: LocalityInfo;
}

export interface LocalityInfo {
    administrative: Array<LocalityProperty>;
    informative: Array<LocalityProperty>;
}

export interface LocalityProperty {
    order: number;
    adminLevel: number;
    name: string;
    description: string;
    isoName: string;
    isoCode: string;
    wikidataId: string;
    geonameId: number;
    chinaAdminCode: string;
}

export interface OoptMetricsResponse {
    oopt: BaseView;
    metrics: MetricCalculatedView[];
    total: string;
}

export interface MetricCalculatedView {
    metric: BaseView;
    value: string;
}

export interface PlantCharacteristicView extends BaseView {}
export interface PlantCharacteristicValueView extends BaseView {
    PlantCharacteristic: BaseView;
}
