import { instance } from 'api/axios';
import * as Binding from 'api/binding';
import {
    IAuthAPI,
    IClusterizationApi,
    IGeocoderApi,
    IIntegrationApi,
    IMetricAPI,
    IMetricRaspredAPI,
    IOoptAPI,
    IPlantAPI,
    IPlantCharacteristicAPI,
    IPlantCharacteristicValueAPI,
    IPlantCladeAPI,
    IPlantClassAPI,
    IPlantFamilyAPI,
    IRarityCategoryAPI,
    IRecognizeAPI,
    IRedBookAPI,
    IRedBookPlantAPI,
    IReportAPI,
    ITotalSearchAPI,
    IUserAPI,
} from 'api/interfaces';
import { PositionModel } from 'api/models';
import * as View from 'api/view';
import { BaseView, PlantCharacteristicValuesView } from 'api/view';
import axios from 'axios';
import { getApi, serialize } from 'utils/api.utils';
import { genPair } from 'utils/utils';

export const PlantCharacteristicAPI: IPlantCharacteristicAPI = getApi('PlantCharacteristic');
export const PlantCladeAPI: IPlantCladeAPI = getApi('PlantClade');
export const PlantClassAPI: IPlantClassAPI = getApi('PlantClass');
export const PlantFamilyAPI: IPlantFamilyAPI = getApi('PlantFamily');
export const PlantAPI: IPlantAPI = getApi('Plant');
export const RedBookAPI: IRedBookAPI = getApi('RedBook');
export const RedBookPlantAPI: IRedBookPlantAPI = getApi('RedBookPlant');
export const RarityCategoryAPI: IRarityCategoryAPI = getApi('RarityCategory');
export const OoptAPI: IOoptAPI = getApi('Oopt');
export const MetricAPI: IMetricAPI = getApi('Metric');
export const MetricRaspredAPI: IMetricRaspredAPI = getApi('MetricRaspred');
export const UserAPI: IUserAPI = {
    roles(): Promise<Array<View.RoleView>> {
        return instance.get<Array<View.RoleView>>('roles').then((res) => res.data);
    },
    ...getApi('user'),
};

export const AuthAPI: IAuthAPI = {
    login(username: string, password: string): Promise<View.UserView> {
        return instance.post<View.LoginResponse>('auth/login', { username, password }).then((res) => {
            const bearer = `${res.data.token}`;
            const refreshToken = `${res.data.refreshToken}`;
            sessionStorage.setItem('token', genPair(bearer, refreshToken));
            return res.data.user;
        });
    },
    logout() {
        sessionStorage.removeItem('token');
    },
    me(): Promise<View.UserView> {
        return instance.get<View.UserView>('user/me').then((res) => res.data);
    },
};

export const TotalSearchAPI: ITotalSearchAPI = {
    search(query: string): Promise<Array<Array<View.BaseView>>> {
        return instance.get<Array<Array<View.BaseView>>>(`TotalSearch?query=${query}`).then((res) => res.data);
    },
};

export const ClusterizationApi: IClusterizationApi = {
    clusterize(mapPositionRequestModel: Binding.MapPositionRequestModel): Promise<Array<View.ClusterView>> {
        return instance
            .post<Array<View.ClusterView>>('Clusterization', mapPositionRequestModel)
            .then((res) => res.data);
    },
};

export const IntegrationApi: IIntegrationApi = {
    integrate(file: File): Promise<string> {
        const formData = new FormData();
        formData.append('file', file);
        return instance
            .post<string>('Integration', formData, { headers: { 'Content-Type': 'multipart/form-data' } })
            .then((res) => res.data);
    },
};

export const RecognizeApi: IRecognizeAPI = {
    predictFamily(file: File): Promise<View.PlantFamilyView> {
        const formData = new FormData();
        formData.append('file', file);
        return instance
            .post<View.PlantFamilyView>('PlantRecognizer/predict-family', formData, {
                headers: { 'Content-Type': 'multipart/form-data' },
            })
            .then((res) => res.data);
    },
};

export const GeocoderAPI: IGeocoderApi = {
    info(position: PositionModel): Promise<View.GeocodeInfoView | undefined> {
        const url = 'https://api.bigdatacloud.net/data/reverse-geocode-client';
        return axios
            .get<View.GeocodeAllView>(
                `${url}?latitude=${position.latitude}&longitude=${position.longitude}&localityLanguage=ru`,
            )
            .then<View.GeocodeAllView>((res) => res.data)
            .then<View.GeocodeInfoView | undefined>((res) =>
                res.localityInfo.administrative.length > 0
                    ? {
                          path: res.localityInfo.administrative.map((x) => x.name).join(', '),
                          description:
                              res.localityInfo.administrative[res.localityInfo.administrative.length - 1].description,
                      }
                    : undefined,
            );
    },
};

export const ReportAPI: IReportAPI = {
    ooptMetrics(model: Binding.OoptMetricsReportRequest): Promise<View.PageView<View.OoptMetricsResponse>> {
        const query = serialize(model);
        return instance
            .get<View.PageView<View.OoptMetricsResponse>>(`report/oopt-metrics?${query}`)
            .then((res) => res.data);
    },
};

export const PlantCharacteristicValueAPI: IPlantCharacteristicValueAPI = {
    getAll(): Promise<PlantCharacteristicValuesView[]> {
        return instance.get<PlantCharacteristicValuesView[]>('PlantCharacteristicValue').then((res) => res.data);
    },
    get(plantCharacteristicId: string): Promise<BaseView[]> {
        return instance.get<BaseView[]>(`PlantCharacteristicValue/${plantCharacteristicId}`).then((res) => res.data);
    },
};
