import * as View from 'api/view';
import axios from 'axios';
import { baseURL } from 'config';
import { genPair, parsePair } from 'utils/utils';

export const instance = axios.create({ baseURL });

instance.interceptors.request.use((config) => {
    const token = sessionStorage.getItem('token');
    if (token) {
        const accessToken = parsePair(token)[0];
        config.headers = {
            ...config.headers,
            Authorization: `Bearer ${accessToken}`,
        };
    }
    return config;
});

instance.interceptors.response.use(
    (res) => res,
    async (err) => {
        const originalConfig = err.config;
        if (originalConfig.url !== 'auth/login' && err.response) {
            if (err.response.status === 401) {
                try {
                    const token = sessionStorage.getItem('token');
                    if (token) {
                        const refreshToken = parsePair(token)[1];
                        const rs = await instance.post<View.RefreshTokenResponse>('/auth/refresh-token', {
                            refreshToken,
                        });
                        const nextToken = rs.data.token;
                        sessionStorage.setItem('token', genPair(nextToken, refreshToken));
                        return instance(originalConfig);
                    }
                } catch (_error) {
                    return Promise.reject(_error);
                }
            }
        }

        return Promise.reject(err);
    },
);
