import { LocalizeProperty, PositionModel } from 'api/models';

export interface BaseBinding {
    id?: string;
    name?: LocalizeProperty;
}

export interface AuthenticationRequestBinding {
    username: string;
    password: string;
}

export interface RedBookBinding extends BaseBinding {}
export interface RedBookPlantBinding extends BaseBinding {
    redBookId: string;
    rarityCategoryId: string;
    plantId: string;
}

export interface RarityCategoryBinding extends BaseBinding {}
export interface PlantCharacteristicBinding extends BaseBinding {}
export interface PlantCharacteristicValueBinding extends BaseBinding {
    plantCharacteristicId?: string;
}
export interface PlantCladeBinding extends BaseBinding {}

export interface PlantClassBinding extends BaseBinding {
    plantCladeId: string;
}

export interface PlantFamilyBinding extends BaseBinding {
    plantClassId?: string;
}

export interface ImageBinding {
    fileBase64: string;
    description: LocalizeProperty;
}

export interface PlantBinding extends BaseBinding {
    redBookComment: LocalizeProperty;
    ecologicalPhytocenoticGroupComment: LocalizeProperty;
    position: PositionModel;
    avatar?: ImageBinding;
    plantFamilyId: string;
    plantCharacteristicValues: PlantCharacteristicValueBinding[];
    ooptIds: string[];
    redBooksPlantIds: string[];
    authorIds: string[];
}

export interface OoptBinding extends BaseBinding {
    square?: number;
    metric8?: number;
    plantIds?: string[];
}

export interface MetricBinding extends BaseBinding {
    metricRaspreds?: string[];
}

export interface MetricRaspredBinding extends BaseBinding {
    bal?: number;
    value?: number;
    metricId?: string;
}

export interface PageBinding {
    page?: number;
    count?: number;
}

export interface SearchBinding<TFilter extends FilterBase> {
    filter?: TFilter | string;
    pagination?: PageBinding;
}

export interface RoleBinding extends BaseBinding {}
export interface UserBinding extends BaseBinding {
    username: string;
    passwordHash: string;
    email: string;
    roleIds: string[];
}

export interface FilterBase {
    objectIds?: string[];
    query?: string;
}
export interface PlantClassFilter extends FilterBase {
    plantCladeIds?: string[];
}

export interface PlantFilter extends FilterBase {
    ecologicalPhytocenoticGroupCommentQuery?: string;
    redBookCommentQuery?: string;

    plantCharacteristicValueIds?: string[];
    plantFamilyIds?: string[];
    rarityCategoryIds?: string[];
    redBookIds?: string[];
    authorIds?: string[];
    ooptIds?: string[];
}
export interface PlantFamilyFilter extends FilterBase {
    plantClassIds?: string[];
}

export interface PlantCladeFilter extends FilterBase {}
export interface RedBookFilter extends FilterBase {}
export interface RedBookPlantFilter extends FilterBase {}
export interface RarityCategoryFilter extends FilterBase {}
export interface UserFilter extends FilterBase {}
export interface OoptFilter extends FilterBase {}
export interface MetricFilter extends FilterBase {}
export interface MetricRaspredFilter extends FilterBase {}
export interface PlantCharacteristicFilter extends FilterBase {}

export interface Point {
    latitude: number;
    longitude: number;
}

export interface MapPositionRequestModel {
    points: Point[];
}

export interface OoptMetricsReportRequest {
    query?: string;
    pagination: PageBinding;
}
