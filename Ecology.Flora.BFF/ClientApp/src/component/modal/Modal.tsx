import {
    ModalPage,
    ModalPageHeader,
    ModalRoot,
    PanelHeaderClose,
    PanelHeaderSubmit,
    useAdaptivity,
    ViewWidth,
} from '@vkontakte/vkui';
import { FilterSwitch } from 'component/FilterSwitch';
import React from 'react';
import { connect, ConnectedProps } from 'react-redux';
import { useLocation } from 'react-router-dom';
import { RootState } from 'store';
import { closeModal } from 'store/slice/modalSlice';

const connector = connect(
    (state: RootState) => ({
        modalId: state.modal.modalId,
    }),
    {
        closeModal,
    },
);

interface Props extends ConnectedProps<typeof connector> {}

export const Modal = connector((props: Props) => {
    const { viewWidth } = useAdaptivity();
    const isDesktop = (viewWidth ?? ViewWidth.DESKTOP) >= ViewWidth.TABLET;
    const location = useLocation();

    return (
        <ModalRoot activeModal={props.modalId} onClose={() => props.closeModal()}>
            <ModalPage
                id="filters"
                onClose={() => props.closeModal()}
                header={
                    <ModalPageHeader
                        left={!isDesktop && <PanelHeaderClose onClick={() => props.closeModal()} />}
                        right={<PanelHeaderSubmit onClick={() => props.closeModal()} />}>
                        Фильтры
                    </ModalPageHeader>
                }>
                <FilterSwitch pathname={`/${location.pathname.split('/')[1]}`} />
            </ModalPage>
        </ModalRoot>
    );
});
