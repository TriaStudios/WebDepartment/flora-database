import { CellButton, Separator } from '@vkontakte/vkui';
import React from 'react';
import { useHistory } from 'react-router-dom';
import { MenuItem } from 'store/models';

interface Props {
    menuItem: MenuItem;
    index: number;
    pathname: string;
    onReload: () => void;
    inactive?: boolean;
}

export const LeftMenuButton = (props: Props) => {
    const history = useHistory();

    const to = (s: string) => {
        if (s === history.location.pathname) {
            props.onReload();
        } else {
            history.push({
                pathname: s,
                state: history.location.pathname,
            });
        }
    };

    return !props.menuItem.mobile ? (
        <div key={props.menuItem.title}>
            {props.menuItem.separatorBefore && <Separator />}
            <CellButton
                multiline
                onClick={() => to(`${props.pathname}${props.menuItem.id ? `/${props.menuItem.id}` : ''}`)}
                data-story={props.index}
                before={props.menuItem.Icon}
                disabled={props.inactive}
                style={{ color: 'black' }}>
                {props.menuItem.title} {props.menuItem.inactive && '(Скоро)'}
            </CellButton>
            {props.menuItem.separatorAfter && <Separator />}
        </div>
    ) : null;
};
