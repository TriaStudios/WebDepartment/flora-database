import { FormItem } from '@vkontakte/vkui';
import { ChipsInputOption } from '@vkontakte/vkui/dist/components/ChipsInput/ChipsInput';
import { ChipsSelect } from '@vkontakte/vkui/dist/unstable';
import { BaseView, PlantCharacteristicValuesView } from 'api/view';
import * as React from 'react';

interface ChipsetProps {
    characteristic?: PlantCharacteristicValuesView;
    all?: BaseView[];
    value: ChipsInputOption[];
    onChange: (x: ChipsInputOption[]) => void;
    title?: string;
    creatable?: boolean;
}

export const Chipset = (chipsetProps: ChipsetProps) => (
    <FormItem top={chipsetProps?.title ?? chipsetProps?.characteristic?.characteristic.name.ru}>
        <ChipsSelect
            options={(chipsetProps?.all ?? chipsetProps?.characteristic?.values ?? []).map(
                (x) =>
                    ({
                        label: x.name.ru,
                        value: x.id.toString(),
                    } as ChipsInputOption),
            )}
            placeholder="Не выбраны"
            creatable={chipsetProps.creatable}
            value={chipsetProps?.value ?? []}
            onChange={chipsetProps?.onChange}
        />
    </FormItem>
);
