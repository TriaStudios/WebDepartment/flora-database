import { Icon24ArrowRightOutline } from '@vkontakte/icons';
import { CellButton, FormItem, FormLayout, FormLayoutGroup, Input, Spinner, Title } from '@vkontakte/vkui';
import React, { ChangeEvent, useState } from 'react';
import { connect, ConnectedProps } from 'react-redux';
import { RootState } from 'store';
import { loginAsync } from 'store/slice/userSlice';

const connector = connect(
    (state: RootState) => ({
        error: state.user.error,
        pending: state.user.pending,
    }),
    { loginAsync },
);

interface Props extends ConnectedProps<typeof connector> {}

export const LoginForm = connector((props: Props) => {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');

    const onSubmit = () => {
        props.loginAsync({ username, password });
    };

    const onChangeUsername = (x: ChangeEvent<HTMLInputElement>) => {
        setUsername((x.currentTarget ?? x.target).value);
    };

    const onChangePassword = (x: ChangeEvent<HTMLInputElement>) => {
        setPassword((x.currentTarget ?? x.target).value);
    };

    return (
        <FormLayout>
            <FormLayoutGroup>
                <FormItem top="Логин" style={{ paddingBottom: 0 }}>
                    <Input
                        inputMode="text"
                        name="username"
                        value={username}
                        onChange={onChangeUsername}
                        disabled={props.pending}
                    />
                </FormItem>
                <FormItem top="Пароль" style={{ paddingBottom: 0 }}>
                    <Input
                        inputMode="text"
                        name="password"
                        type="password"
                        value={password}
                        onChange={onChangePassword}
                        disabled={props.pending}
                    />
                </FormItem>
                <FormItem>
                    <CellButton
                        type="submit"
                        onClick={onSubmit}
                        centered
                        style={{ borderRadius: 0 }}
                        after={
                            props.pending ? (
                                <Spinner style={{ marginLeft: '8px', marginRight: '2px' }} />
                            ) : (
                                <Icon24ArrowRightOutline />
                            )
                        }>
                        Войти
                    </CellButton>
                    <Title
                        level="3"
                        weight="heavy"
                        style={{ whiteSpace: 'normal', textAlign: 'center', color: 'var(--dynamic_red)' }}>
                        {props.error}
                    </Title>
                </FormItem>
            </FormLayoutGroup>
        </FormLayout>
    );
});
