import { FormItem, FormLayoutGroup, Textarea } from '@vkontakte/vkui';
import { LocalizeProperty } from 'api/models';
import * as React from 'react';

interface Props {
    name: LocalizeProperty | undefined;
    setName: (
        name: LocalizeProperty | undefined | ((prev: LocalizeProperty | undefined) => LocalizeProperty | undefined),
    ) => void;
    title: string;
    onBlur?: () => void;
    mode: 'horizontal' | 'vertical';
}

export const LocalizableNameFormItem = ({ name, setName, title, onBlur, mode }: Props) => (
    <FormItem top={title} style={mode === 'vertical' ? { paddingLeft: 0, paddingRight: 0 } : {}}>
        <FormLayoutGroup mode={mode} style={{ padding: 0 }}>
            <FormItem onBlur={onBlur}>
                <Textarea
                    rows={1}
                    placeholder="На латинице"
                    value={name?.la}
                    onChange={(x) =>
                        setName((prev) => ({
                            ...prev,
                            la: (x.currentTarget ?? x.target).value,
                        }))
                    }
                />
            </FormItem>
            <FormItem onBlur={onBlur}>
                <Textarea
                    rows={1}
                    placeholder="На русском"
                    value={name?.ru}
                    onChange={(x) =>
                        setName((prev) => ({
                            ...prev,
                            ru: (x.currentTarget ?? x.target).value,
                        }))
                    }
                />
            </FormItem>
            <FormItem onBlur={onBlur}>
                <Textarea
                    rows={1}
                    placeholder="На английском"
                    value={name?.en}
                    onChange={(x) =>
                        setName((prev) => ({
                            ...prev,
                            en: (x.currentTarget ?? x.target).value,
                        }))
                    }
                />
            </FormItem>
        </FormLayoutGroup>
    </FormItem>
);
