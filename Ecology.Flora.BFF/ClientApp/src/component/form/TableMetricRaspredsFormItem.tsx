import { Icon28AddCircleOutline, Icon28MenuOutline } from '@vkontakte/icons';
import { Group, Header, IconButton } from '@vkontakte/vkui';
import { MetricRaspredAPI } from 'api';
import { MetricRaspredForm } from 'component/form/MetricRaspredForm';
import React, { CSSProperties } from 'react';
import { DragDropContext, Draggable, Droppable, DropResult } from 'react-beautiful-dnd';

interface Props {
    value: string[];
    onChange: (raspreds: string[] | ((prev: string[]) => string[])) => void;
    metricId: string;
}

export const TableMetricRaspredsFormItem = (props: Props) => {
    const reorder = (list: string[], startIndex: number, endIndex: number) => {
        const result = Array.from(list);
        const [removed] = result.splice(startIndex, 1);
        result.splice(endIndex, 0, removed);

        return result;
    };

    const getListStyle = () =>
        ({
            display: 'flex',
            overflow: 'auto',
            textAlign: 'center',
        } as CSSProperties);

    const onDragEnd = (result: DropResult) => {
        if (!result.destination) {
            return;
        }
        const items = reorder(props.value, result.source.index, result.destination.index);
        props.onChange(items);
    };

    const onAddMetricRaspred = () => {
        MetricRaspredAPI.put({
            value: 0,
            bal: 0,
            name: {
                ru: `Распределение №${props.value.length + 1}`,
            },
            metricId: props.metricId,
        }).then((res) => props.onChange((prev) => [...prev, res]));
    };

    return (
        <Group
            mode="plain"
            style={{ padding: '0 1rem' }}
            header={
                <Header
                    aside={
                        <IconButton
                            style={{ color: 'mediumseagreen' }}
                            icon={<Icon28AddCircleOutline />}
                            onClick={onAddMetricRaspred}
                        />
                    }
                    mode="tertiary">
                    Распределения метрик
                </Header>
            }>
            <Group style={{ textAlign: 'center' }}>
                <DragDropContext onDragEnd={onDragEnd}>
                    <Droppable droppableId="droppable" direction="horizontal">
                        {(provided) => (
                            <div ref={provided.innerRef} style={getListStyle()} {...provided.droppableProps}>
                                {props.value.map((x, index) => (
                                    <Draggable key={x} draggableId={x} index={index}>
                                        {(provided2) => (
                                            <div
                                                ref={provided2.innerRef}
                                                {...provided2.draggableProps}
                                                {...provided2.dragHandleProps}>
                                                <Icon28MenuOutline style={{ margin: '0 auto' }} />
                                                <MetricRaspredForm
                                                    id={x}
                                                    onDelete={() =>
                                                        props.onChange((prev) => prev.filter((y) => y !== x))
                                                    }
                                                    metricId={props.metricId}
                                                />
                                            </div>
                                        )}
                                    </Draggable>
                                ))}
                            </div>
                        )}
                    </Droppable>
                </DragDropContext>
            </Group>
        </Group>
    );
};
