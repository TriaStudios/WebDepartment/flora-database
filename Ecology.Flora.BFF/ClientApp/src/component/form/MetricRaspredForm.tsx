import { isNumeric } from '@vkontakte/vkjs';
import { CellButton, FormItem, FormLayout, Group, Input } from '@vkontakte/vkui';
import { MetricRaspredAPI } from 'api';
import { MetricRaspredBinding } from 'api/binding';
import { LocalizeProperty } from 'api/models';
import { LocalizableNameFormItem } from 'component/form/LocalizableNameFormItem';
import React, { useCallback, useEffect, useState } from 'react';

interface Props {
    id?: string;
    onDelete: () => void;
    metricId: string;
}

export function MetricRaspredForm(props: Props) {
    const [name, setName] = useState<LocalizeProperty>();
    const [bal, setBal] = useState<string>();
    const [value, setValue] = useState<string>();

    useEffect(() => {
        if (props.id) {
            MetricRaspredAPI.get(props.id).then((res) => {
                setName(res.name);
                setBal(res.bal.toString());
                setValue(res.value.toString());
            });
        }
    }, [props.id]);

    const createModel = useCallback(
        () =>
            ({
                id: props.id,
                name,
                bal: +(bal ?? 0),
                value: +(value ?? 0),
                metricId: props.metricId,
            } as MetricRaspredBinding),
        [name, props.id, props.metricId, bal, value],
    );

    const onSave = useCallback(() => {
        MetricRaspredAPI.patch(createModel());
    }, [name, props.id, props.metricId, bal, value, createModel]);

    return (
        <Group mode="plain">
            <FormLayout>
                <LocalizableNameFormItem name={name} setName={setName} title="Имя" onBlur={onSave} mode="vertical" />
                <FormItem top="Количество баллов" status={isNumeric(bal) ? 'default' : 'error'} onBlur={onSave}>
                    <Input
                        inputMode="numeric"
                        value={bal}
                        onChange={(x: { currentTarget: any; target: any }) =>
                            setBal((x.currentTarget ?? x.target).value)
                        }
                    />
                </FormItem>
                <FormItem
                    top="Условие больше либо равно"
                    status={isNumeric(value) ? 'default' : 'error'}
                    onBlur={onSave}>
                    <Input
                        inputMode="numeric"
                        value={value}
                        onChange={(x: { currentTarget: any; target: any }) =>
                            setValue((x.currentTarget ?? x.target).value)
                        }
                    />
                </FormItem>
                <FormItem>
                    <CellButton
                        style={{ color: 'var(--dynamic_red)' }}
                        onClick={() => props.id && MetricRaspredAPI.delete(props.id).then(props.onDelete)}
                        centered>
                        Удалить
                    </CellButton>
                </FormItem>
            </FormLayout>
        </Group>
    );
}
