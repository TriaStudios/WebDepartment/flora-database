import { Group, useAdaptivity, ViewWidth } from '@vkontakte/vkui';
import { Roles } from 'api/enums';
import { SadHamburger } from 'component/SadHamburger/SadHamburger';
import { AdminPage } from 'page/admin/AdminPage';
import { AnalyticsPage } from 'page/analytics/AnalyticsPage';
import { CatalogPage } from 'page/catalog/CatalogPage';
import { MainPage } from 'page/main/MainPage';
import { MapPage } from 'page/MapPage';
import { SearchFamilyByPhoto } from 'page/SearchFamilyByPhoto';
import { SearchPageMobile } from 'page/SearchPageMobile';
import { UserPageMobile } from 'page/UserPageMobile';
import React from 'react';
import { connect, ConnectedProps } from 'react-redux';
import { Redirect, Route, Switch, useRouteMatch } from 'react-router-dom';
import { RootState } from 'store';

const connector = connect(
    (state: RootState) => ({
        user: state.user.user,
    }),
    {},
);

interface Props extends ConnectedProps<typeof connector> {
    isReload: boolean;
}

export const AppSwitch = connector((props: Props) => {
    const { viewWidth } = useAdaptivity();
    const isDesktop = (viewWidth ?? ViewWidth.DESKTOP) >= ViewWidth.TABLET;
    let { path } = useRouteMatch();
    if (path === '/') path = '';

    return (
        <Switch>
            <Route exact path={`${path}/`}>
                <Redirect to={`${path}/main`} />
            </Route>
            <Route path={`${path}/main`}>
                <MainPage />
            </Route>
            <Route path={`${path}/catalog`}>
                <CatalogPage isReload={props.isReload} />
            </Route>
            {props.user?.roles.find((x) => x.name.ru === Roles.ADMIN) !== undefined && (
                <Route path={`${path}/admin`}>
                    <AdminPage isReload={props.isReload} />
                </Route>
            )}
            <Route path={`${path}/analytics`}>
                <AnalyticsPage />
            </Route>
            <Route path={`${path}/search`}>
                {isDesktop ? <Redirect to={`${path}/main`} /> : undefined}
                <SearchPageMobile />
            </Route>
            <Route path={`${path}/map`}>
                <MapPage />
            </Route>
            <Route path={`${path}/search-family-by-photo`}>
                <SearchFamilyByPhoto />
            </Route>
            <Route path={`${path}/user`}>
                {isDesktop ? <Redirect to={`${path}/main`} /> : undefined}
                <UserPageMobile />
            </Route>
            <Route>
                <Group>
                    <SadHamburger outerBackgroundColor="white" />
                </Group>
            </Route>
        </Switch>
    );
});
