import { Avatar, SimpleCell } from '@vkontakte/vkui';
import { ImageSize } from 'api/enums';
import { BaseView } from 'api/view';
import React from 'react';
import { useHistory } from 'react-router-dom';
import { fileDataToUrl } from 'utils/utils';

interface Props<T extends BaseView> {
    icon?: React.FC;
    path: string;
    item: T;
    getTitle: (item: T) => React.ReactNode;
    image?: string;
}

export function DefaultCell<T extends BaseView>(props: Props<T>) {
    const history = useHistory();
    const Icon = props.icon;

    return (
        <SimpleCell
            before={
                Icon ? (
                    <Icon />
                ) : (
                    <Avatar
                        size={72}
                        mode={props.image ? 'image' : 'default'}
                        src={props.image ? fileDataToUrl(props.image, ImageSize.Small) : '/notAvatar.jpg'}
                    />
                )
            }
            expandable
            onClick={() => {
                history.push({
                    pathname: `${props.path}/edit/${props.item.id}`,
                });
            }}>
            {props.getTitle(props.item)}
        </SimpleCell>
    );
}
