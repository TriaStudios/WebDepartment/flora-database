import { Panel, View } from '@vkontakte/vkui';
import { PanelHeaderSearch } from 'component/panelHeader/PanelHeaderSearch';
import { PanelHeaderTitle } from 'component/panelHeader/PanelHeaderTitle';
import React from 'react';

export const GlobalView = (props: {
    id: string;
    isDesktop?: boolean;
    children?: React.ReactNode;
    modal?: JSX.Element;
}) => (
    <View id={props.id} activePanel={props.id} modal={props.modal}>
        <Panel id={props.id}>
            {props.isDesktop ? <PanelHeaderSearch /> : <PanelHeaderTitle />}
            {props.children}
        </Panel>
    </View>
);
