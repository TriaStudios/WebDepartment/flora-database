import { Icon28ArrowLeftOutline } from '@vkontakte/icons';
import { CellButton, Group, Panel, Separator, SplitCol, View } from '@vkontakte/vkui';
import { Roles } from 'api/enums';
import { FilterSwitch } from 'component/FilterSwitch';
import { LeftMenuButton } from 'component/LeftMenuButton';
import { PanelHeaderTitle } from 'component/panelHeader/PanelHeaderTitle';
import React from 'react';
import { connect, ConnectedProps } from 'react-redux';
import { Route, Switch, useHistory, useRouteMatch } from 'react-router-dom';
import { RootState } from 'store';
import { buttonsAdmin, buttonsCatalog, buttonsMain } from 'utils/menuButtons';

const connector = connect(
    (state: RootState) => ({
        user: state.user.user,
    }),
    {},
);

interface Props extends ConnectedProps<typeof connector> {
    onReload: () => void;
}

export const LeftMenu = connector((props: Props) => {
    const history = useHistory();
    let { path } = useRouteMatch();
    if (path === '/') path = '';

    const reload = () => {
        props.onReload();
    };

    return (
        <SplitCol width="20rem" maxWidth="20rem" animate={false} spaced>
            <View activePanel="kek">
                <Panel id="kek">
                    <PanelHeaderTitle />
                    <Switch>
                        <Route path={`${path}/catalog`}>
                            <Group mode="plain">
                                <CellButton
                                    before={<Icon28ArrowLeftOutline />}
                                    style={{
                                        color: 'black',
                                        minHeight: '35px',
                                    }}
                                    onClick={() => history.push('/')}>
                                    На главную
                                </CellButton>
                                <Separator />
                                {Object.values(buttonsCatalog).map((x, index) => (
                                    <LeftMenuButton
                                        key={x.title}
                                        menuItem={x}
                                        index={index}
                                        inactive={x.inactive}
                                        pathname="/catalog"
                                        onReload={reload}
                                    />
                                ))}
                            </Group>

                            <FilterSwitch pathname={`${path}/catalog`} />
                        </Route>
                        {props.user?.roles.find((x) => x.name.ru === Roles.ADMIN) !== undefined && (
                            <Route path={`${path}/admin`}>
                                <Group mode="plain">
                                    <CellButton
                                        before={<Icon28ArrowLeftOutline />}
                                        style={{
                                            color: 'black',
                                            minHeight: '35px',
                                        }}
                                        onClick={() => history.push('/')}>
                                        На главную
                                    </CellButton>
                                    <Separator />
                                    {Object.values(buttonsAdmin).map((x, index) => (
                                        <LeftMenuButton
                                            key={x.id}
                                            menuItem={x}
                                            index={index}
                                            inactive={x.inactive}
                                            pathname="/admin"
                                            onReload={reload}
                                        />
                                    ))}
                                </Group>

                                <FilterSwitch pathname={`${path}/admin`} />
                            </Route>
                        )}
                        <Route path={path}>
                            <Group mode="plain">
                                {Object.values(buttonsMain).map(
                                    (x, index) =>
                                        !x.mobile &&
                                        (!x.admin ||
                                            props.user?.roles.find((y) => y.name.ru === Roles.ADMIN) !== undefined) && (
                                            <LeftMenuButton
                                                key={x.id}
                                                menuItem={x}
                                                index={index}
                                                inactive={x.inactive}
                                                pathname={path}
                                                onReload={reload}
                                            />
                                        ),
                                )}
                            </Group>
                        </Route>
                    </Switch>
                </Panel>
            </View>
        </SplitCol>
    );
});
