import { Group, Header } from '@vkontakte/vkui';
import { PlantFilter } from 'component/catalog/plant/PlantFilter';
import { PlantClassFilterMenu } from 'component/catalog/plantClass/PlantClassFilterMenu';
import { PlantFamilyFilter } from 'component/catalog/plantFamily/PlantFamilyFilter';
import React from 'react';
import { Route, Switch } from 'react-router-dom';

interface Props {
    pathname: string;
}

export const FilterSwitch = (props: Props) => (
    <Switch>
        <Route exact path={`${props.pathname}/plant-classes`}>
            <Group mode="plain" header={<Header>Фильтр</Header>}>
                <PlantClassFilterMenu />
            </Group>
        </Route>
        <Route exact path={`${props.pathname}/plant-families`}>
            <Group mode="plain" header={<Header>Фильтр</Header>}>
                <PlantFamilyFilter />
            </Group>
        </Route>
        <Route exact path={`${props.pathname}/plants`}>
            <Group mode="plain" header={<Header>Фильтр</Header>}>
                <PlantFilter />
            </Group>
        </Route>
    </Switch>
);
