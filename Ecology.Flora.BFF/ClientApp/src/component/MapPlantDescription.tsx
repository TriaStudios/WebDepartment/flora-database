import { Spinner } from '@vkontakte/vkui';
import { PlantView } from 'api/view';
import React from 'react';

interface Props {
    plant: PlantView | undefined;
}

export const MapPlantDescription = (props: Props) =>
    props.plant ? <>{props.plant.name.ru}</> : <Spinner size="small" />;
