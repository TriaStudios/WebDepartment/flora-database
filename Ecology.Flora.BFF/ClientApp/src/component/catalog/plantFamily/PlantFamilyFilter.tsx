import { ChipsInputOption } from '@vkontakte/vkui/dist/components/ChipsInput/ChipsInput';
import { PlantClassAPI } from 'api';
import { BaseView } from 'api/view';
import { Chipset } from 'component/form/Chipset';
import { useQuery } from 'hook/useQuery';
import React, { useEffect, useState } from 'react';

export const PlantFamilyFilter = () => {
    const [plantClasses, setPlantClasses] = useState<BaseView[]>([]);
    const [selectedPlantClasses, setSelectedPlantClasses] = useState<ChipsInputOption[]>([]);
    const { query, setQuery, deleteQuery } = useQuery();

    useEffect(() => {
        PlantClassAPI.searchLinks().then((res) => {
            setPlantClasses(res.elements);
            setSelectedPlantClasses(
                query.getAll('plantClassIds').map(
                    (x) =>
                        ({
                            label: res.elements.filter((y) => y.id === x)[0].name.ru,
                            value: x,
                        } as ChipsInputOption),
                ),
            );
        });
    }, []);

    useEffect(() => {
        if (selectedPlantClasses.length) {
            deleteQuery({ name: 'plantClassIds' });
            setQuery({ name: 'plantClassIds', values: selectedPlantClasses.map((y) => y.value as string) });
        }
    }, [selectedPlantClasses]);

    return <Chipset all={plantClasses} onChange={setSelectedPlantClasses} title="Отдел" value={selectedPlantClasses} />;
};
