import { Icon28SubtitlesOutline } from '@vkontakte/icons';
import { SimpleCell } from '@vkontakte/vkui';
import { PlantFamilyView } from 'api/view';
import React from 'react';
import { useHistory } from 'react-router-dom';

interface Props {
    plantFamily: PlantFamilyView;
}

export const PlantFamilyCell = (props: Props) => {
    const history = useHistory();

    return (
        <SimpleCell
            before={<Icon28SubtitlesOutline />}
            expandable
            onClick={() => history.push(`/catalog/plants?plantFamilyIds=${props.plantFamily.id}`)}>
            {props.plantFamily.name.ru} ({props.plantFamily.name.la})
        </SimpleCell>
    );
};
