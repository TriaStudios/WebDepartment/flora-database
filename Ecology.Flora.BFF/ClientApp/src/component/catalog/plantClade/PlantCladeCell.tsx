import { Icon28SubtitlesOutline } from '@vkontakte/icons';
import { SimpleCell } from '@vkontakte/vkui';
import { PlantCladeView } from 'api/view';
import React from 'react';
import { useHistory } from 'react-router-dom';

interface Props {
    department: PlantCladeView;
}

export const PlantCladeCell = (props: Props) => {
    const history = useHistory();

    return (
        <SimpleCell
            before={<Icon28SubtitlesOutline />}
            expandable
            onClick={() => history.push(`/catalog/plant-classes?plantCladeIds=${props.department.id}`)}>
            {props.department.name.ru} ({props.department.name.la})
        </SimpleCell>
    );
};
