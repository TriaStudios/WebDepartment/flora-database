import { Icon28SubtitlesOutline } from '@vkontakte/icons';
import { SimpleCell } from '@vkontakte/vkui';
import { PlantClassView } from 'api/view';
import React from 'react';
import { useHistory } from 'react-router-dom';

interface Props {
    category: PlantClassView;
}

export const PlantClassCell = (props: Props) => {
    const history = useHistory();

    return (
        <SimpleCell
            before={<Icon28SubtitlesOutline />}
            expandable
            onClick={() => history.push(`/catalog/plant-families?plantClassIds=${props.category.id}`)}>
            {props.category.name.ru} ({props.category.name.la})
        </SimpleCell>
    );
};
