import { ChipsInputOption } from '@vkontakte/vkui/dist/components/ChipsInput/ChipsInput';
import { PlantCladeAPI } from 'api';
import { PlantCladeView } from 'api/view';
import { Chipset } from 'component/form/Chipset';
import { useQuery } from 'hook/useQuery';
import React, { useEffect, useState } from 'react';

export const PlantClassFilterMenu = () => {
    const [plantClades, setPlantClades] = useState<PlantCladeView[]>([]);
    const [selectedPlantClades, setSelectedPlantClades] = useState<ChipsInputOption[]>([]);
    const { query, setQuery, deleteQuery } = useQuery();

    useEffect(() => {
        PlantCladeAPI.searchLinks().then((res) => {
            setPlantClades(res.elements);
            setSelectedPlantClades(
                query.getAll('plantCladeIds').map(
                    (x) =>
                        ({
                            label: res.elements.filter((y) => y.id === x)[0].name.ru,
                            value: x,
                        } as ChipsInputOption),
                ),
            );
        });
    }, []);

    useEffect(() => {
        if (selectedPlantClades.length) {
            deleteQuery({ name: 'plantCladeIds' });
            setQuery({ name: 'plantCladeIds', values: selectedPlantClades.map((y) => y.value as string) });
        }
    }, [selectedPlantClades]);

    return <Chipset all={plantClades} onChange={setSelectedPlantClades} title="Отдел" value={selectedPlantClades} />;
};
