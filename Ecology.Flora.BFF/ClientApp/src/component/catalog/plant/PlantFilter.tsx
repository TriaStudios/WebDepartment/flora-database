import { ChipsInputOption } from '@vkontakte/vkui/dist/components/ChipsInput/ChipsInput';
import { OoptAPI, PlantCharacteristicValueAPI, PlantFamilyAPI, RarityCategoryAPI, RedBookAPI, UserAPI } from 'api';
import { FilterBase } from 'api/binding';
import { ISearch } from 'api/interfaces';
import { BaseView, PlantCharacteristicValuesView } from 'api/view';
import { Chipset } from 'component/form/Chipset';
import { DelQuery, Query, useQuery, UseQueryProps } from 'hook/useQuery';
import React, { useCallback, useEffect, useState } from 'react';

export const PlantFilter = () => {
    const [plantFamilies, setPlantFamilies] = useState<BaseView[]>([]);
    const [rarityCategories, setRarityCategories] = useState<BaseView[]>([]);
    const [redBook, setRedBooks] = useState<BaseView[]>([]);
    const [authors, setAuthors] = useState<BaseView[]>([]);
    const [oopts, setOopts] = useState<BaseView[]>([]);
    const [characteristics, setCharacteristics] = useState<PlantCharacteristicValuesView[]>([]);
    const q = useQuery();

    const handleApi = useCallback((res: PlantCharacteristicValuesView[]) => {
        setCharacteristics(res);
        const queries: Query[] = [];
        res.forEach((x) => {
            const buf = q.query.getAll(x.characteristic.name.la!).filter((y) => x.values.find((z) => z.id === y));
            queries.push({ name: x.characteristic.name.la!, values: buf });
        });
        q.deleteQuery(
            ...queries.map(
                (x) =>
                    ({
                        name: x.name,
                    } as DelQuery),
            ),
        );
        q.setQuery(...queries);
    }, []);

    useEffect(() => {
        const load = <TFilter extends FilterBase, TDto extends BaseView>(
            search: ISearch<TFilter, TDto>,
            save: (m: Array<BaseView>) => void,
        ) => {
            search.searchLinks<BaseView>().then((res) => {
                save(res.elements);
            });
        };
        load(PlantFamilyAPI, setPlantFamilies);
        load(RarityCategoryAPI, setRarityCategories);
        load(RedBookAPI, setRedBooks);
        load(UserAPI, setAuthors);
        load(OoptAPI, setOopts);
        PlantCharacteristicValueAPI.getAll().then(handleApi);
    }, [handleApi]);

    const SelectComponent = (innerProps: {
        title: string;
        idName: string;
        characteristic?: PlantCharacteristicValuesView;
        all?: BaseView[];
        q: UseQueryProps;
    }) => {
        const getValue = useCallback(
            () =>
                innerProps.q.query
                    .getAll(innerProps.idName)
                    .map((x) => {
                        const buf = (innerProps.all ?? innerProps.characteristic?.values)?.find((y) => y.id === x);
                        if (!buf) {
                            return undefined;
                        }
                        return {
                            label: buf.name.ru,
                            value: x,
                        } as ChipsInputOption;
                    })
                    .filter((x) => x)
                    .map((x) => x!),
            [innerProps],
        );

        return (
            <Chipset
                characteristic={innerProps.characteristic}
                all={innerProps.all}
                value={getValue()}
                onChange={(x) => {
                    innerProps.q.deleteQuery({ name: innerProps.idName });
                    innerProps.q.setQuery({ name: innerProps.idName, values: x.map((y) => y.value as string) });
                }}
                title={innerProps.title}
            />
        );
    };

    return (
        <div>
            <SelectComponent title="ООПТ" idName="ooptIds" all={oopts} q={q} />
            <SelectComponent title="Подкатегория" idName="plantFamilyIds" all={plantFamilies} q={q} />
            <SelectComponent title="Категория редкости" idName="rarityCategoryIds" all={rarityCategories} q={q} />
            <SelectComponent title="Красная книга" idName="redBookIds" all={redBook} q={q} />
            <SelectComponent title="Автор" idName="authorIds" all={authors} q={q} />
            {characteristics.map((x) => (
                <SelectComponent
                    key={x.characteristic.id}
                    title={x.characteristic.name.ru!}
                    idName={x.characteristic.name.la!}
                    characteristic={x}
                    q={q}
                />
            ))}
        </div>
    );
};
