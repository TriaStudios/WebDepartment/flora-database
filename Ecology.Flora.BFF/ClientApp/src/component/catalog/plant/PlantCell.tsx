import { Icon24Home } from '@vkontakte/icons';
import { Avatar, RichCell, SimpleCell } from '@vkontakte/vkui';
import { ImageSize } from 'api/enums';
import { PlantView, RedBookPlantView } from 'api/view';
import React from 'react';
import { useHistory } from 'react-router-dom';
import { fileDataToUrl } from 'utils/utils';

interface Props {
    plant: PlantView;
}

export const PlantCell = (props: Props) => {
    const history = useHistory();

    const toKK = (kk: RedBookPlantView) => `${kk.rarityCategory.name.en} - ${kk.rarityCategory.name.ru}`;

    return (
        <RichCell
            onClick={() => history.push(`/catalog/plant?plantId=${props.plant.id}`)}
            multiline
            before={
                <Avatar
                    size={72}
                    mode={props.plant.avatarId ? 'image' : 'default'}
                    src={props.plant.avatarId ? fileDataToUrl(props.plant.avatarId, ImageSize.Small) : '/notAvatar.jpg'}
                />
            }
            text={props.plant.name.ru}
            caption={props.plant.redBookPlants?.map((x) => `${x.redBook.name.ru}: ${toKK(x)} `) ?? ''}>
            <SimpleCell
                style={{ padding: '0' }}
                disabled
                before={props.plant.name.la}
                after={
                    <SimpleCell
                        disabled
                        before={
                            props.plant.plantCharacteristicValues?.find(
                                (x) => x.characteristic.name.la === 'OriginSpecies',
                            )?.values[0].name.ru
                        }
                        after={
                            <Icon24Home
                                style={{
                                    color:
                                        props.plant.plantCharacteristicValues?.find(
                                            (x) => x.characteristic.name.la === 'OriginSpecies',
                                        )?.values[0].name.ru === 'аборигенное'
                                            ? 'var(--dynamic_green)'
                                            : 'var(--dynamic_red)',
                                }}
                            />
                        }
                    />
                }
            />
        </RichCell>
    );
};
