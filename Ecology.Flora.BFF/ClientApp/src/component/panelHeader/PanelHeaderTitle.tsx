import { Icon24ImageFilterOutline } from '@vkontakte/icons';
import { PanelHeader } from '@vkontakte/vkui';
import React from 'react';

export const PanelHeaderTitle = () => <PanelHeader left={<Icon24ImageFilterOutline />}>FLORA database</PanelHeader>;
