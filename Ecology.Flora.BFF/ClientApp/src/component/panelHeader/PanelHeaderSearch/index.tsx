import {
    Icon16Chevron,
    Icon24ChevronRight,
    Icon28ChevronRightCircleOutline,
    Icon28GhostSimleOutline,
    Icon28User,
} from '@vkontakte/icons';
import { classNames } from '@vkontakte/vkjs';
import { Avatar, Card, CellButton, PanelHeader, Search, Separator, SimpleCell } from '@vkontakte/vkui';
import { LoginForm } from 'component/form/LoginForm';
import styles from 'component/panelHeader/PanelHeaderSearch/index.module.scss';
import { useClickOutside } from 'hook/useClickOutside';
import { useTotalSearch } from 'hook/useTotalSearch';
import React, { useEffect, useRef, useState } from 'react';
import { connect, ConnectedProps } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { RootState } from 'store';
import { logout } from 'store/slice/userSlice';
import { buttonsCatalog } from 'utils/menuButtons';

const connector = connect(
    (state: RootState) => ({
        loaded: state.user.loaded,
        user: state.user.user,
    }),
    {
        logout,
    },
);

interface Props extends ConnectedProps<typeof connector> {}

export const PanelHeaderSearch = connector((props: Props) => {
    const [open, setOpen] = useState(false);
    const [searchOpen, setSearchOpen] = useState(false);
    const [search, setSearch] = useState('');

    const searchData = useTotalSearch(search);

    const history = useHistory();

    const userCellRef = useRef<any>();
    const searchRef = useRef<any>();
    const subSearchRef = useRef<HTMLInputElement | null>(null);

    const chevronClasses = classNames(styles.chevron, open && styles.open);
    const dropdownClasses = classNames(styles.dropdown, open && styles.open);
    const searchDropClasses = classNames(styles.dropdown, searchOpen && styles.open);
    const simpleCellClasses = classNames(styles.userTopCard, open && styles.open);

    useEffect(() => {
        if (searchOpen) {
            setSearch('');
        }
    }, [searchOpen]);

    useClickOutside(() => {
        setOpen(false);
    }, userCellRef);

    useClickOutside(() => {
        setSearchOpen(false);
    }, searchRef);

    useEffect(() => {
        if (props.loaded) {
            setOpen(false);
        }
    }, [props.loaded]);

    useEffect(() => {
        if (searchOpen) {
            subSearchRef.current?.focus();
        }
    }, [searchOpen]);

    const onLogout = () => {
        props.logout();
        history.go(0);
    };

    return (
        <PanelHeader
            left={
                <div className={styles.parentSearch} ref={searchRef}>
                    <Search
                        className={styles.search}
                        placeholder="Поиск"
                        value={search}
                        onChange={(e: { currentTarget: any; target: any }) =>
                            setSearch((e.currentTarget ?? e.target).value)
                        }
                        onFocusCapture={() => setSearchOpen(true)}
                    />

                    <Card mode="shadow" className={searchDropClasses}>
                        <Search
                            getRef={subSearchRef}
                            placeholder="Поиск"
                            value={search}
                            onChange={(e: { currentTarget: any; target: any }) =>
                                setSearch((e.currentTarget ?? e.target).value)
                            }
                            autoFocus
                            style={{ backgroundColor: 'transparent', margin: '1rem calc(1rem - 4px)' }}
                        />
                        {Object.entries(buttonsCatalog)
                            .filter((x) => x[1].id)
                            .map((y) => (
                                <div key={y[0]}>
                                    <Separator />
                                    <SimpleCell
                                        disabled
                                        before={y[1].Icon}
                                        after={
                                            <CellButton
                                                style={{ borderRadius: '0 0 10px 10px' }}
                                                after={<Icon24ChevronRight />}
                                                onClick={() => {
                                                    history.push(`/catalog/${y[1].id}?query=${search}`);
                                                    setSearchOpen(false);
                                                    setSearch('');
                                                }}>
                                                Развернуть
                                            </CellButton>
                                        }
                                        style={{ margin: '0 1rem' }}>
                                        {y[1].title}
                                    </SimpleCell>
                                    {searchData[y[0]]?.map((x) => (
                                        <CellButton
                                            key={x.id}
                                            style={{ borderRadius: '0', padding: '0 2rem', color: 'black' }}
                                            onClick={() => {
                                                history.push(`/catalog/${y[1].child}?${y[1].fieldFilter}=${x.id}`);
                                                setSearchOpen(false);
                                                setSearch('');
                                            }}>
                                            {x.name.ru} ({x.name.la})
                                        </CellButton>
                                    ))}
                                    {searchData[y[0]]?.length === 0 ? (
                                        <p style={{ textAlign: 'center', color: 'black' }}>Ничего нет</p>
                                    ) : undefined}
                                </div>
                            ))}
                    </Card>
                </div>
            }
            right={
                <div className={styles.parentDropdown} ref={userCellRef}>
                    <SimpleCell
                        hoverMode={styles.hover}
                        activeMode="active"
                        onClick={() => setOpen((old) => !old)}
                        className={simpleCellClasses}
                        before={
                            <Avatar width={28} height={28} shadow={false}>
                                <Icon28User />
                            </Avatar>
                        }
                        after={<Icon16Chevron className={chevronClasses} />}>
                        <div style={{ userSelect: 'none' }}>
                            {props.loaded
                                ? ((name: string[]) => `${name[0]} ${(name[1] ?? ' ')[0]}. ${(name[2] ?? ' ')[0]}.`)(
                                      props.user?.name.ru?.split(' ') ?? [],
                                  )
                                : 'Пользователь'}
                        </div>
                    </SimpleCell>
                    {props.loaded ? (
                        <Card mode="outline" className={dropdownClasses}>
                            <CellButton
                                before={<Icon28GhostSimleOutline />}
                                activeMode="active"
                                className={styles.dropdownItem}
                                onClick={() => setOpen(false)}>
                                Моя Страница
                            </CellButton>
                            <CellButton
                                before={<Icon28ChevronRightCircleOutline />}
                                activeMode="active"
                                className={styles.dropdownItem}
                                onClick={onLogout}>
                                Выйти
                            </CellButton>
                        </Card>
                    ) : (
                        <Card mode="outline" className={dropdownClasses}>
                            <LoginForm />
                        </Card>
                    )}
                </div>
            }
        />
    );
});
