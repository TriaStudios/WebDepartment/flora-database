import { useEffect } from 'react';

export const useVisibilityPage = (visibled: () => void, invisibled?: () => void): void => {
    const handleVisibilityEvent = () => (document.hidden ? invisibled && invisibled() : visibled());

    useEffect(() => {
        document.addEventListener('visibilitychange', handleVisibilityEvent);
        return () => document.removeEventListener('visibilitychange', handleVisibilityEvent);
    }, [invisibled, visibled]);

    useEffect(() => {
        handleVisibilityEvent();
    }, []);
};
