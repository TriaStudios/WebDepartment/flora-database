import { useEffect, useState } from 'react';
import { useHistory, useLocation } from 'react-router-dom';

export interface UseQueryProps {
    query: URLSearchParams;
    setQuery: (...query: Query[]) => void;
    deleteQuery: (...query: DelQuery[]) => void;
}

export interface Query {
    name: string;
    values: string[] | string;
}

export interface DelQuery {
    name: string;
    values?: string[] | string | null;
}

export const useQuery = (): UseQueryProps => {
    const location = useLocation();
    const history = useHistory();
    const [query, setQueryAll] = useState(new URLSearchParams(location.search));
    const setQuery = (...qs: Query[]) => {
        qs.forEach((q) => {
            if (Array.isArray(q.values)) {
                q.values.forEach((x) => query.append(q.name, x));
            } else {
                query.append(q.name, q.values);
            }
        });
        history.push(`${location.pathname}?${query}`);
    };

    const deleteQuery = (...qs: DelQuery[]) => {
        qs.forEach((q) => {
            const all = query.getAll(q.name);
            const value = q.values;
            if (value) {
                if (Array.isArray(value)) {
                    if (value.length > 0) {
                        query.delete(q.name);
                        all.filter((x) => !value.find((y) => y === x)).forEach((x) => query.append(q.name, x));
                    }
                } else if (all.find((x) => x === value)) {
                    query.delete(q.name);
                    all.filter((x) => x !== value).forEach((x) => query.append(q.name, x));
                } else {
                    query.delete(q.name);
                }
            } else {
                query.delete(q.name);
            }
        });
        history.push(`${location.pathname}?${query}`);
    };

    useEffect(() => {
        setQueryAll(new URLSearchParams(location.search));
    }, [location.search]);

    return { query, setQuery, deleteQuery };
};
