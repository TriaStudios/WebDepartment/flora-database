import { TotalSearchAPI } from 'api';
import { BaseView } from 'api/view';
import { useEffect, useState } from 'react';

export const useTotalSearch = (search: string) => {
    const [searchData, setSearchData] = useState<{ [key: string]: Array<BaseView> | undefined }>({
        plants: undefined,
        plantClade: undefined,
        plantClasses: undefined,
        plantFamilies: undefined,
    });

    useEffect(() => {
        TotalSearchAPI.search(search).then((res) => {
            setSearchData({
                plants: res[0].map((x) => ({ name: x.name, id: x.id })),
                plantClade: res[1].map((x) => ({ name: x.name, id: x.id })),
                plantClasses: res[2].map((x) => ({ name: x.name, id: x.id })),
                plantFamilies: res[3].map((x) => ({ name: x.name, id: x.id })),
            });
        });
    }, [search]);

    return searchData;
};
