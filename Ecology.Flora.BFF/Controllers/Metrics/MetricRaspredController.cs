﻿using Ecology.Flora.DAL.Logic.BindingModels.Metrics;
using Ecology.Flora.DAL.Logic.Crud.Metrics;
using Ecology.Flora.DAL.Logic.ViewModels.Metrics;

namespace Ecology.Flora.BFF.Controllers.Metrics;

public class MetricRaspredController : BaseCrudController<MetricRaspredViewModel, MetricRaspredBindingModel, FilterBase,
    IMetricRaspredCrudAsync>
{
    public MetricRaspredController(IMetricRaspredCrudAsync service) : base(service)
    {
    }
}