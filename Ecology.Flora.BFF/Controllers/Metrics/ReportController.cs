﻿using Ecology.Flora.DAL.Logic.Crud.Plants;

namespace Ecology.Flora.BFF.Controllers.Metrics;

[ApiController]
[Route(Consts.RouteWithControllerName)]
public class ReportController : ControllerBase
{
    private readonly IOoptCrudAsync _ooptCrudAsync;
    private readonly IMetricService _metricService;

    public ReportController(IOoptCrudAsync ooptCrudAsync, IMetricService metricService)
    {
        _ooptCrudAsync = ooptCrudAsync;
        _metricService = metricService;
    }

    [HttpGet]
    [ProducesResponseType(typeof(PageView<OoptMetricsResponse>), StatusCodes.Status200OK)]
    [Route("oopt-metrics")]
    public async Task<IActionResult> OoptMetrics(
        [FromQuery] string query,
        [FromQuery] PageBindingModel pageBindingModel)
    {
        var results = new List<OoptMetricsResponse>();
        // var metrics = new List<ViewModelBase>();
        // for (var i = 0; i < 3; i++)
        // {
        //     metrics.Add(new ViewModelBase
        //     {
        //         Id = Guid.NewGuid(),
        //         Name = new LocalizeProperty($"Some Metric {i}")
        //     });
        // }
        // for (var j = 0; j < pageBindingModel.Count; j++)
        // {
        //     var calcs = new List<MetricCalculatedViewModel>();
        //     for (var i = 0; i < 3; i++)
        //     {
        //         calcs.Add(new MetricCalculatedViewModel
        //         {
        //             Metric = metrics[i],
        //             Value = (Random.Shared.Next(0, 100) < 50 ? Random.Shared.Next(-1000, 1001) : Random.Shared.NextSingle())
        //                 .ToString(CultureInfo.InvariantCulture)
        //         });
        //     }
        //
        //     results.Add(new OoptMetricsResponse
        //     {
        //         Oopt = new ViewModelBase
        //         {
        //             Id = Guid.NewGuid(),
        //             Name = new LocalizeProperty($"Some Oopt {pageBindingModel.Page * pageBindingModel.Count + j + 1}")
        //         },
        //         Metrics = calcs,
        //         Total = Random.Shared.Next().ToString()
        //     });
        // }
        //
        // if (!string.IsNullOrEmpty(query))
        // {
        //     results = results.FindAll(x => x.Oopt.Name.Ru.Contains(query, StringComparison.CurrentCultureIgnoreCase));
        // }

        var oopts = await _ooptCrudAsync.ReadAsync(new FilterBase {Query = query}, pageBindingModel);

        foreach (var oopt in oopts.Elements)
        {
            var metrics = await _metricService.CalcOoptAsync(oopt);
            results.Add(new OoptMetricsResponse()
            {
                Oopt = oopt,
                Metrics = metrics,
                Total = metrics.Sum(x => x.Value).ToString()
            });
        }

        return Ok(new PageView<OoptMetricsResponse>(results, oopts.Count));
    }
}
