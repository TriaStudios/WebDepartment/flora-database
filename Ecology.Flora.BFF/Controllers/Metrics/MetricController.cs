﻿using Ecology.Flora.DAL.Logic.BindingModels.Metrics;
using Ecology.Flora.DAL.Logic.Crud.Metrics;
using Ecology.Flora.DAL.Logic.ViewModels.Metrics;

namespace Ecology.Flora.BFF.Controllers.Metrics;

public class MetricController : BaseCrudController<MetricViewModel, MetricBindingModel, FilterBase, IMetricCrudAsync>
{
    public MetricController(IMetricCrudAsync service) : base(service)
    {
    }
}