namespace Ecology.Flora.BFF.Controllers.Map;

[ApiController]
[Route(Consts.RouteWithControllerName)]
public class ClusterizationController : ControllerBase
{
    private readonly IMapService _mapService;

    public ClusterizationController(IMapService mapService)
    {
        _mapService = mapService;
    }

    [HttpPost]
    public async Task<ActionResult<IEnumerable<Cluster>>> Clusterize(
        [FromBody] MapPositionRequest points, CancellationToken cancellationToken)
    {
        var clusters =
            await _mapService.GetClustersOnMap(new MapPositionBindingModel(points.Points), cancellationToken);

        return Ok(clusters.Clusters);
    }
}