using Google.Protobuf;

namespace Ecology.Flora.BFF.Controllers.Integration;

[Authorize("ADMIN")]
[ApiController]
[Route(Consts.RouteWithControllerName)]
[RequestSizeLimit(100000000)]
public class IntegrationController : ControllerBase
{
    private readonly IntegrationGrpc.IntegrationGrpcClient _integrationGrpcClient;
    private readonly ITaskWrapper _taskWrapper;

    public IntegrationController(IntegrationGrpc.IntegrationGrpcClient integrationGrpcClient, ITaskWrapper taskWrapper)
    {
        _integrationGrpcClient = integrationGrpcClient;
        _taskWrapper = taskWrapper;
    }

    [HttpPost]
    public async Task<IActionResult> StartIntegration(IFormFile file, CancellationToken cancellationToken)
    {
        if (file == null) return BadRequest("Отправьте файл");

        var memoryStream = new MemoryStream();
        await file.OpenReadStream().CopyToAsync(memoryStream, cancellationToken);

        _taskWrapper.WrapTask(
            _integrationGrpcClient,
            token =>
            {
                var buf = _integrationGrpcClient.IntegrateWordFileAsync(new IntegrateWordFileRequest
                {
                    FileData = ByteString.CopyFrom(memoryStream.ToArray())
                }, cancellationToken: token).ResponseAsync;

                return buf;
            },
            "Сервис интеграции",
            () => { memoryStream.Dispose(); });
        return Ok("Загрузка началась");
    }
}