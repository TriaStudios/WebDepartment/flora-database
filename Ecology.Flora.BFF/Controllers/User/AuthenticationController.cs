﻿namespace Ecology.Flora.BFF.Controllers.User;

[ApiController]
[Route($"{Consts.BaseRoute}/auth")]
public class AuthenticationController : Controller
{
    private readonly IAuthService _authService;

    public AuthenticationController(IAuthService authService)
    {
        _authService = authService;
    }

    [HttpPost]
    [Route("login")]
    public async Task<IActionResult> Auth([FromBody] AuthRequest authRequest)
    {
        var auth = await _authService.Auth(new AuthBindingModel
        {
            Password = authRequest.Password,
            Username = authRequest.Username
        });

        if (auth == null) return BadRequest("Не верный логин или пароль");

        var response = new AuthResponse
        {
            RefreshToken = auth.RefreshToken,
            Token = auth.Token,
            User = auth.User
        };

        return Ok(response);
    }

    [HttpPost]
    [Route("refresh-token")]
    public async Task<IActionResult> RefreshToken(RefreshTokenRequest refreshTokenRequest)
    {
        var auth = await _authService.RefreshToken(new RefreshTokenViewModel
        {
            RefreshToken = refreshTokenRequest.RefreshToken
        });

        if (auth == null) return BadRequest("Пользователь с таким токеном обновления не найден или срок истек");

        return Ok(await Task.Run(() => new RefreshTokenResponse
        {
            Token = auth.Token
        }));
    }
}