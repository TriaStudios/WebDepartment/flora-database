﻿using Ecology.Flora.DAL.Logic.Crud.Users;

namespace Ecology.Flora.BFF.Controllers.User;

[ApiController]
[Route($"{Consts.BaseRoute}/roles")]
[Authorize("ADMIN")]
public class RoleController : ControllerBase
{
    private readonly IRoleCrudAsync _roleCrudAsync;

    public RoleController(IRoleCrudAsync roleCrudAsync)
    {
        _roleCrudAsync = roleCrudAsync;
    }

    [HttpGet]
    public async Task<IActionResult> GetRoles()
    {
        try
        {
            return Ok((await _roleCrudAsync.ReadAsync(new FilterBase(), new PageBindingModel(0, 10))
                .ConfigureAwait(false)).Elements);
        }
        catch (Exception e)
        {
            return BadRequest(e);
        }
    }
}