﻿using Ecology.Flora.BFF.Services.Implementations;
using Ecology.Flora.DAL.Logic.BindingModels.Users;
using Ecology.Flora.DAL.Logic.Crud.Users;
using Ecology.Flora.DAL.Logic.Filters.Users;
using Ecology.Flora.DAL.Logic.ViewModels.Users;

namespace Ecology.Flora.BFF.Controllers.User;

[ApiController]
[Route($"{Consts.BaseRoute}/user")]
public class UserController : ControllerBase
{
    private readonly IUserCrudAsync _userCrudAsync;
    private readonly IUserService _userService;

    public UserController(IUserService userService, IUserCrudAsync userCrudAsync)
    {
        _userService = userService;
        _userCrudAsync = userCrudAsync;
    }

    [HttpGet]
    [Route("me")]
    [Authorize]
    public async Task<IActionResult> MyInfo()
    {
        var id = ((UserViewModel) HttpContext.Items["User"])?.Id;
        if (!id.HasValue)
            return BadRequest("You are not authorized!");
        return Ok(await _userService.GetById(id.Value));
    }

    [HttpGet("search")]
    [Authorize]
    public async Task<ActionResult<PageView<UserViewModel>>> Search([FromQuery] UserFilter filter,
        [FromQuery] PageBindingModel pageBindingModel)
    {
        try
        {
            return Ok(await _userCrudAsync.ReadAsync(filter, pageBindingModel));
        }
        catch (Exception e)
        {
            return BadRequest(e);
        }
    }

    [HttpGet("search-links")]
    public async Task<ActionResult<PageView<ViewModelBase>>> SearchLinks([FromQuery] UserFilter filter,
        [FromQuery] PageBindingModel pageBindingModel)
    {
        try
        {
            return Ok(await _userCrudAsync.ReadLinksAsync(filter, pageBindingModel));
        }
        catch (Exception e)
        {
            return BadRequest(e);
        }
    }

    [HttpGet("{id}")]
    [Authorize]
    public async Task<ActionResult<UserViewModel>> Get([FromRoute] string id)
    {
        try
        {
            return Ok(await _userCrudAsync.FirstOrDefaultAsync(new UserFilter
            {
                ObjectIds = new HashSet<Guid> {Guid.Parse(id)}
            }));
        }
        catch (Exception e)
        {
            return BadRequest(e);
        }
    }

    [HttpPut]
    [Authorize]
    public async Task<ActionResult<Guid>> Put(UserBindingModel binding)
    {
        try
        {
            //TODO: validate password
            binding.PasswordHash = AuthService.HashPassword(binding.PasswordHash);
            return Ok(await _userCrudAsync.CreateUser(binding));
        }
        catch (Exception e)
        {
            return BadRequest(e);
        }
    }

    [HttpPatch]
    [Authorize]
    public async Task<ActionResult<Guid>> Patch(UserBindingModel binding)
    {
        try
        {
            //TODO: validate password
            binding.PasswordHash = AuthService.HashPassword(binding.PasswordHash);
            return Ok(await _userCrudAsync.UpdateUser(binding));
        }
        catch (Exception e)
        {
            return BadRequest(e);
        }
    }

    [HttpDelete("{id}")]
    [Authorize]
    public async Task<IActionResult> Delete([FromRoute] string id)
    {
        try
        {
            await _userCrudAsync.DeleteAsync(Guid.Parse(id));
            return Ok();
        }
        catch (Exception e)
        {
            return BadRequest(e);
        }
    }
}