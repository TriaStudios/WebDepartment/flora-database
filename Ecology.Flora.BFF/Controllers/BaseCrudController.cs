﻿namespace Ecology.Flora.BFF.Controllers;

[Route(Consts.RouteWithControllerName)]
[ApiController]
public abstract class BaseCrudController<TView, TBinding, TFilter, TService> : ControllerBase
    where TView : ViewModelBase
    where TBinding : BindingModelBase
    where TFilter : FilterBase, new()
    where TService : ICrudAsync<TView, TBinding, TFilter>
{
    private readonly TService _service;

    protected BaseCrudController(TService service)
    {
        _service = service;
    }

    [HttpGet("{id}")]
    public async Task<ActionResult<TView>> Get([FromRoute] string id)
    {
        try
        {
            return Ok(await _service.FirstOrDefaultAsync(new TFilter
            {
                ObjectIds = new HashSet<Guid> {Guid.Parse(id)}
            }));
        }
        catch (Exception e)
        {
            return BadRequest(e);
        }
    }

    [HttpPut]
    public async Task<ActionResult<Guid>> Put(TBinding binding)
    {
        try
        {
            return Ok(await _service.CreateOrUpdateAsync(binding));
        }
        catch (Exception e)
        {
            return BadRequest(e);
        }
    }

    [HttpPatch]
    public async Task<ActionResult<Guid>> Patch(TBinding binding)
    {
        try
        {
            return Ok(await _service.CreateOrUpdateAsync(binding));
        }
        catch (Exception e)
        {
            return BadRequest(e);
        }
    }

    [HttpDelete("{id}")]
    public async Task<IActionResult> Delete([FromRoute] string id)
    {
        try
        {
            await _service.DeleteAsync(Guid.Parse(id));
            return Ok();
        }
        catch (Exception e)
        {
            return BadRequest(e);
        }
    }

    [HttpGet("search")]
    public async Task<ActionResult<PageView<TView>>> Search([FromQuery] TFilter filter,
        [FromQuery] PageBindingModel pagination)
    {
        try
        {
            return Ok(await _service.ReadAsync(filter, pagination));
        }
        catch (Exception e)
        {
            return BadRequest(e);
        }
    }

    [HttpGet("search-links")]
    public async Task<ActionResult<PageView<TView>>> SearchLinks([FromQuery] TFilter filter,
        [FromQuery] PageBindingModel pagination)
    {
        try
        {
            return Ok(await _service.ReadLinksAsync(filter, pagination));
        }
        catch (Exception e)
        {
            return BadRequest(e);
        }
    }
}