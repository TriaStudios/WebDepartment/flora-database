﻿using Ecology.Flora.DAL.Logic.BindingModels.Workers;
using Ecology.Flora.DAL.Logic.Crud.Workers;
using Ecology.Flora.DAL.Logic.Filters.Workers;
using Ecology.Flora.DAL.Logic.ViewModels.Workers;

namespace Ecology.Flora.BFF.Controllers.WorkTasks;

public class WorkTaskController : BaseCrudController<TaskViewModel, TaskBindingModel, TaskFilter, ITaskCrudAsync>
{
    public WorkTaskController(ITaskCrudAsync service) : base(service)
    {
    }
}