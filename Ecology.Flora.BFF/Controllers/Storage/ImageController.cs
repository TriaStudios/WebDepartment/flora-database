﻿using Ecology.DAL.Common.Enums;
using Ecology.Flora.DAL.Logic.Services.Storage;

namespace Ecology.Flora.BFF.Controllers.Storage;

[ApiController]
[Route(Consts.RouteWithControllerName)]
public class ImageController : ControllerBase
{
    private readonly IImageService _imageService;

    public ImageController(IImageService imageService)
    {
        _imageService = imageService;
    }

    [HttpGet]
    [Route("{id:guid}")]
    public IActionResult Get(Guid id, [FromQuery] ImageSize imageSize)
    {
        try
        {
            var file = _imageService.Load(id, imageSize);
            var f = File(file, "application/octet-stream");
            f.FileDownloadName = $"{id}_{imageSize}.jpg";
            return f;
        }
        catch (FileNotFoundException e)
        {
            return NotFound(e.Message);
        }
    }
}