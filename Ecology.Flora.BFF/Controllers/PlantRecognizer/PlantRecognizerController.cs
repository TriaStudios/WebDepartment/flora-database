using Ecology.Flora.DAL.Logic.Crud.Plants;
using Ecology.Flora.DAL.Logic.Filters.Plants;
using Ecology.Flora.DAL.Logic.ViewModels.Plants;
using Ecology.Flora.PlantRecognizer.Api;
using Google.Protobuf;

namespace Ecology.Flora.BFF.Controllers.PlantRecognizer;

[ApiController]
[Route(Consts.RouteWithControllerName)]
public class PlantRecognizerController : Controller
{
    private readonly IPlantFamilyCrudAsync _plantFamilyCrudAsync;
    private readonly PlantRecognizerGrpc.PlantRecognizerGrpcClient _plantRecognizer;
    private readonly ITaskWrapper _taskWrapper;

    public PlantRecognizerController(PlantRecognizerGrpc.PlantRecognizerGrpcClient plantRecognizer,
        ITaskWrapper taskWrapper, IPlantFamilyCrudAsync plantFamilyCrudAsync)
    {
        _plantRecognizer = plantRecognizer;
        _taskWrapper = taskWrapper;
        _plantFamilyCrudAsync = plantFamilyCrudAsync;
    }

    [HttpGet("train")]
    [Authorize("ADMIN")]
    public IActionResult Train([FromQuery] int epochs)
    {
        _taskWrapper.WrapTask(_plantRecognizer, token => _plantRecognizer.TrainNetAsync(new TrainNetRequest
        {
            Epochs = epochs
        }, cancellationToken: token).ResponseAsync, "Обучение нейронной сети");
        return Ok("Загрузка началась");
    }

    [HttpGet("updateDataSet")]
    [Authorize("ADMIN")]
    public IActionResult UpdateDataSet()
    {
        _taskWrapper.WrapTask(_plantRecognizer,
            token => _plantRecognizer.UpdateDataSetAsync(new UpdateDataSetRequest(), cancellationToken: token)
                .ResponseAsync, "Обновление датасета нейронной сети");
        return Ok("Загрузка началась");
    }

    [HttpPost("predict-family")]
    [ProducesResponseType(typeof(string), 400)]
    [ProducesResponseType(typeof(string), 404)]
    [ProducesResponseType(typeof(PlantFamilyViewModel), 200)]
    public async Task<IActionResult> PredictFamily(CancellationToken cancellationToken)
    {
        var form = await HttpContext.Request.ReadFormAsync(cancellationToken);
        var file = form.Files.GetFile("file");
        if (file == null) return BadRequest("Отправьте файл");

        var memoryStream = new MemoryStream();
        await file.OpenReadStream().CopyToAsync(memoryStream, cancellationToken);
        var response = await _plantRecognizer.PredictNetAsync(new PredictNetRequest
        {
            ImageData = ByteString.CopyFrom(memoryStream.ToArray())
        }, cancellationToken: cancellationToken);
        var plantFamily = await _plantFamilyCrudAsync.FirstOrDefaultAsync(new PlantFamilyFilter
        {
            Query = response.PlantFamilyName.ToLower()
        }, cancellationToken);
        if (plantFamily == null)
            return NotFound($"Предсказанное семейство растений \"{response.PlantFamilyName}\" не существует в базе");

        return Ok(plantFamily);
    }
}