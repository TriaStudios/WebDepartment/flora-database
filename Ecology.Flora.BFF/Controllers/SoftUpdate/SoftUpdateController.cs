﻿using System.Diagnostics;
using Ecology.Flora.DAL.PostgreSQL.Models.Users;

namespace Ecology.Flora.BFF.Controllers.SoftUpdate;

[Authorize(Roles.Admin)]
[ApiController]
[Route(Consts.RouteWithControllerName)]
public class SoftUpdateController : ControllerBase
{
    private static Process _lastProcess;

    [HttpGet]
    public IActionResult SoftUpdateStatus() 
        => Ok(_lastProcess is {HasExited: false} ? "Updating" : "Up to date");

    [HttpPost]
    public IActionResult SoftUpdate()
    {
        if (_lastProcess is {HasExited: false})
            return StatusCode(StatusCodes.Status102Processing);

        var startInfo = new ProcessStartInfo("soft-update.sh")
        {
            UseShellExecute = true
        };
        _lastProcess = Process.Start(startInfo);
        
        return Ok();
    }
}
