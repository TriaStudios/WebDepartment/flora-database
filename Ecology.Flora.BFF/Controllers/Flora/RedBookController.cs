﻿using Ecology.Flora.DAL.Logic.BindingModels.Plants;
using Ecology.Flora.DAL.Logic.Crud.Plants;
using Ecology.Flora.DAL.Logic.ViewModels.Plants;

namespace Ecology.Flora.BFF.Controllers.Flora;

public class RedBookController :
    BaseCrudController<RedBookViewModel, RedBookBindingModel, FilterBase, IRedBookCrudAsync>
{
    public RedBookController(IRedBookCrudAsync service) : base(service)
    {
    }
}