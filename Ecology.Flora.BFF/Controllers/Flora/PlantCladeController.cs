﻿using Ecology.Flora.DAL.Logic.BindingModels.Plants;
using Ecology.Flora.DAL.Logic.Crud.Plants;
using Ecology.Flora.DAL.Logic.ViewModels.Plants;

namespace Ecology.Flora.BFF.Controllers.Flora;

public class PlantCladeController :
    BaseCrudController<PlantCladeViewModel, PlantCladeBindingModel, FilterBase, IPlantCladeCrudAsync>
{
    public PlantCladeController(IPlantCladeCrudAsync service) : base(service)
    {
    }
}