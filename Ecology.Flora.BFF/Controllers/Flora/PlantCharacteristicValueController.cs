﻿using Ecology.Flora.DAL.Logic.Crud.Plants;
using Ecology.Flora.DAL.Logic.Filters.Plants;
using Ecology.Flora.DAL.Logic.ViewModels.Plants;

namespace Ecology.Flora.BFF.Controllers.Flora;

[ApiController]
[Route(Consts.RouteWithControllerName)]
public class PlantCharacteristicValueController : ControllerBase
{
    private readonly IPlantCharacteristicValueCrudAsync _service;

    public PlantCharacteristicValueController(IPlantCharacteristicValueCrudAsync service)
    {
        _service = service;
    }

    [HttpGet("{plantCharacteristicId:guid}")]
    [ProducesResponseType(typeof(IEnumerable<ViewModelBase>), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(string), StatusCodes.Status404NotFound)]
    public async Task<IActionResult> Get(Guid plantCharacteristicId, CancellationToken cancellationToken)
    {
        var values = await _service.ReadLinksAsync(new PlantCharacteristicValueFilter
        {
            PlantCharacteristicIds = {plantCharacteristicId}
        }, new PageBindingModel(0, 0), cancellationToken);
        if (values.Count == 0)
        {
            return NotFound("Значения не найдены");
        }

        return Ok(values.Elements);
    }

    [HttpGet]
    [ProducesResponseType(typeof(IEnumerable<PlantCharacteristicValuesViewModel>), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(string), StatusCodes.Status404NotFound)]
    public async Task<IActionResult> GetAll(CancellationToken cancellationToken)
    {
        var page = await _service.ReadAsync(
            new PlantCharacteristicValueFilter(), 
            new PageBindingModel(0, 0),
            cancellationToken);
        var values = page.Elements;
        var result = values.GroupBy(x => x.PlantCharacteristic.Id)
            .Select(x => new PlantCharacteristicValuesViewModel
            {
                Characteristic = x.First().PlantCharacteristic,
                Values = x.Select(y => new ViewModelBase
                {
                    Id = y.Id,
                    Name = y.Name
                }).ToList()
            });
        return Ok(result);
    }
}
