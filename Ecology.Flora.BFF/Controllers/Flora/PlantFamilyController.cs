﻿using Ecology.Flora.DAL.Logic.BindingModels.Plants;
using Ecology.Flora.DAL.Logic.Crud.Plants;
using Ecology.Flora.DAL.Logic.Filters.Plants;
using Ecology.Flora.DAL.Logic.ViewModels.Plants;

namespace Ecology.Flora.BFF.Controllers.Flora;

public class PlantFamilyController :
    BaseCrudController<PlantFamilyViewModel, PlantFamilyBindingModel, PlantFamilyFilter, IPlantFamilyCrudAsync>
{
    public PlantFamilyController(IPlantFamilyCrudAsync service) : base(service)
    {
    }
}