﻿using Ecology.Flora.DAL.Logic.BindingModels.Plants;
using Ecology.Flora.DAL.Logic.Crud.Plants;
using Ecology.Flora.DAL.Logic.ViewModels.Plants;

namespace Ecology.Flora.BFF.Controllers.Flora;

public class PlantCharacteristicController :
    BaseCrudController<PlantCharacteristicViewModel, PlantCharacteristicBindingModel, FilterBase, IPlantCharacteristicCrudAsync>
{
    public PlantCharacteristicController(IPlantCharacteristicCrudAsync service) : base(service)
    {
    }
}
