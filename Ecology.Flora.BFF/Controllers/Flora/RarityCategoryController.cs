﻿using Ecology.Flora.DAL.Logic.BindingModels.Plants;
using Ecology.Flora.DAL.Logic.Crud.Plants;
using Ecology.Flora.DAL.Logic.Filters.Plants;
using Ecology.Flora.DAL.Logic.ViewModels.Plants;

namespace Ecology.Flora.BFF.Controllers.Flora;

public class RarityCategoryController :
    BaseCrudController<RarityCategoryViewModel, RarityCategoryBindingModel, RarityCategoryFilter,
        IRarityCategoryCrudAsync>
{
    public RarityCategoryController(IRarityCategoryCrudAsync service) : base(service)
    {
    }
}