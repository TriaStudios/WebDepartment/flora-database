﻿using Ecology.Flora.DAL.Logic.BindingModels.Plants;
using Ecology.Flora.DAL.Logic.Crud.Plants;
using Ecology.Flora.DAL.Logic.ViewModels.Plants;

namespace Ecology.Flora.BFF.Controllers.Flora;

public class OoptController : BaseCrudController<OoptViewModel, OoptBindingModel, FilterBase, IOoptCrudAsync>
{
    public OoptController(IOoptCrudAsync service) : base(service)
    {
    }
}