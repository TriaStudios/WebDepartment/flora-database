﻿using Ecology.Flora.DAL.Logic.BindingModels.Plants;
using Ecology.Flora.DAL.Logic.Crud.Plants;
using Ecology.Flora.DAL.Logic.Filters.Plants;
using Ecology.Flora.DAL.Logic.ViewModels.Plants;

namespace Ecology.Flora.BFF.Controllers.Flora;

public class PlantController :
    BaseCrudController<PlantViewModel, PlantBindingModel, PlantFilter, IPlantCrudAsync>
{
    public PlantController(IPlantCrudAsync service) : base(service)
    {
        //TODO: убрать словари в биндинг модели
    }
}