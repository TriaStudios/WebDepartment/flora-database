﻿using Ecology.Flora.DAL.Logic.BindingModels.Plants;
using Ecology.Flora.DAL.Logic.Crud.Plants;
using Ecology.Flora.DAL.Logic.ViewModels.Plants;

namespace Ecology.Flora.BFF.Controllers.Flora;

public class RedBookPlantController : BaseCrudController<RedBookPlantViewModel, RedBookPlantBindingModel, FilterBase,
    IRedBookPlantCrudAsync>
{
    public RedBookPlantController(IRedBookPlantCrudAsync service) : base(service)
    {
    }
}