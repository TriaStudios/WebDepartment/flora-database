﻿using Ecology.Flora.DAL.Logic.Crud.Plants;
using Ecology.Flora.DAL.Logic.Filters.Plants;

namespace Ecology.Flora.BFF.Controllers.Flora;

[Route("api/v1/[controller]")]
[ApiController]
public class TotalSearchController : ControllerBase
{
    private readonly IPlantCladeCrudAsync _plantCladeCrudAsync;
    private readonly IPlantClassCrudAsync _plantClassCrudAsync;
    private readonly IPlantCrudAsync _plantCrudAsync;
    private readonly IPlantFamilyCrudAsync _plantFamilyCrudAsync;

    public TotalSearchController(
        IPlantCrudAsync plantCrudAsync,
        IPlantCladeCrudAsync plantCladeCrudAsync,
        IPlantClassCrudAsync plantClassCrudAsync,
        IPlantFamilyCrudAsync plantFamilyCrudAsync)
    {
        _plantCrudAsync = plantCrudAsync;
        _plantCladeCrudAsync = plantCladeCrudAsync;
        _plantClassCrudAsync = plantClassCrudAsync;
        _plantFamilyCrudAsync = plantFamilyCrudAsync;
    }

    [HttpGet]
    public async Task<ActionResult<List<List<ViewModelBase>>>> Search([FromQuery] string query)
    {
        try
        {
            List<IEnumerable<ViewModelBase>> res = new();
            var page = new PageBindingModel(0, 3);
            res.Add((await _plantCrudAsync.ReadLinksAsync(new PlantFilter
            {
                Query = query
            }, page)).Elements);

            res.Add((await _plantCladeCrudAsync.ReadLinksAsync(new FilterBase
            {
                Query = query
            }, page)).Elements);

            res.Add((await _plantClassCrudAsync.ReadLinksAsync(new PlantClassFilter
            {
                Query = query
            }, page)).Elements);

            res.Add((await _plantFamilyCrudAsync.ReadLinksAsync(new PlantFamilyFilter
            {
                Query = query
            }, page)).Elements);

            return Ok(res);
        }
        catch (Exception e)
        {
            return BadRequest(e);
        }
    }
}