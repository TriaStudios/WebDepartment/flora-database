using Ecology.Clustering.Api;
using Ecology.Flora.BFF.Middlewares;
using Ecology.Flora.BFF.Services.Implementations;
using Ecology.Flora.DAL.Logic.Services.Plants;
using Ecology.Flora.DAL.PostgreSQL;
using Ecology.Flora.DAL.PostgreSQL.Implements.Plants;
using Ecology.Flora.PlantRecognizer.Api;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;
using Serilog;
using Swashbuckle.AspNetCore.SwaggerUI;
using ILogger = Serilog.ILogger;

var builder = WebApplication.CreateBuilder(args);
builder.Host.UseSerilog((_, configuration) => configuration.ReadFrom.Configuration(builder.Configuration));

builder.Services.Configure<FormOptions>(options => { options.MultipartBodyLengthLimit = 100000000; });

builder.Services.AddFloraDb(builder.Configuration.GetConnectionString("FloraDatabase"));

builder.Services.AddIntegrationGrpcClient(builder.Configuration.GetConnectionString("IntegrationService"));
builder.Services.AddClusteringGrpcClient(builder.Configuration.GetConnectionString("ClusteringService"));
builder.Services.AddPlantRecognizerGrpcClient(builder.Configuration.GetConnectionString("PlantRecognizerService"));

builder.Services.AddControllers()
    .AddNewtonsoftJson(options => options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore);
builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v1", new OpenApiInfo {Title = "Ecology.Flora.BFF", Version = "v1"});
    c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
    {
        Description = @"JWT Authorization header using the Bearer scheme. \r\n\r\n 
                      Enter 'Bearer' [space] and then your token in the text input below.
                      \r\n\r\nExample: 'Bearer 12345abcdef'",
        Name = "Authorization",
        In = ParameterLocation.Header,
        Type = SecuritySchemeType.ApiKey,
        Scheme = "Bearer"
    });

    c.AddSecurityRequirement(new OpenApiSecurityRequirement
    {
        {
            new OpenApiSecurityScheme
            {
                Reference = new OpenApiReference
                {
                    Type = ReferenceType.SecurityScheme,
                    Id = "Bearer"
                },
                Scheme = "oauth2",
                Name = "Bearer",
                In = ParameterLocation.Header
            },
            new List<string>()
        }
    });
});

builder.Services.AddSingleton<IAuthService, AuthService>();
builder.Services.AddSingleton<IMapService, MapService>();
builder.Services.AddSingleton<IUserService, UserService>();
builder.Services.AddSingleton<ITaskWrapper, TaskWrapper>();
builder.Services.AddScoped<IMetricService, MetricService>();
builder.Services.AddScoped<IPlantService, PlantService>();

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
    app.UseSwagger();
    app.UseSwaggerUI(c =>
    {
        c.DocExpansion(DocExpansion.None);
        c.SwaggerEndpoint("/swagger/v1/swagger.json", "Ecology.Flora.BFF v1");
    });
}

app.UseRouting();
app.UseCors(o => o.AllowAnyHeader()
    .AllowAnyMethod()
    .AllowCredentials()
    .WithOrigins("http://flora.athene.tech", "http://ecology.flora.ru"));

app.UseStaticFiles();

app.UseMiddleware<JwtMiddleware>();

app.UseEndpoints(endpoints => { endpoints.MapControllers(); });

app.MapFallbackToFile("index.html");

var logger = app.Services.GetService<ILogger>();
logger.Information("NET_ENVIRONMENT = {Kek}", Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT"));
logger.Information(
    "\n▀█████████▄     ▄████████    ▄████████ \n" +
    "  ███    ███   ███    ███   ███    ███ \n" +
    "  ███    ███   ███    █▀    ███    █▀  \n" +
    " ▄███▄▄▄██▀   ▄███▄▄▄      ▄███▄▄▄     \n" +
    "▀▀███▀▀▀██▄  ▀▀███▀▀▀     ▀▀███▀▀▀     \n" +
    "  ███    ██▄   ███          ███        \n" +
    "  ███    ███   ███          ███        \n" +
    "▄█████████▀    ███          ███        \n"
);

app.Run();