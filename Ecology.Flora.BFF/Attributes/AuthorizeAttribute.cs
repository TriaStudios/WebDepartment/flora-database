﻿using Ecology.Flora.DAL.Logic.ViewModels.Users;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Ecology.Flora.BFF.Attributes;

[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
public class AuthorizeAttribute : Attribute, IAuthorizationFilter
{
    public AuthorizeAttribute(params string[] roles)
    {
        Roles = roles;
    }

    private string[] Roles { get; }

    public void OnAuthorization(AuthorizationFilterContext context)
    {
        var user = (UserViewModel) context.HttpContext.Items["User"];
        if (user == null)
            context.Result = new JsonResult(new {message = "Unauthorized"})
                {StatusCode = StatusCodes.Status401Unauthorized};
        else if (Roles.Length > 0 && !Roles.Intersect(user.Roles.Select(x => x.Name.Ru)).Any())
            context.Result = new JsonResult(new {message = "Forbidden by role"})
                {StatusCode = StatusCodes.Status403Forbidden};
    }
}