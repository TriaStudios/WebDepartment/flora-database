﻿namespace Ecology.Flora.BFF.Models;

public class RefreshTokenViewModel
{
    public string RefreshToken { get; set; }
}