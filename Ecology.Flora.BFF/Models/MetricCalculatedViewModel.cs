﻿namespace Ecology.Flora.BFF.Models;

public class MetricCalculatedViewModel
{
    public ViewModelBase Metric { get; set; }
    public int Value { get; set; }
}
