﻿namespace Ecology.Flora.BFF.Models.Request;

public class MapPositionRequest
{
    public List<Point> Points { get; set; }
}
