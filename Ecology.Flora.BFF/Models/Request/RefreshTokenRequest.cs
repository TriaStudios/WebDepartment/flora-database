﻿namespace Ecology.Flora.BFF.Models.Request;

public class RefreshTokenRequest
{
    public string RefreshToken { get; set; }
}
