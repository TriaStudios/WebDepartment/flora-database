﻿namespace Ecology.Flora.BFF.Models.Request;

public class AuthRequest
{
    public string Username { get; set; }
    public string Password { get; set; }
}
