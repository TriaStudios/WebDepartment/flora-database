﻿namespace Ecology.Flora.BFF.Models.Response;

public class MapPositionResponse
{
    public IEnumerable<Cluster> Clusters { get; set; }
}
