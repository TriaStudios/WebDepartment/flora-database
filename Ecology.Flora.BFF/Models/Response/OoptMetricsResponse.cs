﻿namespace Ecology.Flora.BFF.Models.Response;

public class OoptMetricsResponse
{
    public ViewModelBase Oopt { get; set; }
    public List<MetricCalculatedViewModel> Metrics { get; set; }
    public string Total { get; set; }
}
