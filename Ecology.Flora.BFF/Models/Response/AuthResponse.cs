﻿using Ecology.Flora.DAL.Logic.ViewModels.Users;

namespace Ecology.Flora.BFF.Models.Response;

public class AuthResponse
{
    public string Token { get; set; }
    public string RefreshToken { get; set; }
    public UserViewModel User { get; set; }
}
