﻿namespace Ecology.Flora.BFF.Models.Response;

public class RefreshTokenResponse
{
    public string Token { get; set; }
}
