﻿namespace Ecology.Flora.BFF.Models;

public class AuthBindingModel
{
    public string Username { get; set; }
    public string Password { get; set; }
}