﻿using Ecology.Flora.DAL.Logic.ViewModels.Users;

namespace Ecology.Flora.BFF.Services.Abstractions;

public interface IUserService
{
    /// <summary>
    ///     Get user by identification
    /// </summary>
    /// <param name="id">identification of user</param>
    /// <returns>user data</returns>
    Task<UserViewModel> GetById(Guid id);
}