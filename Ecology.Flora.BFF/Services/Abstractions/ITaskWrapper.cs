using Grpc.Core;

namespace Ecology.Flora.BFF.Services.Abstractions;

public interface ITaskWrapper
{
    /// <summary>
    ///     Wrap task around task manager to log to database
    /// </summary>
    /// <param name="baseClient">caller</param>
    /// <param name="wrapped">task to wrap</param>
    /// <param name="title">title of task</param>
    /// <param name="continueWith">finally action (to dispose unmanaged resources)</param>
    void WrapTask(ClientBase baseClient, Func<CancellationToken, Task> wrapped, string title,
        Action continueWith = default);
}
