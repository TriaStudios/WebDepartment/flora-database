﻿namespace Ecology.Flora.BFF.Services.Abstractions;

public interface IMapService
{
    /// <summary>
    ///     For clustering points on map
    /// </summary>
    /// <param name="mapPosition">map bounds</param>
    /// <param name="cancellationToken">token</param>
    /// <returns>map data</returns>
    Task<MapViewModel> GetClustersOnMap(MapPositionBindingModel mapPosition, CancellationToken cancellationToken);
}