﻿using Ecology.Flora.DAL.Logic.ViewModels.Plants;

namespace Ecology.Flora.BFF.Services.Abstractions;

public interface IMetricService
{
    public Task<List<MetricCalculatedViewModel>> CalcOoptAsync(OoptViewModel ooptViewModel);
}