﻿namespace Ecology.Flora.BFF.Services.Abstractions;

public interface IAuthService
{
    /// <summary>
    ///     Authenticate user and register claims
    /// </summary>
    /// <param name="authBindingModel">user login data</param>
    /// <returns>auth data</returns>
    Task<AuthViewModel> Auth(AuthBindingModel authBindingModel);

    Task<AuthViewModel> RefreshToken(RefreshTokenViewModel refreshTokenViewModel);
}