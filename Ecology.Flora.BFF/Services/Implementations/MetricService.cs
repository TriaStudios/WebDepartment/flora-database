﻿using Ecology.DAL.Common.Constants;
using Ecology.Flora.DAL.Logic.Crud.Metrics;
using Ecology.Flora.DAL.Logic.Crud.Plants;
using Ecology.Flora.DAL.Logic.Enums;
using Ecology.Flora.DAL.Logic.Services.Plants;
using Ecology.Flora.DAL.Logic.ViewModels.Plants;

namespace Ecology.Flora.BFF.Services.Implementations;

public class MetricService : IMetricService
{
    private readonly IPlantService _plantService;
    private readonly IMetricCrudAsync _metricCrud;
    private readonly IMetricRaspredCrudAsync _metricRaspred;

    public MetricService(IPlantService plantService, IMetricCrudAsync metricCrud, IMetricRaspredCrudAsync metricRaspred)
    {
        _plantService = plantService;
        _metricCrud = metricCrud;
        _metricRaspred = metricRaspred;
    }

    public async Task<List<MetricCalculatedViewModel>> CalcOoptAsync(OoptViewModel ooptViewModel)
    {
        var metricsCalculated = new List<MetricCalculatedViewModel>();

        var plants = (await _plantService.GetServicePlantByOopt(ooptViewModel.Id));

        var metrics = (await _metricCrud.ReadAsync(new FilterBase(),
            new PageBindingModel(0, int.MaxValue))).Elements;
        
        foreach (var metric in metrics)
        {
            var val = 0;

            // Cтепень изученности растительного покрова
            if (metric.Id.Equals(MetricsConst.TheDegreeOfStudyOfVegetationCover))
            {
                val = plants.Count(x => x.PlantCharacteristicValues
                    .FirstOrDefault(x => x.Characteristic.Name.La == PlantCharacteristicNames.OriginSpecies)?
                    .Values
                    .FirstOrDefault()?.Name.Ru == "аборигенное");
            }
            
            // Демонстрационное (эталонное) значение
            if (metric.Id.Equals(MetricsConst.DemonstrationValue))
            {
                //TODO: Не известно какой вид мы изучаем чтобы узнать процент
                val = 0;
            }
            
            // Площадь памятника природы
            if (metric.Id.Equals(MetricsConst.NatureMonumentSquare))
            {
                val = ooptViewModel.Square;
            }
            
            // Антропотолерантность растительного покрова
            if (metric.Id.Equals(MetricsConst.AnthropotoleranceOfVegetationCover))
            {
                val = plants.Count(x => x.PlantCharacteristicValues
                    .FirstOrDefault(x => x.Characteristic.Name.En == PlantCharacteristicNames.ArealType)?
                    .Values
                    .FirstOrDefault()?.Name.Ru == "плюризональный");
            }
            
            // Ценотнческое разнообрази
            if (metric.Id.Equals(MetricsConst.CenoticDiversity))
            {
                val = plants.Select(x => x.PlantFamily.PlantClass.Id).ToHashSet().Count;
            }
            
            // Общая численность видового разнообразия
            if (metric.Id.Equals(MetricsConst.TotalNumberSspeciesDiversity))
            {
                val = plants.Count;
            }
            
            // Число видов, занесенных в Красную книгу Российской Федерации и Самарской области
            if (metric.Id.Equals(MetricsConst.CountRareSpecies))
            {
                val = plants.Count(x => x.RedBookPlants.Any());
            }
            
            // Степень трансформпрованности
            if (metric.Id.Equals(MetricsConst.DegreeTransformation))
            {
                val = ooptViewModel.Metric8;
            }
            
            // Восстановительный потенциал
            if (metric.Id.Equals(MetricsConst.RecoveryPotential))
            {
                val = plants.Select(x => x.PlantFamily.Id).ToHashSet().Count;
            }
            

            var metricRaspreds = (await _metricRaspred.ReadAsync(new FilterBase()
            {
                ObjectIds = metric.MetricRaspreds.ToHashSet()
            }, new PageBindingModel(0, int.MaxValue))).Elements;

            var orders = metricRaspreds.OrderBy(x => x.Value);
            
            foreach (var metricRaspred in orders)
            {
                if (metricRaspred.Value < val && orders.Last() != metricRaspred) continue;
                
                metricsCalculated.Add(new MetricCalculatedViewModel()
                {
                    Metric = metric,
                    Value = metricRaspred.Bal
                });
                break;
            }
        }

        return metricsCalculated;
    }
}
