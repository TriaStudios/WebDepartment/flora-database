﻿using Ecology.Flora.DAL.Logic.Crud.Users;
using Ecology.Flora.DAL.Logic.Filters.Users;
using Ecology.Flora.DAL.Logic.ViewModels.Users;

namespace Ecology.Flora.BFF.Services.Implementations;

public class UserService : IUserService
{
    private readonly IUserCrudAsync _userCrudAsync;

    public UserService(IUserCrudAsync userCrudAsync)
    {
        _userCrudAsync = userCrudAsync;
    }

    public async Task<UserViewModel> GetById(Guid id)
    {
        var user = await _userCrudAsync.FirstOrDefaultAsync(new UserFilter
        {
            ObjectIds = new HashSet<Guid> {id}
        });

        return user;
    }
}