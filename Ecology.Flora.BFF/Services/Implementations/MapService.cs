﻿using Ecology.Clustering.Api;
using Ecology.Flora.DAL.Logic.Crud.Plants;
using Grpc.Core;
using Cluster = Ecology.DAL.Common.Models.Cluster;

namespace Ecology.Flora.BFF.Services.Implementations;

public class MapService : IMapService
{
    private readonly ClusteringGrpc.ClusteringGrpcClient _clusteringGrpcClient;
    private readonly IPlantCrudAsync _plantCrudAsync;

    public MapService(ClusteringGrpc.ClusteringGrpcClient clusteringGrpcClient, IPlantCrudAsync plantCrudAsync)
    {
        _clusteringGrpcClient = clusteringGrpcClient;
        _plantCrudAsync = plantCrudAsync;
    }

    public async Task<MapViewModel> GetClustersOnMap(MapPositionBindingModel mapPosition,
        CancellationToken cancellationToken)
    {
        if (mapPosition.Points.Count < 2)
            throw new Exception($"Не хватает ограничения точек на карте. Сейчас их {mapPosition.Points.Count}");

        var points = await _plantCrudAsync.GetPointsByBounds(new BoundsModel
        {
            LeftDown = new PositionProperty
            {
                Latitude = mapPosition.Points[0].Latitude,
                Longitude = mapPosition.Points[0].Longitude
            },
            RightUp = new PositionProperty
            {
                Latitude = mapPosition.Points[1].Latitude,
                Longitude = mapPosition.Points[1].Longitude
            }
        });
        var indexedPoints = points.Select((x, i) => new
        {
            Index = i,
            Object = x
        }).ToDictionary(x => x.Index, x => x.Object);

        var clusters = await _clusteringGrpcClient.ClusterPointsAsync(new ClusterPointsRequest
        {
            Points =
            {
                indexedPoints.Select(x => new MapObject
                {
                    UniqueIndex = x.Key,
                    Point = new GeographicPoint
                    {
                        Latitude = x.Value.Latitude,
                        Longitude = x.Value.Longitude
                    }
                })
            },
            HexagonSize = MathF.Abs(mapPosition.Points[1].Latitude - mapPosition.Points[0].Latitude) / 10
        }, Metadata.Empty, DateTime.UtcNow + TimeSpan.FromHours(1), cancellationToken);

        return new MapViewModel(
            clusters.Clusters.Select(x => new Cluster(
                x.CountPoints == 1 ? indexedPoints[x.UniqueIndex].Id : null,
                new IndexCluster(x.Index.Q, x.Index.R),
                new Point(x.Center.Latitude, x.Center.Longitude),
                new Point(x.MinBound.Latitude, x.MinBound.Longitude),
                new Point(x.MaxBound.Latitude, x.MaxBound.Longitude),
                x.CountPoints,
                x.CountPoints == 1 ? indexedPoints[x.UniqueIndex].Name.Ru : $"{x.CountPoints} растений",
                x.CountPoints == 1
                    ? "rgb(0, 255, 0)"
                    : $"rgb({Math.Min(x.CountPoints / 100.0 * 255, 255)}, {255 - Math.Min(x.CountPoints / 100.0 * 255, 255)}, 0)"
            )));
    }
}
