﻿using System.Security.Cryptography;
using Ecology.Flora.DAL.Logic.BindingModels.Users;
using Ecology.Flora.DAL.Logic.Crud.Users;
using Ecology.Flora.DAL.Logic.Filters.Users;

namespace Ecology.Flora.BFF.Services.Implementations;

public class AuthService : IAuthService
{
    private readonly IConfiguration _configuration;
    private readonly IUserCrudAsync _userCrudAsync;

    public AuthService(IUserCrudAsync userCrudAsync, IConfiguration configuration)
    {
        _userCrudAsync = userCrudAsync;
        _configuration = configuration;
    }

    public async Task<AuthViewModel> Auth(AuthBindingModel authBindingModel)
    {
        var user = await _userCrudAsync.FirstOrDefaultAsync(new UserFilter
        {
            PasswordHash = HashPassword(authBindingModel.Password),
            Username = authBindingModel.Username
        });

        if (user == null)
            return null;

        var token = GenerateJwtToken(user);
        var refreshToken = GenerateRefreshJwtToken(user);

        await _userCrudAsync.UpdateRefreshToken(new UserRefreshTokenBindingModel
        {
            Id = user.Id,
            RefreshToken = refreshToken
        });

        return new AuthViewModel
        {
            Token = token,
            RefreshToken = refreshToken,
            User = user
        };
    }

    public async Task<AuthViewModel> RefreshToken(RefreshTokenViewModel refreshTokenViewModel)
    {
        if (!ValidateRefreshToken(refreshTokenViewModel.RefreshToken))
            return null;

        var user = await _userCrudAsync.FirstOrDefaultAsync(new UserFilter
        {
            RefreshToken = refreshTokenViewModel.RefreshToken
        });

        if (user == null)
            return null;

        var token = GenerateJwtToken(user);

        return new AuthViewModel
        {
            Token = token,
            RefreshToken = refreshTokenViewModel.RefreshToken,
            User = user
        };
    }

    private string GenerateJwtToken(ViewModelBase user)
    {
        var claims = new List<Claim>
        {
            new("id", user.Id.ToString())
        };

        var tokenHandler = new JwtSecurityTokenHandler();
        var key = Encoding.ASCII.GetBytes(_configuration["Token:Key"]);
        var tokenDescriptor = new SecurityTokenDescriptor
        {
            Subject = new ClaimsIdentity(claims),
            Expires = DateTime.UtcNow.Add(TimeSpan.FromMinutes(_configuration.GetValue<int>("Token:LifeTime"))),
            SigningCredentials =
                new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
        };
        var token = tokenHandler.CreateToken(tokenDescriptor);
        return tokenHandler.WriteToken(token);
    }

    private string GenerateRefreshJwtToken(ViewModelBase user)
    {
        var claims = new List<Claim>
        {
            new("id", user.Id.ToString())
        };

        var tokenHandler = new JwtSecurityTokenHandler();
        var key = Encoding.ASCII.GetBytes(_configuration["RefreshToken:Key"]);
        var tokenDescriptor = new SecurityTokenDescriptor
        {
            Subject = new ClaimsIdentity(claims),
            Expires = DateTime.UtcNow.Add(TimeSpan.FromMinutes(_configuration.GetValue<int>("RefreshToken:LifeTime"))),
            SigningCredentials =
                new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
        };
        var token = tokenHandler.CreateToken(tokenDescriptor);
        return tokenHandler.WriteToken(token);
    }

    public static string HashPassword(string password)
    {
        using var sha256 = SHA256.Create();
        var hashedBytes = sha256.ComputeHash(Encoding.UTF8.GetBytes(password));
        var hash = BitConverter.ToString(hashedBytes).Replace("-", "").ToLower();

        return hash;
    }

    private bool ValidateRefreshToken(string refreshToken)
    {
        try
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_configuration["RefreshToken:Key"]);
            tokenHandler.ValidateToken(refreshToken, new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(key),
                ValidateIssuer = false,
                ValidateAudience = false,
                ClockSkew = TimeSpan.Zero
            }, out _);

            return true;
        }
        catch (Exception)
        {
            return false;
        }
    }
}