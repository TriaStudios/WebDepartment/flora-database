using Ecology.Flora.DAL.Logic.BindingModels.Workers;
using Ecology.Flora.DAL.Logic.Crud.Workers;
using Ecology.Flora.DAL.Logic.Enums;
using Grpc.Core;
using ILogger = Serilog.ILogger;

namespace Ecology.Flora.BFF.Services.Implementations;

public class TaskWrapper : ITaskWrapper
{
    private readonly Dictionary<string, CancellationTokenSource> _lastTasks;
    private readonly ILogger _logger;
    private readonly ITaskCrudAsync _taskCrud;

    public TaskWrapper(ITaskCrudAsync taskCrud, ILogger logger)
    {
        _taskCrud = taskCrud;
        _logger = logger;
        _lastTasks = new Dictionary<string, CancellationTokenSource>();
    }

    public void WrapTask(ClientBase baseClient, Func<CancellationToken, Task> wrapped, string title,
        Action continueWith = default)
    {
        _lastTasks.GetValueOrDefault(baseClient.GetType().Namespace)?.Cancel();
        if (!_lastTasks.ContainsKey(baseClient.GetType().Namespace ?? ""))
            _lastTasks.Add(baseClient.GetType().Namespace ?? "", null);
        _lastTasks[baseClient.GetType().Namespace ?? ""] = new CancellationTokenSource();
        var curToken = _lastTasks[baseClient.GetType().Namespace ?? ""].Token;

        Task.Run(async () =>
        {
            var taskId = await _taskCrud.CreateOrUpdateAsync(new TaskBindingModel
            {
                Name = new LocalizeProperty {Ru = title},
                Namespace = baseClient.GetType().Namespace,
                Status = StatusWorker.InProgress,
                Message = "Задача взята в работу"
            }, curToken);

            await wrapped(curToken).ContinueWith(async (t, _) =>
            {
                if (t.IsCompletedSuccessfully)
                {
                    _logger.Information("Integration is done!");
                    await _taskCrud.CreateOrUpdateAsync(new TaskBindingModel
                    {
                        Id = taskId,
                        Status = StatusWorker.Done,
                        Message = "Задача выполнена",
                        Namespace = baseClient.GetType().Namespace,
                        Name = new LocalizeProperty {Ru = title}
                    });
                }
                else if (t.Exception?.InnerException is RpcException {StatusCode: StatusCode.Cancelled})
                {
                    await _taskCrud.CreateOrUpdateAsync(new TaskBindingModel
                    {
                        Id = taskId,
                        Status = StatusWorker.Cancelled,
                        Message = "Отменена новой задачей",
                        Namespace = baseClient.GetType().Namespace,
                        Name = new LocalizeProperty {Ru = title}
                    });
                }
                else
                {
                    _logger.Error(t.Exception, "Integration is thrown exception");
                    await _taskCrud.CreateOrUpdateAsync(new TaskBindingModel
                    {
                        Id = taskId,
                        Status = StatusWorker.Failed,
                        Message = "Задача завершилась с ошибкой",
                        Namespace = baseClient.GetType().Namespace,
                        Name = new LocalizeProperty {Ru = title}
                    });
                }

                continueWith?.Invoke();
            }, null);
        });
    }
}
