﻿using System;
using Microsoft.Extensions.DependencyInjection;

namespace Ecology.Flora.Integration.Api;

public static class IntegrationExtensions
{
    public static void AddIntegrationGrpcClient(this IServiceCollection services, string uri)
    {
        services.AddGrpcClient<IntegrationGrpc.IntegrationGrpcClient>(options => options.Address = new Uri(uri));
    }
}