using System;
using System.Collections.Generic;
using System.Linq;
using AutoFixture;
using AutoFixture.AutoMoq;
using AutoMapper;
using Ecology.DAL.Common.Abstract;
using Ecology.Flora.DAL.Logic.Enums;
using Ecology.Flora.DAL.Logic.ViewModels.Metrics;
using Ecology.Flora.DAL.Logic.ViewModels.Plants;
using Ecology.Flora.DAL.Logic.ViewModels.Users;
using Ecology.Flora.DAL.Logic.ViewModels.Workers;
using Ecology.Flora.DAL.PostgreSQL.Models.Metrics;
using Ecology.Flora.DAL.PostgreSQL.Models.Plants;
using Ecology.Flora.DAL.PostgreSQL.Models.Users;
using Ecology.Flora.DAL.PostgreSQL.Models.Workers;
using FluentAssertions;
using NUnit.Framework;
using TestsHelpers;

namespace Ecology.Flora.DAL.PostgreSQL.Tests;

[TestFixture]
public class Mappers : TestBase
{
    [Test]
    [TestCase(typeof(Metric), typeof(MetricViewModel))]
    [TestCase(typeof(MetricRaspred), typeof(MetricRaspredViewModel))]
    [TestCase(typeof(Oopt), typeof(OoptViewModel))]
    [TestCase(typeof(PlantClade), typeof(PlantCladeViewModel))]
    [TestCase(typeof(PlantClass), typeof(PlantClassViewModel))]
    [TestCase(typeof(PlantFamily), typeof(PlantFamilyViewModel))]
    [TestCase(typeof(Plant), typeof(PlantViewModel), 2)]
    [TestCase(typeof(RarityCategory), typeof(RarityCategoryViewModel))]
    [TestCase(typeof(RedBook), typeof(RedBookViewModel))]
    [TestCase(typeof(RedBookPlant), typeof(RedBookPlantViewModel))]
    [TestCase(typeof(RedBookPlant), typeof(RedBookRarityCategory))]
    [TestCase(typeof(PlantCharacteristic), typeof(PlantCharacteristicViewModel))]
    [TestCase(typeof(PlantCharacteristicValue), typeof(PlantCharacteristicValueViewModel))]
    [TestCase(typeof(Role), typeof(RoleViewModel))]
    [TestCase(typeof(User), typeof(UserViewModel))]
    [TestCase(typeof(WorkTask), typeof(TaskViewModel))]
    [TestCase(typeof(Plant), typeof(SerivcePlantViewModel), 2)]
    public void MappersTest(Type tModel, Type tView, int recursive = 1)
    {
        var fixture = new Fixture().Customize(new AutoMoqCustomization());
        fixture.Behaviors.OfType<ThrowingRecursionBehavior>().ToList()
            .ForEach(b => fixture.Behaviors.Remove(b));
        fixture.Behaviors.Add(new OmitOnRecursionBehavior(recursive));
        var rndGuid = Guid.NewGuid();
        fixture.Register(() => rndGuid);
        fixture.Register(() => "string");
        fixture.Register(() => 10);
        fixture.Register(() => StatusWorker.InProgress);
        fixture.Register(() => new PositionProperty {Latitude = 123, Longitude = 456});
        fixture.Register((PlantCharacteristicValuesViewModel p) => new List<PlantCharacteristicValuesViewModel> {p});
        var input = typeof(FixtureFreezer)
            .GetMethod("Freeze", new[] {typeof(IFixture)})?
            .MakeGenericMethod(tModel)
            .Invoke(null, new object?[] {fixture});
        var expected = typeof(FixtureFreezer)
            .GetMethod("Freeze", new[] {typeof(IFixture)})?
            .MakeGenericMethod(tView)
            .Invoke(null, new object?[] {fixture});
        var configurationProvider = new MapperConfiguration(x => x.AddMaps(tModel));
        var mapper = new Mapper(configurationProvider);
        var actual = mapper.Map(input, tModel, tView);
        expected.Should().BeEquivalentTo(actual);
    }
}
