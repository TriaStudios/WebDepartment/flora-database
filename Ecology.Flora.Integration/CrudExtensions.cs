using System;
using System.Collections.Generic;
using System.Linq;
using Ecology.DAL.Common.Abstract;
using Ecology.DAL.Common.Crud;
using Ecology.DAL.Common.Models;

namespace Ecology.Flora.Integration;

public static class CrudExtensions
{
    public static Dictionary<string, Guid> GetDictNamesRu<T1, T2, T3>(this ICrudAsync<T1, T2, T3> crud)
        where T1 : ViewModelBase where T2 : BindingModelBase where T3 : FilterBase, new()
    {
        return crud.ReadAsync(new T3(), new PageBindingModel(0, 10000))
            .GetAwaiter()
            .GetResult()
            .Elements
            .ToDictionary(x => x.Name.Ru, x => x.Id);
    }
    
    public static Dictionary<string, Guid> GetDictNamesLa<T1, T2, T3>(this ICrudAsync<T1, T2, T3> crud)
        where T1 : ViewModelBase where T2 : BindingModelBase where T3 : FilterBase, new()
    {
        return crud.ReadAsync(new T3(), new PageBindingModel(0, 10000))
            .GetAwaiter()
            .GetResult()
            .Elements
            .ToDictionary(x => x.Name.La, x => x.Id);
    }
}
