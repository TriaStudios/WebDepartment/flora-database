using System.Threading.Tasks;
using Ecology.Flora.DAL.PostgreSQL;
using Ecology.Flora.Integration.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;

namespace Ecology.Flora.Integration;

public class Program
{
    public static async Task Main(string[] args)
    {
        var builder = Host.CreateDefaultBuilder(args)
            .UseSerilog((context, services, configuration) => configuration
                .ReadFrom.Configuration(context.Configuration)
                .ReadFrom.Services(services))
            .ConfigureServices((context, services) =>
            {
                services.AddGrpc();
                services.AddFloraDb(context.Configuration.GetConnectionString("FloraDatabase"));
                services.AddHostedService<HelloService>();
            })
            .ConfigureWebHostDefaults(webHostBuilder =>
            {
                webHostBuilder.Configure((context, app) =>
                {
                    app.UseRouting();
                    
                    app.UseEndpoints(endpoints =>
                    {
                        endpoints.MapGrpcService<IntegrationService>();
                    });
                });
            });

        using var app = builder.Build();
        await app.RunAsync();
    }
}
