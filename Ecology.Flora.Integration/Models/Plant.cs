﻿namespace Ecology.Flora.Integration.Models;

public class Plant
{
    public string LatinName { get; set; }
    public string RussianName { get; set; }
    public PlantSetting PlantSetting { get; set; }
}