﻿using System.Collections.Generic;

namespace Ecology.Flora.Integration.Models;

public class Class
{
    public string LatinName { get; set; }
    public string RussianName { get; set; }

    public List<Subclass> Subclasses { get; } = new();
}
