﻿using System.Collections.Generic;

namespace Ecology.Flora.Integration.Models;

public class Department
{
    public string LatinName { get; set; }
    public string RussianName { get; set; }

    public List<Class> Classes { get; } = new();
}
