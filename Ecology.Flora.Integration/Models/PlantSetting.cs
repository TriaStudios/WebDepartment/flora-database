﻿namespace Ecology.Flora.Integration.Models;

public class PlantSetting
{
    public string HGroup { get; set; }
    public string EpGroupComment { get; set; }
    public string OccurrenceSpeciesName { get; set; }
    public string SgLifeFormName { get; set; }
    public string TypeAreaName { get; set; }
    public string IuchComment { get; set; }

    public Iuch[] IucNs { get; set; }
    public string[] RLifeFormNames { get; set; }
    public string[] EconomicValues { get; set; }
    public string[] AdventiveDatas { get; set; }
    public string[] EpGroups { get; set; }
}