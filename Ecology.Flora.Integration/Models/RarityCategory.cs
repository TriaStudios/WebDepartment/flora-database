﻿namespace Ecology.Flora.Integration.Models;

public class RarityCategory
{
    public static readonly string[] RarityCodes =
    {
        "Ex", "E", "V", "R", "I"
    };
}