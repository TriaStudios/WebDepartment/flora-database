﻿using System.Collections.Generic;

namespace Ecology.Flora.Integration.Models;

public class Subclass
{
    public string LatinName { get; set; }
    public string RussianName { get; set; }

    public List<Plant> Plants { get; } = new();
}
