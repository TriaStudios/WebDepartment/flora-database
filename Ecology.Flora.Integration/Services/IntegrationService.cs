﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Ecology.DAL.Common.Abstract;
using Ecology.Flora.DAL.Logic.BindingModels.Plants;
using Ecology.Flora.DAL.Logic.Crud.Plants;
using Ecology.Flora.DAL.Logic.Enums;
using Ecology.Flora.DAL.Logic.Filters.Plants;
using Ecology.Flora.DAL.PostgreSQL;
using Ecology.Flora.DAL.PostgreSQL.Models.Plants;
using Ecology.Flora.Integration.Api;
using Ecology.Flora.Integration.Models;
using Grpc.Core;
using Microsoft.EntityFrameworkCore;
using Serilog;
using Syncfusion.DocIO;
using Syncfusion.DocIO.DLS;
using Plant = Ecology.Flora.Integration.Models.Plant;
using RarityCategory = Ecology.Flora.Integration.Models.RarityCategory;

namespace Ecology.Flora.Integration.Services;

public class IntegrationService : IntegrationGrpc.IntegrationGrpcBase
{
    private static readonly char[] TrimSymbols =
    {
        ' ', '.', ',', ';', '-',
        '?', '!', '\'', '"', '*',
        '%', '$', '#', '<', '>',
        ':', '[', ']', '{', '}'
    };

    private readonly IDbContextFactory<FloraDatabaseContext> _contextFactory;
    private readonly ILogger _logger;

    private readonly IPlantCladeCrudAsync _plantCladeCrud;
    private readonly IPlantClassCrudAsync _plantClassCrud;
    private readonly IPlantCrudAsync _plantCrud;
    private readonly IPlantFamilyCrudAsync _plantFamilyCrud;
    private readonly IRarityCategoryCrudAsync _rarityCategoryCrud;
    private readonly IRedBookCrudAsync _redBookCrud;
    private readonly IPlantCharacteristicCrudAsync _plantCharacteristicCrud;

    public IntegrationService(
        IPlantCladeCrudAsync plantCladeCrud,
        IPlantClassCrudAsync plantClassCrud,
        IPlantFamilyCrudAsync plantFamilyCrud,
        IPlantCrudAsync plantCrud,
        IRarityCategoryCrudAsync rarityCategoryCrud,
        IRedBookCrudAsync redBookCrud,
        IPlantCharacteristicCrudAsync plantCharacteristicCrud,
        IDbContextFactory<FloraDatabaseContext> contextFactory,
        ILogger logger)
    {
        _plantCladeCrud = plantCladeCrud;
        _plantClassCrud = plantClassCrud;
        _plantFamilyCrud = plantFamilyCrud;
        _plantCrud = plantCrud;
        _rarityCategoryCrud = rarityCategoryCrud;
        _redBookCrud = redBookCrud;
        _plantCharacteristicCrud = plantCharacteristicCrud;
        _contextFactory = contextFactory;
        _logger = logger;
    }

    public override async Task<IntegrateWordFileResponse> IntegrateWordFile(IntegrateWordFileRequest request,
        ServerCallContext context)
    {
        _logger.Information("Start integration from word file ({Count} bytes)", request.FileData.Length);

        await using var streamFile = new MemoryStream(request.FileData.ToByteArray());
        using WordDocument document = new(streamFile, FormatType.Doc);

        var departmentsFromWordFileAsync = ParseDepartmentsFromWordFileAsync(document);
        var preparingDataAsync = PreparingDataAsync(departmentsFromWordFileAsync, context.CancellationToken);
        await MigrateDataToDatabaseAsync(preparingDataAsync, context.CancellationToken);

        return new IntegrateWordFileResponse();
    }

    private static async IAsyncEnumerable<Department> ParseDepartmentsFromWordFileAsync(IWordDocument wordDocument)
    {
        Regex departmentRegex =
            new(
                @"^Отдел[ 	]*(?<latinName>[a-zA-Zа-яА-Я0-9 .()\[\],]+)[ 	]*[–-][ 	]*(?<russianName>[a-zA-Zа-яА-Я0-9 .()\[\],]+)[ 	]*$");
        Regex classRegex =
            new(
                @"^Класс[ 	]+(?<latinName>[a-zA-Zа-яА-Я0-9 .()\[\],]+)[ 	]*[–-][ 	]*(?<russianName>[a-zA-Zа-яА-Я0-9 .()\[\],]+)[ 	]*$");
        Regex subclassRegex =
            new(
                @"Сем[ 	]*.[ 	]+(?<latinName>[a-zA-Zа-яА-Я0-9 .()\[\],]+)[ 	]*[–-][ 	]*(?<russianName>[a-zA-Zа-яА-Я0-9 .()\[\],]+)[ 	]*");
        Regex plantRegex =
            new(@"(?<latinName>[a-zA-Zа-яА-Я0-9 .()\[\],]+)[ 	]*[–-][ 	]*(?<russianName>[a-zA-Zа-яА-Я0-9 .()\[\],]+).");

        Department currentDepartment = null;
        Class currentClass = null;
        Subclass currentSubclass = null;
        Plant currentPlant = null;
        var state = 0;

        foreach (WParagraph section in ((WTextBody) wordDocument.Sections[0].ChildEntities[0]).ChildEntities)
        {
            if (string.IsNullOrWhiteSpace(section.Text) || section.Text.StartsWith("Прим. ")) continue;

            var departmentMatch = departmentRegex.Match(section.Text);
            var classMatch = classRegex.Match(section.Text);
            var subclassMatch = subclassRegex.Match(section.Text);
            switch (state)
            {
                case 5 when departmentMatch.Success:
                case 0 when departmentMatch.Success:
                    if (currentDepartment != null) yield return currentDepartment;

                    currentDepartment = new Department
                    {
                        LatinName = departmentMatch.Groups["latinName"].Value,
                        RussianName = departmentMatch.Groups["russianName"].Value
                    };
                    state = 1;
                    break;
                case 5 when classMatch.Success:
                case 1 when classMatch.Success:
                    currentClass = new Class
                    {
                        LatinName = classMatch.Groups["latinName"].Value,
                        RussianName = classMatch.Groups["russianName"].Value
                    };
                    currentDepartment.Classes.Add(currentClass);
                    state = 2;
                    break;
                case 5 when subclassMatch.Success:
                case 2 when subclassMatch.Success:
                    currentSubclass = new Subclass
                    {
                        LatinName = subclassMatch.Groups["latinName"].Value,
                        RussianName = subclassMatch.Groups["russianName"].Value
                    };
                    currentClass.Subclasses.Add(currentSubclass);
                    state = 3;
                    break;
                case 5 when !departmentMatch.Success && !classMatch.Success && !subclassMatch.Success:
                case 3 when !departmentMatch.Success && !classMatch.Success && !subclassMatch.Success:
                    var match = plantRegex.Match(section.Text);
                    var ln = match.Groups["latinName"].Value
                        .Replace("\u001f", "")
                        .Trim('.', ' ', ',', '\t', '1', ';');
                    ln = Regex.Replace(ln, @"[\s]{2,}", " ");
                    ln = Regex.Replace(ln, @"[.](?<data>[\S])", y => $". {y.Groups["data"].Value}");
                    ln = Regex.Replace(ln, @"[\s][.]", ".");
                    ln = Regex.Replace(ln, @"[,](?<data>[\S])", y => $", {y.Groups["data"].Value}");
                    ln = Regex.Replace(ln, @"[\s][,]", ",");
                    var rn = match.Groups["russianName"].Value
                        .Replace("\u001f", "")
                        .Trim('.', ' ', ',', '\t', '1', ';');
                    rn = Regex.Replace(rn, @"[\s]{2,}", " ");
                    rn = Regex.Replace(rn, @"[.](?<data>[\S])", y => $". {y.Groups["data"].Value}");
                    rn = Regex.Replace(rn, @"[\s][.]", ".");
                    rn = Regex.Replace(rn, @"[,](?<data>[\S])", y => $", {y.Groups["data"].Value}");
                    rn = Regex.Replace(rn, @"[\s][,]", ",");
                    currentPlant = new Plant
                    {
                        LatinName = ln,
                        RussianName = rn
                    };
                    currentSubclass.Plants.Add(currentPlant);
                    state = 4;
                    break;
                case 4 when section.Text.StartsWith("1."):
                    var res = Regex.Replace(section.Text, @"([;.][  ]*)+[\d].[ ]*", "$")
                        .Replace("\u001f", "")
                        .TrimStart('.', ' ', ',', '\t', ';', '1')
                        .TrimEnd('.', ' ', ',', '\t', ';')
                        .Split("$");
                    res = res
                        .Select(x => Regex.Replace(x, @"[\s]{2,}", " "))
                        .Select(x => Regex.Replace(x, @"[.](?<data>[\S])", y => $". {y.Groups["data"].Value}"))
                        .Select(x => Regex.Replace(x, @"[\s][.]", "."))
                        .Select(x => Regex.Replace(x, @"[,](?<data>[\S])", y => $", {y.Groups["data"].Value}"))
                        .Select(x => Regex.Replace(x, @"[\s][,]", ","))
                        .ToArray();
                    var additiveData = Regex.IsMatch(res[^1], @"(Археофит|Кенофит)", RegexOptions.IgnoreCase);
                    var iuch = res
                        .Select(x => Regex.Match(x,
                            @"(?<iuches>(Кк (РФ|УО|РТ) ([\d])[,. ;]*)+)(?<comment>[a-zA-Zа-яА-Я0-9 .()\[\],\-]*)",
                            RegexOptions.IgnoreCase))
                        .FirstOrDefault(x => x.Success);
                    var iuches = Array.Empty<Iuch>();
                    if (iuch != null)
                        iuches = iuch.Groups["iuches"].Value.Split(", ", StringSplitOptions.RemoveEmptyEntries).Select(
                            x =>
                            {
                                var m = Regex.Match(x, @"Кк (РФ|УО|РТ) ([\d])", RegexOptions.IgnoreCase);
                                return new Iuch
                                {
                                    Kk = int.Parse(m.Groups[2].Value),
                                    Place = m.Groups[1].Value.ToUpper()
                                };
                            }).ToArray();

                    var iuchComment = iuch?.Groups["comment"].Value;
                    iuchComment = string.IsNullOrWhiteSpace(iuchComment) ? null : iuchComment;
                    var epgrouphui = res[3].Split(new[] {'.', ';'},
                        StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries);
                    var epGroupSplitI = 1;
                    for (var i = 1; i < epgrouphui.Length; i++)
                    {
                        var ep = epgrouphui[i].Split(' ', StringSplitOptions.RemoveEmptyEntries);
                        if (ep.Length <= 1) continue;
                        epGroupSplitI = i;
                        break;
                    }

                    var dataPlant = currentPlant.PlantSetting = new PlantSetting
                    {
                        SgLifeFormName = res[0],
                        RLifeFormNames = res[1].ToLower().Split(", "),
                        HGroup = res[2].ToLower(),
                        EpGroupComment = string.Join('.', epgrouphui.Skip(epGroupSplitI)),
                        EpGroups = epgrouphui.Take(epGroupSplitI).ToArray(),
                        TypeAreaName = res[4],
                        OccurrenceSpeciesName = res[5],
                        AdventiveDatas = additiveData ? res[^1].ToLower().Split(", ") : Array.Empty<string>(),
                        EconomicValues = Array.Empty<string>(),
                        IuchComment = iuchComment,
                        IucNs = iuches
                    };
                    if (res.Length == 9 ||
                        iuch == null && additiveData && res.Length == 8)
                        dataPlant.EconomicValues = res[^2].ToLower().Split(", ");
                    else if (res.Length == 7 && !additiveData && iuch == null ||
                             iuch != null && !additiveData && res.Length == 8)
                        dataPlant.EconomicValues = res[^1].ToLower().Split(", ");

                    state = 5;
                    break;
            }
        }

        yield return currentDepartment;

        await Task.CompletedTask;
    }

    private static async IAsyncEnumerable<Department> PreparingDataAsync(IAsyncEnumerable<Department> departments,
        [EnumeratorCancellation] CancellationToken cancellationToken = default)
    {
        await foreach (var department in departments.WithCancellation(cancellationToken))
        {
            department.LatinName = department.LatinName?.ToLower().Trim(TrimSymbols);
            department.RussianName = department.RussianName?.ToLower().Trim(TrimSymbols);
            foreach (var @class in department.Classes)
            {
                @class.RussianName = @class.RussianName?.ToLower().Trim(TrimSymbols);
                @class.LatinName = @class.LatinName?.ToLower().Trim(TrimSymbols);
                foreach (var subclass in @class.Subclasses)
                {
                    subclass.RussianName = subclass.RussianName?.ToLower().Trim(TrimSymbols);
                    subclass.LatinName = subclass.LatinName?.ToLower().Trim(TrimSymbols);
                    foreach (var plant in subclass.Plants)
                    {
                        plant.RussianName = plant.RussianName?.ToLower().Trim(TrimSymbols);
                        plant.LatinName = plant.LatinName?.ToLower().Trim(TrimSymbols);
                        plant.PlantSetting = new PlantSetting
                        {
                            AdventiveDatas = plant.PlantSetting.AdventiveDatas
                                ?.Select(x => x.ToLower().Trim(TrimSymbols)).ToArray(),
                            EconomicValues = plant.PlantSetting.EconomicValues
                                ?.Select(x => x.ToLower().Trim(TrimSymbols)).ToArray(),
                            EpGroups =
                                plant.PlantSetting.EpGroups?.Select(x => x.ToLower().Trim(TrimSymbols)).ToArray(),
                            RLifeFormNames = plant.PlantSetting.RLifeFormNames
                                ?.Select(x => x.ToLower().Trim(TrimSymbols)).ToArray(),
                            IucNs = plant.PlantSetting.IucNs?.Select(x => new Iuch
                            {
                                Kk = x.Kk,
                                Place = x.Place.ToLower().Trim(TrimSymbols)
                            }).ToArray(),
                            HGroup = plant.PlantSetting.HGroup?.ToLower().Trim(TrimSymbols),
                            IuchComment = plant.PlantSetting.IuchComment?.ToLower().Trim(TrimSymbols),
                            EpGroupComment = plant.PlantSetting.EpGroupComment?.ToLower().Trim(TrimSymbols),
                            OccurrenceSpeciesName =
                                plant.PlantSetting.OccurrenceSpeciesName?.ToLower().Trim(TrimSymbols),
                            TypeAreaName = plant.PlantSetting.TypeAreaName?.ToLower().Trim(TrimSymbols),
                            SgLifeFormName = plant.PlantSetting.SgLifeFormName?.ToLower().Trim(TrimSymbols)
                        };
                    }
                }
            }

            yield return department;
        }
    }

    private async Task MigrateDataToDatabaseAsync(IAsyncEnumerable<Department> departments,
        CancellationToken ct = default)
    {
        await using var context = await _contextFactory.CreateDbContextAsync(ct);
        var plantCharacteristicSet = _plantCharacteristicCrud.GetDictNamesLa();
        var plantCladeSet = _plantCladeCrud.GetDictNamesRu();
        var plantClassSet = _plantClassCrud.GetDictNamesRu();
        var plantFamilySet = _plantFamilyCrud.GetDictNamesRu();
        var plantSet = _plantCrud.GetDictNamesRu();
        var redBookSet = _redBookCrud.GetDictNamesRu();
        var countIntegratedPlant = 0;
        await foreach (var department in departments.WithCancellation(ct))
        {
            plantCladeSet[department.RussianName] = await _plantCladeCrud.CreateOrUpdateAsync(new PlantCladeBindingModel
            {
                Id = plantCladeSet.GetValueOrDefault(department.RussianName),
                Name = new LocalizeProperty(department.RussianName, department.LatinName)
            }, ct);
            foreach (var @class in department.Classes)
            {
                plantClassSet[@class.RussianName] = await _plantClassCrud.CreateOrUpdateAsync(new PlantClassBindingModel
                {
                    Id = plantClassSet.GetValueOrDefault(@class.RussianName),
                    PlantCladeId = plantCladeSet[department.RussianName],
                    Name = new LocalizeProperty(@class.RussianName, @class.LatinName)
                }, ct);
                foreach (var subclass in @class.Subclasses)
                {
                    plantFamilySet[subclass.RussianName] = await _plantFamilyCrud.CreateOrUpdateAsync(
                        new PlantFamilyBindingModel
                        {
                            Id = plantFamilySet.GetValueOrDefault(subclass.RussianName),
                            PlantClassId = plantClassSet[@class.RussianName],
                            Name = new LocalizeProperty(subclass.RussianName, subclass.LatinName)
                        }, ct);
                    foreach (var plant in subclass.Plants)
                    {
                        var plantCharacteristicValues = new HashSet<PlantCharacteristicValueBindingModel>();
                        plantCharacteristicValues.Add(new PlantCharacteristicValueBindingModel
                        {
                            Name = new LocalizeProperty("свияга"),
                            PlantCharacteristicId = plantCharacteristicSet[PlantCharacteristicNames.River]
                        });
                        plantCharacteristicValues.Add(new PlantCharacteristicValueBindingModel
                        {
                            Name = new LocalizeProperty(plant.PlantSetting.OccurrenceSpeciesName),
                            PlantCharacteristicId = plantCharacteristicSet[PlantCharacteristicNames.OccurrenceSpecies]
                        });
                        plantCharacteristicValues.Add(new PlantCharacteristicValueBindingModel
                        {
                            Name = new LocalizeProperty(plant.PlantSetting.SgLifeFormName),
                            PlantCharacteristicId = plantCharacteristicSet[PlantCharacteristicNames.LifeFormSerebryakovGolubev]
                        });
                        plantCharacteristicValues.Add(new PlantCharacteristicValueBindingModel
                        {
                            Name = new LocalizeProperty(plant.PlantSetting.TypeAreaName),
                            PlantCharacteristicId = plantCharacteristicSet[PlantCharacteristicNames.ArealType]
                        });
                        plantCharacteristicValues.Add(new PlantCharacteristicValueBindingModel
                        {
                            Name = new LocalizeProperty(plant.PlantSetting.HGroup),
                            PlantCharacteristicId = plantCharacteristicSet[PlantCharacteristicNames.HydrotypeGroup]
                        });
                        if (plant.PlantSetting.AdventiveDatas.Length > 0)
                        {
                            plantCharacteristicValues.Add(new PlantCharacteristicValueBindingModel
                            {
                                Name = new LocalizeProperty("адвентивное"),
                                PlantCharacteristicId = plantCharacteristicSet[PlantCharacteristicNames.OriginSpecies]
                            });
                            plantCharacteristicValues.Add(new PlantCharacteristicValueBindingModel
                            {
                                Name = new LocalizeProperty(plant.PlantSetting.AdventiveDatas[0]),
                                PlantCharacteristicId = plantCharacteristicSet[PlantCharacteristicNames.IntroductionTimeGroup]
                            });
                            plantCharacteristicValues.Add(new PlantCharacteristicValueBindingModel
                            {
                                Name = new LocalizeProperty(plant.PlantSetting.AdventiveDatas[1]),
                                PlantCharacteristicId = plantCharacteristicSet[PlantCharacteristicNames.IntroductionWay]
                            });
                            plantCharacteristicValues.Add(new PlantCharacteristicValueBindingModel
                            {
                                Name = new LocalizeProperty(plant.PlantSetting.AdventiveDatas[2]),
                                PlantCharacteristicId = plantCharacteristicSet[PlantCharacteristicNames.NaturalizationDegree]
                            });
                        }
                        else
                        {
                            plantCharacteristicValues.Add(new PlantCharacteristicValueBindingModel
                            {
                                Name = new LocalizeProperty("аборигенное"),
                                PlantCharacteristicId = plantCharacteristicSet[PlantCharacteristicNames.OriginSpecies]
                            });
                        }
                        
                        foreach (var item in plant.PlantSetting.EpGroups)
                        {
                            plantCharacteristicValues.Add(new PlantCharacteristicValueBindingModel
                            {
                                Name = new LocalizeProperty(item),
                                PlantCharacteristicId = plantCharacteristicSet[PlantCharacteristicNames.EcologicalPhytocenoticGroup]
                            });
                        }

                        foreach (var item in plant.PlantSetting.RLifeFormNames)
                        {
                            plantCharacteristicValues.Add(new PlantCharacteristicValueBindingModel
                            {
                                Name = new LocalizeProperty(item),
                                PlantCharacteristicId = plantCharacteristicSet[PlantCharacteristicNames.LifeFormRaunkiaer]
                            });
                        }

                        foreach (var item in plant.PlantSetting.EconomicValues)
                        {
                            plantCharacteristicValues.Add(new PlantCharacteristicValueBindingModel
                            {
                                Name = new LocalizeProperty(item),
                                PlantCharacteristicId = plantCharacteristicSet[PlantCharacteristicNames.EconomicValue]
                            });
                        }

                        var filter = new PlantFilter();
                        if (plantSet.TryGetValue(plant.RussianName, out var id))
                        {
                            filter.ObjectIds = new HashSet<Guid> {id};
                        }
                        var plantDb = await _plantCrud.FirstOrDefaultAsync(filter, ct);
                        var plantBind = new PlantBindingModel
                        {
                            Id = plantSet.GetValueOrDefault(plant.RussianName),
                            Name = new LocalizeProperty(plant.RussianName, plant.LatinName),
                            EcologicalPhytocenoticGroupComment = new LocalizeProperty(plant.PlantSetting.EpGroupComment),
                            PlantFamilyId = plantFamilySet[subclass.RussianName],
                            RedBookComment = new LocalizeProperty(plant.PlantSetting.IuchComment),
                            PlantCharacteristicValues = plantCharacteristicValues
                        };
                        if (plantDb != null)
                        {
                            plantBind.Position = plantDb.Position;
                            plantBind.AuthorIds = plantDb.Authors.Select(x => x.Id).ToHashSet();
                            plantBind.RedBooksPlantIds = plantDb.RedBookPlants.Select(x => x.Id).ToHashSet();
                            foreach (var viewModelBase in plantDb.PlantCharacteristicValues
                                         .FindAll(x => x.Characteristic.Name.Ru == PlantCharacteristicNames.Oopt)
                                         .SelectMany(plantCharacteristicValueViewModel => plantCharacteristicValueViewModel.Values))
                            {
                                plantCharacteristicValues.Add(new PlantCharacteristicValueBindingModel
                                {
                                    Name = viewModelBase.Name,
                                    PlantCharacteristicId = plantCharacteristicSet[PlantCharacteristicNames.Oopt]
                                });
                            }
                        }
                        plantSet[plant.RussianName] = await _plantCrud.CreateOrUpdateAsync(plantBind, ct);

                        foreach (var item in plant.PlantSetting.IucNs)
                        {
                            redBookSet[item.Place] = await _redBookCrud.CreateOrUpdateAsync(new RedBookBindingModel
                            {
                                Id = redBookSet.GetValueOrDefault(item.Place),
                                Name = new LocalizeProperty(item.Place)
                            }, ct);
                            var rc = (await _rarityCategoryCrud.FirstOrDefaultAsync(new RarityCategoryFilter
                            {
                                Codes = {RarityCategory.RarityCodes[item.Kk]}
                            }, ct)).Id;

                            await using var ctx = await _contextFactory.CreateDbContextAsync(ct);
                            var rbp = await ctx.Set<RedBookPlant>().FirstOrDefaultAsync(x => x.RedBookId == redBookSet[item.Place] && x.RarityCategoryId == rc, ct);

                            if (rbp == null)
                            {
                                rbp = new RedBookPlant();
                                ctx.Add(rbp);
                            }

                            rbp.PlantId = plantSet[plant.RussianName];
                            rbp.RarityCategoryId = rc;
                            rbp.RedBookId = redBookSet[item.Place];
                            await ctx.SaveChangesAsync(ct);
                            plantBind.RedBooksPlantIds.Add(rbp.Id);
                        }

                        plantBind.Id = plantSet[plant.RussianName];
                        await _plantCrud.CreateOrUpdateAsync(plantBind, ct);
                        _logger.Debug("Plant #{Number} '{Name}' is integrated", ++countIntegratedPlant,
                            plant.RussianName);
                    }
                }
            }
        }
    }
}
