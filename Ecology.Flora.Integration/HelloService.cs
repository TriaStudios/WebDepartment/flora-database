﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Serilog;

namespace Ecology.Flora.Integration;

public class HelloService : IHostedService
{
    private readonly ILogger _logger;

    public HelloService(ILogger logger)
    {
        _logger = logger;
    }

    public Task StartAsync(CancellationToken cancellationToken)
    {
        _logger.Information("NET_ENVIRONMENT = {Kek}", Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT"));
        _logger.Information(
            "\n ▄█  ███▄▄▄▄       ███        ▄████████    ▄██████▄     ▄████████    ▄████████     ███      ▄█   ▄██████▄  ███▄▄▄▄   \n" +
            "███  ███▀▀▀██▄ ▀█████████▄   ███    ███   ███    ███   ███    ███   ███    ███ ▀█████████▄ ███  ███    ███ ███▀▀▀██▄ \n" +
            "███▌ ███   ███    ▀███▀▀██   ███    █▀    ███    █▀    ███    ███   ███    ███    ▀███▀▀██ ███▌ ███    ███ ███   ███ \n" +
            "███▌ ███   ███     ███   ▀  ▄███▄▄▄      ▄███         ▄███▄▄▄▄██▀   ███    ███     ███   ▀ ███▌ ███    ███ ███   ███ \n" +
            "███▌ ███   ███     ███     ▀▀███▀▀▀     ▀▀███ ████▄  ▀▀███▀▀▀▀▀   ▀███████████     ███     ███▌ ███    ███ ███   ███ \n" +
            "███  ███   ███     ███       ███    █▄    ███    ███ ▀███████████   ███    ███     ███     ███  ███    ███ ███   ███ \n" +
            "███  ███   ███     ███       ███    ███   ███    ███   ███    ███   ███    ███     ███     ███  ███    ███ ███   ███ \n" +
            "█▀    ▀█   █▀     ▄████▀     ██████████   ████████▀    ███    ███   ███    █▀     ▄████▀   █▀    ▀██████▀   ▀█   █▀  \n" +
            "                                                       ███    ███                                                    \n"
        );
        return Task.CompletedTask;
    }

    public Task StopAsync(CancellationToken cancellationToken)
    {
        throw new System.NotImplementedException();
    }
}
