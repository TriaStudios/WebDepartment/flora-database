﻿using Ecology.Clustering.Api;
using Ecology.Clustering.Models;
using Ecology.Clustering.Utils;
using FluentAssertions;
using NUnit.Framework;
using TestsHelpers;

namespace Ecology.Clustering.Tests;

[TestFixture]
public class PointUtilsTests : TestBase
{
    [Test]
    [Order(0)]
    public void KavrayskiyViiProjection__CastTo__ShouldReturnKavrayskiyPoint()
    {
        // Arrange
        var geographicPoint = new GeographicPoint
        {
            Latitude = 0.5f,
            Longitude = 0.5f
        };
        
        // Act
        var cartesianPoint = geographicPoint.ToKavrayskiyViiProjection();
        
        // Assert
        cartesianPoint.X.Should().BeApproximately(0.4330077F, float.Epsilon);
        cartesianPoint.Y.Should().BeApproximately(0.5f, float.Epsilon);
    }

    [Test]
    [Order(1)]
    public void KavrayskiyViiProjection__CastFrom__ShouldReturnGeoPoint()
    {
        // Arrange
        var point = new CartesianPoint(0.41623516015624659251069930374806f, 0.5f);
        
        // Act
        var cartesianPoint = point.FromKavrayskiyViiProjection();

        // Assert
        cartesianPoint.Latitude.Should().BeApproximately(0.5f, float.Epsilon);
        cartesianPoint.Longitude.Should().BeApproximately(0.5f, float.Epsilon);
    }

    [Test]
    [Order(3)]
    [TestCase(0, 0, 0, 0, 0.5f)]
    [TestCase(1.5f, -0.43301270189221932338186158537647f, 1, 0, 0.5f)] // second is 0.5 * sqrt(3) / 2
    public void HexagonIndex__CastTo__ShouldReturnHexagonIndex(float x, float y, int q, int r, float hexSize)
    {
        // Arrange
        var cartesianPoint = new CartesianPoint(x, y);
        var expected = new Index
        {
            Q = q,
            R = r
        };
        
        // Act
        var index = cartesianPoint.FromPositionToHexagonIndex(hexSize);
        
        // Assert
        index.Should().Be(expected);
    }
}
