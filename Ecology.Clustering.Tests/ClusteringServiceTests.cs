﻿using System.Linq;
using System.Threading.Tasks;
using Ecology.Clustering.Api;
using Ecology.Clustering.Services;
using FluentAssertions;
using Grpc.Core;
using Moq;
using NUnit.Framework;
using Serilog;
using TestsHelpers;

namespace Ecology.Clustering.Tests;

[TestFixture]
public class ClusteringServiceTests : TestBase
{
    [Test]
    public async Task Clustering__ClusteringTwoPointsInOneCluster__ShouldReturnOneComplexCluster()
    {
        // Arrange
        var logger = new Mock<ILogger>();
        var clusteringService = new ClusteringService(logger.Object);
        var clusterPointsRequest = new ClusterPointsRequest
        {
            HexagonSize = 0.5f,
            Points =
            {
                new MapObject
                {
                    Point = new GeographicPoint
                    {
                        Latitude = 0,
                        Longitude = 0
                    },
                    UniqueIndex = 0
                },
                new MapObject
                {
                    Point = new GeographicPoint
                    {
                        Latitude = 0.2f,
                        Longitude = 0.01f
                    },
                    UniqueIndex = 1
                }
            }
        };
        
        // Act
        var cluster = await clusteringService.ClusterPoints(clusterPointsRequest.Clone(), new Mock<ServerCallContext>().Object);

        // Assert
        cluster.Clusters.Count.Should().Be(1, "Count clusters should be 1");
        cluster.Clusters.First().Center.Should().Be(new GeographicPoint
        {
            Latitude = (clusterPointsRequest.Points[0].Point.Latitude + clusterPointsRequest.Points[1].Point.Latitude) / 2, 
            Longitude = (clusterPointsRequest.Points[0].Point.Longitude + clusterPointsRequest.Points[1].Point.Longitude) / 2,
        }, "Incorrect center of cluster");
        cluster.Clusters.First().Index.Should().Be(new Index {Q = 0, R = 0}, "Incorrect index of cluster");
        cluster.Clusters.First().CountPoints.Should().Be(clusterPointsRequest.Points.Count, "incorrect count points");
        cluster.Clusters.First().HasUniqueIndex.Should().BeFalse("Cluster should be complex cluster");
    }
    
    [Test]
    public async Task Clustering__ClusteringOnePoint__ShouldReturnOneSimpleCluster()
    {
        // Arrange
        var logger = new Mock<ILogger>();
        var clusteringService = new ClusteringService(logger.Object);
        var clusterPointsRequest = new ClusterPointsRequest
        {
            HexagonSize = 0.5f,
            Points =
            {
                new MapObject
                {
                    Point = new GeographicPoint
                    {
                        Latitude = 0.2f,
                        Longitude = 0.01f
                    },
                    UniqueIndex = 101
                }
            }
        };
        
        // Act
        var cluster = await clusteringService.ClusterPoints(clusterPointsRequest, new Mock<ServerCallContext>().Object);

        // Assert
        cluster.Clusters.Count.Should()
            .Be(1, "Count clusters should be 1");
        cluster.Clusters.First().Center.Should().Be(new GeographicPoint
        {
            Latitude = clusterPointsRequest.Points[0].Point.Latitude,
            Longitude = clusterPointsRequest.Points[0].Point.Longitude,
        }, "Incorrect center of cluster");
        cluster.Clusters.First().Index.Should()
            .Be(new Index {Q = 0, R = 0}, "Incorrect index of cluster");
        cluster.Clusters.First().CountPoints.Should()
            .Be(clusterPointsRequest.Points.Count, "incorrect count points");
        cluster.Clusters.First().HasUniqueIndex.Should()
            .BeTrue("Cluster should be complex cluster");
        cluster.Clusters.First().UniqueIndex.Should()
            .Be(clusterPointsRequest.Points.First().UniqueIndex, "Unique index should be like input point");
        cluster.Clusters.First().MinBound.Should()
            .Be(clusterPointsRequest.Points.First().Point);
        cluster.Clusters.First().MaxBound.Should()
            .Be(clusterPointsRequest.Points.First().Point);
    }
}
