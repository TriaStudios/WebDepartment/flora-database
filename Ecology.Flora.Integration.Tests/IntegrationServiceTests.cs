﻿using System.IO;
using System.Threading.Tasks;
using Ecology.Flora.DAL.PostgreSQL;
using Ecology.Flora.DAL.PostgreSQL.Models.Plants;
using Ecology.Flora.DAL.PostgreSQL.Models.Users;
using Ecology.Flora.Integration.Api;
using FluentAssertions;
using Google.Protobuf;
using Grpc.Net.Client;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using TestsHelpers;

namespace Ecology.Flora.Integration.Tests;

[TestFixture]
public class IntegrationServiceTests : TestBase
{
    private WebApplicationFactory<Program> _webApplicationFactory;
    private IntegrationGrpc.IntegrationGrpcClient _integrationGrpcClient;

    [SetUp]
    public void Setup()
    {
        _webApplicationFactory = new WebApplicationFactory<Program>()
            .WithWebHostBuilder(hostBuilder =>
                hostBuilder.ConfigureServices(collection =>
                    collection.UseInMemoryDatabase()));
        var httpClient = _webApplicationFactory.CreateClient();
        var channel = GrpcChannel.ForAddress(httpClient.BaseAddress, new GrpcChannelOptions {HttpClient = httpClient});
        _integrationGrpcClient = new IntegrationGrpc.IntegrationGrpcClient(channel);
    }
    
    [Test]
    [Order(1)]
    public async Task Integration__ImportFiftyNinePlants__ShouldSavePlantsToDatabase()
    {
        await _integrationGrpcClient.IntegrateWordFileAsync(new IntegrateWordFileRequest
        {
            FileData = ByteString.CopyFrom(await File.ReadAllBytesAsync($"Data/{nameof(Integration__ImportFiftyNinePlants__ShouldSavePlantsToDatabase)}.doc"))
        });
        await using var context = await _webApplicationFactory.Services
            .GetRequiredService<IDbContextFactory<FloraDatabaseContext>>()
            .CreateDbContextAsync();

        var plantCladesCount = await context.Set<PlantClade>().CountAsync();
        var plantClassesCount = await context.Set<PlantClass>().CountAsync();
        var plantFamiliesCount = await context.Set<PlantFamily>().CountAsync();
        var plantsCount = await context.Set<Plant>().CountAsync();
        plantCladesCount.Should().Be(5);
        plantClassesCount.Should().Be(6);
        plantFamiliesCount.Should().Be(58);
        plantsCount.Should().Be(59);
    }

    [Test]
    [Order(2)]
    public async Task Integration__ImportExistedPlants__ShouldUpdateExistedPlants()
    {
        await using var context = await _webApplicationFactory.Services
            .GetRequiredService<IDbContextFactory<FloraDatabaseContext>>()
            .CreateDbContextAsync();
        var plant = await context.Set<Plant>()
            .FirstAsync(x => x.Name.Ru.StartsWith("двурядник сплюснутый"));
        var user = await context.Set<User>().FirstAsync();
        plant.Authors.Add(user);
        await context.SaveChangesAsync();
        
        await _integrationGrpcClient.IntegrateWordFileAsync(new IntegrateWordFileRequest
        {
            FileData = ByteString.CopyFrom(await File.ReadAllBytesAsync($"Data/{nameof(Integration__ImportExistedPlants__ShouldUpdateExistedPlants)}.doc"))
        });

        var plantCladesCount = await context.Set<PlantClade>().CountAsync();
        var plantClassesCount = await context.Set<PlantClass>().CountAsync();
        var plantFamiliesCount = await context.Set<PlantFamily>().CountAsync();
        var plantsCount = await context.Set<Plant>().CountAsync();
        plantCladesCount.Should().Be(5);
        plantClassesCount.Should().Be(6);
        plantFamiliesCount.Should().Be(58);
        plantsCount.Should().Be(59);
    }
}
