using Ecology.Flora.DAL.Logic.Enums;

namespace Ecology.Flora.DAL.Logic.BindingModels.Workers;

public class TaskBindingModel : BindingModelBase
{
    public string Namespace { get; set; }
    public StatusWorker Status { get; set; }
    public string Message { get; set; }
}