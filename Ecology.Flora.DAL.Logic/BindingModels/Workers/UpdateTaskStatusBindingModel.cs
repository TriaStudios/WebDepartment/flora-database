using Ecology.Flora.DAL.Logic.Enums;

namespace Ecology.Flora.DAL.Logic.BindingModels.Workers;

public class UpdateTaskStatusBindingModel
{
    public Guid Id { get; set; }
    public StatusWorker Status { get; set; }
    public string Message { get; set; }
}