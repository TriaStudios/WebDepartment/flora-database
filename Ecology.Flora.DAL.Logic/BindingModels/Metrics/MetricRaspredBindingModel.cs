﻿namespace Ecology.Flora.DAL.Logic.BindingModels.Metrics;

public class MetricRaspredBindingModel : BindingModelBase
{
    public int Bal { get; set; }
    public int Value { get; set; }
    public Guid? MetricId { get; set; }
}