﻿namespace Ecology.Flora.DAL.Logic.BindingModels.Metrics;

public class MetricBindingModel : BindingModelBase
{
    public HashSet<Guid> MetricRaspreds { get; } = new();
}