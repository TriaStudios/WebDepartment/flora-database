﻿namespace Ecology.Flora.DAL.Logic.BindingModels.Storage;

public class ImageBinding
{
    public string FileBase64 { get; set; }
    public LocalizeProperty Description { get; set; }
}