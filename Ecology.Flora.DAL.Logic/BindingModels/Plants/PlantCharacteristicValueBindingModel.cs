﻿namespace Ecology.Flora.DAL.Logic.BindingModels.Plants;

public class PlantCharacteristicValueBindingModel : BindingModelBase
{
    public Guid PlantCharacteristicId { get; set; }
}
