﻿namespace Ecology.Flora.DAL.Logic.BindingModels.Plants;

public class OoptBindingModel : BindingModelBase
{
    public int Square { get; set; }
    public int Metric8 { get; set; }

    public HashSet<Guid> PlantIds { get; } = new();
}