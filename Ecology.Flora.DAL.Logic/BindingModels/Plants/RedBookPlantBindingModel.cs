﻿namespace Ecology.Flora.DAL.Logic.BindingModels.Plants;

public class RedBookPlantBindingModel : BindingModelBase
{
    public Guid RedBookId { get; set; }
    public Guid RarityCategoryId { get; set; }
    public Guid PlantId { get; set; }
}