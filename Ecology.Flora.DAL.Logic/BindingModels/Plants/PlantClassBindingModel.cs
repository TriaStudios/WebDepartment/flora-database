﻿namespace Ecology.Flora.DAL.Logic.BindingModels.Plants;

public class PlantClassBindingModel : BindingModelBase
{
    public Guid? PlantCladeId { get; init; }
}