﻿using Ecology.Flora.DAL.Logic.BindingModels.Storage;

namespace Ecology.Flora.DAL.Logic.BindingModels.Plants;

public class PlantBindingModel : BindingModelBase
{
    public LocalizeProperty EcologicalPhytocenoticGroupComment { get; set; } = new();
    public LocalizeProperty RedBookComment { get; set; } = new();
    public PositionProperty Position { get; set; } = new();
    public ImageBinding Avatar { get; init; }
    public Guid PlantFamilyId { get; init; }
    public HashSet<PlantCharacteristicValueBindingModel> PlantCharacteristicValues { get; init; } = new();
    public HashSet<Guid> RedBooksPlantIds { get; set; } = new();
    public HashSet<Guid> OoptIds { get; set; } = new();
    public HashSet<Guid> AuthorIds { get; set; } = new();
}
