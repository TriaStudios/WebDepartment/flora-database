﻿namespace Ecology.Flora.DAL.Logic.BindingModels.Plants;

public class PlantFamilyBindingModel : BindingModelBase
{
    public Guid? PlantClassId { get; init; }
}