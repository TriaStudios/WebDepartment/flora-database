﻿namespace Ecology.Flora.DAL.Logic.BindingModels.Plants;

public class RarityCategoryBindingModel : BindingModelBase
{
    public string Code { get; init; }
}