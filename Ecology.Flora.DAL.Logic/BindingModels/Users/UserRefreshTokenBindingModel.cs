﻿namespace Ecology.Flora.DAL.Logic.BindingModels.Users;

public class UserRefreshTokenBindingModel
{
    public Guid Id { get; set; }
    public string RefreshToken { get; set; }
}