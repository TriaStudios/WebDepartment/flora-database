﻿namespace Ecology.Flora.DAL.Logic.BindingModels.Users;

public class UserBindingModel : BindingModelBase
{
    public string Username { get; set; }
    public string PasswordHash { get; set; }
    public string Email { get; set; }
    public string RefreshToken { get; set; }

    public List<Guid> RoleIds { get; set; }
    public List<Guid> PlantIds { get; set; }
}