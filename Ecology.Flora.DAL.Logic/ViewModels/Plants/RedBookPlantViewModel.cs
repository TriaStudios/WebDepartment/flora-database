﻿namespace Ecology.Flora.DAL.Logic.ViewModels.Plants;

public class RedBookPlantViewModel : ViewModelBase
{
    public ViewModelBase RedBook { get; set; }
    public ViewModelBase RarityCategory { get; set; }
    public ViewModelBase Plant { get; set; }
}