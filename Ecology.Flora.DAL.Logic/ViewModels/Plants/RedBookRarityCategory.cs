﻿namespace Ecology.Flora.DAL.Logic.ViewModels.Plants;

public class RedBookRarityCategory
{
    public ViewModelBase RedBook { get; init; }
    public ViewModelBase RarityCategory { get; init; }
}