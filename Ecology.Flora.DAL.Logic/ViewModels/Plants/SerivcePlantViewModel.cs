﻿namespace Ecology.Flora.DAL.Logic.ViewModels.Plants;

public class SerivcePlantViewModel
{
    public PlantFamilyViewModel PlantFamily { get; init; }
    public List<PlantCharacteristicValuesViewModel> PlantCharacteristicValues { get; init; }
    public List<RedBookPlantViewModel> RedBookPlants { get; init; }
}