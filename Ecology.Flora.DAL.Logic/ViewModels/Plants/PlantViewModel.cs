namespace Ecology.Flora.DAL.Logic.ViewModels.Plants;

public class PlantViewModel : ViewModelBase
{
    public LocalizeProperty EcologicalPhytocenoticGroupComment { get; init; }
    public LocalizeProperty RedBookComment { get; init; }
    public PositionProperty Position { get; init; }
    public Guid? AvatarId { get; set; }
    public ViewModelBase PlantFamily { get; init; }
    public List<ViewModelBase> Oopts { get; init; }
    public List<PlantCharacteristicValuesViewModel> PlantCharacteristicValues { get; init; }
    public List<RedBookPlantViewModel> RedBookPlants { get; init; }
    public List<ViewModelBase> Authors { get; init; }
}
