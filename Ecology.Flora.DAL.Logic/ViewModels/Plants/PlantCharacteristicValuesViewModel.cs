﻿namespace Ecology.Flora.DAL.Logic.ViewModels.Plants;

public class PlantCharacteristicValuesViewModel
{
    public ViewModelBase Characteristic { get; set; }
    public List<ViewModelBase> Values { get; set; }
}
