﻿namespace Ecology.Flora.DAL.Logic.ViewModels.Plants;

public class OoptViewModel : ViewModelBase
{
    public int Square { get; set; }
    public int Metric8 { get; set; }

    public ICollection<ViewModelBase> Plants { get; set; }
}