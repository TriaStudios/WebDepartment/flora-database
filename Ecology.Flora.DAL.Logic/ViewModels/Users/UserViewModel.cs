namespace Ecology.Flora.DAL.Logic.ViewModels.Users;

public class UserViewModel : ViewModelBase
{
    public string Username { get; init; }
    public string Email { get; init; }
    public string RefreshToken { get; set; }
    public List<RoleViewModel> Roles { get; init; }
}