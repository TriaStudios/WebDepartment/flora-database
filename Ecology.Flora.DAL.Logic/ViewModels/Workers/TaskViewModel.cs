using Ecology.Flora.DAL.Logic.Enums;

namespace Ecology.Flora.DAL.Logic.ViewModels.Workers;

public class TaskViewModel : ViewModelBase
{
    public string Namespace { get; set; }
    public StatusWorker Status { get; set; }
    public string Message { get; set; }
}