﻿namespace Ecology.Flora.DAL.Logic.ViewModels.Metrics;

public class MetricViewModel : ViewModelBase
{
    public List<Guid> MetricRaspreds { get; set; }
}