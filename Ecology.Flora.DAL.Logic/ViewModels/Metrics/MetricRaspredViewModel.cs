﻿namespace Ecology.Flora.DAL.Logic.ViewModels.Metrics;

public class MetricRaspredViewModel : ViewModelBase
{
    public int Bal { get; set; }
    public int Value { get; set; }
    public Guid? MetricId { get; set; }
}
