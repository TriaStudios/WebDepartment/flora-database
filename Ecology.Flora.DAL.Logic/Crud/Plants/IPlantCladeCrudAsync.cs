using Ecology.Flora.DAL.Logic.BindingModels.Plants;
using Ecology.Flora.DAL.Logic.ViewModels.Plants;

namespace Ecology.Flora.DAL.Logic.Crud.Plants;

public interface IPlantCladeCrudAsync
    : ICrudAsync<PlantCladeViewModel, PlantCladeBindingModel>
{
}