using Ecology.Flora.DAL.Logic.BindingModels.Plants;
using Ecology.Flora.DAL.Logic.Filters.Plants;
using Ecology.Flora.DAL.Logic.ViewModels.Plants;

namespace Ecology.Flora.DAL.Logic.Crud.Plants;

public interface IPlantClassCrudAsync
    : ICrudAsync<PlantClassViewModel, PlantClassBindingModel, PlantClassFilter>
{
}