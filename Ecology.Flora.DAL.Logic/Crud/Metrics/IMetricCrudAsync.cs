﻿using Ecology.Flora.DAL.Logic.BindingModels.Metrics;
using Ecology.Flora.DAL.Logic.ViewModels.Metrics;

namespace Ecology.Flora.DAL.Logic.Crud.Metrics;

public interface IMetricCrudAsync : ICrudAsync<MetricViewModel, MetricBindingModel, FilterBase>
{
}