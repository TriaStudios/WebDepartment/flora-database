using Ecology.Flora.DAL.Logic.BindingModels.Workers;
using Ecology.Flora.DAL.Logic.Filters.Workers;
using Ecology.Flora.DAL.Logic.ViewModels.Workers;

namespace Ecology.Flora.DAL.Logic.Crud.Workers;

public interface ITaskCrudAsync : ICrudAsync<TaskViewModel, TaskBindingModel, TaskFilter>
{
}