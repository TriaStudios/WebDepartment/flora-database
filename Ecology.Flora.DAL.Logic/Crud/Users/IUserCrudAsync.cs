using Ecology.Flora.DAL.Logic.ViewModels.Users;

namespace Ecology.Flora.DAL.Logic.Crud.Users;

public interface IUserCrudAsync : ICrudAsync<UserViewModel, UserBindingModel, UserFilter>
{
    public Task UpdateRefreshToken(UserRefreshTokenBindingModel userRefreshTokenBindingModel,
        CancellationToken cancellationToken = default);

    public Task<Guid> UpdateUser(UserBindingModel userBindingModel, CancellationToken cancellationToken = default);
    public Task<Guid> CreateUser(UserBindingModel userBindingModel, CancellationToken cancellationToken = default);
}