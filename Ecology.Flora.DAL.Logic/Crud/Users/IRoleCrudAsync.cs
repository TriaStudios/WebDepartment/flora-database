using Ecology.Flora.DAL.Logic.ViewModels.Users;

namespace Ecology.Flora.DAL.Logic.Crud.Users;

public interface IRoleCrudAsync : ICrudAsync<RoleViewModel, RoleBindingModel, FilterBase>
{
}