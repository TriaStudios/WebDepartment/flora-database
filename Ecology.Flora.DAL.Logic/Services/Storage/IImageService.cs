﻿using Ecology.DAL.Common.Enums;
using Ecology.Flora.DAL.Logic.BindingModels.Storage;

namespace Ecology.Flora.DAL.Logic.Services.Storage;

public interface IImageService
{
    Guid Save(ImageBinding imageBinding);
    byte[] Load(Guid id, ImageSize imageSize);
    void Remove(Guid id);
}