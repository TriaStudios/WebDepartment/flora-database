﻿using Ecology.Flora.DAL.Logic.ViewModels.Plants;

namespace Ecology.Flora.DAL.Logic.Services.Plants;

public interface IPlantService
{
    public Task<List<SerivcePlantViewModel>> GetServicePlantByOopt(Guid ooptId);
}
