namespace Ecology.Flora.DAL.Logic.Enums;

public enum StatusWorker
{
    Waiting,
    InProgress,
    Done,
    Failed,
    Cancelled
}