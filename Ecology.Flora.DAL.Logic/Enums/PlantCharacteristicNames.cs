﻿namespace Ecology.Flora.DAL.Logic.Enums;

public class PlantCharacteristicNames
{
    public const string OriginSpecies = nameof(OriginSpecies);
    public const string ArealType = nameof(ArealType);
    public const string EcologicalPhytocenoticGroup = nameof(EcologicalPhytocenoticGroup);
    public const string EconomicValue = nameof(EconomicValue);
    public const string HydrotypeGroup = nameof(HydrotypeGroup);
    public const string IntroductionTimeGroup = nameof(IntroductionTimeGroup);
    public const string IntroductionWay = nameof(IntroductionWay);
    public const string LifeFormRaunkiaer = nameof(LifeFormRaunkiaer);
    public const string LifeFormSerebryakovGolubev = nameof(LifeFormSerebryakovGolubev);
    public const string NaturalizationDegree = nameof(NaturalizationDegree);
    public const string OccurrenceSpecies = nameof(OccurrenceSpecies);
    public const string River = nameof(River);
    public const string Oopt = nameof(Oopt);
}
