namespace Ecology.Flora.DAL.Logic.Filters.Plants;

[Serializable]
public class PlantFilter : FilterBase
{
    public string EcologicalPhytocenoticGroupCommentQuery { get; set; }
    public string RedBookCommentQuery { get; set; }

    public HashSet<Guid> PlantCharacteristicValueIds { get; set; }
    public HashSet<Guid> PlantFamilyIds { get; set; }
    public HashSet<Guid> AuthorIds { get; set; }
    public HashSet<Guid> RarityCategoryIds { get; set; }
    public HashSet<Guid> RedBookIds { get; set; }
    public HashSet<Guid> OoptIds { get; set; }
}
