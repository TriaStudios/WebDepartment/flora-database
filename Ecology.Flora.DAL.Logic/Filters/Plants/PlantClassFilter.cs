namespace Ecology.Flora.DAL.Logic.Filters.Plants;

public class PlantClassFilter : FilterBase
{
    public HashSet<Guid> PlantCladeIds { get; set; }
}