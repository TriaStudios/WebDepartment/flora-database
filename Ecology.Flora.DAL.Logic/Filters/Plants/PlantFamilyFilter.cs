namespace Ecology.Flora.DAL.Logic.Filters.Plants;

public class PlantFamilyFilter : FilterBase
{
    public HashSet<Guid> PlantClassIds { get; set; }
}