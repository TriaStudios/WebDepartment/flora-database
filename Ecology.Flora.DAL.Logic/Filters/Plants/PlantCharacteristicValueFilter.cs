﻿namespace Ecology.Flora.DAL.Logic.Filters.Plants;

public class PlantCharacteristicValueFilter : FilterBase
{
    public HashSet<Guid> PlantCharacteristicIds { get; set; } = new();
}
