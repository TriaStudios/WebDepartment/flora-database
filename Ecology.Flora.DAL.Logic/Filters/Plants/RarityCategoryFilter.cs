namespace Ecology.Flora.DAL.Logic.Filters.Plants;

public class RarityCategoryFilter : FilterBase
{
    public HashSet<string> Codes { get; } = new();
}