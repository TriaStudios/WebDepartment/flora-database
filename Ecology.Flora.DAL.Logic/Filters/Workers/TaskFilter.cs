using Ecology.Flora.DAL.Logic.Enums;

namespace Ecology.Flora.DAL.Logic.Filters.Workers;

public class TaskFilter : FilterBase
{
    public HashSet<StatusWorker> Statuses { get; set; }
}