namespace Ecology.Flora.DAL.Logic.Filters.Users;

public class UserFilter : FilterBase
{
    public string Username { get; set; }
    public string PasswordHash { get; set; }
    public string RefreshToken { get; set; }
}