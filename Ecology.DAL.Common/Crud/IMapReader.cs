﻿namespace Ecology.DAL.Common.Crud;

public interface IMapReader
{
    Task<IEnumerable<PointMap>> GetPointsByBounds(BoundsModel model);
}