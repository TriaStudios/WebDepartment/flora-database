﻿using System.Threading;

namespace Ecology.DAL.Common.Crud;

public interface ICrudAsync<TView, in TBinding, in TFilter>
    where TView : ViewModelBase
    where TBinding : BindingModelBase
    where TFilter : FilterBase
{
    Task<Guid> CreateOrUpdateAsync(TBinding model, CancellationToken cancellationToken = default, DbContext context = null);
    
    Task<PageView<TView>> ReadAsync(TFilter filter, PageBindingModel pageBindingModel,
        CancellationToken cancellationToken = default);

    Task<PageView<ViewModelBase>> ReadLinksAsync(TFilter filter, PageBindingModel pageBindingModel,
        CancellationToken cancellationToken = default);

    Task<TView> FirstOrDefaultAsync(TFilter filter, CancellationToken cancellationToken = default);
    Task DeleteAsync(Guid id, CancellationToken cancellationToken = default);
}

public interface ICrudAsync<TView, in TBinding> : ICrudAsync<TView, TBinding, FilterBase>
    where TView : ViewModelBase
    where TBinding : BindingModelBase
{
}
