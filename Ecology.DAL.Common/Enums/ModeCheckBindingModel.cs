﻿namespace Ecology.DAL.Common.Enums;

public enum ModeCheckBindingModel
{
    Default,
    Update,
    Create
}