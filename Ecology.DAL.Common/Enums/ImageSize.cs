﻿namespace Ecology.DAL.Common.Enums;

/// <summary>
///     WARNING! DON'T CHANGE THIS
/// </summary>
public enum ImageSize
{
    Original = -1,
    Small = 100,
    Medium = 250,
    Large = 500
}