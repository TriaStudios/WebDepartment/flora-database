﻿namespace Ecology.DAL.Common.Models;

public record Point(float Latitude, float Longitude);
