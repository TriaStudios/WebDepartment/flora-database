﻿namespace Ecology.DAL.Common.Models;

public record PageBindingModel(int Page, int Count);