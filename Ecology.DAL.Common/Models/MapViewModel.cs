﻿namespace Ecology.DAL.Common.Models;

public record MapViewModel(IEnumerable<Cluster> Clusters);