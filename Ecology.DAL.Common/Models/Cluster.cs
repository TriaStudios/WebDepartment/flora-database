﻿namespace Ecology.DAL.Common.Models;

public record Cluster(
    Guid? Id,
    IndexCluster Index,
    Point Center,
    Point MinBound,
    Point MaxBound,
    int CountPoints,
    string Title,
    string Color);