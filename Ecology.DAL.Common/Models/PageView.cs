﻿namespace Ecology.DAL.Common.Models;

public class PageView<T>
{
    public PageView(IEnumerable<T> elements, long totalCount)
    {
        Elements = elements;
        Count = totalCount;
    }

    public IEnumerable<T> Elements { get; }
    public long Count { get; }
}
