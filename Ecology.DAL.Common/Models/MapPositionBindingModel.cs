﻿namespace Ecology.DAL.Common.Models;

public record MapPositionBindingModel(List<Point> Points);