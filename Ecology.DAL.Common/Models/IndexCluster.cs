namespace Ecology.DAL.Common.Models;

public record IndexCluster(double Q, double R);