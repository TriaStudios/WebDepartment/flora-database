﻿namespace Ecology.DAL.Common.Constants;

public static class MetricsConst
{
    /// <summary>
    /// Cтепень изученности растительного покрова
    /// </summary>
    public static readonly Guid TheDegreeOfStudyOfVegetationCover = Guid.Parse("ACED897E-1284-444A-A618-86D8F2B1B838");
    /// <summary>
    /// Демонстрационное (эталонное) значение
    /// </summary>
    public static readonly Guid DemonstrationValue = Guid.Parse("F4A86D36-D4A5-4EEC-B6E5-22C8CDA0FE4A");
    /// <summary>
    /// Площадь памятника природы
    /// </summary>
    public static readonly Guid NatureMonumentSquare = Guid.Parse("0BC1A1AF-CE4C-4403-B4AC-13E0984275C1");
    /// <summary>
    /// Антропотолерантность растительного покрова
    /// </summary>
    public static readonly Guid AnthropotoleranceOfVegetationCover = Guid.Parse("A516C974-6828-4F64-A90A-F3F912C8B683");
    /// <summary>
    /// Ценотнческое разнообрази
    /// </summary>
    public static readonly Guid CenoticDiversity = Guid.Parse("E67C62DF-FE6F-420A-92A4-CBE1BBD08AFA");
    /// <summary>
    /// Общая численность видового разнообразия
    /// </summary>
    public static readonly Guid TotalNumberSspeciesDiversity = Guid.Parse("3EC5BE41-785F-41F1-8DDA-A66214D2D999");
    /// <summary>
    /// Число видов, занесенных в Красную книгу Российской Федерации и Самарской области
    /// </summary>
    public static readonly Guid CountRareSpecies = Guid.Parse("7665A6D9-319A-4D29-8F93-BB9ADED0993E");
    /// <summary>
    /// Степень трансформпрованности
    /// </summary>
    public static readonly Guid DegreeTransformation = Guid.Parse("F4075D07-9BAA-4C06-A475-E518B850B4F2");
    /// <summary>
    /// Восстановительный потенциал
    /// </summary>
    public static readonly Guid RecoveryPotential = Guid.Parse("81445E34-2819-4664-9FC7-3DDE860A1FEB");
}