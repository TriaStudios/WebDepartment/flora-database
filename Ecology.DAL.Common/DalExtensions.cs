﻿using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Newtonsoft.Json;

namespace Ecology.DAL.Common;

public static class DalExtensions
{
    public static PropertyBuilder<T> HasJsonConversion<T>(this PropertyBuilder<T> propertyBuilder)
        where T : class, new()
    {
        var options = new JsonSerializerSettings {NullValueHandling = NullValueHandling.Ignore};
        var converter = new ValueConverter<T, string>
        (
            v => JsonConvert.SerializeObject(v, options),
            v => JsonConvert.DeserializeObject<T>(v) ?? new T());

        var comparer = new ValueComparer<T>
        (
            (l, r) => JsonConvert.SerializeObject(l, options) == JsonConvert.SerializeObject(r, options),
            v => v == null ? 0 : JsonConvert.SerializeObject(v).GetHashCode(),
            v => JsonConvert.DeserializeObject<T>(JsonConvert.SerializeObject(v, options))
        );

        propertyBuilder.HasConversion(converter);
        propertyBuilder.Metadata.SetValueConverter(converter);
        propertyBuilder.Metadata.SetValueComparer(comparer);
        propertyBuilder.HasColumnType("jsonb");

        return propertyBuilder;
    }
}
