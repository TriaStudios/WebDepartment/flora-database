﻿namespace Ecology.DAL.Common.Abstract;

public class ViewModelBase
{
    public Guid Id { get; init; }
    public LocalizeProperty Name { get; init; } = new();
}
