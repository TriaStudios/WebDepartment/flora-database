﻿namespace Ecology.DAL.Common.Abstract;

/// <summary>
///     JSONB-entity
/// </summary>
/// <remarks>when you update this entity you DON'T need to create migration</remarks>
public class LocalizeProperty
{
    public string Ru { get; set; }
    public string La { get; set; }
    public string En { get; set; }

    public LocalizeProperty() {}
    
    public LocalizeProperty(string ru = null, string la = null, string en = null)
    {
        Ru = ru;
        La = la;
        En = en;
    }

    public override bool Equals(object y)
    {
        if (ReferenceEquals(this, y)) return true;
        if (ReferenceEquals(this, null)) return false;
        if (y is not LocalizeProperty localizeProperty) return false;
        return Ru == localizeProperty.Ru && La == localizeProperty.La && En == localizeProperty.En;
    }

    public override int GetHashCode()
    {
        return HashCode.Combine(Ru, La, En);
    }
}
