namespace Ecology.DAL.Common.Abstract;

public class BoundsModel
{
    public PositionProperty LeftDown { get; set; }
    public PositionProperty RightUp { get; set; }
}