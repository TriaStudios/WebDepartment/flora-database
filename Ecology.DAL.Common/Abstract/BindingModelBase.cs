﻿namespace Ecology.DAL.Common.Abstract;

public abstract class BindingModelBase
{
    public Guid? Id { get; set; }
    public LocalizeProperty Name { get; init; } = new();
}
