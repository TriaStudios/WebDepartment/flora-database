using System.Threading;
using AutoMapper;
using AutoMapper.QueryableExtensions;

namespace Ecology.DAL.Common.Abstract;

public abstract class CrudAsyncBase<TView, TBinding, TModel, TFilter, TContext> : ICrudAsync<TView, TBinding, TFilter>
    where TView : ViewModelBase, new()
    where TBinding : BindingModelBase
    where TModel : EntityBase, new()
    where TFilter : FilterBase, new()
    where TContext : DbContext
{
    private readonly IMapper _mapper;

    protected CrudAsyncBase(IDbContextFactory<TContext> dbContextFactory, IMapper mapper)
    {
        ContextFactory = dbContextFactory;
        _mapper = mapper;
    }

    protected IDbContextFactory<TContext> ContextFactory { get; }

    public async Task<Guid> CreateOrUpdateAsync(TBinding model, CancellationToken cancellationToken = default, DbContext context = null)
    {
        var deleteCtx = context == null;
        if (deleteCtx) context = await ContextFactory.CreateDbContextAsync(cancellationToken);

        var entity = await Includes(context.Set<TModel>().AsSingleQuery())
            .Where(x => !x.Deleted)
            .FirstOrDefaultAsync(x => x.Id == model.Id, cancellationToken);
        if (entity == null)
        {
            entity = new TModel
            {
                Created = DateTime.UtcNow
            };
            context.Add(entity);
        }
        else
        {
            entity.Updated = DateTime.UtcNow;
        }

        await BindingToDbModel(model, entity, context);

        await context.SaveChangesAsync(cancellationToken);
        if (deleteCtx) await context.DisposeAsync();
        return entity.Id;
    }

    public async Task<PageView<TView>> ReadAsync(TFilter filter, PageBindingModel pageBindingModel,
        CancellationToken cancellationToken = default)
    {
        await using var context = await ContextFactory.CreateDbContextAsync(cancellationToken);
        var result = Includes(context.Set<TModel>().AsSingleQuery().AsNoTracking())
            .Where(x => filter.IncludeDeleted || !x.Deleted)
            .Where(ReadBasePredicate(filter))
            .Where(ReadPredicate(filter))
            .OrderByDescending(x => x.Updated)
            .ProjectTo<TView>(_mapper.ConfigurationProvider);

        var totalCount = await result.LongCountAsync(cancellationToken);
        if (pageBindingModel.Count != 0)
            result = result
                .Skip(pageBindingModel.Count * pageBindingModel.Page)
                .Take(pageBindingModel.Count);

        var res = result.ToArray();
        return new PageView<TView>(res, totalCount);
    }

    public async Task<PageView<ViewModelBase>> ReadLinksAsync(TFilter filter,
        PageBindingModel pageBindingModel, CancellationToken cancellationToken = default)
    {
        await using var context = await ContextFactory.CreateDbContextAsync(cancellationToken);
        var result = context.Set<TModel>()
            .AsSingleQuery()
            .AsNoTracking()
            .Where(x => filter.IncludeDeleted || !x.Deleted)
            .Where(ReadBasePredicate(filter))
            .Where(ReadPredicate(filter))
            .OrderByDescending(x => x.Updated)
            .ProjectTo<ViewModelBase>(_mapper.ConfigurationProvider);

        var totalCount = await result.LongCountAsync(cancellationToken);
        if (pageBindingModel.Count != 0)
            result = result
                .Skip(pageBindingModel.Count * pageBindingModel.Page)
                .Take(pageBindingModel.Count);

        var res = result.ToList();
        return new PageView<ViewModelBase>(res, totalCount);
    }

    public async Task<TView> FirstOrDefaultAsync(TFilter filter, CancellationToken cancellationToken = default)
    {
        await using var context = await ContextFactory.CreateDbContextAsync(cancellationToken);
        var entityBases = Includes(context.Set<TModel>().AsSingleQuery().AsNoTracking());
        var queryable = entityBases
            .Where(x => filter.IncludeDeleted || !x.Deleted)
            .Where(ReadBasePredicate(filter))
            .Where(ReadPredicate(filter));
        var viewModelBases = queryable.ProjectTo<TView>(_mapper.ConfigurationProvider);
        return await viewModelBases.FirstOrDefaultAsync(cancellationToken);
    }

    public async Task DeleteAsync(Guid id, CancellationToken cancellationToken = default)
    {
        await using var context = await ContextFactory.CreateDbContextAsync(cancellationToken);
        var entity = await context.Set<TModel>().FirstOrDefaultAsync(x => x.Id == id, cancellationToken);
        if (entity == null)
            throw new NullReferenceException("Данной модели не существует");

        entity.Updated = DateTime.UtcNow;
        entity.Deleted = true;
        await context.SaveChangesAsync(cancellationToken);
    }

    protected virtual Task BindingToDbModel(TBinding model, TModel dbModel, DbContext context)
    {
        dbModel.Name = model.Name;
        return Task.CompletedTask;
    }

    private static Expression<Func<TModel, bool>> ReadBasePredicate(TFilter filter)
    {
        return model => (filter.ObjectIds == null || filter.ObjectIds.Count == 0 || filter.ObjectIds.Contains(model.Id)) &&
                        (string.IsNullOrEmpty(filter.Query) || EF.Functions.ILike(string.Concat(model.Name.Ru, model.Name.En, model.Name.La), $"%{filter.Query}%"));
    }

    protected virtual Expression<Func<TModel, bool>> ReadPredicate(TFilter filter)
    {
        return model => true;
    }

    protected virtual IQueryable<TModel> Includes(IQueryable<TModel> queryable)
    {
        return queryable;
    }
}

public abstract class
    CrudAsyncBase<TView, TBinding, TModel, TContext> : CrudAsyncBase<TView, TBinding, TModel, FilterBase, TContext>
    where TView : ViewModelBase, new()
    where TBinding : BindingModelBase
    where TModel : EntityBase, new()
    where TContext : DbContext
{
    protected CrudAsyncBase(IDbContextFactory<TContext> context, IMapper mapper) : base(context, mapper)
    {
    }
}
