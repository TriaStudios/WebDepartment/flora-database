namespace Ecology.DAL.Common.Abstract;

public class PointMap : ViewModelBase
{
    public float Latitude { get; set; }
    public float Longitude { get; set; }
}
