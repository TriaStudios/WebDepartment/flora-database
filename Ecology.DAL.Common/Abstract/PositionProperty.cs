namespace Ecology.DAL.Common.Abstract;

/// <summary>
///     JSONB-entity
/// </summary>
/// <remarks>when you update this entity you DON'T need to create migration</remarks>
public class PositionProperty
{
    public float Latitude;
    public float Longitude;
}
