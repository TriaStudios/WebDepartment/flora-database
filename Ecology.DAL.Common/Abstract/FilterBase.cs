namespace Ecology.DAL.Common.Abstract;

public class FilterBase
{
    public HashSet<Guid> ObjectIds { get; set; }
    public string Query { get; set; }
    public bool IncludeDeleted { get; set; }
}