﻿namespace Ecology.DAL.Common.Abstract;

public abstract class EntityBase
{
    public Guid Id { get; set; }
    public LocalizeProperty Name { get; set; }
    public DateTime Created { get; set; }
    public DateTime Updated { get; set; }
    public bool Deleted { get; set; }

    protected bool Equals(EntityBase other)
    {
        return Id.Equals(other.Id);
    }

    public override bool Equals(object obj)
    {
        if (ReferenceEquals(null, obj)) return false;
        if (ReferenceEquals(this, obj)) return true;
        if (obj.GetType() != this.GetType()) return false;
        return Equals((EntityBase) obj);
    }

    public override int GetHashCode()
    {
        return Id.GetHashCode();
    }
}
