﻿using System;
using Microsoft.Extensions.DependencyInjection;

namespace Ecology.Flora.PlantRecognizer.Api
{
    public static class IntegrationExtensions
    {
        public static void AddPlantRecognizerGrpcClient(this IServiceCollection services, string uri)
        {
            services.AddGrpcClient<PlantRecognizerGrpc.PlantRecognizerGrpcClient>(options =>
                options.Address = new Uri(uri));
        }
    }
}